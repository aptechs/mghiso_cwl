package com.dkhanhaptechs.mghiso.Share.CheckVersion;

import android.content.Context;
import android.database.Cursor;

import com.dkhanhaptechs.mghiso.Share.Database_SQLite;


public class sqlite_Checkversion {
    private Database_SQLite database_sqLite ;
    private String Database ="Database_ConfigApp.sqlite";
    private String Table_InfoVersion = "InfoVersion";
    private boolean Status_InfoVersion_Exists = false;
    public sqlite_Checkversion (Context context){
        database_sqLite = new Database_SQLite(context, Database, null, 1);
        if(Create_Table_InfoVersion()){
            Status_InfoVersion_Exists = true;
        }
    }
    public boolean getStatus_Table_InfoVersion_Exists() {
        return Status_InfoVersion_Exists;
    }

    private   boolean Create_Table_InfoVersion(){
        boolean ok =false;
        try {
            String querry = "CREATE TABLE IF NOT EXISTS "+Table_InfoVersion +
                    " (" +
                    " id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                    " version nvarchar(30)" +
                    ");";
            if(database_sqLite.QueryDatabase(querry)) {
                ok = true;
            }
        }catch (Exception e){

        }
        return ok;
    }
    public  boolean Insert_Update_Table_InfoVersion(data_Checkversion infoVersion){
        boolean ok =false;
        try {
            String querry = "SELECT version FROM "+Table_InfoVersion+" WHERE id =1;";
            Cursor data_check = database_sqLite.GetDatabase(querry);
            String version = "";
            while (data_check.moveToNext()) {
                version = data_check.getString(0);

            }
            if(!version.isEmpty()){
                // update
                querry = "UPDATE "+Table_InfoVersion+" SET version ='" + infoVersion.getVersion() + "'"  +
                        " WHERE id =1;" ;
            }else {
                // insert
                querry = "INSERT INTO "+Table_InfoVersion+" (version)" +
                        " VALUES ('" + infoVersion.getVersion() + "');";
            }
            if(database_sqLite.QueryDatabase(querry)) {
                ok = true;
            }
        }catch (Exception e){

        }
        return ok;
    }
    public boolean Delete_InfoVersion(String id){
        boolean ok =false;
        try {
            String querry = "DELETE FROM "+Table_InfoVersion+"  WHERE id = "+id+";";
            if(database_sqLite.QueryDatabase(querry)) {
                ok = true;
            }
        }catch (Exception e){

        }
        return ok;
    }

    public  data_Checkversion Get_InfoVersion(String id){
        data_Checkversion data_version = new data_Checkversion(1,"");
        try {
            String querry = "SELECT * FROM "+Table_InfoVersion+" WHERE id="+id;
            Cursor data_check = database_sqLite.GetDatabase(querry);
            while (data_check.moveToNext()) {

                String version = data_check.getString(1);
                data_version.setId(Integer.parseInt(id));
                data_version.setVersion(version);

            }

        }catch (Exception e){

        }
        return data_version ;
    }
    public  String  Get_InfoVersion_Version(){
        String version ="";
        try {
            String querry = "SELECT version FROM "+Table_InfoVersion+" WHERE id= 1";
            Cursor data_check = database_sqLite.GetDatabase(querry);
            while (data_check.moveToNext()) {

                version = data_check.getString(1);
            }
        }catch (Exception e){

        }
        return version ;
    }

}

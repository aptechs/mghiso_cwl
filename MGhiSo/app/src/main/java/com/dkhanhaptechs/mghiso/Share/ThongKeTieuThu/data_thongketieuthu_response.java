package com.dkhanhaptechs.mghiso.Share.ThongKeTieuThu;

import com.dkhanhaptechs.mghiso.Share.LichSuThayDH.data_Lichsuthaydong_response;

public class data_thongketieuthu_response
{
    private data_result result ;
    private String status;
    private String message ;

    public data_result getResult() {
        return result;
    }

    public void setResult(data_result result) {
        this.result = result;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public  class  data_result {
    private  data_thongketieuthu [] thongketieuthu ;

        public data_thongketieuthu[] getData_thongketieuthus() {
            return thongketieuthu;
        }

        public void setData_thongketieuthus(data_thongketieuthu[] data_thongketieuthus) {
            this.thongketieuthu = data_thongketieuthus;
        }
    }
    public  class  data_thongketieuthu{
        private String nam;
        private String ky;
        private String dot ;
        private String code ;
        private String cscu ;
        private  String csmoi ;
        private String tieuthu;
        private  String ghichu ;
        private String ghichuky;

        public String getNam() {
            return nam;
        }

        public void setNam(String nam) {
            this.nam = nam;
        }

        public String getKy() {
            return ky;
        }

        public void setKy(String ky) {
            this.ky = ky;
        }

        public String getDot() {
            return dot;
        }

        public void setDot(String dot) {
            this.dot = dot;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getCscu() {
            return cscu;
        }

        public void setCscu(String cscu) {
            this.cscu = cscu;
        }

        public String getCsmoi() {
            return csmoi;
        }

        public void setCsmoi(String csmoi) {
            this.csmoi = csmoi;
        }

        public String getTieuthu() {
            return tieuthu;
        }

        public void setTieuthu(String tieuthu) {
            this.tieuthu = tieuthu;
        }

        public String getGhichu() {
            return ghichu;
        }

        public void setGhichu(String ghichu) {
            this.ghichu = ghichu;
        }

        public String getGhichuky() {
            return ghichuky;
        }

        public void setGhichuky(String ghichuky) {
            this.ghichuky = ghichuky;
        }
    }
}

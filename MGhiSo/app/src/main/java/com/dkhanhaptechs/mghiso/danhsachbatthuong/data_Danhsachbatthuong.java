package com.dkhanhaptechs.mghiso.danhsachbatthuong;

public class data_Danhsachbatthuong {
    private int Id ;
    private String Danhba;
    private  String So_st ;
    private  int Dongbo ;
    private  String Hoten ;
    private  String DiaChi;
    private String Tinhtrang;
    private String SoanhChup ;
    private String CSC ;
    private String CSM ;
    private  String Teuthu ;
    private String Binhquan;
    private String TinhTrang_cu;

    public data_Danhsachbatthuong(int id,String danhba,String so_st,int dongbo,String hoten,String diaChi,String tinhtrang,String soanhChup,String CSC,String CSM,String teuthu,String binhquan,String tinhTrang_cu) {
        Id = id;
        Danhba = danhba;
        So_st = so_st;
        Dongbo = dongbo;
        Hoten = hoten;
        DiaChi = diaChi;
        Tinhtrang = tinhtrang;
        SoanhChup = soanhChup;
        this.CSC = CSC;
        this.CSM = CSM;
        Teuthu = teuthu;
        Binhquan = binhquan;
        TinhTrang_cu = tinhTrang_cu;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getDanhba() {
        return Danhba;
    }

    public void setDanhba(String danhba) {
        Danhba = danhba;
    }

    public String getSo_st() {
        return So_st;
    }

    public void setSo_st(String so_st) {
        So_st = so_st;
    }

    public int getDongbo() {
        return Dongbo;
    }

    public void setDongbo(int dongbo) {
        Dongbo = dongbo;
    }

    public String getHoten() {
        return Hoten;
    }

    public void setHoten(String hoten) {
        Hoten = hoten;
    }

    public String getDiaChi() {
        return DiaChi;
    }

    public void setDiaChi(String diaChi) {
        DiaChi = diaChi;
    }

    public String getTinhtrang() {
        return Tinhtrang;
    }

    public void setTinhtrang(String tinhtrang) {
        Tinhtrang = tinhtrang;
    }

    public String getSoanhChup() {
        return SoanhChup;
    }

    public void setSoanhChup(String soanhChup) {
        SoanhChup = soanhChup;
    }

    public String getCSC() {
        return CSC;
    }

    public void setCSC(String CSC) {
        this.CSC = CSC;
    }

    public String getCSM() {
        return CSM;
    }

    public void setCSM(String CSM) {
        this.CSM = CSM;
    }

    public String getTeuthu() {
        return Teuthu;
    }

    public void setTeuthu(String teuthu) {
        Teuthu = teuthu;
    }

    public String getBinhquan() {
        return Binhquan;
    }

    public void setBinhquan(String binhquan) {
        Binhquan = binhquan;
    }

    public String getTinhTrang_cu() {
        return TinhTrang_cu;
    }

    public void setTinhTrang_cu(String tinhTrang_cu) {
        TinhTrang_cu = tinhTrang_cu;
    }
}

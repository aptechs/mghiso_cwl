package com.dkhanhaptechs.mghiso.Share.DanhSach_DocSo;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.util.Log;

import com.dkhanhaptechs.mghiso.Share.Dialog_App;
import com.dkhanhaptechs.mghiso.Share.LichSuThayDH.data_Lichsuthaydh_send;
import com.dkhanhaptechs.mghiso.Share.LichSuThayDH.data_Lichsuthaydong_response;
import com.dkhanhaptechs.mghiso.Share.TienNo.data_tiennos;
import com.dkhanhaptechs.mghiso.Share.TienNo.sqlite_TienNo;
import com.dkhanhaptechs.mghiso.danhsachdocso.activity_Danhsachdocso;
import com.google.gson.Gson;
import com.loopj.android.http.HttpGet;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.utils.URIBuilder;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import cz.msebera.android.httpclient.util.EntityUtils;
import  com.dkhanhaptechs.mghiso.Share.User.*;

public  final   class get_DanhSachDocSo_Asynctask extends AsyncTask<Void,Integer,String> {
    Context contextParent;

    private Dialog_App dialog_app ;
    private String Nam;
    private String Ky ;
    private String Dot;
    private String Somay;
    private String Somay_tangcuong;
    private String Thongbao;
    private String ResponseString;
    private Boolean Status = false;
    private TaskDelegate delegate;
     private   String BASE_URL_API = "/apikhachhang/api/DocSoDanhSachDocSo?";
      private   String BASE_URL = "";
    public interface TaskDelegate {
        //define you method headers to override
        void onTaskFinishGettingData(String data);
    }
    public get_DanhSachDocSo_Asynctask(Context contextParent,String nam,String ky,String dot, String somay , String soma_tangcuong ) throws ParseException {
        this.contextParent = contextParent;
        dialog_app = new Dialog_App(contextParent);
        Nam =nam ;
        Ky = ky;
        Dot = dot;
        Somay = somay;
        Thongbao ="";
        Somay_tangcuong = soma_tangcuong;
        ResponseString ="";
        data_Http data_http = new data_Http(contextParent);
            String url_run = data_http.get_Url_run();
            if(!url_run.isEmpty()){
                BASE_URL = url_run +BASE_URL_API;
            }else {
                BASE_URL = "http://viettel.capnuoccholon.com.vn"+BASE_URL_API;
            }
    }
    public String get_ResponseString(){
        return this.ResponseString;
    }
    public  Boolean get_Status(){
        return  this.Status;
    }
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if(Somay_tangcuong.isEmpty()) {
            dialog_app.Dialog_Progress_Wait_Open("Đang tải dữ liệu đọc số.","Vui lòng chờ");
        }else {
            dialog_app.Dialog_Progress_Wait_Open("Đang tải dữ liệu tăng cường.","Vui lòng chờ");
        }
        //    dialog_app.Dialog_Progress_Wait_Open("Đang lấy ảnh","Vui lòng chờ");
    }

    @Override
    protected void onPostExecute(String aVoid) {
        super.onPostExecute(aVoid);
        dialog_app.Dialog_Progress_Wait_Close();
        Gson gson_reponse = new Gson();
        data_Api_DanhSachDocSo_response data_api_danhSachDocSo_response = gson_reponse.fromJson(ResponseString,data_Api_DanhSachDocSo_response.class);
        Dialog_Progress_TaiDuLieu(data_api_danhSachDocSo_response);
         //  delegate.onTaskFinishGettingData(ResponseString);
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected String doInBackground(Void... voids) {
        try {
            data_Api_DanhSachDocSo_send data_api_danhSachDocSo_send = new data_Api_DanhSachDocSo_send(Nam,Ky,Dot,Somay);
            Gson gson_send = new Gson();
            String json_send = gson_send.toJson(data_api_danhSachDocSo_send);
            HttpClient httpclient = new DefaultHttpClient();
            URI uri_ = new URIBuilder(BASE_URL).build();
            if(Somay_tangcuong.isEmpty()){
            uri_ = new URIBuilder(BASE_URL)
                    .addParameter("thamso",json_send)
                    .build();
            }else {
                uri_ = new URIBuilder(BASE_URL)
                        .addParameter("thamso",json_send)
                        .addParameter("nhanvientangcuong",Somay_tangcuong)
                        .build();
            }
            HttpGet httpget = new HttpGet(uri_);
            //  httppost.setHeader("Accept", "application/json");
            httpget.setHeader("Content-type","application/json;charset=utf-8");
            //     httpget.setEntity(stringEntity_send);
            HttpResponse response = httpclient.execute(httpget);
            if (response.getStatusLine().getStatusCode() == 200) {
                String responseString = EntityUtils.toString(response.getEntity());
                ResponseString =responseString;

            }
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
        return ResponseString;
    }

    ProgressDialog dialog_progress_taidulieu;
    private void Dialog_Progress_TaiDuLieu(final data_Api_DanhSachDocSo_response data_api_danhSachDocSo_response) {
        dialog_progress_taidulieu = new ProgressDialog(contextParent);
        final String nam = data_api_danhSachDocSo_response.getResult().getNam();
        final String ky = data_api_danhSachDocSo_response.getResult().getKy();
        final String dot = data_api_danhSachDocSo_response.getResult().getDot();
        final String may = data_api_danhSachDocSo_response.getResult().getMay();
        final int sum_dh = data_api_danhSachDocSo_response.getResult().getKhachhangs().length;
        String title_dialog = "Tải thông tin đọc số năm: " + nam + ", kỳ: " + ky + ", đợt: " + dot + " của máy: " + may;
        dialog_progress_taidulieu.setTitle(title_dialog);
        dialog_progress_taidulieu.setMessage("Dang tải dữ liệu");
        dialog_progress_taidulieu.setMax(sum_dh);
        dialog_progress_taidulieu.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        dialog_progress_taidulieu.setCancelable(false);
        dialog_progress_taidulieu.show();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    int i = 0;
                    sqlite_DanhSachKhackHang_DocSo sqlite_danhSachKhackHang_docSo = new sqlite_DanhSachKhackHang_DocSo(contextParent,nam,ky,dot,may);
                    sqlite_TienNo sqlite_tienNo = new sqlite_TienNo(contextParent,nam,ky,dot,may);
                    if (sqlite_danhSachKhackHang_docSo.getStatus_Table_DanhSachKhachHang_Exists()) {
                        List<String> all_danhba = sqlite_danhSachKhackHang_docSo.Get_All_DanhBa();
                        data_Api_DanhSachDocSo_response.KhachHangs[] khachHangs = data_api_danhSachDocSo_response.getResult().getKhachhangs();
                        while (dialog_progress_taidulieu.getProgress() < dialog_progress_taidulieu.getMax()) {

                            data_Api_DanhSachDocSo_response.KhachHang khachHang = khachHangs[i].getKhachhang();
                            String danhba = khachHang.getDANHBA();
                            data_tiennos data_tiennos = new data_tiennos(danhba,khachHangs[i].getTiennos());
                            sqlite_tienNo.Insert_Update_Table_DanhSachHangHang_TienNo(data_tiennos);
                            boolean status_add = true;
                            for (int j = 0; j < all_danhba.size(); j++) {
                                if (all_danhba.get(j).equals(danhba)) {
                                    status_add = false;
                                    all_danhba.remove(j);
                                    break;
                                }
                            }

                            if (status_add) {
                                String cs_gan = khachHangs[i].getCSGan();
                                String cs_go = khachHangs[i].getCSGan();
                                String tieuthucu =khachHangs[i].getTieuThuCu();
                                String tieuthu = Objects.toString(khachHangs[i].getKhachhang().getTIEUTHU(),"").trim();
                                String tinhtrang = Objects.toString(khachHangs[i].getKhachhang().getTinhTrang(),"").trim();
                                String tinhtang_dongcua ="";
                                if(!tieuthu.isEmpty() && tinhtrang.equals("Đóng cửa")){
                                    tinhtang_dongcua = "Đóng cửa";
                                }
                                sqlite_danhSachKhackHang_docSo.Insert_Update_Table_DanhSachHangHang(khachHang,cs_go,cs_gan,tieuthucu,"2",tinhtang_dongcua);

                            }
                            if (dialog_progress_taidulieu.getProgress() == dialog_progress_taidulieu.getMax() - 1) {
                                //luu danh sach doc so
                                sqlite_DanhSachDocSo sqlite_danhSachDocSo = new sqlite_DanhSachDocSo(contextParent);
                                if (sqlite_danhSachDocSo.getStatus_Table_DanhSachDocSo_Exists()) {
                                    data_DanhSachDocSo data_danhSachDocSo = new data_DanhSachDocSo(nam,ky,dot,may,String.valueOf(sum_dh),"","");
                                    sqlite_danhSachDocSo.Insert_Update_Table_DanhSachDocSo(data_danhSachDocSo);
                                }

                            }

                            i++;
                            Thread.sleep(2);
                            handler_taidulieu.handleMessage(handler_taidulieu.obtainMessage());

                        }
                        /*
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Display_DanhSachDocSo();
                            }
                        });


                         */
                        dialog_progress_taidulieu.dismiss();


                    }
                    Thread.sleep(100);
                    handler_taidulieu.handleMessage(handler_taidulieu.obtainMessage());

                } catch (Exception e) {
                    Log.e("handler_taidulieu",e.toString());
                }
            }
        }).start();

    }

    Handler handler_taidulieu = new Handler() {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            dialog_progress_taidulieu.incrementProgressBy(1);
        }
    };

}

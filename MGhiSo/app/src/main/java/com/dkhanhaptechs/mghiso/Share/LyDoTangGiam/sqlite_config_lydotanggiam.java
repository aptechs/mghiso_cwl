package com.dkhanhaptechs.mghiso.Share.LyDoTangGiam;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.dkhanhaptechs.mghiso.Share.Config_CongDung.data_Config_CongDung;
import com.dkhanhaptechs.mghiso.Share.Database_SQLite;

import java.util.ArrayList;
import java.util.List;

public class sqlite_config_lydotanggiam {
    private Database_SQLite database_sqLite ;
    private String Database ="Database_LydoTangGiam.sqlite";
    private String Table_ListLyDoTangGiam = "ListLyDoTangGiam";
    private boolean Status_Table_ListLyDoTangGiam_Exists = false;
    public sqlite_config_lydotanggiam (Context context){
        database_sqLite = new Database_SQLite(context, Database, null, 1);
        if(Create_Table_ListLyDoTangGiam()){
            Status_Table_ListLyDoTangGiam_Exists = true;
        }else {
            Status_Table_ListLyDoTangGiam_Exists = false;
        }
    }
    public boolean getStatus_Table_ListLyDoTangGiam_Exists() {
        return Status_Table_ListLyDoTangGiam_Exists;
    }

    private   boolean Create_Table_ListLyDoTangGiam(){
        boolean ok =false;
        try {
            String querry = "CREATE TABLE IF NOT EXISTS "+Table_ListLyDoTangGiam +
                    " (" +
                    " id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                    " ma nvarchar(50)," +
                    " noidung TEXT NOT NULL" +
                    ");";
            if(database_sqLite.QueryDatabase(querry)) {
                ok = true;
            }
        }catch (Exception e){

        }
        return ok;
    }
    public  boolean Insert_Table_ListLyDoTangGiam (data_lydotanggiam data_lydotanggiam){
        boolean ok =false;
        try {

            // insert
            String  querry = "INSERT INTO "+Table_ListLyDoTangGiam+" (ma,noidung)" +
                    " VALUES ('" + data_lydotanggiam.getMa().toString().trim() + "','"+data_lydotanggiam.getNoidung().toString().trim()+"');";
            if(database_sqLite.QueryDatabase(querry)) {
                ok = true;
            }
        }catch (Exception e){
            Log.e("INSERT ListLyDoTangGiam",e.toString());
        }
        return ok;
    }

    public boolean Delete_Table_ListLyDoTangGiam() {
        boolean ok = false;
        try {
            String querry = "DROP TABLE IF EXISTS " + Table_ListLyDoTangGiam + "; ";

            if (database_sqLite.QueryDatabase(querry)) {
                ok = true;
                Log.e("Dl Table_LDTG ","ok");
            }
        } catch (Exception e) {

        }
        return ok ;
    }
    public List<data_lydotanggiam> Get_Table_ListLyDoTangGiam(){
        List<data_lydotanggiam> data_lydotanggiams = new ArrayList<>();
        try {

            String querry = "SELECT * FROM "+Table_ListLyDoTangGiam +" ;" ;
            Cursor data_check = database_sqLite.GetDatabase(querry);
            while (data_check.moveToNext()) {
                String ma = data_check.getString(1);
                String noidung = data_check.getString(2);
                data_lydotanggiam data_lydotanggiam = new data_lydotanggiam(ma,noidung);
                data_lydotanggiams.add(data_lydotanggiam);

            }

        }catch (Exception e){

        }
        return data_lydotanggiams;
    }
}

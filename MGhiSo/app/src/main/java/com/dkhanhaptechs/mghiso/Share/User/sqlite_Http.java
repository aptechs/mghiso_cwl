package com.dkhanhaptechs.mghiso.Share.User;

import android.content.Context;
import android.database.Cursor;

import com.dkhanhaptechs.mghiso.Share.CheckVersion.data_Checkversion;
import com.dkhanhaptechs.mghiso.Share.Database_SQLite;

public class sqlite_Http {
    private Database_SQLite database_sqLite ;
    private String Database ="Database_Http.sqlite";
    private String Table_Http_Run = "Http_Run";
    private Boolean Status_Table_Http_Run = false;


    public sqlite_Http (Context context){
        database_sqLite = new Database_SQLite(context, Database, null, 1);
        if(Create_Table_Http_Run()){
            Status_Table_Http_Run = true;
        }
    }
    public Boolean get_Status_Table_Http_Run(){
        return  Status_Table_Http_Run;
    }
    private     boolean Create_Table_Http_Run(){
        boolean ok =false;
        try {
            String querry = "CREATE TABLE IF NOT EXISTS "+Table_Http_Run +
                    " (" +
                    " id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                    " http_run nvarchar(150)" +
                    ");";
            if(database_sqLite.QueryDatabase(querry)) {
                ok = true;
            }
        }catch (Exception e){

        }
        return ok;
    }
    public  boolean Insert_Update_Table_Http_Run(String  data_http){
        boolean ok =false;
        try {
            String querry = "SELECT http_run FROM "+Table_Http_Run+" WHERE id =1;";
            Cursor data_check = database_sqLite.GetDatabase(querry);
            String http_run = "";
            while (data_check.moveToNext()) {
                http_run = data_check.getString(0);

            }
            if(!http_run.isEmpty()){
                // update
                querry = "UPDATE "+Table_Http_Run+" SET http_run ='" + data_http + "'"  +
                        " WHERE id =1;" ;
            }else {
                // insert
                querry = "INSERT INTO "+Table_Http_Run+" (http_run)" +
                        " VALUES ('" + data_http + "');";
            }
            if(database_sqLite.QueryDatabase(querry)) {
                ok = true;
            }
        }catch (Exception e){

        }
        return ok;
    }
    public String get_Table_Http_Run(){
        String http = "";
        try{
            String querry = "SELECT * FROM "+Table_Http_Run+" WHERE id=1";
            Cursor data_check = database_sqLite.GetDatabase(querry);
            while (data_check.moveToNext()) {
                http = data_check.getString(1);
            }
        }catch (Exception e){

        }
        return  http;
    }

}

package com.dkhanhaptechs.mghiso.hoadon;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.dkhanhaptechs.mghiso.R;
import com.dkhanhaptechs.mghiso.Share.DanhSach_DocSo.sqlite_DanhSachKhackHang_DocSo;
import com.dkhanhaptechs.mghiso.Share.TienNo.*;
import com.dkhanhaptechs.mghiso.Share.User.*;
import com.dkhanhaptechs.mghiso.Share.ThanhToan.*;
import com.dkhanhaptechs.mghiso.danhsachdocso_chitiet_khachhang.data_danhsachdocso_chitiet_khachhang;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class activity_Hoadon extends AppCompatActivity {
    private TextView tv_kynam, tv_tungay, tv_denngay;
    private TextView tv_danhba, tv_sost;
    private TextView tv_tenkhachhang, tv_diachi;
    private TextView tv_giabieu, tv_dinhmuc, tv_code;
    private TextView tv_csc, tv_csm, tv_tieuthu;
    private TextView tv_tiennuoc, tv_tienthue, tv_phibvmt,tv_dvtn,tv_thuedvtn, tv_tongtienky;
    private ListView lv_tienno;
    private TextView tv_tongtien , tv_tongtiengiam,tv_nhanvien, tv_dtzalo_nhanvien;
    private ImageView imv_dongbo;


    private String Json_table_ghiso ="";
    private String Danhba ="";
    private String Nam ="";
    private String Ky = "";
    private String Dot ="";
    private String SoMay ="";
    private ArrayList<data_tiennos.data_tienno> Arraylist_data_tienno;
    private adapter_Hoadon Adapter_hoadon;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hoadon);
        // INIT
        Mappping_Layout();
        Arraylist_data_tienno = new ArrayList<data_tiennos.data_tienno>();
        Adapter_hoadon = new adapter_Hoadon(activity_Hoadon.this,R.layout.dong_tienno,Arraylist_data_tienno);
        lv_tienno.setAdapter(Adapter_hoadon);
        Json_table_ghiso =getIntent().getStringExtra("HOADON");
        if(Get_Table_GhiSo(Json_table_ghiso)){
            tv_kynam.setText(Ky+"/"+Nam);
            tv_danhba.setText(Danhba);
        }
        Display_KhachHang();
    }
    private void Mappping_Layout() {
        imv_dongbo =( ImageView) findViewById(R.id.imageView_dongbo_hoadon);
        tv_kynam = (TextView) findViewById(R.id.textview_kynam_hoadon);
        tv_tungay = (TextView) findViewById(R.id.textview_tungay_hoadon);
        tv_denngay = (TextView) findViewById(R.id.textview_denngay_hoadon);
        tv_danhba = (TextView) findViewById(R.id.textview_danhba_hoadon);
        tv_sost = (TextView) findViewById(R.id.textview_sost_hoadon);
        tv_tenkhachhang = (TextView) findViewById(R.id.textview_hoten_hoadon);
        tv_diachi = (TextView) findViewById(R.id.textview_diachi_hoadon);
        tv_giabieu = (TextView) findViewById(R.id.textview_giabieu_hoadon);
        tv_dinhmuc = (TextView) findViewById(R.id.textview_dinhmuc_hoadon);
        tv_code = (TextView) findViewById(R.id.textview_code_hoadon);
        tv_csc = (TextView) findViewById(R.id.textview_csc_hoadon);
        tv_csm = (TextView) findViewById(R.id.textview_csm_hoadon);
        tv_tieuthu = (TextView) findViewById(R.id.textview_tieuthu_hoadon);
        tv_tiennuoc = (TextView) findViewById(R.id.textview_tiennuoc_hoadon);
        tv_tienthue = (TextView) findViewById(R.id.textview_tienthue_hoadon);
        tv_phibvmt = (TextView) findViewById(R.id.textview_phibvmt_hoadon);
        tv_dvtn = (TextView) findViewById(R.id.textview_dvtn_hoadon);
        tv_thuedvtn = (TextView) findViewById(R.id.textview_thuedvtn_hoadon);
        tv_tongtienky = (TextView) findViewById(R.id.textview_tongtienky_hoadon);
        tv_tongtien = (TextView) findViewById(R.id.textview_tongtien_hoadon);
        tv_tongtiengiam = (TextView) findViewById(R.id.textview_tonggiam_hoadon);
        tv_nhanvien = (TextView) findViewById(R.id.textview_nhanvien_hoadon);
        tv_dtzalo_nhanvien = (TextView) findViewById(R.id.textview_dtzalonhanvien_hoadon);
        lv_tienno = (ListView) findViewById(R.id.listview_tienno_hoadon);

    }
    private boolean Get_Table_GhiSo(String json){
        boolean status = false;
        try{
           // Log.e("hoadon",json);
            JSONObject table = new JSONObject(json);
            Danhba = table.getString("danhba");
            Nam = table.getString("nam");
            Ky = table.getString("ky");
            Dot = table.getString("dot");
            SoMay = table.getString("somay");

            status = true;
        }catch (JSONException e){

        }
        return  status;
    }
    private void Display_KhachHang(){
        String nam = String.valueOf(Nam);
        String ky = String.valueOf(Ky);
        String dot = String.valueOf(Dot);
        String somay = String.valueOf(SoMay);
        long tongtienky =0 ;
        DecimalFormat formatter = new DecimalFormat("###,###,###");
        SimpleDateFormat format_datetime = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat format_datetime_display = new SimpleDateFormat("yyyy/MM/dd");
        // hien thi thong tin doc so
        sqlite_DanhSachKhackHang_DocSo sqlite_danhSachKhackHang_docSo = new sqlite_DanhSachKhackHang_DocSo(activity_Hoadon.this,nam,ky,dot,somay);
        if(sqlite_danhSachKhackHang_docSo.getStatus_Table_DanhSachKhachHang_Exists()){
            data_danhsachdocso_chitiet_khachhang khachhang =  sqlite_danhSachKhackHang_docSo.Get_DanhSachDocSo_Chitiet_KhachHang(Danhba);
            if(khachhang.getId() != 0) {
                String So_St = khachhang.getSothan();
                tv_danhba.setText(Danhba);
                tv_sost.setText("ST: " + So_St);
                String Hoten = khachhang.getHoten() ;
                tv_tenkhachhang.setText(Hoten);
                String sonha_cu = khachhang.getSonha().toString().trim();
                String sonha_moi = khachhang.getSonha_moi().toString().trim();
                String diachi = sonha_cu + "\r\n" + khachhang.getDuong();
                if(!sonha_moi.isEmpty()){
                    diachi = sonha_cu +" ("+sonha_moi+")"+ "\r\n" + khachhang.getDuong();
                }
                tv_diachi.setText(diachi);
                // edt_diachinew.setText(khachhang.getSonha());
                String giabieu = khachhang.getGiabieu();
                tv_giabieu.setText(khachhang.getGiabieu());
                String dinhmuc = khachhang.getDinhmuc();
                tv_dinhmuc.setText(dinhmuc);
                tv_code.setText(khachhang.getCode());
                tv_csc.setText(khachhang.getCscu());
                tv_csm.setText(khachhang.getCsmoi());
                String tieuthu = khachhang.getTieuthu();
                tv_tieuthu.setText(khachhang.getTieuthu());
                String dinhmucngheo = khachhang.getDinhmucngheo();
                String tldv = khachhang.getTiledv();
                String tlhcsn = khachhang.getTilehcsn();
                String tlsh = khachhang.getTilesh();
                String tlsx = khachhang.getTilesx();
                int tieuthu_i = 0;
                try {
                    tieuthu_i = Integer.parseInt(tieuthu);
                }catch(Exception e) {}
                //tieuthu_i =89909;
                int giabieu_i = 0;
                try {
                    giabieu_i = Integer.parseInt(giabieu);
                }catch (Exception e){}

                int dinhmuc_i  = 0;
                try {
                    dinhmuc_i = Integer.parseInt(dinhmuc);
                }catch (Exception e){}


                int dinhmucngheo_i = 0;
                try {
                    dinhmucngheo_i = Integer.parseInt(dinhmucngheo);
                }catch (Exception e){}


                int tl_kd_i = 0;
                try {
                    tl_kd_i= Integer.parseInt(tldv);
                }catch (Exception e){}


                int tl_hcsn_i = 0;
                try {
                    tl_hcsn_i =  Integer.parseInt(tlhcsn);
                }catch (Exception e){}


                int tl_sh_i =0;
                try {
                    tl_sh_i=  Integer.parseInt(tlsh);
                }catch (Exception e){}


                int tl_sx_i =0;
                try {
                    tl_sx_i=  Integer.parseInt(tlsx);
                }catch (Exception e){};
                String tungay = khachhang.getTungay();
                String denngay  = khachhang.getDenngay();

                try {
                    Date date_tungay = format_datetime.parse(tungay);
                    Date date_denngay = format_datetime.parse(denngay);
                    tv_denngay.setText(format_datetime_display.format( date_denngay));
                    tv_tungay.setText(format_datetime_display.format( date_tungay));
                } catch (ParseException e) {
                    e.printStackTrace();
                }



                Thanhtoan_tiennuoc thanhtoan_tiennuoc = new Thanhtoan_tiennuoc();
                data_Tiennuoc  data_tiennuoc = thanhtoan_tiennuoc.Tiennuoc(tieuthu_i,giabieu_i,dinhmuc_i,dinhmucngheo_i,tl_kd_i,tl_hcsn_i,tl_sh_i,tl_sx_i,tungay,denngay);
               // sqlite_danhSachKhackHang_docSo.Update_tieuthu_khachhang(Danhba,String.valueOf(tiennuoc));
                // TIEN NUOC TINH VAO DICH
                long tongtiengiam = 0;
                long tiennuoc =(long) Long.parseLong(data_tiennuoc.getTiennuoc());
                if(nam.equals("2021") && (ky.equals("9") ||ky.equals("10") || ky.equals("11"))){
                    // DICH GIAM 10 %
                    // CHO GB: 11-19, 21-29, 51, 56, 57, 59
                    if(
                            (11<=giabieu_i && giabieu_i<=19) ||
                            (21<=giabieu_i && giabieu_i<=29) ||
                            (giabieu_i == 51 || giabieu_i == 56 || giabieu_i == 59)
                    ){

                        long pre_tiennuoc = tiennuoc ;
                        tiennuoc = (long) (tiennuoc*0.9);
                        tv_tiennuoc.setText(formatter.format(tiennuoc));
                        long thue = (long) Math.round(tiennuoc * 0.05);
                        tv_tienthue.setText(formatter.format(thue));
                        String enble_thuevbmt = khachhang.getKhongtinhphi_bvmt().trim();
                        long thue_mt = 0;
                        if (enble_thuevbmt.equals("0") || enble_thuevbmt.isEmpty()) {
                            if(giabieu_i == 51 || giabieu_i == 56 || giabieu_i == 59){
                                thue_mt = (long) thanhtoan_tiennuoc.Tiennuoc_ThueBVMT_Covid(tieuthu_i,giabieu_i,dinhmuc_i,dinhmucngheo_i,tl_kd_i,tl_hcsn_i,tl_sh_i,tl_sx_i);
                            }else {
                                thue_mt = (long) Math.round(tiennuoc * 0.1);
                            }
                        }
                        tv_phibvmt.setText(formatter.format(thue_mt));
                        tongtienky = tiennuoc + thue + thue_mt;
                        tv_tongtienky.setText(formatter.format(tongtienky));
                        // tien chua giam
                        long thue_chuagiam =  (long) Math.round(pre_tiennuoc * 0.05);
                        long thue_mt_chuagiam = (long) Math.round(pre_tiennuoc * 0.1);
                        long tongtiennuoc_chuagiam = pre_tiennuoc+thue_chuagiam+thue_mt_chuagiam;
                        tongtiengiam = tongtiennuoc_chuagiam - tongtienky ;
                        tv_tongtiengiam.setText(formatter.format(tongtiengiam));
                        long dichvuthoatnuoc = 0;
                        long thue_dichvuthoatnuoc = 0;
                        tv_dvtn.setText(formatter.format(dichvuthoatnuoc));
                        tv_thuedvtn.setText(formatter.format(thue_dichvuthoatnuoc));
                    }else {
                        tv_tiennuoc.setText(formatter.format(tiennuoc));
                        long thue = (long) Math.round(tiennuoc * 0.05);
                        tv_tienthue.setText(formatter.format(thue));
                        String enble_thuevbmt = khachhang.getKhongtinhphi_bvmt().trim();
                        long thue_mt = 0;
                        if (enble_thuevbmt.equals("0") || enble_thuevbmt.isEmpty()) {
                            thue_mt = (long) Math.round(tiennuoc * 0.1);
                        }
                        tv_phibvmt.setText(formatter.format(thue_mt));
                        tongtienky = tiennuoc + thue + thue_mt;
                        tv_tongtienky.setText(formatter.format(tongtienky));
                        tv_tongtiengiam.setText(formatter.format(tongtiengiam));
                        long dichvuthoatnuoc = 0;
                        long thue_dichvuthoatnuoc = 0;
                        tv_dvtn.setText(formatter.format(dichvuthoatnuoc));
                        tv_thuedvtn.setText(formatter.format(thue_dichvuthoatnuoc));
                    }
                }else {
                    // BINH THUONG

                    tv_tiennuoc.setText(formatter.format(tiennuoc));
                    long thue_5 =(long) Long.parseLong(data_tiennuoc.getThuetiennuoc_5());
                    tv_tienthue.setText(formatter.format(thue_5));
                    String enble_thuevbmt = khachhang.getKhongtinhphi_bvmt().trim();
                    long phi_bvmt = 0;
                    long dichvuthoatnuoc = 0;
                    long thue_dichvuthoatnuoc = 0;
                    String thue_dichvuthoatnuoc_s = data_tiennuoc.getThuedichvuthoatnuoc();
                    String dichvuthoatnuoc_s = data_tiennuoc.getDichvuthoatnuoc();
                    if (enble_thuevbmt.equals("0") || enble_thuevbmt.isEmpty()) {
                        String phi_bvmt_s = data_tiennuoc.getPhiBVMT();
                        if (phi_bvmt_s !=null && !phi_bvmt_s.isEmpty()) {
                            phi_bvmt = (int) Double.parseDouble(phi_bvmt_s);
                        }
                        if (dichvuthoatnuoc_s != null && !dichvuthoatnuoc_s.isEmpty()) {
                            dichvuthoatnuoc = (long) Long.parseLong(dichvuthoatnuoc_s);
                        }
                        if (thue_dichvuthoatnuoc_s !=null && !thue_dichvuthoatnuoc_s.isEmpty()) {
                            thue_dichvuthoatnuoc = (long) Long.parseLong(thue_dichvuthoatnuoc_s);
                        }
                    }
                    tv_phibvmt.setText(formatter.format(phi_bvmt));


                    tv_dvtn.setText(formatter.format(dichvuthoatnuoc));




                    tv_thuedvtn.setText(formatter.format(thue_dichvuthoatnuoc));

                    tongtienky = tiennuoc + thue_5 + phi_bvmt + dichvuthoatnuoc + thue_dichvuthoatnuoc;
                    tv_tongtienky.setText(formatter.format(tongtienky));
                    tv_tongtiengiam.setText(formatter.format(tongtiengiam));

                }

                String sync = khachhang.getSync();
                int dongbo = Integer.parseInt(sync);

                switch (dongbo) {
                    case 2: {
                        // timeout
                        imv_dongbo.setImageResource(R.drawable.synced_2);
                        break;
                    }
                    case 1: {
                        // dang do bo
                        imv_dongbo.setImageResource(R.drawable.synced_1);
                        break;
                    }
                    case 0: {
                        // da dong bo
                        imv_dongbo.setImageResource(R.drawable.synced_0);
                        break;
                    }
                }
            }
        }
        // hien thi thong tin no
        sqlite_TienNo sqlite_tienNo = new sqlite_TienNo(activity_Hoadon.this,nam,ky,dot,somay);
        try{
            Arraylist_data_tienno.clear();
        }catch (Exception e){

        }
        int tongtienno = 0;
        if(sqlite_tienNo.getStatus_Table_DanhSachHangHang_TienNo_Exists()){
            data_tiennos tiennos = sqlite_tienNo.Get_Data_TienNo(Danhba);
           // data_tiennos.data_tienno []  all_tienno = tiennos.getTiennos();

            for(int i = 0;  i< tiennos.getTiennos().length; i++){
                String tienno_ky = tiennos.getTiennos()[i].getTienNo();
                tongtienno =tongtienno +(int)Double.parseDouble(tienno_ky);
                Arraylist_data_tienno.add(tiennos.getTiennos()[i]);
            }

        }
        Adapter_hoadon.notifyDataSetChanged();
        long tongtien = (long) tongtienky + (long) tongtienno ;


        tv_tongtien.setText(formatter.format(tongtien));
        // hien thi thong tin nhan vien
        sqlite_User sqlite_user = new sqlite_User(activity_Hoadon.this);
        if(sqlite_user.getStatus_Table_User_Exists()){
            data_User nhanvien = sqlite_user.Get_User("1");
            String ten_nhanvien = nhanvien.getTennhanvien().toString().trim();
            String dienthoai_nhanvien  =nhanvien.getdienthoai().toString().trim();
            tv_nhanvien.setText(ten_nhanvien);
            tv_dtzalo_nhanvien.setText(dienthoai_nhanvien);

        }


    }
}
package com.dkhanhaptechs.mghiso.Share.User;

public class data_User {
    private int id ;
    private String mac ;
    private String tennhanvien ;
    private String somay;
    private String dienthoai;


    public data_User(int id,String mac,String tennhanvien,String somay,String dienthoai) {
        this.id = id;
        this.mac = mac;
        this.tennhanvien = tennhanvien;
        this.somay = somay;
        this.dienthoai = dienthoai;
    }
    public data_User() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getTennhanvien() {
        return tennhanvien;
    }

    public void setTennhanvien(String tennhanvien) {
        this.tennhanvien = tennhanvien;
    }

    public String getSomay() {
        return somay;
    }

    public void setSomay(String somay) {
        this.somay = somay;
    }

    public String getdienthoai() {
        return dienthoai;
    }

    public void setdienthoai(String sodienthoai) {
        this.dienthoai = sodienthoai;
    }
}
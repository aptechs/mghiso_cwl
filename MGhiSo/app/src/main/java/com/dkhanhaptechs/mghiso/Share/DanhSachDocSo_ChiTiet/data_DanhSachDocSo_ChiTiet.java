package com.dkhanhaptechs.mghiso.Share.DanhSachDocSo_ChiTiet;

class data_DanhSachDocSo_ChiTiet {
    private  int id ;
    private String sodanhba ;
    private String sost;
    private String tenkh;
    private String sonha ;
    private String tenduong ;
    private String sync;

    public data_DanhSachDocSo_ChiTiet(String sodanhba,String sost,String tenkh,String sonha,String tenduong,String sync) {
        this.sodanhba = sodanhba;
        this.sost = sost;
        this.tenkh = tenkh;
        this.sonha = sonha;
        this.tenduong = tenduong;
        this.sync = sync;
    }

    public data_DanhSachDocSo_ChiTiet() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSodanhba() {
        return sodanhba;
    }

    public void setSodanhba(String sodanhba) {
        this.sodanhba = sodanhba;
    }

    public String getSost() {
        return sost;
    }

    public void setSost(String sost) {
        this.sost = sost;
    }

    public String getTenkh() {
        return tenkh;
    }

    public void setTenkh(String tenkh) {
        this.tenkh = tenkh;
    }

    public String getSonha() {
        return sonha;
    }

    public void setSonha(String sonha) {
        this.sonha = sonha;
    }

    public String getTenduong() {
        return tenduong;
    }

    public void setTenduong(String tenduong) {
        this.tenduong = tenduong;
    }

    public String getSync() {
        return sync;
    }

    public void setSync(String sync) {
        this.sync = sync;
    }
}

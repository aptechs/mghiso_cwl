package com.dkhanhaptechs.mghiso.Share.Bluetooth;

public class data_AddBluetooth {
    private int id ;
    private String type ;
    private String add ;
    private String current ;


    public data_AddBluetooth() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAdd() {
        return add;
    }

    public void setAdd(String add) {
        this.add = add;
    }

    public String getCurrent() {
        return current;
    }

    public void setCurrent(String current) {
        this.current = current;
    }
}

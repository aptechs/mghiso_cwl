package com.dkhanhaptechs.mghiso.danhsachdocso_chitiet_khachhang;

public class data_danhsachdocso_chitiet_khachhang {
    private int id;
    private String danhba ;
    private String st ;
    private String hoten ;
    private String sonha ;
    private String duong ;
    private String sync ;
    private String sothan ;
    private String giabieu ;
    private String dinhmuc ;
    private String dienthoai1 ;
    private String dienthoai2 ;
    private String code ;
    private String cscu ;
    private String csmoi ;
    private String tieuthu;
    private String tieuthucu;
    private String binhquan ;
    private String ghichu1 ;
    private String ghichu2 ;
    private String tungay ;
    private String denngay ;
    private String dinhmucngheo ;
              private String tiledv ;
              private String tilehcsn ;
              private String tilesh ;
              private String tilesx ;
              private String congdung ;
              private String vitri_dh ;
              private String tinhtrang_dh;
    private String sonha_moi ;
    private String machigoc;
    private String tinhtrangks ;
    private String hieudh;
    private  String nam_dh ;
    private String co_dh ;
    private String chi ;
    private String codemoi ;
    private String tinhtrangmoi;
    private String csgo ;
    private String csgan ;
    private String ngaythaydh;
    private String lydo ;
    private String khongtinhphi_bvmt;

    public data_danhsachdocso_chitiet_khachhang() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDanhba() {
        return danhba;
    }

    public void setDanhba(String danhba) {
        this.danhba = danhba;
    }

    public String getSt() {
        return st;
    }

    public void setSt(String st) {
        this.st = st;
    }

    public String getHoten() {
        return hoten;
    }

    public void setHoten(String hoten) {
        this.hoten = hoten;
    }

    public String getSonha() {
        return sonha;
    }

    public void setSonha(String sonha) {
        this.sonha = sonha;
    }

    public String getDuong() {
        return duong;
    }

    public void setDuong(String duong) {
        this.duong = duong;
    }

    public String getSync() {
        return sync;
    }

    public void setSync(String sync) {
        this.sync = sync;
    }

    public String getSothan() {
        return sothan;
    }

    public void setSothan(String sothan) {
        this.sothan = sothan;
    }

    public String getGiabieu() {
        return giabieu;
    }

    public void setGiabieu(String giabieu) {
        this.giabieu = giabieu;
    }

    public String getDinhmuc() {
        return dinhmuc;
    }

    public void setDinhmuc(String dinhmuc) {
        this.dinhmuc = dinhmuc;
    }

    public String getDienthoai1() {
        return dienthoai1;
    }

    public void setDienthoai1(String dienthoai1) {
        this.dienthoai1 = dienthoai1;
    }

    public String getDienthoai2() {
        return dienthoai2;
    }

    public void setDienthoai2(String dienthoai2) {
        this.dienthoai2 = dienthoai2;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCscu() {
        return cscu;
    }

    public void setCscu(String cscu) {
        this.cscu = cscu;
    }

    public String getBinhquan() {
        return binhquan;
    }

    public void setBinhquan(String binhquan) {
        this.binhquan = binhquan;
    }

    public String getGhichu1() {
        return ghichu1;
    }

    public void setGhichu1(String ghichu1) {
        this.ghichu1 = ghichu1;
    }

    public String getGhichu2() {
        return ghichu2;
    }

    public void setGhichu2(String ghichu2) {
        this.ghichu2 = ghichu2;
    }

    public String getCsmoi() {
        return csmoi;
    }

    public void setCsmoi(String csmoi) {
        this.csmoi = csmoi;
    }

    public String getTieuthu() {
        return tieuthu;
    }

    public void setTieuthu(String tieuthu) {
        this.tieuthu = tieuthu;
    }

    public String getTieuthucu() {
        return tieuthucu;
    }

    public void setTieuthucu(String tieuthucu) {
        this.tieuthucu = tieuthucu;
    }

    public String getTungay() {
        return tungay;
    }

    public void setTungay(String tungay) {
        this.tungay = tungay;
    }

    public String getDenngay() {
        return denngay;
    }

    public void setDenngay(String denngay) {
        this.denngay = denngay;
    }

    public String getDinhmucngheo() {
        return dinhmucngheo;
    }

    public void setDinhmucngheo(String dinhmucngheo) {
        this.dinhmucngheo = dinhmucngheo;
    }

    public String getTiledv() {
        return tiledv;
    }

    public void setTiledv(String tiledv) {
        this.tiledv = tiledv;
    }

    public String getTilehcsn() {
        return tilehcsn;
    }

    public void setTilehcsn(String tilehcsn) {
        this.tilehcsn = tilehcsn;
    }

    public String getTilesh() {
        return tilesh;
    }

    public void setTilesh(String tilesh) {
        this.tilesh = tilesh;
    }

    public String getTilesx() {
        return tilesx;
    }

    public void setTilesx(String tilesx) {
        this.tilesx = tilesx;
    }

    public String getCongdung() {
        return congdung;
    }

    public void setCongdung(String congdung) {
        this.congdung = congdung;
    }


    public String getVitri_dh() {
        return vitri_dh;
    }

    public void setVitri_dh(String vitri_dh) {
        this.vitri_dh = vitri_dh;
    }

    public String getTinhtrang_dh() {
        return tinhtrang_dh;
    }

    public void setTinhtrang_dh(String tinhtrang_dh) {
        this.tinhtrang_dh = tinhtrang_dh;
    }

    public String getSonha_moi() {
        return sonha_moi;
    }

    public void setSonha_moi(String sonha_moi) {
        this.sonha_moi = sonha_moi;
    }

    public String getMachigoc() {
        return machigoc;
    }

    public void setMachigoc(String machigoc) {
        this.machigoc = machigoc;
    }

    public String getTinhtrangks() {
        return tinhtrangks;
    }

    public void setTinhtrangks(String tinhtrangks) {
        this.tinhtrangks = tinhtrangks;
    }

    public String getHieudh() {
        return hieudh;
    }

    public void setHieudh(String hieudh) {
        this.hieudh = hieudh;
    }

    public String getNam_dh() {
        return nam_dh;
    }

    public void setNam_dh(String nam_dh) {
        this.nam_dh = nam_dh;
    }

    public String getCo_dh() {
        return co_dh;
    }

    public void setCo_dh(String co_dh) {
        this.co_dh = co_dh;
    }

    public String getChi() {
        return chi;
    }

    public void setChi(String chi) {
        this.chi = chi;
    }

    public String getCodemoi() {
        return codemoi;
    }

    public void setCodemoi(String codemoi) {
        this.codemoi = codemoi;
    }

    public String getTinhtrangmoi() {
        return tinhtrangmoi;
    }

    public void setTinhtrangmoi(String tinhtrangmoi) {
        this.tinhtrangmoi = tinhtrangmoi;
    }

    public String getCsgo() {
        return csgo;
    }

    public void setCsgo(String csgo) {
        this.csgo = csgo;
    }

    public String getCsgan() {
        return csgan;
    }

    public void setCsgan(String csgan) {
        this.csgan = csgan;
    }

    public String getNgaythaydh() {
        return ngaythaydh;
    }

    public void setNgaythaydh(String ngaythaydh) {
        this.ngaythaydh = ngaythaydh;
    }

    public String getLydo() {
        return lydo;
    }

    public void setLydo(String lydo) {
        this.lydo = lydo;
    }

    public String getKhongtinhphi_bvmt() {
        return khongtinhphi_bvmt;
    }

    public void setKhongtinhphi_bvmt(String khongtinhphi_bvmt) {
        this.khongtinhphi_bvmt = khongtinhphi_bvmt;
    }
}

package com.dkhanhaptechs.mghiso.danhsachbatthuong;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.dkhanhaptechs.mghiso.R;

import java.util.List;
import java.util.Objects;

class adapter_Danhsachbatthuong extends BaseAdapter {

    private Context Context_ ;
    private int Layout_ ;
    private List<data_Danhsachbatthuong> List_data_danhsachbatthuong ;
    public adapter_Danhsachbatthuong(Context context, int layout, List<data_Danhsachbatthuong> list_data_danhsachbatthuong){
        this.Context_ = context;
        this.Layout_ = layout;
        this.List_data_danhsachbatthuong = list_data_danhsachbatthuong;

    }
    @Override
    public int getCount() {
        return List_data_danhsachbatthuong.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
    private  class  Viewholder_data_danhsachbatthuong{
        TextView tv_danhba ;
        TextView tv_so_st ;
        ImageView imv_dongbo ;
        TextView tv_hoten ;
        TextView tv_diachi;
        TextView tv_tinhtrang;
        TextView tv_soanh;
        TextView tv_csc;
        TextView tv_csm;
        TextView tv_tieuthu;
    }
    @Override
    public View getView(int position,View convertView,ViewGroup parent) {
        adapter_Danhsachbatthuong.Viewholder_data_danhsachbatthuong viewholder ;
        if(convertView == null){
            viewholder = new adapter_Danhsachbatthuong.Viewholder_data_danhsachbatthuong();
            LayoutInflater inflater = (LayoutInflater) Context_.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(Layout_,null);
            // Map
            viewholder.tv_danhba = (TextView) convertView.findViewById(R.id.textview_danhba_dongdanhsachbatthuong);
            viewholder.tv_so_st = (TextView) convertView.findViewById(R.id.textview_sost_dongdanhsachbatthuong);
            viewholder.imv_dongbo =(ImageView) convertView.findViewById(R.id.imageView_dongbo_dongdanhsachbatthuong);
            viewholder.tv_hoten = (TextView) convertView.findViewById(R.id.textview_hoten_dongdanhsachbatthuong);
            viewholder.tv_diachi = (TextView) convertView.findViewById(R.id.textview_diachi_dongdanhsachbatthuong);
            viewholder.tv_tinhtrang = (TextView) convertView.findViewById(R.id.textview_tinhtrang_dongdanhsachbatthuong);
            viewholder.tv_soanh = (TextView) convertView.findViewById(R.id.textview_soanhchup_dongdanhsachbatthuong);
            viewholder.tv_csc = (TextView) convertView.findViewById(R.id.textview_csc_dongdanhsachbatthuong);
            viewholder.tv_csm = (TextView) convertView.findViewById(R.id.textview_csm_dongdanhsachbatthuong);
            viewholder.tv_tieuthu = (TextView) convertView.findViewById(R.id.textview_tieuthu_dongdanhsachbatthuong);
            //////////
            convertView.setTag(viewholder);
        }else {
            viewholder = (adapter_Danhsachbatthuong.Viewholder_data_danhsachbatthuong) convertView.getTag();
        }
        //
        viewholder.imv_dongbo.setFocusable(false);
        viewholder.imv_dongbo.setFocusableInTouchMode(false);
        // GET DATA
        data_Danhsachbatthuong data_danhsachbatthuong = List_data_danhsachbatthuong.get(position);
        // SET DATA
        viewholder.tv_danhba.setText(data_danhsachbatthuong.getDanhba());
        viewholder.tv_so_st.setText(data_danhsachbatthuong.getSo_st());
        int dongbo = data_danhsachbatthuong.getDongbo();
        if(!data_danhsachbatthuong.getTeuthu().trim().isEmpty()){
            dongbo =1;
        }

        switch(dongbo) {
            case 2:{
                // timeout
                viewholder.imv_dongbo.setImageResource(R.drawable.synced_2);
                break;
            }
            case 1:{
                // dang do bo
                viewholder.imv_dongbo.setImageResource(R.drawable.synced_1);
                break;
            }
            case 0:{
                // da dong bo
                viewholder.imv_dongbo.setImageResource(R.drawable.synced_0);
                break;
            }
        }
        viewholder.tv_hoten.setText(data_danhsachbatthuong.getHoten());
        viewholder.tv_diachi.setText(data_danhsachbatthuong.getDiaChi());
        String tieuthu = Objects.toString(data_danhsachbatthuong.getTeuthu(),"");
        String tinhtrang_luu = data_danhsachbatthuong.getTinhtrang().trim();
        if(!tieuthu.trim().isEmpty() && tinhtrang_luu.trim().isEmpty()){
            tinhtrang_luu = data_danhsachbatthuong.getTinhTrang_cu().trim();
        }
        viewholder.tv_tinhtrang.setText(tinhtrang_luu);
        viewholder.tv_soanh.setText(data_danhsachbatthuong.getSoanhChup());
        viewholder.tv_csc.setText(data_danhsachbatthuong.getCSC());
        viewholder.tv_csm.setText(data_danhsachbatthuong.getCSM());
        viewholder.tv_tieuthu.setText(data_danhsachbatthuong.getTeuthu());

        return convertView;
    }

}

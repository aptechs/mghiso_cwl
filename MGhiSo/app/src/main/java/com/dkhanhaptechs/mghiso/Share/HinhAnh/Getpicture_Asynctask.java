package com.dkhanhaptechs.mghiso.Share.HinhAnh;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.util.Base64;
import android.util.Log;

import com.dkhanhaptechs.mghiso.Share.Dialog_App;
import com.dkhanhaptechs.mghiso.chuphinh.activity_Xemanh;
import com.dkhanhaptechs.mghiso.chuphinh.data_Chupanh;
import com.loopj.android.http.TextHttpResponseHandler;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import cz.msebera.android.httpclient.util.EntityUtils;
import  com.dkhanhaptechs.mghiso.Share.User.*;

public class Getpicture_Asynctask extends AsyncTask<Void,Integer,Void> {
    Context contextParent;
    private String Nam;
    private String Ky ;
    private  String Danhba;
    private Dialog_App dialog_app ;
    private List<Bitmap> List_bitmap_anh;
    private int Status_Send_Finish = 0;
    private String BASE_URL_API ="/wsnhanhinhanh/services.asmx?op=DanhSachHinhAnh";
    private String BASE_URL ="";

    public Getpicture_Asynctask(Context contextParent,String nam,String ky,String danhba) {
        this.contextParent = contextParent;
        Nam = nam;
        Ky = ky;
        Danhba = danhba;
        dialog_app = new Dialog_App(contextParent);
        List_bitmap_anh = new ArrayList<>();
        data_Http data_http = new data_Http(contextParent);
            String url_run = data_http.get_Url_run();
            if(!url_run.isEmpty()){
                BASE_URL = url_run +BASE_URL_API;
            }else {
                BASE_URL = "http://viettel.capnuoccholon.com.vn"+BASE_URL_API;
            }
    }

    public List<Bitmap> getList_bitmap_anh() {
        return List_bitmap_anh;
    }

    public void setList_bitmap_anh(List<Bitmap> list_bitmap_anh) {
        List_bitmap_anh = list_bitmap_anh;
    }

    public int getStatus_Send_Finish() {
        return Status_Send_Finish;
    }



    @Override
    protected void onPreExecute() {
        super.onPreExecute();

            dialog_app.Dialog_Progress_Wait_Open("Đang lấy ảnh","Vui lòng chờ");
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

            dialog_app.Dialog_Progress_Wait_Close();

    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected Void doInBackground(Void... voids) {
        /*
        sqlite_Hinhanh sqlite_hinhanh = new sqlite_Hinhanh(contextParent,Nam,Ky,Dot,SoMay);
        if (sqlite_hinhanh.getStatus_Table_ListHinhAnh_Exists()) {
            List<String> list_id_nosync = sqlite_hinhanh.Get_TableID_ListHinhAnh_NoSync(Danhba);

        }
        */
        try {
            Get_HinhAnh_Server();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    private void Get_HinhAnh_Server( ) throws IOException {

        data_Api_Webservice_Hinhanh_get data_api_get = new data_Api_Webservice_Hinhanh_get(Danhba,Nam,Ky);
        String data_send = data_api_get.get_data_get_HinhAnh();
        StringEntity stringEntity_send = new StringEntity(data_send);
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(BASE_URL);
        httppost.setHeader("Content-type", "text/xml;charset=utf-8");
        httppost.setEntity(stringEntity_send);
        HttpResponse response = httpclient.execute(httppost);
        if(response.getStatusLine().getStatusCode() ==200) {

            String responseString = EntityUtils.toString(response.getEntity());
            String[] com_responseString = responseString.split("<string>");
            //  Log.e("com_responseString: ",String.valueOf(com_responseString.length));
            for (int i = 1; i < com_responseString.length; i++) {
                try {
                    String[] comdata_anh = com_responseString[i].split("</string>");
                    String data = comdata_anh[0];
                    Bitmap myBitmap = StringToBitmap(data);
                    if (myBitmap != null) {
                        long lenght = myBitmap.getByteCount();
                        if(lenght >100000) {
                            List_bitmap_anh.add(myBitmap);
                        }
                    }
                }catch (Exception r){

                }
            }
            Status_Send_Finish = 1;
        }else {
            Status_Send_Finish = 2;
        }
    }
    private Bitmap StringToBitmap(String base64){
        Bitmap decodedByte = null;
        try {
            byte[] decodedString = Base64.decode(base64,Base64.DEFAULT);
            decodedByte = BitmapFactory.decodeByteArray(decodedString,0,decodedString.length);

        }catch (Exception e){

        }
        return decodedByte;
    }

}

package com.dkhanhaptechs.mghiso.dangnhap;

import android.content.Context;
import android.util.Log;


import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import  com.dkhanhaptechs.mghiso.Share.User.*;




class data_Dangnhap_send {

    private String type;
    private  data_content content  = new data_content();

    public  data_Dangnhap_send(String mac){
        this.type ="dang nhap";
        this.content.setMac(mac);
    }
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public data_content getContent() {
        return content;
    }

    public void setContent(data_content content) {
        this.content = content;
    }




    class  data_content {
        private  String mac ;

        public data_content() {
        }

        public String getMac() {
            return mac;
        }

        public void setMac(String mac) {
            this.mac = mac;
        }
    }
}





class  data_Dangnhap_response{
    private  data_result result ;
    private  String status ;
    private  String message ;
    public  data_Dangnhap_response(){

    }

    public data_result getResult() {
        return result;
    }

    public void setResult(data_result result) {
        this.result = result;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    class data_result {
        private String tennhanvien;
        private String somay;
        private String dienthoai;

        public String getTennhanvien() {
            return tennhanvien;
        }

        public void setTennhanvien(String tennhanvien) {
            this.tennhanvien = tennhanvien;
        }

        public String getSomay() {
            return somay;
        }

        public void setSomay(String somay) {
            this.somay = somay;
        }

        public String getDienthoai() {
            return dienthoai;
        }

        public void setDienthoai(String dienthoai) {
            this.dienthoai = dienthoai;
        }
    }

}

 class  Api_DangNhap{
     private static  final  String BASE_URL_API = "/apikhachhang/api/docsodangnhap?thamso=";
    // private  String BASE_URL = "http://viettel1.capnuoccholon.com.vn/apikhachhang/api/docsodangnhap?thamso=";

     private static AsyncHttpClient client = new AsyncHttpClient();

     public static void get(Context context, String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {

         data_Http data_http = new data_Http(context);
         String url_run = data_http.get_Url_run();
        // url_run = "http://viettel1.capnuoccholon.com.vn";
         Log.e("Http CNLC: ",url_run);
         String url_http = "";
         if(!url_run.isEmpty()){
             url_http = url_run +BASE_URL_API;
         }else {
             url_http = "http://viettel.capnuoccholon.com.vn"+BASE_URL_API;
         }
         client.get(getAbsoluteUrl(url_http,url), params, responseHandler);
     }

     public static void post(Context context,String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        // Log.e("url",getAbsoluteUrl(url));
         data_Http data_http = new data_Http(context);
         String url_run = data_http.get_Url_run();
         String url_http = "";
         if(!url_run.isEmpty()){
             url_http = url_run +BASE_URL_API;
         }else {
             url_http = "http://viettel.capnuoccholon.com.vn"+BASE_URL_API;
         }
         client.post(getAbsoluteUrl(url_http,url), params, responseHandler);
     }


     private static String getAbsoluteUrl(String base_url,String relativeUrl) {
         return base_url + relativeUrl;
     }

}


package com.dkhanhaptechs.mghiso.danhsachdocso_chitiet_khachhang;

public class data_danhsachkhach_ghiso {
    private String danhba;
    private String diachhinew ;
    private String doituong;
    private String congdung;
    private String chi ;
    private String machigoc;
    private String sothan;
    private String dienthoai1;
    private String dienthoai2;
    private String tinhtrang ;
    private String csm;
    private String tieuthu;
    private String ghichu1;
    private String ghichu2 ;
    private String tinhtrangks ;

    public data_danhsachkhach_ghiso(String danhba,String diachhinew,String doituong,String congdung,
                                    String chi,String machigoc,String sothan,String dienthoai1,
                                    String dienthoai2,String tinhtrang,String csm,String tieuthu,
                                    String ghichu1,String ghichu2) {
       this.danhba = danhba;
        this.diachhinew = diachhinew;
        this.doituong = doituong;
        this.congdung = congdung;
        this.chi = chi;
        this.machigoc = machigoc;
        this.sothan = sothan;
        this.dienthoai1 = dienthoai1;
        this.dienthoai2 = dienthoai2;
        this.tinhtrang = tinhtrang;
        this.csm = csm;
        this.tieuthu = tieuthu;
        this.ghichu1 = ghichu1;
        this.ghichu2 = ghichu2;
        this.tinhtrangks = tinhtrangks;
    }

    public String getDanhba() {
        return danhba;
    }

    public void setDanhba(String danhba) {
        this.danhba = danhba;
    }

    public String getDiachhinew() {
        return diachhinew;
    }

    public void setDiachhinew(String diachhinew) {
        this.diachhinew = diachhinew;
    }

    public String getDoituong() {
        return doituong;
    }

    public void setDoituong(String doituong) {
        this.doituong = doituong;
    }

    public String getCongdung() {
        return congdung;
    }

    public void setCongdung(String congdung) {
        this.congdung = congdung;
    }

    public String getChi() {
        return chi;
    }

    public void setChi(String chi) {
        this.chi = chi;
    }

    public String getMachigoc() {
        return machigoc;
    }

    public void setMachigoc(String machigoc) {
        this.machigoc = machigoc;
    }

    public String getSothan() {
        return sothan;
    }

    public void setSothan(String sothan) {
        this.sothan = sothan;
    }

    public String getDienthoai1() {
        return dienthoai1;
    }

    public void setDienthoai1(String dienthoai1) {
        this.dienthoai1 = dienthoai1;
    }

    public String getDienthoai2() {
        return dienthoai2;
    }

    public void setDienthoai2(String dienthoai2) {
        this.dienthoai2 = dienthoai2;
    }

    public String getTinhtrang() {
        return tinhtrang;
    }

    public void setTinhtrang(String tinhtrang) {
        this.tinhtrang = tinhtrang;
    }

    public String getCsm() {
        return csm;
    }

    public void setCsm(String csm) {
        this.csm = csm;
    }

    public String getTieuthu() {
        return tieuthu;
    }

    public void setTieuthu(String tieuthu) {
        this.tieuthu = tieuthu;
    }

    public String getGhichu1() {
        return ghichu1;
    }

    public void setGhichu1(String ghichu1) {
        this.ghichu1 = ghichu1;
    }

    public String getGhichu2() {
        return ghichu2;
    }

    public void setGhichu2(String ghichu2) {
        this.ghichu2 = ghichu2;
    }

    public String getTinhtrangks() {
        return tinhtrangks;
    }

    public void setTinhtrangks(String tinhtrangks) {
        this.tinhtrangks = tinhtrangks;
    }


}

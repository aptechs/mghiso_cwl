package com.dkhanhaptechs.mghiso.Share.DanhSach_DocSo;
import com.dkhanhaptechs.mghiso.Share.TienNo.*;

public class data_Api_DanhSachDocSo_response {
    private Result result ;
    public data_Api_DanhSachDocSo_response (){

    }
    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public class Result {
        private String nam ;
        private String ky ;
        private  String dot ;
        private String may ;
        private  KhachHangs [] khachhangs ;

        public String getNam() {
            return nam;
        }

        public void setNam(String nam) {
            this.nam = nam;
        }

        public String getKy() {
            return ky;
        }

        public void setKy(String ky) {
            this.ky = ky;
        }

        public String getDot() {
            return dot;
        }

        public void setDot(String dot) {
            this.dot = dot;
        }

        public String getMay() {
            return may;
        }

        public void setMay(String may) {
            this.may = may;
        }

        public KhachHangs[] getKhachhangs() {
            return khachhangs;
        }

        public void setKhachhangs(KhachHangs[] khachhangs) {
            this.khachhangs = khachhangs;
        }
    }
    public class KhachHangs {
        private KhachHang khachhang ;
        private String CSGo ;
        private String CSGan;
        private String TieuThuCu ;
        private data_tiennos.data_tienno [] tiennos ;

        public KhachHang getKhachhang() {
            return khachhang;
        }

        public void setKhachhang(KhachHang khachhang) {
            this.khachhang = khachhang;
        }

        public String getCSGo() {
            return CSGo;
        }

        public void setCSGo(String CSGo) {
            this.CSGo = CSGo;
        }

        public String getCSGan() {
            return CSGan;
        }

        public void setCSGan(String CSGan) {
            this.CSGan = CSGan;
        }

        public data_tiennos.data_tienno[] getTiennos() {
            return tiennos;
        }

        public void setTiennos(data_tiennos.data_tienno[] tiennos) {
            this.tiennos = tiennos;
        }

        public String getTieuThuCu() {
            return TieuThuCu;
        }

        public void setTieuThuCu(String tieuThuCu) {
            TieuThuCu = tieuThuCu;
        }
    }
    public class KhachHang{
        private String DANHBA ;
        private String HoTen ;
        private String SoNha ;
        private String SoNhaMoi ;
        private String DUONG ;
        private String CHI ;
        private String ViTriDHN ;
        private String SOTHAN ;
        private String MACHIGOC ;
        private String HIEUDHN ;
        private String GiaBieu ;
        private String DinhMuc ;
        private String DinhMucNgheo ;
        private String CONGDUNG ;
        private String TILEDV ;
        private String TILEHCSN ;
        private String TILESH ;
        private String TILESX ;
        private String dienthoai1 ;
        private String dienthoai2 ;
        private String CODE ;
        private String CSCU ;
        private String CSMOI ;
        private String BinhQuan ;
        private String TinhTrang ;
        private String TIEUTHU ;
        private String Lat ;
        private String Lng ;
        private String somay ;
        private String NamDHN ;
        private String MLT ;
        private String TuNgay ;
        private String DenNgay ;
        private String NAM ;
        private String KY ;
        private String DOT ;
        private String CO ;
        private String GHICHU;
        private String KhongTinhPhi;
        public KhachHang() {
        }

        public String getDANHBA() {
            return DANHBA;
        }

        public void setDANHBA(String DANHBA) {
            this.DANHBA = DANHBA;
        }

        public String getHoTen() {
            return HoTen;
        }

        public void setHoTen(String hoTen) {
            HoTen = hoTen;
        }

        public String getSoNha() {
            return SoNha;
        }

        public void setSoNha(String soNha) {
            SoNha = soNha;
        }

        public String getSoNhaMoi() {
            return SoNhaMoi;
        }

        public void setSoNhaMoi(String soNhaMoi) {
            SoNhaMoi = soNhaMoi;
        }

        public String getDUONG() {
            return DUONG;
        }

        public void setDUONG(String DUONG) {
            this.DUONG = DUONG;
        }

        public String getCHI() {
            return CHI;
        }

        public void setCHI(String CHI) {
            this.CHI = CHI;
        }

        public String getViTriDHN() {
            return ViTriDHN;
        }

        public void setViTriDHN(String viTriDHN) {
            ViTriDHN = viTriDHN;
        }

        public String getSOTHAN() {
            return SOTHAN;
        }

        public void setSOTHAN(String SOTHAN) {
            this.SOTHAN = SOTHAN;
        }

        public String getMACHIGOC() {
            return MACHIGOC;
        }

        public void setMACHIGOC(String MACHIGOC) {
            this.MACHIGOC = MACHIGOC;
        }

        public String getHIEUDHN() {
            return HIEUDHN;
        }

        public void setHIEUDHN(String HIEUDHN) {
            this.HIEUDHN = HIEUDHN;
        }

        public String getGiaBieu() {
            return GiaBieu;
        }

        public void setGiaBieu(String giaBieu) {
            GiaBieu = giaBieu;
        }

        public String getDinhMuc() {
            return DinhMuc;
        }

        public void setDinhMuc(String dinhMuc) {
            DinhMuc = dinhMuc;
        }

        public String getDinhMucNgheo() {
            return DinhMucNgheo;
        }

        public void setDinhMucNgheo(String dinhMucNgheo) {
            DinhMucNgheo = dinhMucNgheo;
        }

        public String getCONGDUNG() {
            return CONGDUNG;
        }

        public void setCONGDUNG(String CONGDUNG) {
            this.CONGDUNG = CONGDUNG;
        }

        public String getTILEDV() {
            return TILEDV;
        }

        public void setTILEDV(String TILEDV) {
            this.TILEDV = TILEDV;
        }

        public String getTILEHCSN() {
            return TILEHCSN;
        }

        public void setTILEHCSN(String TILEHCSN) {
            this.TILEHCSN = TILEHCSN;
        }

        public String getTILESH() {
            return TILESH;
        }

        public void setTILESH(String TILESH) {
            this.TILESH = TILESH;
        }

        public String getTILESX() {
            return TILESX;
        }

        public void setTILESX(String TILESX) {
            this.TILESX = TILESX;
        }

        public String getDienthoai1() {
            return dienthoai1;
        }

        public void setDienthoai1(String dienthoai1) {
            this.dienthoai1 = dienthoai1;
        }

        public String getDienthoai2() {
            return dienthoai2;
        }

        public void setDienthoai2(String dienthoai2) {
            this.dienthoai2 = dienthoai2;
        }

        public String getCODE() {
            return CODE;
        }

        public void setCODE(String CODE) {
            this.CODE = CODE;
        }

        public String getCSCU() {
            return CSCU;
        }

        public void setCSCU(String CSCU) {
            this.CSCU = CSCU;
        }

        public String getCSMOI() {
            return CSMOI;
        }

        public void setCSMOI(String CSMOI) {
            this.CSMOI = CSMOI;
        }

        public String getBinhQuan() {
            return BinhQuan;
        }

        public void setBinhQuan(String binhQuan) {
            BinhQuan = binhQuan;
        }

        public String getTinhTrang() {
            return TinhTrang;
        }

        public void setTinhTrang(String tinhTrang) {
            TinhTrang = tinhTrang;
        }

        public String getTIEUTHU() {
            return TIEUTHU;
        }

        public void setTIEUTHU(String TIEUTHU) {
            this.TIEUTHU = TIEUTHU;
        }

        public String getLat() {
            return Lat;
        }

        public void setLat(String lat) {
            Lat = lat;
        }

        public String getLng() {
            return Lng;
        }

        public void setLng(String lng) {
            Lng = lng;
        }

        public String getSomay() {
            return somay;
        }

        public void setSomay(String somay) {
            this.somay = somay;
        }

        public String getNamDHN() {
            return NamDHN;
        }

        public void setNamDHN(String namDHN) {
            NamDHN = namDHN;
        }

        public String getMLT() {
            return MLT;
        }

        public void setMLT(String MLT) {
            this.MLT = MLT;
        }

        public String getTuNgay() {
            return TuNgay;
        }

        public void setTuNgay(String tuNgay) {
            TuNgay = tuNgay;
        }

        public String getDenNgay() {
            return DenNgay;
        }

        public void setDenNgay(String denNgay) {
            DenNgay = denNgay;
        }

        public String getNAM() {
            return NAM;
        }

        public void setNAM(String NAM) {
            this.NAM = NAM;
        }

        public String getKY() {
            return KY;
        }

        public void setKY(String KY) {
            this.KY = KY;
        }

        public String getDOT() {
            return DOT;
        }

        public void setDOT(String DOT) {
            this.DOT = DOT;
        }

        public String getCO() {
            return CO;
        }

        public void setCO(String CO) {
            this.CO = CO;
        }

        public String getGHICHU() {
            return GHICHU;
        }

        public void setGHICHU(String GHICHU) {
            this.GHICHU = GHICHU;
        }

        public String getKhongTinhPhi() {
            return KhongTinhPhi;
        }

        public void setKhongTinhPhi(String khongTinhPhi) {
            KhongTinhPhi = khongTinhPhi;
        }
    }

}

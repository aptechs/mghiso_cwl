package com.dkhanhaptechs.mghiso.danhsachdocso_chitiet;

public class data_Enable_Ghiso {
    private String somay;
    private String ngaykhoadoc;
    private String giokhoadoc;
    private String giokhoadocF;
    private String ngaydoctruoc;
    private String giodoctruoc;
    private String duocphepkhongchuphinh;

    public data_Enable_Ghiso(String somay,String ngaykhoadoc,String giokhoadoc,String giokhoadocF,String ngaydoctruoc,String giodoctruoc,String duocphepkhongchuphinh) {
        this.somay = somay;
        this.ngaykhoadoc = ngaykhoadoc;
        this.giokhoadoc = giokhoadoc;
        this.giokhoadocF = giokhoadocF;
        this.ngaydoctruoc = ngaydoctruoc;
        this.giodoctruoc = giodoctruoc;
        this.duocphepkhongchuphinh = duocphepkhongchuphinh;
    }

    public String getSomay() {
        return somay;
    }

    public void setSomay(String somay) {
        this.somay = somay;
    }

    public String getNgaykhoadoc() {
        return ngaykhoadoc;
    }

    public void setNgaykhoadoc(String ngaykhoadoc) {
        this.ngaykhoadoc = ngaykhoadoc;
    }

    public String getGiokhoadoc() {
        return giokhoadoc;
    }

    public void setGiokhoadoc(String giokhoadoc) {
        this.giokhoadoc = giokhoadoc;
    }

    public String getGiokhoadocF() {
        return giokhoadocF;
    }

    public void setGiokhoadocF(String giokhoadocF) {
        this.giokhoadocF = giokhoadocF;
    }

    public String getNgaydoctruoc() {
        return ngaydoctruoc;
    }

    public void setNgaydoctruoc(String ngaydoctruoc) {
        this.ngaydoctruoc = ngaydoctruoc;
    }

    public String getGiodoctruoc() {
        return giodoctruoc;
    }

    public void setGiodoctruoc(String giodoctruoc) {
        this.giodoctruoc = giodoctruoc;
    }

    public String getDuocphepkhongchuphinh() {
        return duocphepkhongchuphinh;
    }

    public void setDuocphepkhongchuphinh(String duocphepkhongchuphinh) {
        this.duocphepkhongchuphinh = duocphepkhongchuphinh;
    }
}

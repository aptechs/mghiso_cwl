package com.dkhanhaptechs.mghiso.danhsachdocso;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dkhanhaptechs.mghiso.R;

import java.util.List;

class adapter_Danhsachdocso extends BaseAdapter {
    //BIEN
    private Context Context_ ;
    private int Layout_ ;
    private List<data_Danhsachdocso> List_data_danhsachdocsos_ ;
    public adapter_Danhsachdocso(Context context, int layout, List<data_Danhsachdocso> list_data_danhsachdocso){
        this.Context_ = context;
        this.Layout_ = layout;
        this.List_data_danhsachdocsos_ = list_data_danhsachdocso;
    }
    @Override
    public int getCount() {
        return List_data_danhsachdocsos_.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    private  class  Viewholder_data_danhsachdocso{
        CheckBox cb_chon ;
        TextView tv_tieude ;
        ImageView imv_dongbo ;
        TextView tv_sodongho ;
        TextView tv_sodongho_dadoc;
        TextView tv_sanluong ;
        TextView tv_hoanthanh;
        TextView tv_sohinh;
        LinearLayout ln_dong ;

    }
    @Override
    public View getView(final int position,View convertView,ViewGroup parent) {
        final adapter_Danhsachdocso.Viewholder_data_danhsachdocso viewholder ;
        if(convertView == null){
            viewholder = new Viewholder_data_danhsachdocso();
            LayoutInflater inflater = (LayoutInflater) Context_.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(Layout_,null);
            // Map
            viewholder.cb_chon = (CheckBox) convertView.findViewById(R.id.checkbox_chon_dongdanhsachdocso);
            viewholder.tv_tieude = (TextView) convertView.findViewById(R.id.textview_tieude_dongdanhsachdocso);
            viewholder.imv_dongbo =(ImageView) convertView.findViewById(R.id.imageView_dongbo_dongdanhsachdocso);
            viewholder.tv_sodongho = (TextView) convertView.findViewById(R.id.textview_sodongho_dongdanhsachdocso);
            viewholder.tv_sodongho_dadoc = (TextView) convertView.findViewById(R.id.textview_sodonghodadoc_dongdanhsachdocso);
            viewholder.tv_sanluong = (TextView) convertView.findViewById(R.id.textview_sanluong_dongdanhsachdocso);
            viewholder.tv_hoanthanh = (TextView) convertView.findViewById(R.id.textview_hoanthanh_dongdanhsachdocso);
            viewholder.tv_sohinh = (TextView) convertView.findViewById(R.id.textview_sohinhsync_dongdanhsachdocso);
            viewholder.ln_dong =(LinearLayout) convertView.findViewById(R.id.ln_dong_danhsachdocso);
            //////////
            convertView.setTag(viewholder);
        }else {
            viewholder = (Viewholder_data_danhsachdocso) convertView.getTag();
        }
        //
        viewholder.cb_chon.setFocusable(false);
        viewholder.cb_chon.setFocusableInTouchMode(false);
        viewholder.imv_dongbo.setFocusable(false);
        viewholder.imv_dongbo.setFocusableInTouchMode(false);
        // lay du lieu
        final  data_Danhsachdocso data_danhsachdocso = List_data_danhsachdocsos_.get(position);
        // gan du lieu
        int dongbo = data_danhsachdocso.getDongbo();
        switch(dongbo) {
            case 2:{
                // timeout
                viewholder.imv_dongbo.setImageResource(R.drawable.synced_2);
                break;
            }
            case 1:{
                // dang dong bo
                viewholder.imv_dongbo.setImageResource(R.drawable.synced_1);
                break;
            }
            case 0:{
                // da dong bo
                viewholder.imv_dongbo.setImageResource(R.drawable.synced_0);
                break;
            }
        }
         String tieude = data_danhsachdocso.getTieude();
         viewholder.tv_tieude.setText(tieude);
        //
        int sodongho = data_danhsachdocso.getSo_dh();
        viewholder.tv_sodongho.setText(String.valueOf(sodongho));
        //
        int sodohongdadoc = data_danhsachdocso.getSo_dh_doc();
        viewholder.tv_sodongho_dadoc.setText(String.valueOf(sodohongdadoc));
        //
        int sanluong = data_danhsachdocso.getSanluong();
        viewholder.tv_sanluong.setText(String.valueOf(sanluong));
        //
        int sohinh = data_danhsachdocso.getSo_hinh();
        viewholder.tv_sohinh.setText(String.valueOf(sohinh));
        //
        double hoanthanh = sodohongdadoc*100/sodongho ;
        viewholder.tv_hoanthanh.setText(String.valueOf(hoanthanh)+"%");
        // xu ly du lieu
        if(position ==0){
            String a ="";
        }
        boolean chon = data_danhsachdocso.getChon();
        if(!chon){
            viewholder.cb_chon.setChecked(false);
        }else {
            viewholder.cb_chon.setChecked(true);
        }
        // chon
        viewholder.cb_chon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(viewholder.cb_chon.isChecked()){
                    data_danhsachdocso.setChon(true);

                }else {
                    data_danhsachdocso.setChon(false);

                }
                List_data_danhsachdocsos_.set(position,data_danhsachdocso);
            }
        });

        //
        if(data_danhsachdocso.getId() %2 ==1){
            viewholder.ln_dong.setBackgroundResource(R.color.dong_danhsach_0);
        }else {
            viewholder.ln_dong.setBackgroundResource(R.color.dong_danhsach_1);
        }

        return convertView;
    }
}

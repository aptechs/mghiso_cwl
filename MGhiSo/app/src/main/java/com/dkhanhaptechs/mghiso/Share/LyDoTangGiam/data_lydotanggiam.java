package com.dkhanhaptechs.mghiso.Share.LyDoTangGiam;

public class data_lydotanggiam {
    private String ma ;
    private String noidung ;

    public data_lydotanggiam(String ma,String noidung) {
        this.ma = ma;
        this.noidung = noidung;
    }

    public String getMa() {
        return ma;
    }

    public void setMa(String ma) {
        this.ma = ma;
    }

    public String getNoidung() {
        return noidung;
    }

    public void setNoidung(String noidung) {
        this.noidung = noidung;
    }
}

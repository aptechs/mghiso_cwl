package com.dkhanhaptechs.mghiso.Share.CheckVersion;

public class data_Checkversion {
    private int id ;
    private String version ;

    public data_Checkversion(int id,String version) {
        this.id = id;
        this.version = version;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}

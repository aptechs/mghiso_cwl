package com.dkhanhaptechs.mghiso.lichsuthaydongho;

class data_Lichsuthaydongho {
    private int Id ;
    private String Datetime ;
    private String Hieu ;
    private String Co;
    private String Sothan;
    private String Lydo;
    private String CSGo;
    private String CSGan;

    public data_Lichsuthaydongho(int id,String datetime,String hieu,String co,String sothan,String lydo,String CSGo,String CSGan) {
        Id = id;
        Datetime = datetime;
        Hieu = hieu;
        Co = co;
        Sothan = sothan;
        Lydo = lydo;
        this.CSGo = CSGo;
        this.CSGan = CSGan;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getDatetime() {
        return Datetime;
    }

    public void setDatetime(String datetime) {
        Datetime = datetime;
    }

    public String getHieu() {
        return Hieu;
    }

    public void setHieu(String hieu) {
        Hieu = hieu;
    }

    public String getCo() {
        return Co;
    }

    public void setCo(String co) {
        Co = co;
    }

    public String getSothan() {
        return Sothan;
    }

    public void setSothan(String sothan) {
        Sothan = sothan;
    }

    public String getLydo() {
        return Lydo;
    }

    public void setLydo(String lydo) {
        Lydo = lydo;
    }

    public String getCSGo() {
        return CSGo;
    }

    public void setCSGo(String CSGo) {
        this.CSGo = CSGo;
    }

    public String getCSGan() {
        return CSGan;
    }

    public void setCSGan(String CSGan) {
        this.CSGan = CSGan;
    }
}

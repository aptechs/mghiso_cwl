package com.dkhanhaptechs.mghiso.chuphinh;

import android.graphics.Bitmap;

public class data_Chupanh {
    private int id ;
    private Bitmap anh ;
    private boolean selete ;
    private String sync_anh;


    public data_Chupanh(int id,Bitmap anh,boolean selete,String sync_anh) {
        this.id = id;
        this.anh = anh;
        this.selete = selete;
        this.sync_anh = sync_anh;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Bitmap getAnh() {
        return anh;
    }

    public void setAnh(Bitmap anh) {
        this.anh = anh;
    }

    public boolean isSelete() {
        return selete;
    }

    public void setSelete(boolean selete) {
        this.selete = selete;
    }

    public String getSync_anh() {
        return sync_anh;
    }

    public void setSync_anh(String sync_anh) {
        this.sync_anh = sync_anh;
    }
}

package com.dkhanhaptechs.mghiso.Share.LyDoTangGiam;

public class data_lydotanggiam_response {
    private String status;
    private String message;
    private data_result result ;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public data_result getResult() {
        return result;
    }

    public void setResult(data_result result) {
        this.result = result;
    }

    public class data_result {
        private data_lydotanggiam [] lydotanggiam;

        public data_lydotanggiam[] getLydotanggiam() {
            return lydotanggiam;
        }

        public void setLydotanggiam(data_lydotanggiam[] lydotanggiam) {
            this.lydotanggiam = lydotanggiam;
        }
    }
    public  class data_lydotanggiam{
        private String ma;
        private String noidung ;

        public String getMa() {
            return ma;
        }

        public void setMa(String ma) {
            this.ma = ma;
        }

        public String getNoidung() {
            return noidung;
        }

        public void setNoidung(String noidung) {
            this.noidung = noidung;
        }
    }

}

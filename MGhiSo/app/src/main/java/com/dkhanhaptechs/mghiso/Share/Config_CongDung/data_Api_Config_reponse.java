package com.dkhanhaptechs.mghiso.Share.Config_CongDung;


public class data_Api_Config_reponse {
    private data_result result ;

    public data_result getResult() {
        return result;
    }

    public void setResult(data_result result) {
        this.result = result;
    }

    public class data_result {
        private String version;
        private data_congdung [] congdungs ;

        public String getVersion() {
            return version;
        }

        public void setVersion(String version) {
            this.version = version;
        }

        public data_congdung[] getCongdungs() {
            return congdungs;
        }

        public void setCongdungs(data_congdung[] congdungs) {
            this.congdungs = congdungs;
        }
    }
    public class data_congdung {
        private String DoiTuong ;
        private data_ListCongDung [] ListCongDung;

        public String getDoiTuong() {
            return DoiTuong;
        }

        public void setDoiTuong(String doiTuong) {
            DoiTuong = doiTuong;
        }

        public data_ListCongDung[] getListCongDung() {
            return ListCongDung;
        }

        public void setListCongDung(data_ListCongDung[] listCongDung) {
            ListCongDung = listCongDung;
        }
    }
    public class data_ListCongDung{
        private String cong_dung ;

        public String getCong_dung() {
            return cong_dung;
        }

        public void setCong_dung(String cong_dung) {
            this.cong_dung = cong_dung;
        }
    }
}

package com.dkhanhaptechs.mghiso.danhsachbatthuong;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.dkhanhaptechs.mghiso.R;

import com.dkhanhaptechs.mghiso.Share.DanhSach_DocSo.data_DanhSachDocSo;
import com.dkhanhaptechs.mghiso.Share.DanhSach_DocSo.sqlite_DanhSachDocSo;
import com.dkhanhaptechs.mghiso.Share.DanhSach_DocSo.sqlite_DanhSachKhackHang_DocSo;
import com.dkhanhaptechs.mghiso.Share.HinhAnh.*;
import com.dkhanhaptechs.mghiso.danhsachdocso_chitiet.data_Enable_Ghiso;
import com.dkhanhaptechs.mghiso.danhsachdocso_chitiet_khachhang.activity_Danhsachdocso_chitiet_khachhang;


import org.json.JSONException;
import org.json.JSONObject;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;



public class activity_Danhsachbatthuong extends AppCompatActivity {
    private String Json_table_ghiso ="";
    private Spinner spn_Danhsachdocso, spn_Tinhtrang;
    private Button btn_Chuaghiso, btn_Chuachupanh;
    private TextView tv_sumkhachhang, tv_tinhtrangbatthuong;
    private ListView lv_Danhsachbatthuong;
    private ArrayAdapter adapter_tinhtrang;
    private ArrayList<String> array_tinhtrang ;
    private String [] data_TinhTrang = {
            "B.Thường",
            "Tăng - Giảm",
            "Chủ ghi",
            "Chủ báo",
            "Đóng cửa",
            "Thay Đ.Kỳ",
            "Thay B.Thường",
            "Mất",
            "Đ/C K.ở",
            "N.C.N",
            "Chất đồ",
            "Giải tỏa",
            "Kính mờ",
            "Kẹt khóa",
            "Lệch số",
            "Kẹt số",
            "Chạy ngược",
            "Ngập nước",
            "Gắn mới",
            "TLDB",
            "Retour4",
            "Retour5",
            "Lấp",
            "Đóng Nước",
            "Bấm Chì"
    };

    private ArrayList<data_Danhsachbatthuong> Arraylist_data_danhsachbatthuong;
    private adapter_Danhsachbatthuong Adapter_danhsachbatthuong;

    private ArrayList<String> array_Danhsachdocso ;
    private  ArrayAdapter adapter_Danhsachdocso ;
    private String Nam, Ky,Dot, SoMay ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_danhsachbatthuong);
        Mappping_Layout();


        array_tinhtrang = new ArrayList<String>();
        adapter_tinhtrang = new ArrayAdapter(activity_Danhsachbatthuong.this,android.R.layout.simple_spinner_item,array_tinhtrang);
        adapter_tinhtrang.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
        spn_Tinhtrang.setAdapter(adapter_tinhtrang);
        array_tinhtrang.addAll(Arrays.asList(data_TinhTrang));
        adapter_tinhtrang.notifyDataSetChanged();

        Arraylist_data_danhsachbatthuong = new ArrayList<data_Danhsachbatthuong>();
        Adapter_danhsachbatthuong = new adapter_Danhsachbatthuong(activity_Danhsachbatthuong.this,R.layout.dong_danhsachbatthuong,Arraylist_data_danhsachbatthuong);
        lv_Danhsachbatthuong.setAdapter(Adapter_danhsachbatthuong);

        array_Danhsachdocso = new ArrayList<String>();
        adapter_Danhsachdocso = new ArrayAdapter(activity_Danhsachbatthuong.this,android.R.layout.simple_spinner_item,array_Danhsachdocso);
        adapter_Danhsachdocso.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
        spn_Danhsachdocso.setAdapter(adapter_Danhsachdocso);

        Json_table_ghiso = getIntent().getStringExtra("TABLE_DANHSACHDOCSO_BATTHUONG");
        if (Json_table_ghiso.isEmpty()) {
            Display_DanhSachDocSo();
        } else {
            Get_Table_GhiSo(Json_table_ghiso);
            String danhsachdocso = "M" + Nam + "-Đ" + Dot + "-K" + Ky + "-M" + SoMay;
            array_Danhsachdocso.add(danhsachdocso);
        }
        adapter_Danhsachdocso.notifyDataSetChanged();

        btn_Chuaghiso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_tinhtrangbatthuong.setText("Tình trạng: Sót đọc số");
                Display_DanhSachKhachHang_ChuaGhiSo();
            }
        });
        btn_Chuachupanh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_tinhtrangbatthuong.setText("Tình trạng: Chưa chụp ảnh");
                Display_DanhSachKhachHang_ChuaChupAnh();
            }
        });
        spn_Tinhtrang.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent,View view,int position,long id) {
                String tinhtrang = data_TinhTrang[position].trim();
                tv_tinhtrangbatthuong.setText("Tình trạng: " + tinhtrang);
                Display_DanhSachKhachHang_ChuaChupAnh(tinhtrang);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spn_Danhsachdocso.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent,View view,int position,long id) {

                String title = array_Danhsachdocso.get(position);
                Get_NamKyDot(title);
                String tinhtrang = spn_Tinhtrang.getSelectedItem().toString().toUpperCase(Locale.getDefault());
                ;

                Display_DanhSachKhachHang_ChuaChupAnh(tinhtrang);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        lv_Danhsachbatthuong.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent,View view,int position,long id) {


                String lbl_tinhtrang = tv_tinhtrangbatthuong.getText().toString().trim();
                String[] com_tinhtrang = lbl_tinhtrang.split(":");
                String tinhtrang = com_tinhtrang[1].trim();

                // Date currentTime = Calendar.getInstance().getTime();
                // String h_currentDate = new SimpleDateFormat("HH",Locale.getDefault()).format(currentTime);
                // int hour = Integer.parseInt(h_currentDate);

                data_Danhsachbatthuong data_danhsach_batthuong = Arraylist_data_danhsachbatthuong.get(position);
                String danhba = data_danhsach_batthuong.getDanhba();
                String nam = String.valueOf(Nam);
                String ky = String.valueOf(Ky);
                String dot = String.valueOf(Dot);
                String somay = String.valueOf(SoMay);
                String list_danhba = "";


                if (Arraylist_data_danhsachbatthuong.size() == 1) {
                    list_danhba = "'" + danhba + "'";
                } else {
                    for (int i = 0; i < Arraylist_data_danhsachbatthuong.size(); i++) {
                        data_Danhsachbatthuong batthuong = Arraylist_data_danhsachbatthuong.get(i);

                        if (i == Arraylist_data_danhsachbatthuong.size() - 1) {
                            list_danhba = list_danhba + "'" + batthuong.getDanhba().trim() + "'";
                        } else {
                            list_danhba = list_danhba + "'" + batthuong.getDanhba().trim() + "',";
                        }
                    }
                }
                tinhtrang = tinhtrang + "#" + list_danhba;

                String json_send = "{\"danhba\":\""+danhba+"\","+
                        "\"nam\":\""+nam+"\","+
                        "\"ky\":\""+ky+"\","+
                        "\"dot\":\""+dot+"\","+
                        "\"somay\":\""+somay+"\","+
                        "\"tinhtrang\":\""+tinhtrang+"\"}";

                Intent intent = new Intent(activity_Danhsachbatthuong.this,activity_Danhsachdocso_chitiet_khachhang.class);
                intent.putExtra("TABLE_DANHSACHDOCSO_CHITIET",json_send);
                startActivity(intent);


            }
        });


    }

    @Override
    protected void onResume() {
        try{
            String lbl_batthuong = tv_tinhtrangbatthuong.getText().toString().trim();
            String  [] com_lbl_batthuong = lbl_batthuong.split(":");
            if(com_lbl_batthuong.length >1){
                if(!com_lbl_batthuong[1].trim().isEmpty()){
                    if(com_lbl_batthuong[1].trim().equals("Sót đọc số")){
                        Display_DanhSachKhachHang_ChuaGhiSo();
                    }else  if(com_lbl_batthuong[1].trim().equals("Chưa chụp ảnh")){
                        Display_DanhSachKhachHang_ChuaChupAnh();
                    }else {
                        String tinhtrang = spn_Tinhtrang.getSelectedItem().toString().trim();
                        Display_DanhSachKhachHang_ChuaChupAnh(tinhtrang);
                    }
                }
            }

        }catch (Exception e){

        }
        super.onResume();
    }

    private boolean Get_Table_GhiSo(String json){
        boolean status = false;
        try{
            JSONObject table = new JSONObject(json);
            Nam = table.getString("Nam");
            Ky = table.getString("Ky");
            Dot = table.getString("Dot");
            SoMay = table.getString("SoMay");
            status = true;
        }catch (JSONException e){

        }
        return  status;
    }
    private void  Mappping_Layout(){
        tv_sumkhachhang = (TextView) findViewById(R.id.textview_tongsokhachhang_Danhsachbatthuong);
        tv_tinhtrangbatthuong = (TextView) findViewById(R.id.textview_tinhtrangbatthuong_danhsachbatthuong);
        spn_Danhsachdocso = (Spinner) findViewById(R.id.spinner_danhsachdocso_Danhsachbatthuong);
        spn_Tinhtrang = (Spinner) findViewById(R.id.spinner_trangthai_Danhsachbatthuong);
        btn_Chuaghiso = (Button) findViewById(R.id.button_chuaghiso_Danhsachbatthuong);
        btn_Chuachupanh = (Button) findViewById(R.id.button_chuachupanh_Danhsachbatthuong);
        lv_Danhsachbatthuong = (ListView) findViewById(R.id.listview_danhsachbatthuong_Danhsachbatthuong);
    }
    private String Check_Enable_Ghiso(data_Enable_Ghiso data_enable_ghiso){
        String status = "0";
        try{
            SimpleDateFormat format_datetime_chukyds = new SimpleDateFormat("dd/MM/yyyy'T'HH:mm:ss");
            Date currenttime = Calendar.getInstance().getTime();
            String h_start = Objects.toString(data_enable_ghiso.getGiodoctruoc(),"").trim();
            String date_start = Objects.toString(data_enable_ghiso.getNgaydoctruoc(),"").trim();
            String h_end = Objects.toString(data_enable_ghiso.getGiokhoadoc(),"").trim();
            String date_end = Objects.toString(data_enable_ghiso.getNgaykhoadoc(),"").trim();
            if(!h_start.isEmpty()
                    && !date_start.isEmpty()
                    && !h_end.isEmpty()
                    && !date_end.isEmpty()){
                String date_enable_start = date_start+"T"+h_start+":00:00";
                String date_enable_end = date_end+"T"+h_end+":00:00";
                Date date_start_ghiso = format_datetime_chukyds.parse(date_enable_start);
                Date date_end_ghiso = format_datetime_chukyds.parse(date_enable_end);

                long diff_start = date_start_ghiso.getTime();
                long diff_end = date_end_ghiso.getTime();
                long diff_current = currenttime.getTime();
                if((diff_current >= diff_start) && (diff_current <= diff_end)){
                    status ="1";
                    return  status;
                }

            }else if(  !h_start.isEmpty()
                    && !date_start.isEmpty()){

                String date_enable_start = date_start+"T"+h_start+":00:00";
                Date date_start_ghiso = format_datetime_chukyds.parse(date_enable_start);

                long diff_start = date_start_ghiso.getTime();
                long diff_current = currenttime.getTime();
                if((diff_current >= diff_start) ){
                    status ="1";
                    return  status;
                }
            }
            else if(  !h_end.isEmpty()
                    && !date_end.isEmpty()){

                String date_enable_end = date_end+"T"+h_end+":00:00";
                Date date_end_ghiso = format_datetime_chukyds.parse(date_enable_end);

                long diff_end = date_end_ghiso.getTime();
                long diff_current = currenttime.getTime();
                if( (diff_current <= diff_end)){
                    status ="1";
                    return  status;
                }
            }
        }catch (Exception e){

        }
        return  status;
    }
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),0);
        }
        return super.dispatchTouchEvent(ev);
    }
    private void Display_DanhSachDocSo() {
        sqlite_DanhSachDocSo sqlite_danhSachDocSo = new sqlite_DanhSachDocSo(activity_Danhsachbatthuong.this);

        if (sqlite_danhSachDocSo.getStatus_Table_DanhSachDocSo_Exists()) {
            List<data_DanhSachDocSo> all_danhSachDocSo = sqlite_danhSachDocSo.Get_All_DanhSachDocSo();
            array_Danhsachdocso.clear();
            for (int i = all_danhSachDocSo.size() - 1; i >= 0; i--) {
                data_DanhSachDocSo data_sql = all_danhSachDocSo.get(i);
                String title = "N" + data_sql.getNam() + " - Đ" + data_sql.getDot() + " - K" + data_sql.getKy() + " - M" + data_sql.getSomay();
                array_Danhsachdocso.add(title);
            }


        }
    }
    private void Get_NamKyDot(String title){

        String[] com_title = title.split("-");
        Nam = com_title[0].trim().substring(1).toString().trim();
        Dot = com_title[1].trim().substring(1).toString().trim();
        Ky = com_title[2].trim().substring(1).toString().trim();
        SoMay = com_title[3].trim().substring(1).toString().trim();
    }
    private void Display_DanhSachKhachHang_ChuaGhiSo(){
        sqlite_DanhSachKhackHang_DocSo sqlite_danhSachKhackHang_docSo = new sqlite_DanhSachKhackHang_DocSo(activity_Danhsachbatthuong.this,Nam,Ky,Dot,SoMay);
        Arraylist_data_danhsachbatthuong.clear();
        sqlite_Hinhanh sqlite_hinhanh = new sqlite_Hinhanh(activity_Danhsachbatthuong.this,Nam,Ky,Dot,SoMay);
        if(sqlite_danhSachKhackHang_docSo.getStatus_Table_DanhSachKhachHang_Exists()){
            List<data_Danhsachbatthuong> khachhangs = sqlite_danhSachKhackHang_docSo.Get_DanhSachDocSo_Chitiet_ChuaghiSo();
            for(data_Danhsachbatthuong khachhang : khachhangs) {
                String tieuthu = khachhang.getTeuthu().trim();
                int sync = khachhang.getDongbo();
                if(tieuthu.isEmpty() || (sync ==1) || (sync ==2)) {
                    String danhba = khachhang.getDanhba();
                    int num_anh = sqlite_hinhanh.Get_Count_Table_ListHinhAnh(danhba);
                    khachhang.setSoanhChup(String.valueOf(num_anh));
                    Arraylist_data_danhsachbatthuong.add(khachhang);
                }
            }
        }
        int songuoi = Arraylist_data_danhsachbatthuong.size();
        tv_sumkhachhang.setText(String.valueOf(songuoi));
        Adapter_danhsachbatthuong.notifyDataSetChanged();

    }
    private void Display_DanhSachKhachHang_ChuaChupAnh(){

        sqlite_DanhSachKhackHang_DocSo sqlite_danhSachKhackHang_docSo = new sqlite_DanhSachKhackHang_DocSo(activity_Danhsachbatthuong.this,Nam,Ky,Dot,SoMay);
        Arraylist_data_danhsachbatthuong.clear();
        sqlite_Hinhanh sqlite_hinhanh = new sqlite_Hinhanh(activity_Danhsachbatthuong.this,Nam,Ky,Dot,SoMay);
        if(sqlite_danhSachKhackHang_docSo.getStatus_Table_DanhSachKhachHang_Exists()){
            if(sqlite_hinhanh.getStatus_Table_ListHinhAnh_Exists()) {
                List<String> list_danhbacoanh = sqlite_hinhanh.Get_Table_ListDanhBaCoAnh();
                List<data_Danhsachbatthuong> khachhangs = sqlite_danhSachKhackHang_docSo.Get_DanhSachDocSo_Chitiet_ChuaghiSo();
                for (data_Danhsachbatthuong khachhang : khachhangs) {
                    Boolean status_add =true;
                    String danhba = khachhang.getDanhba();
                    for (String danhba_anh : list_danhbacoanh) {

                        if(danhba.equals(danhba_anh)){
                            status_add =false;
                            break;
                        }

                    }
                    if(status_add){
                        int num_anh = sqlite_hinhanh.Get_Count_Table_ListHinhAnh(danhba);
                        if (num_anh == 0) {
                            Arraylist_data_danhsachbatthuong.add(khachhang);
                        }
                    }
                }
            }
        }
        final int songuoi = Arraylist_data_danhsachbatthuong.size();

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tv_sumkhachhang.setText(String.valueOf(songuoi));
                Adapter_danhsachbatthuong.notifyDataSetChanged();
            }
        });
    }
    private void Display_DanhSachKhachHang_ChuaChupAnh(String tinhtrang){
        sqlite_DanhSachKhackHang_DocSo sqlite_danhSachKhackHang_docSo = new sqlite_DanhSachKhackHang_DocSo(activity_Danhsachbatthuong.this,Nam,Ky,Dot,SoMay);
        try {
            Arraylist_data_danhsachbatthuong.clear();
            Adapter_danhsachbatthuong.notifyDataSetChanged();
        }catch (Exception e){

        }
        tinhtrang = tinhtrang.toUpperCase(Locale.getDefault());
        sqlite_Hinhanh sqlite_hinhanh = new sqlite_Hinhanh(activity_Danhsachbatthuong.this,Nam,Ky,Dot,SoMay);
        if(sqlite_danhSachKhackHang_docSo.getStatus_Table_DanhSachKhachHang_Exists()){
                List<data_Danhsachbatthuong> khachhangs = sqlite_danhSachKhackHang_docSo.Get_DanhSachDocSo_Chitiet_ChuaghiSo();
                for (data_Danhsachbatthuong khachhang : khachhangs) {
                    String danhba = khachhang.getDanhba();
                    String tieuthu = Objects.toString(khachhang.getTeuthu(),"");
                    String tinhtrang_luu = khachhang.getTinhtrang().trim().toUpperCase(Locale.getDefault());
                    if(!tieuthu.trim().isEmpty() && tinhtrang_luu.trim().isEmpty()){
                        tinhtrang_luu = khachhang.getTinhTrang_cu().trim().toUpperCase(Locale.getDefault());
                    }
                    int num_anh = sqlite_hinhanh.Get_Count_Table_ListHinhAnh(danhba);
                    khachhang.setSoanhChup(String.valueOf(num_anh));
                    if(tinhtrang.equals(("TĂNG - GIẢM"))){
                        Log.e("tang","ok");
                       khachhang =  Tinh_TrangThai_TangGiam(khachhang);
                       if(!khachhang.getTinhtrang().isEmpty()) {
                           Arraylist_data_danhsachbatthuong.add(khachhang);
                       }
                    }else {
                        if (tinhtrang_luu.equals(tinhtrang.toUpperCase(Locale.getDefault()))) {
                            Arraylist_data_danhsachbatthuong.add(khachhang);
                        }
                    }

                }
        }
        final int songuoi = Arraylist_data_danhsachbatthuong.size();

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tv_sumkhachhang.setText(String.valueOf(songuoi));
                Adapter_danhsachbatthuong.notifyDataSetChanged();
            }
        });

    }
    private data_Danhsachbatthuong Tinh_TrangThai_TangGiam(data_Danhsachbatthuong khachhang) {
        String binhquan = khachhang.getBinhquan().toString().trim();
        String tieuthu = khachhang.getTeuthu().toString().trim();
        int per_tieuthu_alarm =10;
        if (!tieuthu.isEmpty()) {
            int tieuthu_i = Integer.parseInt(tieuthu) ;
            int binhquan_i = Integer.parseInt(binhquan);
            if (binhquan_i > 1 && binhquan_i <= 100) {
                per_tieuthu_alarm = 30;
            } else if (binhquan_i > 100 && binhquan_i <= 1000) {
                per_tieuthu_alarm = 20;
            } else {
                per_tieuthu_alarm = 10;
            }
            int thr_tieuthu_nho = binhquan_i - binhquan_i * per_tieuthu_alarm / 100;
            int thr_tieuthu_lon = binhquan_i + binhquan_i * per_tieuthu_alarm / 100;
            if (tieuthu_i < thr_tieuthu_nho) {
                khachhang.setTinhtrang("Tiêu thụ nhỏ hơn " + String.valueOf(per_tieuthu_alarm) + "%");

                // Dialog_Notification_alarm_luu(notification);
            } else if (tieuthu_i > thr_tieuthu_lon) {
                khachhang.setTinhtrang("Tiêu thụ lớn hơn " + String.valueOf(per_tieuthu_alarm) + "%");
                //Dialog_Notification_alarm_luu(notification);
            } else {
                // XacNhan_Ghiso();
                khachhang.setTinhtrang("");
            }

        }else {
            khachhang.setTinhtrang("");
        }
        return  khachhang;
    }

}
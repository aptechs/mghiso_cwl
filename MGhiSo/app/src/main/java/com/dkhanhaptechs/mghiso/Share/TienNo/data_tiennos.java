package com.dkhanhaptechs.mghiso.Share.TienNo;

public class data_tiennos {
    private String danhba ;
   private data_tienno [] tiennos;

    public data_tiennos(String danhba,data_tienno[] tiennos) {
        this.danhba = danhba;
        this.tiennos = tiennos;
    }
    public data_tiennos(String danhba) {
        this.danhba = danhba;
        this.tiennos = tiennos;
    }

    public String getDanhba() {
        return danhba;
    }

    public void setDanhba(String danhba) {
        this.danhba = danhba;
    }

    public data_tienno[] getTiennos() {
        return tiennos;
    }

    public void setTiennos(data_tienno[] tiennos) {
        this.tiennos = tiennos;
    }

    public static class  data_tienno {
        private String Nam;
        private String Ky ;
        private String TienNo;

        public data_tienno(String nam,String ky,String tienNo) {
            Nam = nam;
            Ky = ky;
            TienNo = tienNo;
        }

        public String getNam() {
            return Nam;
        }

        public void setNam(String nam) {
            Nam = nam;
        }

        public String getKy() {
            return Ky;
        }

        public void setKy(String ky) {
            Ky = ky;
        }

        public String getTienNo() {
            return TienNo;
        }

        public void setTienNo(String tienNo) {
            TienNo = tienNo;
        }
    }

}

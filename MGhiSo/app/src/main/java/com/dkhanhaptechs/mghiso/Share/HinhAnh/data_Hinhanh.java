package com.dkhanhaptechs.mghiso.Share.HinhAnh;

import java.util.List;

public class data_Hinhanh {

    private String danhba ;
    private List<data_anh> hinhanhs ;

    public data_Hinhanh(String danhba) {
        this.danhba = danhba;
    }

    public static class data_anh {
        private int id ;
        private String anh;
        private String sync_anh;
        private String type ;
        public data_anh(String anh) {
            this.anh = anh;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getAnh() {
            return anh;
        }

        public void setAnh(String anh) {
            this.anh = anh;
        }

        public String getSync_anh() {
            return sync_anh;
        }

        public void setSync_anh(String sync_anh) {
            this.sync_anh = sync_anh;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }

    public String getDanhba() {
        return danhba;
    }

    public void setDanhba(String danhba) {
        this.danhba = danhba;
    }

    public List<data_anh> getHinhanhs() {
        return hinhanhs;
    }

    public void setHinhanhs(List<data_anh> hinhanhs) {
        this.hinhanhs = hinhanhs;
    }
}

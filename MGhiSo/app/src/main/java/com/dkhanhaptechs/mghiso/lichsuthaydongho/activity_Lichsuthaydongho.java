package com.dkhanhaptechs.mghiso.lichsuthaydongho;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.dkhanhaptechs.mghiso.R;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import com.dkhanhaptechs.mghiso.Share.LichSuThayDH.* ;
import com.dkhanhaptechs.mghiso.Share.User.data_Http;
import com.google.gson.Gson;
import com.loopj.android.http.TextHttpResponseHandler;

import cz.msebera.android.httpclient.Header;

public class activity_Lichsuthaydongho extends AppCompatActivity {
    // BIEN LAYOUT
    private TextView tv_danhba, tv_sost, tv_hoten,tv_diachi;
    private ImageView imv_dongbo;
    private Toolbar toolbar ;
    private String Json_table_ghiso_chitiet = "";
    private ListView lv_danhsach_lichsuthaydongho;

    private String  So_st, Hoten, Diachi;
    private String Danhba, Nam, Ky, Dot, SoMay;
    private int Dongbo;

    // BIEN
    private ArrayList<data_Lichsuthaydongho> Arraylist_data_lichsuthaydongho;
    private adapter_Lichsuthaydongho Adapter_lichsuthaydongho;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lichsuthaydongho);
        // INIT
        Mappping_Layout();
        Arraylist_data_lichsuthaydongho = new ArrayList<data_Lichsuthaydongho>();
        Adapter_lichsuthaydongho = new adapter_Lichsuthaydongho(activity_Lichsuthaydongho.this,R.layout.dong_lichsuthaydongho,Arraylist_data_lichsuthaydongho);
        lv_danhsach_lichsuthaydongho.setAdapter(Adapter_lichsuthaydongho);
        // GET DATA
        Json_table_ghiso_chitiet =getIntent().getStringExtra("LICHSUTHAYDONGHO");

        // SET DATA
        if(Get_Table_GhiSo_Chitiet(Json_table_ghiso_chitiet)){
            tv_danhba.setText(Danhba);
            tv_sost.setText(So_st);
            tv_hoten.setText(Hoten);
            tv_diachi.setText(Diachi);
            switch (Dongbo){
                case 2:{
                    // timeout
                    imv_dongbo.setImageResource(R.drawable.synced_2);
                    break;
                }
                case 1:{
                    // dang do bo
                    imv_dongbo.setImageResource(R.drawable.synced_1);
                    break;
                }
                case 0:{
                    // da dong bo
                    imv_dongbo.setImageResource(R.drawable.synced_0);
                    break;
                }
            }
        }

        Display_LichSuThayDongHo();



    }
    private void Test_GanData_Listview() {
        data_Lichsuthaydongho data1 = new data_Lichsuthaydongho(1,"10/08/2020","ABB","15","123423221","Kiem dinh","123453","0");
        Arraylist_data_lichsuthaydongho.add(data1);
        data_Lichsuthaydongho data2 = new data_Lichsuthaydongho(2,"10/01/2020","SIE","25","123423221","Kiem dinh","123453","0");
        Arraylist_data_lichsuthaydongho.add(data2);
        data_Lichsuthaydongho data3 = new data_Lichsuthaydongho(3,"10/04/2020","ITR","55","123423221","Kiem dinh","123453","0");
        Arraylist_data_lichsuthaydongho.add(data3);
        data_Lichsuthaydongho data4 = new data_Lichsuthaydongho(4,"10/07/2020","ABB","65","123423221","Kiem dinh","123453","0");
        Arraylist_data_lichsuthaydongho.add(data4);
        data_Lichsuthaydongho data5 = new data_Lichsuthaydongho(5,"10/09/2020","ITR","75","123423221","Kiem dinh","123453","0");
        Arraylist_data_lichsuthaydongho.add(data5);


    }
    private boolean Get_Table_GhiSo_Chitiet(String json){
        boolean status = false;
        try{
            JSONObject table = new JSONObject(json);

            Danhba = table.getString("danhba");
            So_st = table.getString("so_st");
            Hoten = table.getString("hoten");
            Diachi = table.getString("diachi") ;
            String sync = table.getString("sync");
            Dongbo = Integer.parseInt(sync);
            Nam = table.getString("nam");
            Ky = table.getString("ky");
            Dot = table.getString("dot");
            SoMay = table.getString("somay");

            status = true;
        }catch (JSONException e){

        }
        return  status;
    }
    private void Mappping_Layout() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tv_danhba =(TextView) findViewById(R.id.textview_danhba_lichsuthaydongho);
        tv_sost = ( TextView) findViewById(R.id.textview_sost_lichsuthaydongho);
        imv_dongbo =( ImageView) findViewById(R.id.imageView_dongbo_lichsuthaydongho);
        tv_hoten = ( TextView) findViewById(R.id.textview_hoten_lichsuthaydongho);
        tv_diachi = ( TextView) findViewById(R.id.textview_diachi_lichsuthaydongho);
        lv_danhsach_lichsuthaydongho =( ListView) findViewById(R.id.listview_danhsach_lichsuthaydongho);
    }

    private void Display_LichSuThayDongHo(){
        String danhba = Danhba;
     //   Log.e("Danhba",Danhba);
        data_Lichsuthaydh_send data_lichsuthaydh_send = new data_Lichsuthaydh_send(danhba);
        Gson gson_request = new Gson();
        String json_send = gson_request.toJson(data_lichsuthaydh_send);
        Api_Lichsuthaydh.get(activity_Lichsuthaydongho.this,json_send,
                null,
                new TextHttpResponseHandler() {
                    @Override
                    public void onFailure(int statusCode,Header[] headers,String responseString,Throwable throwable) {
                        String text = "Lỗi lay lich su thay dong ho"+"\r\n"+"Vui lòng kiểm tra lại.";
                        Toast.makeText(activity_Lichsuthaydongho.this,text,Toast.LENGTH_SHORT).show();
                        data_Http data_http = new data_Http(activity_Lichsuthaydongho.this);
                        data_http.switch_Url_run();
                    }

                    @Override
                    public void onSuccess(int statusCode,Header[] headers,String responseString) {
                        try {
                            Gson gson_reponse = new Gson();
                            data_Lichsuthaydong_response data_lichsuthaydong_response = gson_reponse.fromJson(responseString,data_Lichsuthaydong_response.class);
                            //  Log.e("data_LSTDH_response",gson_reponse.toJson( data_lichsuthaydong_response));
                            data_Lichsuthaydong_response.data_lichsudongho[] all_data_thongtindh = data_lichsuthaydong_response.getResult().getLichsudonghos();
                            try {
                                Arraylist_data_lichsuthaydongho.clear();
                            } catch (Exception e) {

                            }
                            for (int i = all_data_thongtindh.length - 1; i >= 0; i--) {
                                try {

                                    String ngaythay = all_data_thongtindh[i].getNgaythay();
                                    String hieu = all_data_thongtindh[i].getHieu();
                                    String co = all_data_thongtindh[i].getCo();
                                    String sothan = all_data_thongtindh[i].getSoThan();
                                    String lydo = all_data_thongtindh[i].getLydo();
                                    String csgo = all_data_thongtindh[i].getCSGo();
                                    String csgan = all_data_thongtindh[i].getCSGan();
                                    data_Lichsuthaydongho data_lichsuthaydongho = new data_Lichsuthaydongho(i,ngaythay,hieu,co,sothan,lydo,csgo,csgan);
                                    Arraylist_data_lichsuthaydongho.add(data_lichsuthaydongho);
                                } catch (Exception e) {

                                }
                            }
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Adapter_lichsuthaydongho.notifyDataSetChanged();
                                }
                            });
                        }catch (Exception e){

                        }
                    }
                });
    }

}
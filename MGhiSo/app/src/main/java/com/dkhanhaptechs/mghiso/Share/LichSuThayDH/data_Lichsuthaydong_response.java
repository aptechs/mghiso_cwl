package com.dkhanhaptechs.mghiso.Share.LichSuThayDH;


public class data_Lichsuthaydong_response {
    private data_result result ;
    private String status;
    private String message ;

    public data_result getResult() {
        return result;
    }

    public void setResult(data_result result) {
        this.result = result;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public class  data_result {
        private  data_lichsudongho [] lichsudonghos ;

        public data_lichsudongho[] getLichsudonghos() {
            return lichsudonghos;
        }

        public void setLichsudonghos(data_lichsudongho[] lichsudonghos) {
            this.lichsudonghos = lichsudonghos;
        }
    }
    public  class  data_lichsudongho {
        private String ngaythay;
        private String Hieu;
        private String Co;
        private String SoThan;
        private String lydo;
        private String CSGo;
        private String CSGan;
        private String MaChiGoc;

        public String getNgaythay() {
            return ngaythay;
        }

        public void setNgaythay(String ngaythay) {
            this.ngaythay = ngaythay;
        }

        public String getHieu() {
            return Hieu;
        }

        public void setHieu(String hieu) {
            Hieu = hieu;
        }

        public String getCo() {
            return Co;
        }

        public void setCo(String co) {
            Co = co;
        }

        public String getSoThan() {
            return SoThan;
        }

        public void setSoThan(String soThan) {
            SoThan = soThan;
        }

        public String getLydo() {
            return lydo;
        }

        public void setLydo(String lydo) {
            this.lydo = lydo;
        }

        public String getCSGo() {
            return CSGo;
        }

        public void setCSGo(String CSGo) {
            this.CSGo = CSGo;
        }

        public String getCSGan() {
            return CSGan;
        }

        public void setCSGan(String CSGan) {
            this.CSGan = CSGan;
        }

        public String getMaChiGoc() {
            return MaChiGoc;
        }

        public void setMaChiGoc(String maChiGoc) {
            MaChiGoc = maChiGoc;
        }
    }

}

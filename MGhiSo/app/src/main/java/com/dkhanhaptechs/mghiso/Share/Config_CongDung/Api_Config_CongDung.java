package com.dkhanhaptechs.mghiso.Share.Config_CongDung;

import android.content.Context;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import  com.dkhanhaptechs.mghiso.Share.User.*;

public class Api_Config_CongDung {
     private static  final  String BASE_URL_API = "/apikhachhang/api/docsolistcongdung?thamso=";
  //  private static final String BASE_URL = "http://viettel.capnuoccholon.com.vn/apikhachhang/api/docsolistcongdung?thamso=";

    private static AsyncHttpClient client = new AsyncHttpClient();

    public static void get(Context context, String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.setTimeout(20000);
        client.setConnectTimeout(20000);
        client.setResponseTimeout(20000);

       data_Http data_http = new data_Http(context);
         String url_run = data_http.get_Url_run();
         String url_http = "";
         if(!url_run.isEmpty()){
             url_http = url_run +BASE_URL_API;
         }else {
             url_http = "http://viettel.capnuoccholon.com.vn"+BASE_URL_API;
         }
         client.get(getAbsoluteUrl(url_http,url), params, responseHandler);
    }

    public static void post(Context context,String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.setTimeout(20000);
        client.setConnectTimeout(20000);
        client.setResponseTimeout(20000);
       data_Http data_http = new data_Http(context);
         String url_run = data_http.get_Url_run();
         String url_http = "";
         if(!url_run.isEmpty()){
             url_http = url_run +BASE_URL_API;
         }else {
             url_http = "http://viettel.capnuoccholon.com.vn"+BASE_URL_API;
         }
         client.post(getAbsoluteUrl(url_http,url), params, responseHandler);
    }

     private static String getAbsoluteUrl(String base_url,String relativeUrl) {
         return base_url + relativeUrl;
     }
}

package com.dkhanhaptechs.mghiso.Share.ThanhToan;

public class data_Tiennuoc {
    private String Danhba;
    private String Tiennuoc;
    private String Thuetiennuoc_5;
    private String PhiBVMT;
    private String Dichvuthoatnuoc;
    private String Thuedichvuthoatnuoc;
    public data_Tiennuoc (){

    };
    public data_Tiennuoc(String danhba, String tiennuoc, String thuetiennuoc_5, String phiBVMT, String dichvuthoatnuoc, String thuedichvuthoatnuoc) {
        Danhba = danhba;
        Tiennuoc = tiennuoc;
        Thuetiennuoc_5 = thuetiennuoc_5;
        PhiBVMT = phiBVMT;
        Dichvuthoatnuoc = dichvuthoatnuoc;
        Thuedichvuthoatnuoc = thuedichvuthoatnuoc;
    }

    public String getDanhba() {
        return Danhba;
    }

    public void setDanhba(String danhba) {
        Danhba = danhba;
    }

    public String getTiennuoc() {
        return Tiennuoc;
    }

    public void setTiennuoc(String tiennuoc) {
        Tiennuoc = tiennuoc;
    }

    public String getThuetiennuoc_5() {
        return Thuetiennuoc_5;
    }

    public void setThuetiennuoc_5(String thuetiennuoc_5) {
        Thuetiennuoc_5 = thuetiennuoc_5;
    }

    public String getPhiBVMT() {
        return PhiBVMT;
    }

    public void setPhiBVMT(String phiBVMT) {
        PhiBVMT = phiBVMT;
    }

    public String getDichvuthoatnuoc() {
        return Dichvuthoatnuoc;
    }

    public void setDichvuthoatnuoc(String dichvuthoatnuoc) {
        Dichvuthoatnuoc = dichvuthoatnuoc;
    }

    public String getThuedichvuthoatnuoc() {
        return Thuedichvuthoatnuoc;
    }

    public void setThuedichvuthoatnuoc(String thuedichvuthoatnuoc) {
        Thuedichvuthoatnuoc = thuedichvuthoatnuoc;
    }
}

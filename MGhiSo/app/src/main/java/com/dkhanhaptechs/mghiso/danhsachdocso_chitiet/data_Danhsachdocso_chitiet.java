package com.dkhanhaptechs.mghiso.danhsachdocso_chitiet;

public class data_Danhsachdocso_chitiet {
    private int Id ;
    private String Danhba;
    private  String So_st ;
    private  int Dongbo ;
    private  String Hoten ;
    private  String DiaChi;
    private String Tieuthu;
    private String Mlt;

    public data_Danhsachdocso_chitiet(int id,String danhba,String so_st,int dongbo,String hoten,String diaChi,String tieuthu, String mlt) {
        Id = id;
        Danhba = danhba;
        So_st = so_st;
        Hoten = hoten;
        DiaChi = diaChi;
        Dongbo = dongbo;
        Tieuthu = tieuthu;
        Mlt = mlt;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getDanhba() {
        return Danhba;
    }

    public void setDanhba(String danhba) {
        Danhba = danhba;
    }

    public String getSo_st() {
        return So_st;
    }

    public void setSo_st(String so_st) {
        So_st = so_st;
    }

    public String getHoten() {
        return Hoten;
    }

    public void setHoten(String hoten) {
        Hoten = hoten;
    }

    public String getDiaChi() {
        return DiaChi;
    }

    public void setDiaChi(String diaChi) {
        DiaChi = diaChi;
    }

    public int getDongbo() {
        return Dongbo;
    }

    public void setDongbo(int dongbo) {
        Dongbo = dongbo;
    }

    public String getTieuthu() {
        return Tieuthu;
    }

    public void setTieuthu(String tieuthu) {
        Tieuthu = tieuthu;
    }

    public String getMlt() {
        return Mlt;
    }

    public void setMlt(String mlt) {
        Mlt = mlt;
    }
}

package com.dkhanhaptechs.mghiso.Share.DanhSach_DocSo;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.dkhanhaptechs.mghiso.Share.Database_SQLite;
import com.dkhanhaptechs.mghiso.Share.SyncGhiSo.*;
import com.dkhanhaptechs.mghiso.danhsachdocso_chitiet.data_Danhsachdocso_chitiet;
import com.dkhanhaptechs.mghiso.danhsachdocso_chitiet_khachhang.* ;
import com.dkhanhaptechs.mghiso.danhsachbatthuong.* ;
import com.dkhanhaptechs.mghiso.thongketieuthu.data_Laydulieumaychu;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class sqlite_DanhSachKhackHang_DocSo {
    private Database_SQLite database_sqLite ;
    private String Database ="Database_DocSo";
    private String Table_DanhSachHangHang = "DanhSachHangHang";
    private boolean Status_DanhSachKhachHang_Exists = false;
    public  sqlite_DanhSachKhackHang_DocSo(Context context, String nam,String ky, String dot, String somay){
        this.Database = Database +"_"+nam+".sqlite";
        this.Table_DanhSachHangHang = Table_DanhSachHangHang +"_"+nam+"_"+ky+"_"+dot+"_"+somay ;
        database_sqLite = new Database_SQLite(context, Database, null, 1);
        if(Create_Table_DanhSachHangHang()){
            Status_DanhSachKhachHang_Exists = true;
        }else {
            Status_DanhSachKhachHang_Exists =false;
        }
    }
    public boolean getStatus_Table_DanhSachKhachHang_Exists() {
        return Status_DanhSachKhachHang_Exists;
    }
    private boolean Create_Table_DanhSachHangHang (){
        boolean ok = false;
        try{

            String querry = "CREATE TABLE IF NOT EXISTS "+Table_DanhSachHangHang +
                    " (" +
                    " id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                    " danhba nvarchar(20)," +
                    " hoten nvarchar(100)," +
                    " sonha nvarchar(50)," +
                    " sonhamoi nvarchar(50)," +
                    " duong nvarchar(100)," +
                    " chi nvarchar(20)," +
                    " vitridhn nvarchar(10)," +
                    " sothan nvarchar(20)," +
                    " machigoc nvarchar(10)," +
                    " hieudh nvarchar(20)," +
                    " giabieu nvarchar(10)," +
                    " dinhmuc nvarchar(10)," +
                    " dinhmucngheo nvarchar(10)," +
                    " congdung nvarchar(100)," +
                    " tiledv nvarchar(10)," +
                    " tilehcsn nvarchar(10)," +
                    " tilesh nvarchar(10)," +
                    " tilesx nvarchar(10)," +
                    " dienthoai1 nvarchar(15)," +
                    " dienthoai2 nvarchar(15)," +
                    " code nvarchar(5)," +
                    " codemoi nvarchar(5)," +
                    " cscu nvarchar(15)," +
                    " csmoi nvarchar(15)," +
                    " binhquan nvarchar(15)," +
                    " tinhtrang nvarchar(50)," +
                    " tinhtrangmoi nvarchar(50)," +
                    " tieuthu nvarchar(15)," +
                    " tieuthucu nvarchar(15)," +
                    " lat nvarchar(15)," +
                    " lng nvarchar(15)," +
                    " somay nvarchar(5)," +
                    " namdhn nvarchar(5)," +
                    " mlt nvarchar(20)," +
                    " tungay datetime," +
                    " denngay datetime," +
                    " nam nvarchar(5)," +
                    " ky nvarchar(5)," +
                    " dot nvarchar(5)," +
                    " csgo nvarchar(15)," +
                    " csgan nvarchar(15)," +
                    " ngaythaydh nvarchar(50)," +
                    " lydo nvarchar(100)," +
                    " ghichu1 nvarchar(250)," +
                    " ghichu2 nvarchar(250)," +
                    " sync nvarchar(2)," +
                    " tinhtrangkh nvarchar(100)," +
                    " codh nvarchar(10)," +
                    " khongtinhphi_bvmt nvarchar(5)" +
                    ");";
            if(database_sqLite.QueryDatabase(querry)) {
                ok = true;
            }
        }catch (Exception e){
            Log.e("Create DanhSachHangHang", e.toString());
        }
        return  ok;
    }
   public  boolean Update_Table_DanhSachHangHang_GhiSo(data_danhsachkhach_ghiso data_ghiso){
       boolean ok =false;
       try {
           String querry ="UPDATE "+Table_DanhSachHangHang+
                   " SET " +
                   " sonhamoi ='"+Objects.toString(data_ghiso.getDiachhinew(),"")+"'," +
                   " congdung ='"+Objects.toString(data_ghiso.getCongdung(),"")+"'," +
                   " chi ='"+Objects.toString(data_ghiso.getChi(),"")+"'," +
                   " machigoc ='"+Objects.toString(data_ghiso.getMachigoc(),"")+"'," +
                   " sothan ='"+Objects.toString(data_ghiso.getSothan(),"")+"'," +
                   " dienthoai1 ='"+Objects.toString(data_ghiso.getDienthoai1(),"")+"'," +
                   " dienthoai2 ='"+Objects.toString(data_ghiso.getDienthoai2(),"")+"'," +
                   " tinhtrang ='"+Objects.toString(data_ghiso.getTinhtrang(),"")+"'," +
                   " csmoi ='"+Objects.toString(data_ghiso.getCsm(),"")+"'," +
                   " tieuthu ='"+Objects.toString(data_ghiso.getTieuthu(),"")+"'," +
                   " ghichu1 ='"+Objects.toString(data_ghiso.getGhichu1(),"")+"'," +
                   " ghichu2 ='"+Objects.toString(data_ghiso.getGhichu2(),"")+"'," +
                   " tinhtrangkh ='"+Objects.toString(data_ghiso.getTinhtrangks(),"")+"'" +
                   " WHERE danhba ='"+data_ghiso.getDanhba()+"' ;";
           if(database_sqLite.QueryDatabase(querry)) {
               ok = true;
           }
       }catch (Exception e){

       }
       return  ok;
   }
    public  boolean Update_TaiDulieuMayChu_DanhSachHangHang_GhiSo(String danhba , String csmoi,String codemoi, String tieuthu,String sync){
        boolean ok =false;
        try {
            String querry ="UPDATE "+Table_DanhSachHangHang+
                    " SET " +
                    " csmoi ='"+Objects.toString(csmoi,"")+"'," +
                    " codemoi ='"+Objects.toString(codemoi,"")+"'," +
                    " tieuthu ='"+Objects.toString(tieuthu,"")+"'," +
                    " sync ='"+Objects.toString(sync,"")+"'" +
                    " WHERE danhba ='"+danhba+"' ;";
            if(database_sqLite.QueryDatabase(querry)) {
                ok = true;
            }
        }catch (Exception e){

        }
        return  ok;
    }
    public  boolean Insert_Update_Table_DanhSachHangHang(data_Api_DanhSachDocSo_response.KhachHang khachhang ,String csgo, String csgan,String tieuthucu, String sync,String tinhtrang_dongcua){
        boolean ok =false;
        try {
            String querry = "SELECT * FROM "+Table_DanhSachHangHang +
                    " WHERE " +
                    "  danhba ='"+khachhang.getDANHBA().toString()+"'" +
                    ";";
            Cursor data_check = database_sqLite.GetDatabase(querry);
            String id = "";
            while (data_check.moveToNext()) {
                id = data_check.getString(0);
            }
            String danhbatest = khachhang.getDANHBA();
            if(danhbatest.equals("06056530740")){
                String a ="";
            }
            if(!id.isEmpty()){
                // update
                querry = "UPDATE "+Table_DanhSachHangHang+
                        " SET " +
                        " danhba ='"+Objects.toString(khachhang.getDANHBA(),"")+"'," +
                        " hoten ='"+Objects.toString(khachhang.getHoTen(),"")+"'," +
                        " sonha ='"+Objects.toString(khachhang.getSoNha(),"")+"'," +
                        " sonhamoi ='"+Objects.toString(khachhang.getSoNhaMoi(),"")+"'," +
                        " duong ='"+Objects.toString(khachhang.getDUONG(),"")+"'," +
                        " chi ='"+Objects.toString(khachhang.getCHI(),"")+"'," +
                        " vitridhn ='"+Objects.toString(khachhang.getViTriDHN(),"")+"'," +
                        " sothan ='"+Objects.toString(khachhang.getSOTHAN(),"")+"'," +
                        " machigoc ='"+Objects.toString(khachhang.getMACHIGOC(),"")+"'," +
                        " hieudh ='"+Objects.toString(khachhang.getHIEUDHN(),"")+"'," +
                        " giabieu ='"+Objects.toString(khachhang.getGiaBieu(),"")+"'," +
                        " dinhmuc ='"+Objects.toString(khachhang.getDinhMuc(),"")+"'," +
                        " dinhmucngheo ='"+Objects.toString(khachhang.getDinhMucNgheo(),"")+"'," +
                        " congdung ='"+Objects.toString(khachhang.getCONGDUNG(),"")+"'," +
                        " tiledv ='"+Objects.toString(khachhang.getTILEDV(),"")+"'," +
                        " tilehcsn ='"+Objects.toString(khachhang.getTILEHCSN(),"")+"'," +
                        " tilesh ='"+Objects.toString(khachhang.getTILESH(),"")+"'," +
                        " tilesx ='"+Objects.toString(khachhang.getTILESX(),"")+"'," +
                        " dienthoai1 ='"+Objects.toString(khachhang.getDienthoai1(),"")+"'," +
                        " dienthoai2 ='"+Objects.toString(khachhang.getDienthoai2(),"")+"'," +
                        " code ='"+Objects.toString(khachhang.getCODE(),"")+"'," +
                        " cscu ='"+Objects.toString(khachhang.getCSCU(),"")+"'," +
                        " csmoi ='"+Objects.toString(khachhang.getCSMOI(),"")+"'," +
                        " binhquan ='"+Objects.toString(khachhang.getBinhQuan(),"")+"'," +
                        " tinhtrang ='"+Objects.toString(khachhang.getTinhTrang(),"")+"'," +
                        " tieuthu ='"+Objects.toString(khachhang.getTIEUTHU(),"")+"'," +
                        " tieuthucu ='"+Objects.toString(tieuthucu,"")+"'," +
                        " lat ='"+Objects.toString(khachhang.getLat(),"")+"'," +
                        " lng ='"+Objects.toString(khachhang.getLng(),"")+"'," +
                        " somay ='"+Objects.toString(khachhang.getSomay(),"")+"'," +
                        " namdhn ='"+Objects.toString(khachhang.getNamDHN(),"")+"'," +
                        " mlt ='"+Objects.toString(khachhang.getMLT(),"")+"'," +
                        " tungay ='"+Objects.toString(khachhang.getTuNgay(),"")+"'," +
                        " denngay ='"+Objects.toString(khachhang.getDenNgay(),"")+"'," +
                        " nam ='"+Objects.toString(khachhang.getNAM(),"")+"'," +
                        " ky ='"+Objects.toString(khachhang.getKY(),"")+"'," +
                        " dot ='"+Objects.toString(khachhang.getDOT(),"")+"'," +
                        " csgo ='"+csgo.toString()+"'," +
                        " csgan ='"+csgan.toString()+"'," +
                        " codh ='"+Objects.toString(khachhang.getCO(),"")+"'," +
                        " tinhtrangkh ='"+tinhtrang_dongcua+"'," +
                        " khongtinhphi_bvmt ='"+Objects.toString(khachhang.getKhongTinhPhi(),"0")+"'," +
                        " sync ='"+sync.toString()+"'" +
                        " WHERE " +
                        "  danhba ='"+khachhang.getDANHBA().toString()+"'" +
                        ";";
            }else {
                // insert
                querry = "INSERT INTO "+Table_DanhSachHangHang+" (" +
                        "danhba,hoten,sonha,sonhamoi,duong," +
                        "chi,vitridhn,sothan,machigoc,hieudh," +
                        "giabieu,dinhmuc,dinhmucngheo,congdung,tiledv," +
                        "tilehcsn,tilesh,tilesx,dienthoai1,dienthoai2," +
                        "code,cscu,csmoi,binhquan,tinhtrang," +
                        "tieuthu,tieuthucu,lat,lng,somay,namdhn,mlt," +
                        "tungay,denngay,nam,ky,dot," +
                        "csgo,csgan,tinhtrangkh,codh,ghichu1,khongtinhphi_bvmt,sync" +
                        ")" +
                        " VALUES (" +
                        "'" +  Objects.toString(khachhang.getDANHBA(),"") + "'," +
                        "'" +  Objects.toString(khachhang.getHoTen(),"") + "'," +
                        "'" +  Objects.toString(khachhang.getSoNha(),"") + "'," +
                        "'" +  Objects.toString(khachhang.getSoNhaMoi(),"") + "'," +
                        "'" +  Objects.toString(khachhang.getDUONG(),"") + "'," +
                        "'0'," +
                        "'" +  Objects.toString(khachhang.getViTriDHN(),"") + "'," +
                        "'" +  Objects.toString(khachhang.getSOTHAN(),"")+ "'," +
                        "'" +  Objects.toString(khachhang.getMACHIGOC(),"")+"'," +
                        "'" +  Objects.toString(khachhang.getHIEUDHN(),"")+ "'," +
                        "'" +  Objects.toString(khachhang.getGiaBieu(),"") + "'," +
                        "'" +  Objects.toString(khachhang.getDinhMuc(),"") + "'," +
                        "'" +  Objects.toString(khachhang.getDinhMucNgheo(),"") + "'," +
                        "'" +  Objects.toString(khachhang.getCONGDUNG(),"") + "'," +
                        "'" +  Objects.toString(khachhang.getTILEDV(),"") + "'," +
                        "'" +  Objects.toString(khachhang.getTILEHCSN(),"") + "'," +
                        "'" +  Objects.toString(khachhang.getTILESH(),"") + "'," +
                        "'" +  Objects.toString(khachhang.getTILESX(),"") + "'," +
                        "'" +  Objects.toString(khachhang.getDienthoai1(),"") + "'," +
                        "'" +  Objects.toString(khachhang.getDienthoai2(),"") + "'," +
                        "'" +  Objects.toString(khachhang.getCODE(),"") + "'," +
                        "'" +  Objects.toString(khachhang.getCSCU(),"") + "'," +
                        "'" +  Objects.toString(khachhang.getCSMOI(),"") + "'," +
                        "'" +  Objects.toString(khachhang.getBinhQuan(),"") + "'," +
                        "'" +  Objects.toString(khachhang.getTinhTrang(),"") + "'," +
                        "'" +  Objects.toString(khachhang.getTIEUTHU(),"") + "'," +
                        "'" +  Objects.toString(tieuthucu,"") + "'," +
                        "'" +  Objects.toString(khachhang.getLat(),"") + "'," +
                        "'" +  Objects.toString(khachhang.getLng(),"") + "'," +
                        "'" +  Objects.toString(khachhang.getSomay(),"")+ "'," +
                        "'" +  Objects.toString(khachhang.getNamDHN(),"") + "'," +
                        "'" +  Objects.toString(khachhang.getMLT(),"") + "'," +
                        "'" +  Objects.toString(khachhang.getTuNgay(),"") + "'," +
                        "'" +  Objects.toString(khachhang.getDenNgay(),"") + "'," +
                        "'" +  Objects.toString(khachhang.getNAM(),"") + "'," +
                        "'" +  Objects.toString(khachhang.getKY(),"") + "'," +
                        "'" +  Objects.toString(khachhang.getDOT(),"") + "'," +
                        "'" +  csgo + "'," +
                        "'" +  csgan + "'," +
                        "'" +  tinhtrang_dongcua+"',"+
                        "'" +  Objects.toString(khachhang.getCO(),"") + "'," +
                        "'" +  Objects.toString(khachhang.getGHICHU(),"") + "'," +
                        "'" +  Objects.toString(khachhang.getKhongTinhPhi(),"") + "'," +
                        "'" +sync+ "'" +
                        ");";
            }
            if(database_sqLite.QueryDatabase(querry)) {
                ok = true;
            }
        }catch (Exception e){
            Log.e("Loi", e.toString());
        }
        return ok;
    }

    public  boolean Update_Table_DanhSachHangHang_Maychu(data_Laydulieumaychu.KhachHang khachhang , String sync){
        boolean ok =false;
        try {
            String querry = "SELECT * FROM "+Table_DanhSachHangHang +
                    " WHERE " +
                    "  danhba ='"+khachhang.getDANHBA().toString()+"'" +
                    ";";
            Cursor data_check = database_sqLite.GetDatabase(querry);
            String id = "";
            while (data_check.moveToNext()) {
                id = data_check.getString(0);
            }
            if(!id.isEmpty()){
                // update
                querry = "UPDATE "+Table_DanhSachHangHang+
                        " SET " +
                        " danhba ='"+Objects.toString(khachhang.getDANHBA(),"")+"'," +
                        " hoten ='"+Objects.toString(khachhang.getHoTen(),"")+"'," +
                        " sonha ='"+Objects.toString(khachhang.getSoNha(),"")+"'," +
                        " sonhamoi ='"+Objects.toString(khachhang.getSoNhaMoi(),"")+"'," +
                        " duong ='"+Objects.toString(khachhang.getDUONG(),"")+"'," +
                        " chi ='"+Objects.toString(khachhang.getCHI(),"")+"'," +
                        " vitridhn ='"+Objects.toString(khachhang.getViTriDHN(),"")+"'," +
                        " sothan ='"+Objects.toString(khachhang.getSOTHAN(),"")+"'," +
                        " machigoc ='"+Objects.toString(khachhang.getMACHIGOC(),"")+"'," +
                        " hieudh ='"+Objects.toString(khachhang.getHIEUDHN(),"")+"'," +
                        " giabieu ='"+Objects.toString(khachhang.getGiaBieu(),"")+"'," +
                        " dinhmuc ='"+Objects.toString(khachhang.getDinhMuc(),"")+"'," +
                        " dinhmucngheo ='"+Objects.toString(khachhang.getDinhMucNgheo(),"")+"'," +
                        " congdung ='"+Objects.toString(khachhang.getCONGDUNG(),"")+"'," +
                        " tiledv ='"+Objects.toString(khachhang.getTILEDV(),"")+"'," +
                        " tilehcsn ='"+Objects.toString(khachhang.getTILEHCSN(),"")+"'," +
                        " tilesh ='"+Objects.toString(khachhang.getTILESH(),"")+"'," +
                        " tilesx ='"+Objects.toString(khachhang.getTILESX(),"")+"'," +
                        " dienthoai1 ='"+Objects.toString(khachhang.getDienthoai1(),"")+"'," +
                        " dienthoai2 ='"+Objects.toString(khachhang.getDienthoai2(),"")+"'," +
                        " code ='"+Objects.toString(khachhang.getCODE(),"")+"'," +
                        " cscu ='"+Objects.toString(khachhang.getCSCU(),"")+"'," +
                        " csmoi ='"+Objects.toString(khachhang.getCSMOI(),"")+"'," +
                        " binhquan ='"+Objects.toString(khachhang.getBinhQuan(),"")+"'," +
                        " tinhtrang ='"+Objects.toString(khachhang.getTinhTrang(),"")+"'," +
                        " tieuthu ='"+Objects.toString(khachhang.getTIEUTHU(),"")+"'," +

                        " lat ='"+Objects.toString(khachhang.getLat(),"")+"'," +
                        " lng ='"+Objects.toString(khachhang.getLng(),"")+"'," +
                        " somay ='"+Objects.toString(khachhang.getSomay(),"")+"'," +
                        " namdhn ='"+Objects.toString(khachhang.getNamDHN(),"")+"'," +
                        " mlt ='"+Objects.toString(khachhang.getMLT(),"")+"'," +
                        " tungay ='"+Objects.toString(khachhang.getTuNgay(),"")+"'," +
                        " denngay ='"+Objects.toString(khachhang.getDenNgay(),"")+"'," +
                        " nam ='"+Objects.toString(khachhang.getNAM(),"")+"'," +
                        " ky ='"+Objects.toString(khachhang.getKY(),"")+"'," +
                        " dot ='"+Objects.toString(khachhang.getDOT(),"")+"'," +

                        " codh ='"+Objects.toString(khachhang.getCO(),"")+"'," +
                        " sync ='"+sync.toString()+"'" +
                        " WHERE " +
                        "  danhba ='"+khachhang.getDANHBA().toString()+"'" +
                        ";";
                if(database_sqLite.QueryDatabase(querry)) {
                    ok = true;
                }
            }

        }catch (Exception e){
            Log.e("Loi", e.toString());
        }
        return ok;
    }

    public boolean Update_Sync_DanhSachDocSo_KhachHang(String danhba){
        boolean ok = false;
        try{

                    String querry = "UPDATE " + Table_DanhSachHangHang +
                            " SET " +
                            " sync = '0'" +
                            " WHERE danhba='" + danhba + "'";
                   if( database_sqLite.QueryDatabase(querry)) {
                    ok = true;
                   }
        }catch (Exception e){
            Log.e(" Update Sync", e.toString());
        }
        return  ok;
    }
    public String Get_Sync_DanhSachDocSo_KhachHang(String danhba){
        String sync = "" ;
        try {

            String querry = "SELECT sync FROM "+Table_DanhSachHangHang +" WHERE danhba='" + danhba + "'";
            Cursor data_check = database_sqLite.GetDatabase(querry);
            while (data_check.moveToNext()) {
                sync= data_check.getString(0);
            }
        }catch (Exception e){

        }
        return   sync;
    }
    public String Get_Tieuthu_DanhSachDocSo_KhachHang(String danhba){
        String tieuthu = "" ;
        try {

            String querry = "SELECT tieuthu FROM "+Table_DanhSachHangHang +" WHERE danhba='" + danhba + "'";
            Cursor data_check = database_sqLite.GetDatabase(querry);
            while (data_check.moveToNext()) {
                tieuthu= data_check.getString(0);
            }
        }catch (Exception e){

        }
        return   tieuthu;
    }
    public boolean Update_TinhTrang_KH_DanhSachDocSo_KhachHang(String danhba,String tinhtrangkh){
        boolean ok = false;
        try{
            String querry = "UPDATE " + Table_DanhSachHangHang +
                    " SET " +
                    " tinhtrangkh = '"+tinhtrangkh+"'" +
                    " WHERE danhba='" + danhba + "'";
            if( database_sqLite.QueryDatabase(querry)) {
                ok = true;
            }
        }catch (Exception e){
            Log.e(" Update Sync", e.toString());
        }
        return  ok;
    }
    public boolean Update_SoNhaMoi_DanhSachDocSo_KhachHang(String danhba, String sonhamoi){
        boolean ok = false;
        try{

            String querry = "UPDATE " + Table_DanhSachHangHang +
                    " SET " +
                    " sonhamoi = '"+sonhamoi+"'" +
                    " WHERE danhba='" + danhba + "'";
            if( database_sqLite.QueryDatabase(querry)) {
                ok = true;
            }
        }catch (Exception e){
            Log.e(" Update Add New", e.toString());
        }
        return  ok;
    }
    public boolean Update_CongDung_DanhSachDocSo_KhachHang(String danhba, String congdung){
        boolean ok = false;
        try{

            String querry = "UPDATE " + Table_DanhSachHangHang +
                    " SET " +
                    " congdung = '"+congdung+"'" +
                    " WHERE danhba='" + danhba + "'";
            if( database_sqLite.QueryDatabase(querry)) {
                ok = true;
            }
        }catch (Exception e){
            Log.e(" Update Add New", e.toString());
        }
        return  ok;
    }
    public boolean Update_Chi_VTDH_DanhSachDocSo_KhachHang(String danhba, String chi,String vitrihd){
        boolean ok = false;
        try{

            String querry = "UPDATE " + Table_DanhSachHangHang +
                    " SET " +
                    " chi = '"+chi+"'," +
                    " vitridhn = '"+vitrihd+"'" +
                    " WHERE danhba='" + danhba + "'";
            if( database_sqLite.QueryDatabase(querry)) {
                ok = true;
            }
        }catch (Exception e){
            Log.e(" Update Add New", e.toString());
        }
        return  ok;
    }
    public boolean Update_Dienthoai_DanhSachDocSo_KhachHang(String danhba, String dienthoai1,String dienthoai2){
        boolean ok = false;
        try{

            String querry = "UPDATE " + Table_DanhSachHangHang +
                    " SET " +
                    " dienthoai1 = '"+dienthoai1+"'," +
                    " dienthoai2 = '"+dienthoai2+"'" +
                    " WHERE danhba='" + danhba + "'";
            if( database_sqLite.QueryDatabase(querry)) {
                ok = true;
            }
        }catch (Exception e){
            Log.e(" Update Add New", e.toString());
        }
        return  ok;
    }
    public boolean Update_Dienthoai1_DanhSachDocSo_KhachHang(String danhba, String dienthoai1){
        boolean ok = false;
        try{

            String querry = "UPDATE " + Table_DanhSachHangHang +
                    " SET " +
                    " dienthoai1 = '"+dienthoai1+"'" +
                    " WHERE danhba='" + danhba + "'";
            if( database_sqLite.QueryDatabase(querry)) {
                ok = true;
            }
        }catch (Exception e){
            Log.e(" Update Add New", e.toString());
        }
        return  ok;
    }
    public boolean Update_Dienthoai2_DanhSachDocSo_KhachHang(String danhba, String dienthoai2){
        boolean ok = false;
        try{

            String querry = "UPDATE " + Table_DanhSachHangHang +
                    " SET " +
                    " dienthoai2 = '"+dienthoai2+"'" +
                    " WHERE danhba='" + danhba + "'";
            if( database_sqLite.QueryDatabase(querry)) {
                ok = true;
            }
        }catch (Exception e){
            Log.e(" Update Add New", e.toString());
        }
        return  ok;
    }
    public boolean Update_Tinhtrang_Code_DanhSachDocSo_KhachHang(String danhba, String tinhtrang,String code){
        boolean ok = false;
        try{

            String querry = "UPDATE " + Table_DanhSachHangHang +
                    " SET " +
                    " tinhtrangmoi = '"+tinhtrang+"'," +
                    " codemoi = '"+code+"'" +
                    " WHERE danhba='" + danhba + "'";
            if( database_sqLite.QueryDatabase(querry)) {
                ok = true;
            }
        }catch (Exception e){
            Log.e(" Update Add New", e.toString());
        }
        return  ok;
    }
    public boolean Update_TinhtrangKhachHang_DanhSachDocSo_KhachHang(String danhba, String tinhtrangks){
        boolean ok = false;
        try{

            String querry = "UPDATE " + Table_DanhSachHangHang +
                    " SET " +
                    " tinhtrangkh = '"+tinhtrangks+"'" +
                    " WHERE danhba='" + danhba + "'";
            Log.e("querry",querry);
            if( database_sqLite.QueryDatabase(querry)) {
                ok = true;
            }
        }catch (Exception e){
            Log.e(" Update Add New", e.toString());
        }
        return  ok;
    }
    public boolean Update_ThayDH_DanhSachDocSo_KhachHang(String danhba, String csgo,String csgan,String ngaythay, String lydo){
        boolean ok = false;
        try{

            String querry = "UPDATE " + Table_DanhSachHangHang +
                    " SET " +
                    " csgo = '"+csgo+"'," +
                    " csgan = '"+csgan+"'," +
                    " ngaythaydh = '"+ngaythay+"'," +
                    " lydo = '"+lydo+"'" +
                    " WHERE danhba='" + danhba + "'";
            Log.e("querry",querry);
            if( database_sqLite.QueryDatabase(querry)) {
                ok = true;
            }
        }catch (Exception e){
            Log.e(" Update Add New", e.toString());
        }
        return  ok;
    }

    public boolean Update_CSM_TieuThu_DanhSachDocSo_KhachHang(String danhba, String csmoi,String tieuthu){
        boolean ok = false;
        try{

            String querry = "UPDATE " + Table_DanhSachHangHang +
                    " SET " +
                    " csmoi = '"+csmoi+"'," +
                    " tieuthu = '"+tieuthu+"'" +
                    " WHERE danhba='" + danhba + "'";
            if( database_sqLite.QueryDatabase(querry)) {
                ok = true;
            }
        }catch (Exception e){
            Log.e(" Update Add New", e.toString());
        }
        return  ok;
    }
    public boolean Update_Khachhang_DanhSachDocSo_KhachHang(String danhba, String khachhang){
        boolean ok = false;
        try{

            String querry = "UPDATE " + Table_DanhSachHangHang +
                    " SET " +
                    " khachhang = '"+khachhang+"'" +
                    " WHERE danhba='" + danhba + "'";
            if( database_sqLite.QueryDatabase(querry)) {
                ok = true;
            }
        }catch (Exception e){
            Log.e(" Update Add New", e.toString());
        }
        return  ok;
    }
    public boolean Update_Sync_DanhSachDocSo_KhachHangs(List<String> danhbas){
        boolean ok = false;
        try{
            for (int i =0 ; i < danhbas.size(); i++){
                try {
                    String danhba = danhbas.get(i);
                    String querry = "UPDATE " + Table_DanhSachHangHang +
                            " SET " +
                            " sync = '0'" +
                            " WHERE danhba='" + danhba + "'";
                    database_sqLite.QueryDatabase(querry);
                }catch (Exception e){

                }
            }
            ok = true;
        }catch (Exception e){
            Log.e(" Update Sync", e.toString());
        }
        return  ok;
    }
    public boolean Update_Sync_DanhSachDocSo_KhachHangs_CongDung(String danhba,String congdung){
        boolean ok = false;
        try{

                try {

                    String querry = "UPDATE " + Table_DanhSachHangHang +
                            " SET " +
                            " congdung = '"+congdung+"'" +
                            " WHERE danhba='" + danhba + "'";
                    database_sqLite.QueryDatabase(querry);
                }catch (Exception e){

                }
            ok = true;
        }catch (Exception e){
            Log.e(" Update Cong Dung", e.toString());
        }
        return  ok;
    }
    public boolean Update_GhiChu1_DanhSachDocSo_KhachHangs_CongDung(String danhba,String ghichu1){
        boolean ok = false;
        try{

            try {

                String querry = "UPDATE " + Table_DanhSachHangHang +
                        " SET " +
                        " ghichu1 = '"+ghichu1+"'" +
                        " WHERE danhba='" + danhba + "'";
                database_sqLite.QueryDatabase(querry);
            }catch (Exception e){

            }
            ok = true;
        }catch (Exception e){
            Log.e(" Update Cong Dung", e.toString());
        }
        return  ok;
    }
    public boolean Update_GhiChu2_DanhSachDocSo_KhachHangs_CongDung(String danhba,String ghichu2){
        boolean ok = false;
        try{

            try {

                String querry = "UPDATE " + Table_DanhSachHangHang +
                        " SET " +
                        " ghichu2 = '"+ghichu2+"'" +
                        " WHERE danhba='" + danhba + "'";
                database_sqLite.QueryDatabase(querry);
            }catch (Exception e){

            }
            ok = true;
        }catch (Exception e){
            Log.e(" Update Cong Dung", e.toString());
        }
        return  ok;
    }
    public boolean Delete_DanhSachDocSo_HackHang(String  danhba){
        boolean ok =false;
        try {
            String querry = "DELETE FROM "+Table_DanhSachHangHang+"  " +
                    " WHERE " +
                    "  danhba ='"+danhba+"'" +
                    " ;" ;
            if(database_sqLite.QueryDatabase(querry)) {
                ok = true;
            }
        }catch (Exception e){

        }
        return ok;
    }
    public boolean Delete_Table_DanhSachDocSo_HackHang(){
        boolean ok =false;
        try {
            String querry = "DROP TABLE IF EXISTS "+Table_DanhSachHangHang+"; ";

            if(database_sqLite.QueryDatabase(querry)) {
                ok = true;
                Log.e("D T DSDS_HackHang ","ok");
            }
        }catch (Exception e){

        }
        return ok;
    }
    public  List<String> Get_All_DanhBa(){
        List<String> danhbas = new ArrayList<>();
        try {

            String querry = "SELECT danhba FROM "+Table_DanhSachHangHang +" ;" ;
            Cursor data_check = database_sqLite.GetDatabase(querry);
            while (data_check.moveToNext()) {
                String danhba = data_check.getString(0);
                danhbas.add(danhba);

            }

        }catch (Exception e){

        }
        return danhbas ;
    }
    public  String Get_CongDung(String danhba){
        String congdung = "";
        try {

            String querry = "SELECT congdung FROM "+Table_DanhSachHangHang +" WHERE danhba ='"+danhba+"';" ;
            Cursor data_check = database_sqLite.GetDatabase(querry);
            while (data_check.moveToNext()) {
                congdung = data_check.getString(0);
            }

        }catch (Exception e){

        }
        return congdung ;
    }
    public  List<String> Get_TinhTrangThayDH(String danhba){
        List<String> tinhtrangthaydh = new ArrayList<>();
        try {

            String querry = "SELECT csgo,csgan,code,tinhtrang,lydo,ngaythaydh FROM "+Table_DanhSachHangHang +" WHERE danhba ='"+danhba+"';" ;
            Cursor data_check = database_sqLite.GetDatabase(querry);
            while (data_check.moveToNext()) {
                String csgo =  Objects.toString(data_check.getString(0),"");
                String csgan = Objects.toString(data_check.getString(1),"");
                String codecu = Objects.toString(data_check.getString(2),"");
                String tinhtrangcu = Objects.toString(data_check.getString(3),"");
                String lydo = Objects.toString(data_check.getString(4),"");
                String ngay_thaydh = Objects.toString(data_check.getString(5),"");
                tinhtrangthaydh.add(csgo);
                tinhtrangthaydh.add(csgan);
                tinhtrangthaydh.add(codecu);
                tinhtrangthaydh.add(tinhtrangcu);
                tinhtrangthaydh.add(lydo);
                tinhtrangthaydh.add(ngay_thaydh);
            }


        }catch (Exception e){

        }
        return tinhtrangthaydh ;
    }
    public List<data_syncghiso_send.data_khachhang> Get_KhachHangChua_Sync(){
        List<data_syncghiso_send.data_khachhang> khs_no_sync = new ArrayList<data_syncghiso_send.data_khachhang>();
        try{
            String querry = "SELECT " +
                    "danhba, " +
                    "hoten, " +
                    "sonha, " +
                    "sonhamoi, " +
                    "chi, " +
                    "vitridhn, " +
                    "dienthoai1, " +
                    "dienthoai2, " +
                    "code, " +
                    "csmoi, " +
                    "tinhtrang, " +
                    "tieuthu, " +
                    "ghichu1, " +
                    "ghichu2, " +
                    "lat," +
                    "lng," +
                    "congdung " +
                    "FROM "+Table_DanhSachHangHang +
                    " WHERE " +
                   " sync != '0' AND tieuthu !=''" +
                  //  " cast(tieuthu in int) > 0"+
                    ";";
            Cursor data_khs = database_sqLite.GetDatabase(querry);
            while (data_khs.moveToNext()) {
                String danhba = Objects.toString(data_khs.getString(0),"").trim();
                String hoten = Objects.toString(data_khs.getString(1),"").trim();
                String sonha = Objects.toString(data_khs.getString(2),"").trim();
                String sonhamoi = Objects.toString(data_khs.getString(3),"").trim();
                String chi = Objects.toString(data_khs.getString(4),"").trim();
                String vitridh = Objects.toString(data_khs.getString(5),"").trim();
                String dienthoai1 = Objects.toString(data_khs.getString(6),"").trim();
                String dienthoai2 = Objects.toString(data_khs.getString(7),"").trim();
                String code = Objects.toString(data_khs.getString(8),"").trim();
                String csmoi = Objects.toString(data_khs.getString(9),"").trim();
                String tinhtrang = Objects.toString(data_khs.getString(10),"").trim();
                String tieuthu = Objects.toString(data_khs.getString(11),"").trim();
                String ghichu1 = Objects.toString(data_khs.getString(12),"").trim();
                String ghichu2 = Objects.toString(data_khs.getString(13),"").trim();
                String lat = Objects.toString(data_khs.getString(14),"").trim();
                String lng = Objects.toString(data_khs.getString(15),"").trim();
                String congdung = Objects.toString(data_khs.getString(16),"").trim();
                data_syncghiso_send.data_khachhang khachhang = new data_syncghiso_send.data_khachhang(danhba,hoten,sonha,sonhamoi,chi,vitridh,dienthoai1,dienthoai2,code,csmoi,tinhtrang,tieuthu,ghichu1,ghichu2,lat,lng,congdung);
                khs_no_sync.add(khachhang);
            }
        }catch (Exception e){

        }
        return  khs_no_sync;
    }
    public data_syncghiso_send.data_khachhang Get_KhachHang_Send(String danhba){
        data_syncghiso_send.data_khachhang kh_send = new data_syncghiso_send.data_khachhang(
                "","","","","","","","","","","","","","","","","");
        try{
            String querry = "SELECT " +
                    "hoten, " +
                    "sonha, " +
                    "sonhamoi, " +
                    "chi, " +
                    "vitridhn, " +
                    "dienthoai1, " +
                    "dienthoai2, " +
                    "codemoi, " +
                    "csmoi, " +
                    "tinhtrangmoi, " +
                    "tieuthu, " +
                    "ghichu1, " +
                    "ghichu2, " +
                    "lat, " +
                    "lng, " +
                    "congdung " +
                    "FROM "+Table_DanhSachHangHang +
                    " WHERE " +
                    " danhba = '"+danhba+"'"+
                    //  " cast(tieuthu in int) > 0"+
                    ";";
            Cursor data_khs = database_sqLite.GetDatabase(querry);
            while (data_khs.moveToNext()) {

                String hoten = Objects.toString( data_khs.getString(0),"");
                String sonha = Objects.toString( data_khs.getString(1),"");
                String sonhamoi = Objects.toString( data_khs.getString(2),"");
                String chi = Objects.toString( data_khs.getString(3),"");
                String vitridh = Objects.toString( data_khs.getString(4),"");
                String dienthoai1 = Objects.toString( data_khs.getString(5),"");
                String dienthoai2 = Objects.toString( data_khs.getString(6),"");
                String codemoi = Objects.toString( data_khs.getString(7),"");
                String csmoi = Objects.toString( data_khs.getString(8),"");
                String tinhtrangmoi = Objects.toString( data_khs.getString(9),"");
                String tieuthu = Objects.toString( data_khs.getString(10),"");
                String ghichu1 = Objects.toString( data_khs.getString(11),"");
                String ghichu2 = Objects.toString( data_khs.getString(12),"");
                String lat = Objects.toString( data_khs.getString(13),"");
                String lng = Objects.toString( data_khs.getString(14),"");
                String congdung = Objects.toString( data_khs.getString(15),"");
                kh_send = new data_syncghiso_send.data_khachhang(danhba,hoten,sonha,sonhamoi,chi,vitridh,dienthoai1,dienthoai2,codemoi,csmoi,tinhtrangmoi,tieuthu,ghichu1,ghichu2,lat,lng,congdung);

            }
        }catch (Exception e){

        }
        return  kh_send;
    }
    public List<String> Get_KhachHang_NAMKYDOT(String danhba){
        List<String> namkydot =new ArrayList<>();
      try{
            String querry = "SELECT " +
                    "nam, " +
                    "ky, " +
                    "dot, " +
                    "somay " +
                    "FROM "+Table_DanhSachHangHang +
                    " WHERE " +
                    " danhba = '"+danhba+"'"+
                    //  " cast(tieuthu in int) > 0"+
                    ";";
            Cursor data_khs = database_sqLite.GetDatabase(querry);
            while (data_khs.moveToNext()) {

                String nam = Objects.toString( data_khs.getString(0),"");
                String ky = Objects.toString( data_khs.getString(1),"");
                String dot = Objects.toString( data_khs.getString(2),"");
                String somay = Objects.toString( data_khs.getString(3),"");
                namkydot.add(nam);
                namkydot.add(ky);
                namkydot.add(dot);
                namkydot.add(somay);
            }
        }catch (Exception e){

        }
        return  namkydot;
    }


    public  List<String> Get_All_MaST(){
        List<String> mlts = new ArrayList<>();
        try {

            String querry = "SELECT mlt FROM "+Table_DanhSachHangHang +" ;" ;
            Cursor data_check = database_sqLite.GetDatabase(querry);
            while (data_check.moveToNext()) {
                String mlt = data_check.getString(0);
                mlts.add(mlt);

            }

        }catch (Exception e){

        }
        return mlts ;
    }
    public List<data_Danhsachdocso_chitiet> Get_DanhSachDocSo_Chitiet(){
        List<data_Danhsachdocso_chitiet> data_danhsachdocso_chitiets = new ArrayList<>();
        try {

            String querry = "SELECT id ,danhba, sothan, hoten , sonha ,duong , sync,tieuthu, mlt  FROM "+Table_DanhSachHangHang +" ORDER BY CAST(mlt AS INTEGER)  ASC;" ;
            Cursor data_check = database_sqLite.GetDatabase(querry);
            while (data_check.moveToNext()) {
                String stt = data_check.getString(0).trim();
                String danhba = data_check.getString(1).trim();
                String st = data_check.getString(2).trim();
                String hoten = data_check.getString(3).trim();
                String sonha = data_check.getString(4).trim();
                String duong = data_check.getString(5).trim();
                String sync = data_check.getString(6).trim();
                String tieuthu = data_check.getString(7).trim();
                String mlt = data_check.getString(8).trim();
                String diachi = sonha +" - "+duong ;
                int id = Integer.parseInt(stt);
                int donbo =2;
                try {
                    donbo = Integer.parseInt(sync);
                }catch (Exception e){

                }
                data_Danhsachdocso_chitiet data_danhsachdocso_chitiet =  new data_Danhsachdocso_chitiet(id,danhba,st,donbo,hoten,diachi,tieuthu, mlt);
                data_danhsachdocso_chitiets.add(data_danhsachdocso_chitiet);

            }

        }catch (Exception e){

        }
        return data_danhsachdocso_chitiets ;
    }
    public String Get_DanhSachDocSo_Denngay(String danhba){
        String denngay ="";
        try {
            String querry = "SELECT denngay   FROM "+Table_DanhSachHangHang +
                    " WHERE " +
                    " danhba = '"+danhba+"'";
            Cursor data_check = database_sqLite.GetDatabase(querry);
            while (data_check.moveToNext()) {
                denngay = Objects.toString(data_check.getString(0),"");
            }

        }catch (Exception e){

        }
        return denngay ;
    }

    public String Get_DanhSachDocSo_TinhTrangKH(String danhba){
        String tinhtrangkh ="";
        try {
            String querry = "SELECT tinhtrangkh   FROM "+Table_DanhSachHangHang +
                    " WHERE " +
                    " danhba = '"+danhba+"'";
            Cursor data_check = database_sqLite.GetDatabase(querry);
            while (data_check.moveToNext()) {
                tinhtrangkh = Objects.toString(data_check.getString(0),"");
            }

        }catch (Exception e){

        }
        return tinhtrangkh ;
    }
    public List<data_Danhsachbatthuong> Get_DanhSachDocSo_Chitiet_ChuaghiSo(){
        List<data_Danhsachbatthuong> data_danhsachdocso_chitiets_chuaghiso = new ArrayList<>();
        try {
            //String querry = "SELECT id ,danhba, sothan, hoten , sonha ,duong , sync  FROM "+Table_DanhSachHangHang +" ORDER BY mlt ASC;" ;
            String querry = "SELECT id ,danhba, sothan, hoten , sonha ,duong , sync,tinhtrangmoi,cscu,csmoi,tieuthu,binhquan,tinhtrang" +
                    "  FROM "+Table_DanhSachHangHang +
                    " ORDER BY CAST(mlt AS INTEGER) ASC;" ;
            Cursor data_check = database_sqLite.GetDatabase(querry);
            while (data_check.moveToNext()) {
                String stt = data_check.getString(0).trim();
                String danhba = Objects.toString(data_check.getString(1),"");
                String st = Objects.toString(data_check.getString(2),"");
                String hoten = Objects.toString(data_check.getString(3),"");
                String sonha = Objects.toString(data_check.getString(4),"");
                String duong = Objects.toString(data_check.getString(5),"");
                String sync = Objects.toString(data_check.getString(6),"");
                String tinhtrangmoi = Objects.toString(data_check.getString(7),"");
                String csc =  Objects.toString(data_check.getString(8),"");
                String csm = Objects.toString(data_check.getString(9),"");
                String tieuthu = Objects.toString(data_check.getString(10),"");
                String binhquan = Objects.toString(data_check.getString(11),"");
                String tinhtrangcu = Objects.toString(data_check.getString(12),"");
                String diachi = sonha +" - "+duong ;
                int id = Integer.parseInt(stt);
                int donbo =2;
                try {
                    donbo = Integer.parseInt(sync);
                }catch (Exception e){

                }
                data_Danhsachbatthuong data_danhsachdocso_chitiet_bathuong =  new data_Danhsachbatthuong(id,danhba,st,donbo,hoten,diachi,tinhtrangmoi,"0",csc,csm,tieuthu,binhquan,tinhtrangcu);
                data_danhsachdocso_chitiets_chuaghiso.add(data_danhsachdocso_chitiet_bathuong);
            }

        }catch (Exception e){
            String a =e.toString();
        }
        return data_danhsachdocso_chitiets_chuaghiso ;
    }
    public List<data_Danhsachbatthuong> Get_DanhSachDocSo_Chitiet_ChuaChupAnh(){
        List<data_Danhsachbatthuong> data_danhsachdocso_chitiets_chuaghiso = new ArrayList<>();
        try {
            //String querry = "SELECT id ,danhba, sothan, hoten , sonha ,duong , sync  FROM "+Table_DanhSachHangHang +" ORDER BY mlt ASC;" ;
            String querry = "SELECT A.id ,A.danhba, A.sothan, A.hoten , A.sonha ,duong , sync,tinhtrangmoi,cscu,csmoi,tieuthu,binhquan,tinhtrang" +
                    "  FROM "+Table_DanhSachHangHang +" AS A"+
                    " WHERE tieuthu IS NULL OR tieuthu ='' "+
                    " ORDER BY mlt ASC;" ;
            Cursor data_check = database_sqLite.GetDatabase(querry);
            while (data_check.moveToNext()) {
                String stt = data_check.getString(0).trim();
                String danhba = Objects.toString(data_check.getString(1),"");
                String st = Objects.toString(data_check.getString(2),"");
                String hoten = Objects.toString(data_check.getString(3),"");
                String sonha = Objects.toString(data_check.getString(4),"");
                String duong = Objects.toString(data_check.getString(5),"");
                String sync = Objects.toString(data_check.getString(6),"");
                String tinhtrangmoi = Objects.toString(data_check.getString(7),"");
                String csc =  Objects.toString(data_check.getString(8),"");
                String csm = Objects.toString(data_check.getString(9),"");
                String tieuthu = Objects.toString(data_check.getString(10),"");
                String binhquan = Objects.toString(data_check.getString(11),"");
                String tinhtrangcu = Objects.toString(data_check.getString(11),"");
                String diachi = sonha +" - "+duong ;
                int id = Integer.parseInt(stt);
                int donbo =2;
                try {
                    donbo = Integer.parseInt(sync);
                }catch (Exception e){

                }
                data_Danhsachbatthuong data_danhsachdocso_chitiet_bathuong =  new data_Danhsachbatthuong(id,danhba,st,donbo,hoten,diachi,tinhtrangmoi,"0",csc,csm,tieuthu,binhquan,tinhtrangcu);
                data_danhsachdocso_chitiets_chuaghiso.add(data_danhsachdocso_chitiet_bathuong);
            }

        }catch (Exception e){
            String a =e.toString();
        }
        return data_danhsachdocso_chitiets_chuaghiso ;
    }
    public boolean Update_tieuthu_khachhang(String danhba , String tieuthu){
        boolean ok = false;
        try{

            try {

                String querry = "UPDATE " + Table_DanhSachHangHang +
                        " SET " +
                        " tieuthu = '"+tieuthu+"'" +
                        " WHERE danhba='" + danhba + "'";
                database_sqLite.QueryDatabase(querry);
            }catch (Exception e){

            }
            ok = true;
        }catch (Exception e){
            Log.e(" Update TieuThu", e.toString());
        }
        return  ok;
    }
    public boolean Update_lat_long_khachhang(String danhba , String lat, String lng){
        boolean ok = false;
        try{

            try {

                String querry = "UPDATE " + Table_DanhSachHangHang +
                        " SET " +
                        "lat ='"+lat+"', " +
                        "lng = '"+lng+"'   " +
                        " WHERE danhba='" + danhba + "'";
                database_sqLite.QueryDatabase(querry);
            }catch (Exception e){

            }
            ok = true;
        }catch (Exception e){
            Log.e(" Update TieuThu", e.toString());
        }
        return  ok;
    }

    public data_danhsachdocso_chitiet_khachhang Get_DanhSachDocSo_Chitiet_KhachHang(String danhba_){
        data_danhsachdocso_chitiet_khachhang khachhang =new data_danhsachdocso_chitiet_khachhang();
        khachhang.setId(0);
        try {

            String querry = "SELECT id ," +
                    " danhba," +
                    " mlt," +
                    " hoten ," +
                    " sonha ," +
                    " duong , " +
                    " sync," +
                    " sothan," +
                    " giabieu," +
                    " dinhmuc," +
                    " dienthoai1," +
                    " dienthoai2," +
                    " code," +
                    " cscu," +
                    " csmoi," +
                    " tieuthu," +
                    " binhquan," +
                    " tungay," +
                    " denngay," +
                    " dinhmucngheo," +
                    " tiledv," +
                    " tilehcsn," +
                    " tilesh, "+
                    " tilesx, " +
                    " congdung, "+
                    " vitridhn, " +
                    " tinhtrang," +
                    " sonhamoi," +
                    " machigoc," +
                    " ghichu1," +
                    " ghichu2," +
                    " tinhtrangkh," +
                    " hieudh,"+
                    " namdhn,"+
                    " codh,"+
                    " chi,"+
                    " codemoi," +
                    " tinhtrangmoi," +
                    " csgo," +
                    " csgan," +
                    " ngaythaydh," +
                    " lydo," +
                    " tieuthucu," +
                    " khongtinhphi_bvmt" +
                    " FROM "+Table_DanhSachHangHang +
                    " WHERE danhba ='"+danhba_+"';" ;

            Cursor data_check = database_sqLite.GetDatabase(querry);
            while (data_check.moveToNext()) {
                String stt = data_check.getString(0).trim();
                String danhba = data_check.getString(1).trim();
                String st = data_check.getString(2).trim();
                String hoten = data_check.getString(3).trim();
                String sonha = data_check.getString(4).trim();
                String duong = data_check.getString(5).trim();
                String sync = data_check.getString(6).trim();
                String sothan = data_check.getString(7).trim();
                String giabieu = data_check.getString(8).trim();
                String dinhmuc = data_check.getString(9).trim();
                String dienthoai1 = data_check.getString(10).trim();
                String dienthoai2 = data_check.getString(11).trim();
                String code = data_check.getString(12).trim();
                String cscu = data_check.getString(13).trim();
                String csmoi = data_check.getString(14).trim();
                String tieuthu = data_check.getString(15).trim();
                String binhquan = data_check.getString(16).trim();
                String tungay = data_check.getString(17).trim();
                String denngay = data_check.getString(18).trim();
                String dinhmucngheo = data_check.getString(19).trim();
                String tiledv = data_check.getString(20).trim();
                String tilehcsn = data_check.getString(21).trim();
                String tilesh = data_check.getString(22).trim();
                String tilesx  = data_check.getString(23).trim();
                String congdung = data_check.getString(24).trim();
                String vitri_dh = data_check.getString(25).trim();
                String tinhtrang = data_check.getString(26).trim();
                String sonha_moi = data_check.getString(27).trim();
                String machigoc =data_check.getString(28).trim();
                String ghichu1 = Objects.toString(data_check.getString(29),"");
                String ghichu2 =Objects.toString(data_check.getString(30),"");
                String tinhtrang_kh = Objects.toString(data_check.getString(31),"");
                String hieu_dh = data_check.getString(32);
                String nam_dh = data_check.getString(33);
                String co_dh = data_check.getString(34);
                String chi  = data_check.getString(35);
                String codemoi  = Objects.toString(data_check.getString(36),"");
                String tinhtrangmoi  = Objects.toString(data_check.getString(37),"");
                String csgo  = Objects.toString(data_check.getString(38),"");
                String csgan  = Objects.toString(data_check.getString(39),"");
                String ngaythaydh  = Objects.toString(data_check.getString(40),"");
                String lydo = Objects.toString(data_check.getString(41),"");
                String tieuthucu = Objects.toString(data_check.getString(42),"");
                String khongtinhphi_bvmt = Objects.toString(data_check.getString(43),"");
                int id = Integer.parseInt(stt);

                khachhang.setId(id);
                khachhang.setDanhba(danhba);
                khachhang.setSt(st);
                khachhang.setHoten(hoten);
                khachhang.setSonha(sonha);
                khachhang.setDuong(duong);
                khachhang.setSync(sync);
                khachhang.setSothan(sothan);
                khachhang.setGiabieu(giabieu);
                khachhang.setDinhmuc(dinhmuc);
                khachhang.setDienthoai1(dienthoai1);
                khachhang.setDienthoai2(dienthoai2);
                khachhang.setCode(code);
                khachhang.setCscu(cscu);
                khachhang.setCsmoi(csmoi);
                khachhang.setTieuthu(tieuthu);
                khachhang.setBinhquan(binhquan);
                khachhang.setTungay(tungay);
                khachhang.setDenngay(denngay);
                khachhang.setDinhmucngheo(dinhmucngheo);
                khachhang.setTiledv(tiledv);
                khachhang.setTilehcsn(tilehcsn);
                khachhang.setTilesh(tilesh);
                khachhang.setTilesx(tilesx);
                khachhang.setCongdung(congdung);
                khachhang.setVitri_dh(vitri_dh);
                khachhang.setTinhtrang_dh(tinhtrang);
                khachhang.setSonha_moi(sonha_moi);
                khachhang.setMachigoc(machigoc);
                khachhang.setGhichu1(ghichu1);
                khachhang.setGhichu2(ghichu2);
                khachhang.setTinhtrangks(tinhtrang_kh);
                khachhang.setHieudh(hieu_dh);
                khachhang.setNam_dh(nam_dh);
                khachhang.setCo_dh(co_dh);
                khachhang.setChi(chi);
                khachhang.setCodemoi(codemoi);
                khachhang.setTinhtrangmoi(tinhtrangmoi);
                khachhang.setCsgo(csgo);
                khachhang.setCsgan(csgan);
                khachhang.setNgaythaydh(ngaythaydh);
                khachhang.setLydo(lydo);
                khachhang.setTieuthucu(tieuthucu);
                khachhang.setKhongtinhphi_bvmt(khongtinhphi_bvmt);
            }

        }catch (Exception e){

        }
        return khachhang ;
    }
    public String  Get_DanhSachDocSo_Chitiet_KhachHang_sothan(String sothan_){
        String danhba ="";
        try {

            String querry = "SELECT " +
                    " danhba" +
                    " FROM "+Table_DanhSachHangHang +
                    " WHERE sothan ='"+sothan_+"' ;" ;

            Cursor data_check = database_sqLite.GetDatabase(querry);
            while (data_check.moveToNext()) {

                danhba = data_check.getString(0).trim();

            }

        }catch (Exception e){

        }
        return danhba ;
    }
    public List<String>  Get_DanhSachDocSo_Chitiet_KhachHang_listsothan(){
       List<String> list_sothan = new ArrayList<>();
        try {

            String querry = "SELECT " +
                    " sothan" +
                    " FROM "+Table_DanhSachHangHang ;

            Cursor data_check = database_sqLite.GetDatabase(querry);
            while (data_check.moveToNext()) {

               String  sothan = data_check.getString(0).trim();
               list_sothan.add(sothan);

            }

        }catch (Exception e){

        }
        return list_sothan ;
    }
    public List<String>  Get_DanhSachDocSo_Chitiet_KhachHang_listdanhba(){
        List<String> list_danhba = new ArrayList<>();
        try {

            String querry = "SELECT " +
                    " danhba" +
                    " FROM "+Table_DanhSachHangHang ;

            Cursor data_check = database_sqLite.GetDatabase(querry);
            while (data_check.moveToNext()) {

                String  danhba = data_check.getString(0).trim();
                list_danhba.add(danhba);

            }

        }catch (Exception e){

        }
        return list_danhba ;
    }

    public String Get_DanhSachDocSo_Chitiet_KhachHang_Next(String mast,String querry_tinhtrang,String danhba){

        try {
            String querry ="";
            if(!mast.isEmpty()) {
                if (!querry_tinhtrang.isEmpty()) {
                    querry = "SELECT " +
                            " danhba" +
                            " FROM " + Table_DanhSachHangHang +
                            " WHERE CAST(mlt AS INTEGER) > " + mast + " AND " + querry_tinhtrang +
                            " ORDER BY CAST(mlt AS INTEGER) ASC LIMIT 1;";
                } else {
                    querry = "SELECT " +
                            " danhba" +
                            " FROM " + Table_DanhSachHangHang +
                            " WHERE CAST(mlt AS INTEGER) > " + mast + " " +
                            " ORDER BY CAST(mlt AS INTEGER) ASC LIMIT 1;";
                }
            }else {
                querry = "SELECT " +
                        " danhba" +
                        " FROM " + Table_DanhSachHangHang +
                        " WHERE danhba > '" + danhba + "' AND mlt =''"  +
                        " ORDER BY danhba ASC LIMIT 1;";
            }
            danhba = "";
            Cursor data_check = database_sqLite.GetDatabase(querry);
            while (data_check.moveToNext()) {
                danhba = data_check.getString(0).trim();

            }


        }catch (Exception e){

        }
        return danhba ;
    }
    public String Get_DanhSachDocSo_Chitiet_KhachHang_NextMax(String querry_tinhtrang){
        String danhba ="";
        try {
            String querry ="";
            if(!querry_tinhtrang.isEmpty()) {
                querry = "SELECT " +
                        " danhba" +
                        " FROM " + Table_DanhSachHangHang +
                        " WHERE " + querry_tinhtrang +
                        " ORDER BY CAST(mlt AS INTEGER) ASC LIMIT 1;";
            }else {
                querry = "SELECT " +
                        " danhba" +
                        " FROM " + Table_DanhSachHangHang +
                        " " +
                        " ORDER BY CAST(mlt AS INTEGER) ASC LIMIT 1;";
            }

            Cursor data_check = database_sqLite.GetDatabase(querry);
            while (data_check.moveToNext()) {
                danhba = data_check.getString(0).trim();

            }

        }catch (Exception e){

        }
        return danhba ;
    }
    public String Get_DanhSachDocSo_Chitiet_KhachHang_Back(String mast,String querry_tinhtrang,String danhba){

        try {
            String querry ="";
            if(!mast.isEmpty()) {
                if (!querry_tinhtrang.isEmpty()) {
                    querry = "SELECT " +
                            " danhba" +
                            " FROM " + Table_DanhSachHangHang +
                            " WHERE CAST(mlt AS INTEGER) < " + mast + " AND " + querry_tinhtrang +
                            " ORDER BY CAST(mlt AS INTEGER) DESC LIMIT 1;";
                } else {
                    querry = "SELECT " +
                            " danhba" +
                            " FROM " + Table_DanhSachHangHang +
                            " WHERE CAST(mlt AS INTEGER) < " + mast + " " +
                            " ORDER BY CAST(mlt AS INTEGER) DESC LIMIT 1;";
                }
            }else {
                querry = "SELECT " +
                        " danhba" +
                        " FROM " + Table_DanhSachHangHang +
                        " WHERE danhba < '" + danhba + "' AND ( mlt IS NULL OR  mlt ='')  " +
                        " ORDER BY danhba  DESC LIMIT 1;";
            }

            danhba ="";
            Cursor data_check = database_sqLite.GetDatabase(querry);
            while (data_check.moveToNext()) {
                danhba = data_check.getString(0).trim();

            }

        }catch (Exception e){

        }
        return danhba ;
    }
    public String Get_DanhSachDocSo_Chitiet_KhachHang_BackMax(String querry_tinhtrang){
        String danhba ="";
        try {
            String querry ="";
            if(!querry_tinhtrang.isEmpty()) {
                querry = "SELECT " +
                        " danhba" +
                        " FROM " + Table_DanhSachHangHang +
                        " WHERE " + querry_tinhtrang +
                        " ORDER BY CAST(mlt AS INTEGER) DESC LIMIT 1;";
            }else {
                querry = "SELECT " +
                        " danhba" +
                        " FROM " + Table_DanhSachHangHang +
                        "" +
                        " ORDER BY CAST(mlt AS INTEGER) DESC LIMIT 1;";
            }

            Cursor data_check = database_sqLite.GetDatabase(querry);
            while (data_check.moveToNext()) {
                danhba = data_check.getString(0).trim();

            }

        }catch (Exception e){

        }
        return danhba ;
    }
    public int Get_Sum_DH(){
        int sum_dh = 0 ;
        try {

            String querry = "SELECT COUNT(id) FROM "+Table_DanhSachHangHang +";" ;
            Cursor data_check = database_sqLite.GetDatabase(querry);
            while (data_check.moveToNext()) {
                sum_dh= data_check.getInt(0);
            }

        }catch (Exception e){

        }
        return   sum_dh;
    }
    public int Get_Sum_Sync(){
        int sum_sync = 0 ;
        try {

            String querry = "SELECT COUNT(id) FROM "+Table_DanhSachHangHang +"WHERE sync ='0';" ;
            Cursor data_check = database_sqLite.GetDatabase(querry);
            while (data_check.moveToNext()) {
                sum_sync= data_check.getInt(0);
            }

        }catch (Exception e){

        }
        return   sum_sync;
    }
    public int Get_Sum_DaDoc(){
        int dadoc = 0 ;
        try {

            String querry = "SELECT COUNT(id) FROM "+Table_DanhSachHangHang +"  WHERE tieuthu IS NOT NULL AND tieuthu !='';" ;
            Cursor data_check = database_sqLite.GetDatabase(querry);
            while (data_check.moveToNext()) {
               dadoc=  dadoc= data_check.getInt(0);
            }

        }catch (Exception e){

        }
        return   dadoc;
    }
    public int Get_Sum_SanLuong(){
        int sum_sanluong = 0 ;
        try {

            String querry = "SELECT SUM(CAST(tieuthu as int)) FROM "+Table_DanhSachHangHang +"  WHERE tieuthu IS NOT NULL AND tieuthu !='';" ;
            Cursor data_check = database_sqLite.GetDatabase(querry);
            while (data_check.moveToNext()) {
                sum_sanluong= data_check.getInt(0);
            }

        }catch (Exception e){

        }
        return   sum_sanluong;
    }

/*
    public  data_DanhSachDocSo Get_DanhSachDocSo_KhachHang( ){

        try {
            String querry = "SELECT  FROM "+Table_DanhSachHangHang +
                    " WHERE " +
                    "  nam ='"+data_danhsachdocso.getNam().toString()+"'" +
                    "  AND ky ='"+data_danhsachdocso.getKy().toString()+"'" +
                    "  AND dot ='"+data_danhsachdocso.getDot().toString()+"'" +
                    "  AND somay ='"+data_danhsachdocso.getSomay().toString()+"' ;" ;
            Cursor data_check = database_sqLite.GetDatabase(querry);
            while (data_check.moveToNext()) {
                String id = data_check.getString(0);
                data_danhsachdocso.setId(Integer.parseInt(id));

                String sum_dh = data_check.getString(5);
                String sum_dadoc = data_check.getString(6);
                String sum_sanluong = data_check.getString(7);

                data_danhsachdocso.setSum_dh(sum_dh);
                data_danhsachdocso.setDadoc(sum_dadoc);
                data_danhsachdocso.setSanluong(sum_sanluong);

            }

        }catch (Exception e){

        }
        return data_danhsachdocso ;
    }
    public List<data_DanhSachDocSo> Get_All_DanhSachDocSo(){
        List< data_DanhSachDocSo > data_all_danhsachdocso = new ArrayList<>();
        try {
            String querry = "SELECT * FROM "+Table_DanhSachHangHang + ";";

            Cursor data_check = database_sqLite.GetDatabase(querry);
            while (data_check.moveToNext()) {
                data_DanhSachDocSo  data_danhsachdocso = new data_DanhSachDocSo();

                String id = data_check.getString(0);

                String nam = data_check.getString(1);
                String ky = data_check.getString(2);
                String dot = data_check.getString(3);
                String somay = data_check.getString(4);

                String sum_dh = data_check.getString(5);
                String sum_dadoc = data_check.getString(6);
                String sum_sanluong = data_check.getString(7);

                data_danhsachdocso.setId(Integer.parseInt(id));
                data_danhsachdocso.setNam(nam);
                data_danhsachdocso.setKy(ky);
                data_danhsachdocso.setDot(dot);
                data_danhsachdocso.setSomay(somay);
                data_danhsachdocso.setSum_dh(sum_dh);
                data_danhsachdocso.setDadoc(sum_dadoc);
                data_danhsachdocso.setSanluong(sum_sanluong);
                data_all_danhsachdocso.add(data_danhsachdocso);

            }

        }catch (Exception e){

        }
        return data_all_danhsachdocso ;
    }

 */
}

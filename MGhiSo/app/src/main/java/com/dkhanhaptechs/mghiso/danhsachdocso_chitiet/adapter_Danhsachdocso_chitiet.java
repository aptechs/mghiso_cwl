package com.dkhanhaptechs.mghiso.danhsachdocso_chitiet;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.dkhanhaptechs.mghiso.R;


import java.util.ArrayList;
import java.util.List;

class adapter_Danhsachdocso_chitiet extends BaseAdapter {
    private Context Context_ ;
    private int Layout_ ;
    private List<data_Danhsachdocso_chitiet> List_data_danhsachdocsos__chitiet_ ;
    private ArrayList<data_Danhsachdocso_chitiet> arraylist;
    public adapter_Danhsachdocso_chitiet(Context context, int layout, List<data_Danhsachdocso_chitiet> list_data_danhsachdocso_chitiet){
        this.Context_ = context;
        this.Layout_ = layout;
        this.List_data_danhsachdocsos__chitiet_ = list_data_danhsachdocso_chitiet;
        this.arraylist = new ArrayList<data_Danhsachdocso_chitiet>();
        this.arraylist.addAll(list_data_danhsachdocso_chitiet);
    }


    @Override
    public int getCount() {
        return List_data_danhsachdocsos__chitiet_.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
    private  class  Viewholder_data_danhsachdocso_chitiet{
        TextView tv_danhba ;
        TextView tv_so_st ;
        ImageView imv_dongbo ;
        TextView tv_hoten ;
        TextView tv_diachi;
        TextView tv_mlt;

    }
    @Override
    public View getView(int position,View convertView,ViewGroup parent) {
        adapter_Danhsachdocso_chitiet.Viewholder_data_danhsachdocso_chitiet viewholder ;
        if(convertView == null){
            viewholder = new adapter_Danhsachdocso_chitiet.Viewholder_data_danhsachdocso_chitiet();
            LayoutInflater inflater = (LayoutInflater) Context_.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(Layout_,null);
            // Map
            viewholder.tv_danhba = (TextView) convertView.findViewById(R.id.textview_danhba_dongdanhsachdocso_chitiet);
            viewholder.tv_so_st = (TextView) convertView.findViewById(R.id.textview_sost_dongdanhsachdocso_chitiet);
            viewholder.tv_mlt = (TextView) convertView.findViewById(R.id.textview_mlt_dongdanhsachdocso_chitiet);
            viewholder.imv_dongbo =(ImageView) convertView.findViewById(R.id.imageView_dongbo_dongdanhsachdocso_chitiet);
            viewholder.tv_hoten = (TextView) convertView.findViewById(R.id.textview_hoten_dongdanhsachdocso_chitiet);
            viewholder.tv_diachi = (TextView) convertView.findViewById(R.id.textview_diachi_dongdanhsachdocso_chitiet);
            //////////
            convertView.setTag(viewholder);
        }else {
            viewholder = (adapter_Danhsachdocso_chitiet.Viewholder_data_danhsachdocso_chitiet) convertView.getTag();
        }
        //
        viewholder.imv_dongbo.setFocusable(false);
        viewholder.imv_dongbo.setFocusableInTouchMode(false);
        // GET DATA
        data_Danhsachdocso_chitiet data_danhsachdocso_chitiet = List_data_danhsachdocsos__chitiet_.get(position);
        // SET DATA
        viewholder.tv_danhba.setText(data_danhsachdocso_chitiet.getDanhba());
        viewholder.tv_so_st.setText(data_danhsachdocso_chitiet.getSo_st());
        viewholder.tv_mlt.setText(data_danhsachdocso_chitiet.getMlt());
        int dongbo = data_danhsachdocso_chitiet.getDongbo();
        if(dongbo == 2) {
            if (!data_danhsachdocso_chitiet.getTieuthu().trim().isEmpty()){
                dongbo =1;
            }
        }else if(dongbo ==3) {
            dongbo = 0;
        }
        switch(dongbo) {
            case 2:{
                // timeout
                viewholder.imv_dongbo.setImageResource(R.drawable.synced_2);
                break;
            }
            case 1:{
                // dang do bo
                viewholder.imv_dongbo.setImageResource(R.drawable.synced_1);
                break;
            }
            case 0:{
                // da dong bo
                viewholder.imv_dongbo.setImageResource(R.drawable.synced_0);
                break;
            }
        }
        viewholder.tv_hoten.setText(data_danhsachdocso_chitiet.getHoten());
        viewholder.tv_diachi.setText(data_danhsachdocso_chitiet.getDiaChi());

        return convertView;
    }

}

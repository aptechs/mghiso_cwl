package com.dkhanhaptechs.mghiso.congdung;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.CountDownTimer;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.dkhanhaptechs.mghiso.R;
import com.dkhanhaptechs.mghiso.Share.Config_CongDung.sqlite_Config_CongDung;
import com.dkhanhaptechs.mghiso.Share.Dialog_App;
import com.dkhanhaptechs.mghiso.Share.HinhAnh.SendPicture_AsyncTask;
import com.dkhanhaptechs.mghiso.Share.HinhAnh.data_Hinhanh;
import com.dkhanhaptechs.mghiso.Share.HinhAnh.sqlite_Hinhanh;
import com.dkhanhaptechs.mghiso.chuphinh.*;
import com.dkhanhaptechs.mghiso.Share.DanhSach_DocSo.*;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;


public class activity_Congdung extends AppCompatActivity {

    private TextView tv_danhba, tv_sost, tv_hoten,tv_diachi, tv_congdungserver;
    private ImageView imv_dongbo;
    private Toolbar toolbar ;
    private String Json_table_ghiso_chitiet = "";
    private Button btn_camera, btn_xoaanh, btn_upload , btn_luu;
    private ListView lv_anhs;
    private Spinner spn_doituong, spn_congdung;
    private EditText edt_noidung;
    private static final int REQUEST_ID_IMAGE_CAPTURE = 100;

    private String  So_st, Hoten, Diachi;
    private String Danhba, Nam, Ky, Dot, SoMay;
    private int Dongbo;
    private Bitmap bitmap_anhsave;

    private ArrayList<data_Chupanh> Arraylist_data_chupanh;
    private adapter_Chupanh Adapter_chupanh;

    private ArrayList<String> array_doituong ;
    private ArrayList<String> array_congdung ;
    private ArrayAdapter adapter_doituong;
    private ArrayAdapter adapter_congdung;
    private int Doituong_Current = 0;
    private int Congdung_Current = 0;
    private String CongDung_Server ="";
    private static final int DISABLE_GHISO_CHUADENGIO = 1;
    private static final int DISABLE_GHISO_QUAGIO = 2;
    private static final int DISABLE_KHONGCODENNGAY = 3;
    private int Enable_Ghiso= 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_congdung);

        Mappping_Layout();
        // GET DATA
        Json_table_ghiso_chitiet = getIntent().getStringExtra("CONGDUNG");
        // SET DATA
        if (Get_Table_GhiSo_Chitiet(Json_table_ghiso_chitiet)) {
            tv_danhba.setText(Danhba);
            tv_sost.setText(So_st);
            tv_hoten.setText(Hoten);
            tv_diachi.setText(Diachi);
            tv_congdungserver.setText(CongDung_Server);
            String [] com_doituong_congdung = CongDung_Server.split("-");
            if(com_doituong_congdung[0].trim().equals("SH")){
                Doituong_Current = 1;
            }
            switch (Dongbo) {
                case 2: {
                    // timeout
                    imv_dongbo.setImageResource(R.drawable.synced_2);
                    break;
                }
                case 1: {
                    // dang do bo
                    imv_dongbo.setImageResource(R.drawable.synced_1);
                    break;
                }
                case 0: {
                    // da dong bo
                    imv_dongbo.setImageResource(R.drawable.synced_0);
                    break;
                }
            }
        }
        array_doituong = new ArrayList<String>();
        adapter_doituong = new ArrayAdapter(activity_Congdung.this, android.R.layout.simple_spinner_item,array_doituong);
        adapter_doituong.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
        spn_doituong.setAdapter(adapter_doituong);

        array_congdung = new ArrayList<String>();
        adapter_congdung = new ArrayAdapter(activity_Congdung.this, android.R.layout.simple_spinner_item,array_congdung);
        adapter_congdung.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
        spn_congdung.setAdapter(adapter_congdung);
        Display_DoiTuong();
        Arraylist_data_chupanh = new ArrayList<data_Chupanh>();
        Adapter_chupanh = new adapter_Chupanh(activity_Congdung.this,R.layout.dong_hinhanh,Arraylist_data_chupanh);
        lv_anhs.setAdapter(Adapter_chupanh);
        lv_anhs.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

        // ACTION : CAMERA
        btn_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*
                if(Enable_Ghiso==DISABLE_GHISO_CHUADENGIO){
                    Dialog_App dialog_app =new Dialog_App(activity_Congdung.this);
                    String notification = "Chưa đến thời gian ghi sô."+"\r\n"+"Không thể mở camera."+"\r\n"+"Xin cám ơn";
                    dialog_app.Dialog_Notification(notification);
                }else {
                    dispatchTakePictureIntent();
                }

                 */
                dispatchTakePictureIntent();
            }
        });
        Display_HinhAnh();

        btn_xoaanh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text_n ="Bạn muốn xóa ảnh đã chọn";
                Dialog_Notification_xoaanh(text_n);
            }
        });
        spn_doituong.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent,View view,int position,long id) {

                //Doituong_Current = position;
                String doituong_selete = array_doituong.get(position).toString().trim();
                Display_CongDung(doituong_selete);
                if(position == Doituong_Current){
                    sqlite_DanhSachKhackHang_DocSo sqlite_danhSachKhackHang_docSo =  new sqlite_DanhSachKhackHang_DocSo(activity_Congdung.this,Nam,Ky,Dot,SoMay);
                    if(sqlite_danhSachKhackHang_docSo.getStatus_Table_DanhSachKhachHang_Exists()){
                        String congdung = sqlite_danhSachKhackHang_docSo.Get_CongDung(Danhba);
                        if(congdung.trim().equals("")){
                            spn_congdung.setSelection(Congdung_Current);
                        }else {
                            String [] com_congdung = congdung.split("-");
                            String noidung = "";
                            if(com_congdung.length >2){
                                for(int i = 2 ; i < com_congdung.length ; i++){
                                    if(noidung.isEmpty()){
                                        noidung = com_congdung[i];
                                    }else {
                                        noidung = noidung +"-"+com_congdung[i];
                                    }
                                }
                                edt_noidung.setText(noidung);

                                for(int i =0 ; i < array_doituong.size() ; i++){
                                    try {
                                        String data = array_doituong.get(i);
                                        if (data.toUpperCase().equals(com_congdung[0].toUpperCase())) {
                                            spn_doituong.setSelection(i);
                                            Display_CongDung(data);
                                            break;
                                        }
                                    }catch (Exception e){

                                    }
                                }

                                for(int i = 0 ; i < array_congdung.size() ; i++){
                                    try {

                                        String data = array_congdung.get(i);
                                        if (data.toUpperCase().equals(com_congdung[1].toUpperCase())) {
                                            spn_congdung.setSelection(i);
                                            break;
                                        }
                                    }catch (Exception e){

                                    }
                                }
                            }else if(com_congdung.length >1){

                                for(int i =0 ; i < array_doituong.size() ; i++){
                                    try {
                                        String data = array_doituong.get(i);
                                        if (data.toUpperCase().equals(com_congdung[0].toUpperCase())) {
                                            spn_doituong.setSelection(i);
                                            Display_CongDung(data);

                                            break;
                                        }
                                    }catch (Exception e){

                                    }
                                }
                                boolean status_khac = true;
                                for(int i = 0 ; i < array_congdung.size() ; i++){
                                    try {
                                        String data = array_congdung.get(i);
                                        if (data.toUpperCase().equals(com_congdung[0].toUpperCase())) {
                                            spn_congdung.setSelection(i);
                                            status_khac = false;
                                            break;
                                        }
                                    }catch (Exception e){

                                    }
                                }
                                // hien thi khac
                                if(status_khac) {
                                    for (int i = 0; i < array_congdung.size(); i++) {
                                        try {
                                            String data = array_congdung.get(i);
                                            if (data.toUpperCase().equals("KHÁC")) {
                                                spn_congdung.setSelection(i);
                                                edt_noidung.setText(com_congdung[1]);
                                                break;
                                            }
                                        } catch (Exception e) {

                                        }
                                    }
                                }
                            }else {
                                spn_congdung.setSelection(Congdung_Current);
                            }
                        }
                    }

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spn_congdung.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent,View view,int position,long id) {

               // Congdung_Current = position;

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        btn_luu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Luu_CongDung();
            }
        });
        btn_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(Enable_Ghiso==DISABLE_GHISO_CHUADENGIO){
                    Dialog_App dialog_app =new Dialog_App(activity_Congdung.this);
                    String notification = "Chưa đến thời gian ghi sô."+"\r\n"+"Không thể gửi ảnh về server."+"\r\n"+"Xin cám ơn";
                    dialog_app.Dialog_Notification(notification);
                }else {
                    GuiAnh();
                }

            }
        });
        spn_doituong.setSelection(Doituong_Current);


    }
    String currentPhotoPath ="";

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }
    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File

            }
            // Continue only if the File was successfully created
            if (photoFile != null) {

                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.example.android.fileprovider",
                        photoFile);
                Log.e("Uri",String.valueOf(photoURI));
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_ID_IMAGE_CAPTURE);
            }
        }
    }

    @Override
    protected void onResume() {

        super.onResume();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),0);
        }
        return super.dispatchTouchEvent(ev);
    }
    private void captureImage() {
        // Create an implicit intent, for image capture.
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        // Start camera and wait for the results.
        this.startActivityForResult(intent, REQUEST_ID_IMAGE_CAPTURE);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);
        if(resultCode == activity_Chuphinh.RESULT_OK) {

            String filePath = getImageFilePath(data);
            Log.e("filePath:" ,filePath);
            Log.e("requestCode ", String.valueOf(requestCode));
            Log.e("resultCode ", String.valueOf(resultCode));
            switch (requestCode) {
                case REQUEST_ID_IMAGE_CAPTURE: {
                    try {
                        if (resultCode == RESULT_OK) {
                            //  bitmap_anhsave = (Bitmap) data.getExtras().get("data");
                            Log.e("currentPhotoPath",currentPhotoPath);
                            if(!currentPhotoPath.isEmpty()) {
                                String text_notification = "Bạn có muốn lưu ảnh";
                                Dialog_Notification(text_notification);
                            }
                        }
                    }catch (Exception ee){

                    }
                    break;
                }
            }
        } else if (resultCode == RESULT_CANCELED) {
            Toast.makeText(this,"Action canceled",Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this,"Action Failed",Toast.LENGTH_LONG).show();
        }
    }
    public String getImageFilePath(Intent data) {
        return getImageFromFilePath(data);
    }
    private String getImageFromFilePath(Intent data) {
        boolean isCamera = data == null || data.getData() == null;

        if (isCamera) return getCaptureImageOutputUri().getPath();
        else return getPathFromURI(data.getData());

    }
    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = getExternalFilesDir("");
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getAbsolutePath(), ""));
        }
        return outputFileUri;
    }
    private String getPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Audio.Media.DATA};
        Cursor cursor = getContentResolver().query(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }
    private void Mappping_Layout() {
        tv_danhba =(TextView) findViewById(R.id.textview_danhba_congdung);
        tv_sost = ( TextView) findViewById(R.id.textview_sost_congdung);
        imv_dongbo =( ImageView) findViewById(R.id.imageView_dongbo_congdung);
        tv_hoten = ( TextView) findViewById(R.id.textview_hoten_congdung);
        tv_diachi = ( TextView) findViewById(R.id.textview_diachi_congdung);
        tv_congdungserver = ( TextView) findViewById(R.id.textview_congdungserver_Congdung);
        btn_camera = (Button) findViewById(R.id.button_camera_congdung);
        lv_anhs =( ListView) findViewById(R.id.listview_anh_congdung);
        btn_xoaanh =(Button) findViewById(R.id.button_xoa_congdung);
        btn_upload = (Button) findViewById(R.id.button_upload_congdung);
        btn_luu = (Button) findViewById(R.id.button_luu_congdung);
        spn_doituong = ( Spinner) findViewById(R.id.spinner_doituong_congdung);
        spn_congdung = ( Spinner) findViewById(R.id.spinner_congdung_congdung);
        edt_noidung = (EditText) findViewById(R.id.edittext_noidung_congdung);
    }
    private boolean Get_Table_GhiSo_Chitiet(String json){
        boolean status = false;
        try{
            JSONObject table = new JSONObject(json);
            Danhba = table.getString("danhba");
            So_st = table.getString("so_st");
            Hoten = table.getString("hoten");
            Diachi = table.getString("diachi") ;
            String sync = table.getString("sync");
            Dongbo = Integer.parseInt(sync);
            Nam = table.getString("nam");
            Ky = table.getString("ky");
            Dot = table.getString("dot");
            SoMay = table.getString("somay");
            CongDung_Server = table.getString("congdung");
            String enable_ghiso = Objects.toString(table.getString("enable_ghiso"),"");
            if(!enable_ghiso.isEmpty()){
                Enable_Ghiso= Integer.parseInt(enable_ghiso);
            }
            if(!sync.isEmpty()){
                Dongbo = Integer.parseInt(sync);
            }
            status = true;
        }catch (JSONException e){

        }
        return  status;
    }
    private  AlertDialog dialog_notification ;
    public void Dialog_Notification(final String text){
        AlertDialog.Builder alert_update_version =new AlertDialog.Builder(activity_Congdung.this);
        alert_update_version.setMessage(text);
        // dong y
        alert_update_version.setPositiveButton("Xác nhận",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog,int which) {
                // xu ly anh

                sqlite_Hinhanh sqlite_hinhanh = new sqlite_Hinhanh(activity_Congdung.this,Nam,Ky,Dot,SoMay);
                if (sqlite_hinhanh.getStatus_Table_ListHinhAnh_Exists()) {
                    List<data_Hinhanh.data_anh> list_data_anh = new ArrayList<>();
                    data_Hinhanh.data_anh value_anh = new data_Hinhanh.data_anh(currentPhotoPath);
                    value_anh.setType("congdung");
                    list_data_anh.add(value_anh);
                    data_Hinhanh data_anhsave = new data_Hinhanh(Danhba);
                    data_anhsave.setHinhanhs(list_data_anh);
                    sqlite_hinhanh.Insert_Table_ListHinhAnh(data_anhsave);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Display_HinhAnh();
                        }
                    });
                }
                dialog_notification.dismiss();
            }
        });
        // bo qua
        alert_update_version.setNegativeButton("Hủy",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog,int which) {
                dialog_notification.dismiss();
            }
        });

        dialog_notification = alert_update_version.create();
        dialog_notification.show();
    }
    public Bitmap getResizedBitmap(Bitmap bm) {
        Log.e("Size anh:",String.valueOf(bm.getByteCount()));
        int width = bm.getWidth();
        int height = bm.getHeight();
        Log.e("width anh:",String.valueOf(width));
        Log.e("height anh:",String.valueOf(height));
        double ratio = (float) (1000000.0/(bm.getByteCount()));
        float ratio_scale =(float) Math.sqrt(ratio);
        Log.e("Size anh:",String.valueOf(ratio_scale));
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(ratio_scale, ratio_scale);
        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        Log.e("Size anh n:",String.valueOf(resizedBitmap.getByteCount()));
        return resizedBitmap;
    }
    private void  Luu_CongDung(){
        try {
            int doituong_selete = spn_doituong.getSelectedItemPosition();
            int congdung_selete = spn_congdung.getSelectedItemPosition();
            String congdung = array_congdung.get(congdung_selete);
            String doituong = array_doituong.get(doituong_selete);
            String noidung = edt_noidung.getText().toString().trim();

            String save = doituong + "-" + congdung + "-" + noidung;
            if (congdung.toUpperCase().equals("KHÁC")) {
                save = doituong + "-" + noidung;
            }
            sqlite_DanhSachKhackHang_DocSo sqlite_danhSachKhackHang_docSo = new sqlite_DanhSachKhackHang_DocSo(activity_Congdung.this,Nam,Ky,Dot,SoMay);
            if (sqlite_danhSachKhackHang_docSo.getStatus_Table_DanhSachKhachHang_Exists()) {
                sqlite_danhSachKhackHang_docSo.Update_Sync_DanhSachDocSo_KhachHangs_CongDung(Danhba,save);
                Dialog_App dialog_app_ok = new Dialog_App(activity_Congdung.this);
                dialog_app_ok.Dialog_Notification("Lưu công dụng thành công.");
            }
        }catch ( Exception e){

        }
    }

    private String BitmapToString (Bitmap bitmap){
        String databse64 ="";
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG,100,byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            databse64 = Base64.encodeToString(byteArray,Base64.DEFAULT);
        }catch (Exception e){

        }
        return  databse64;
    }
    private Bitmap StringToBitmap(String base64){
        byte[] decodedString = Base64.decode(base64, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        return decodedByte;
    }
    private void  Display_HinhAnh(){
        sqlite_Hinhanh sqlite_hinhanh = new sqlite_Hinhanh(activity_Congdung.this,Nam,Ky,Dot,SoMay);
        try{
            Arraylist_data_chupanh.clear();
        }catch (Exception e){

        }
        if(sqlite_hinhanh.getStatus_Table_ListHinhAnh_Exists()){
            data_Hinhanh data_hinhanh_khachhang = sqlite_hinhanh.Get_Table_ListHinhAnh(Danhba,"congdung");
            List<data_Hinhanh.data_anh> list_anh = data_hinhanh_khachhang.getHinhanhs();
            if(list_anh !=null) {
                for (int i = 0; i < list_anh.size(); i++) {
                    int id = list_anh.get(i).getId();
                    String anh = list_anh.get(i).getAnh();
                    String sync_anh = list_anh.get(i).getSync_anh();
                    String type = list_anh.get(i).getType();
                    if(type.equals("congdung")) {
                        File imgfile = new File(anh);
                        if (imgfile.exists()) {
                            //Dialog_App dialog_app = new Dialog_App(activity_Chuphinh.this);
                            //  dialog_app.Dialog_Notification(imgfile.getAbsolutePath());
                            // BitmapFactory.Options options;
                            Bitmap myBitmap = BitmapFactory.decodeFile(imgfile.getAbsolutePath());

                            Bitmap bMapScaled = Bitmap.createScaledBitmap(myBitmap,1024,768,true);

                            if (bMapScaled != null) {
                                /*    if (myBitmap.getByteCount() > 1000000) {
                                        //   myBitmap = getResizedBitmap(myBitmap);
                                    }

                                 */
                                data_Chupanh data_chupanh = new data_Chupanh(id,bMapScaled,false,sync_anh);
                                Arraylist_data_chupanh.add(data_chupanh);
                            }
                        }
                        //   Bitmap bitmap = StringToBitmap(anh);
                        //   img_anhchup.setImageBitmap(bitmap);
                    }
                }
                Adapter_chupanh.notifyDataSetChanged();
            }
        }
    }
    private boolean Xoa_Anh(){

        boolean status = false;
        sqlite_Hinhanh sqlite_hinhanh = new sqlite_Hinhanh(activity_Congdung.this, Nam,Ky, Dot,SoMay);
        if(sqlite_hinhanh.getStatus_Table_ListHinhAnh_Exists()) {
            for(int i = Arraylist_data_chupanh.size() -1 ;  i>= 0; i--){
                boolean selete = Arraylist_data_chupanh.get(i).isSelete();
                if(selete){
                    int id = Arraylist_data_chupanh.get(i).getId();
                    sqlite_hinhanh.Delete_Anh_ListDataAnh(id,Danhba);
                    Arraylist_data_chupanh.remove(i);
                }
            }
            /*
            for (data_Chupanh data_anh : Arraylist_data_chupanh) {
                if (data_anh.isSelete()) {
                    int id = data_anh.getId();
                    sqlite_hinhanh.Delete_Anh_ListDataAnh(id,Danhba);
                    Arraylist_data_chupanh.
                }
            }

             */
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Display_HinhAnh();
            }
        });

        return  status ;
    }
    private  AlertDialog dialog_notification_xoaanh ;
    public void Dialog_Notification_xoaanh(final String text){
        AlertDialog.Builder alert_update_version =new AlertDialog.Builder(activity_Congdung.this);
        alert_update_version.setMessage(text);
        // dong y
        alert_update_version.setPositiveButton("Xác nhận",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog,int which) {
                // xu ly anh

                Xoa_Anh();
                dialog_notification_xoaanh.dismiss();
            }
        });
        // bo qua
        alert_update_version.setNegativeButton("Hủy",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog,int which) {
                dialog_notification_xoaanh.dismiss();
            }
        });

        dialog_notification_xoaanh = alert_update_version.create();
        dialog_notification_xoaanh.show();
    }
    private void  Display_DoiTuong(){
        sqlite_Config_CongDung sqlite_config_congDung = new sqlite_Config_CongDung(activity_Congdung.this);
        if(sqlite_config_congDung.getStatus_Table_ListCongDung_Exists()) {
            List<String> doituongs = sqlite_config_congDung.Get_DoiTuong();
            array_doituong.addAll(doituongs);
            adapter_doituong.notifyDataSetChanged();
        }
    }
    private  void Display_CongDung(String doituong){
        try{
            array_congdung.clear();
            adapter_congdung.clear();
            adapter_congdung.notifyDataSetChanged();
        }catch (Exception e){

        }
        sqlite_Config_CongDung  sqlite_config_congDung = new sqlite_Config_CongDung(activity_Congdung.this);
        if(sqlite_config_congDung.getStatus_Table_ListCongDung_Exists()) {
            List<String> congdungs = sqlite_config_congDung.Get_CongDung(doituong);
            array_congdung.addAll(congdungs);
            adapter_congdung.notifyDataSetChanged();
        }
    }


    List<SendPicture_AsyncTask> list_myAsyncTask ;
    private void GuiAnh(){
        try {
            //  SendPicture_AsyncTask myAsyncTask = new SendPicture_AsyncTask(activity_Chuphinh.this,Danhba,Nam,Ky,Dot,SoMay);
            //  myAsyncTask.execute();
            Boolean status_countdown = false;
            list_myAsyncTask = new ArrayList<SendPicture_AsyncTask>();
            sqlite_Hinhanh sqlite_hinhanh = new sqlite_Hinhanh(activity_Congdung.this,Nam,Ky,Dot,SoMay);
            if(sqlite_hinhanh.getStatus_Table_ListHinhAnh_Exists()) {
                for(int i = Arraylist_data_chupanh.size() -1 ;  i>= 0; i--){
                    boolean selete = Arraylist_data_chupanh.get(i).isSelete();
                    if(selete){
                        String sync  = Arraylist_data_chupanh.get(i).getSync_anh();
                        if(!sync.equals("Rồi")){
                            status_countdown = true;
                            int id_anh = Arraylist_data_chupanh.get(i).getId();
                            Bitmap bitmap_send = Arraylist_data_chupanh.get(i).getAnh();
                            // String link_anh = sqlite_hinhanh.Get_Table_1HinhAnh(Danhba,String.valueOf(id_anh));
                            SendPicture_AsyncTask myAsyncTask = new SendPicture_AsyncTask(activity_Congdung.this,Danhba,Nam,Ky,Dot,SoMay,String.valueOf(id_anh),bitmap_send,false);
                            list_myAsyncTask.add(myAsyncTask);
                        }

                    }
                }

                for(int i =0 ; i < list_myAsyncTask.size() ; i++){
                    SendPicture_AsyncTask myAsyncTask = list_myAsyncTask.get(i);
                    if(i == list_myAsyncTask.size()-1){
                        myAsyncTask.setOn_dialog(true);
                    }
                    myAsyncTask.execute();
                }
                if(status_countdown) {
                    countDownTimer.start();
                }
            }


            //  GuiAnh();
        }catch (Exception e){}
    }

    CountDownTimer countDownTimer = new CountDownTimer(30000,1000) {
        @Override
        public void onTick(long millisUntilFinished) {

            List<Integer> list_status = new ArrayList<>();
            for(int i = 0 ;  i<list_myAsyncTask.size(); i++){
                SendPicture_AsyncTask myAsyncTask=list_myAsyncTask.get(i);
                list_status.add(myAsyncTask.Status_Send());
            }
            // kiemtra gui thanh cong
            boolean status_ok = true;
            for (int status : list_status){
                if(status==0 || status ==2){
                    status_ok = false;
                    break;
                }
            }


            // kiemtra gui loi
            boolean status_error = true;
            for (int status : list_status){
                if(status==2){

                }else {
                    status_error = false;
                    break;
                }
            }
            if(status_error){
                countDownTimer.cancel();
            }
            // kiemtra gui hoan thanh
            boolean status_finish = true;
            for (int status : list_status){
                if(status!=0){

                }else {
                    status_finish = false;
                    break;
                }
            }
            if(status_finish||status_ok){
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Display_HinhAnh();
                    }
                });
                countDownTimer.cancel();
            }
            Log.e("Picture",String.valueOf(status_finish)+","+String.valueOf(status_ok)+","+String.valueOf(status_error));

        }

        @Override
        public void onFinish() {
            countDownTimer.cancel();
        }
    };
}
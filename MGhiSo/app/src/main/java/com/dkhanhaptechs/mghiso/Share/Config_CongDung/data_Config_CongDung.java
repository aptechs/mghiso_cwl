package com.dkhanhaptechs.mghiso.Share.Config_CongDung;

public class data_Config_CongDung {
    private int id ;
    private String doituong;
    private String congdung;

    public data_Config_CongDung(String doituong,String congdung) {
        this.doituong = doituong;
        this.congdung = congdung;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDoituong() {
        return doituong;
    }

    public void setDoituong(String doituong) {
        this.doituong = doituong;
    }

    public String getCongdung() {
        return congdung;
    }

    public void setCongdung(String congdung) {
        this.congdung = congdung;
    }
}

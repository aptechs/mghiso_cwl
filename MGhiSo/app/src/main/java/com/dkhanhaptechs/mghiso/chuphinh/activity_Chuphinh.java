package com.dkhanhaptechs.mghiso.chuphinh;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.dkhanhaptechs.mghiso.R;

import org.json.JSONException;
import org.json.JSONObject;

import com.dkhanhaptechs.mghiso.Share.Dialog_App;
import com.dkhanhaptechs.mghiso.Share.HinhAnh.*;



import java.io.ByteArrayOutputStream;
import java.io.File;

import java.io.IOException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import  static  android.Manifest.permission.CAMERA;

public class activity_Chuphinh extends AppCompatActivity {
    // BIEN LAYOUT
    private TextView tv_danhba, tv_sost, tv_hoten,tv_diachi;
    private ImageView imv_dongbo;
    private Toolbar toolbar ;
    private String Json_table_ghiso_chitiet = "";
    private Button btn_camera, btn_xoaanh, btn_upload, btn_anhlocal, btn_xemanh;
    private ListView lv_anhs;
    private static final int REQUEST_ID_IMAGE_CAPTURE = 100;
    private static final int RESULT_LOAD_IMG = 99;

    private String  So_st, Hoten, Diachi;
    private String Danhba, Nam, Ky, Dot, SoMay;
    private int Dongbo;
    private Bitmap bitmap_anhsave;

    private ArrayList<data_Chupanh> Arraylist_data_chupanh;
    private adapter_Chupanh Adapter_chupanh;

    private ArrayList<String> permissionsToRequest;
    private ArrayList<String> permissionsRejected = new ArrayList<>();
    private ArrayList<String> permissions = new ArrayList<>();
    private final static int ALL_PERMISSIONS_RESULT = 107;

    private static final int DISABLE_GHISO_CHUADENGIO = 1;
    private static final int DISABLE_GHISO_QUAGIO = 2;
    private static final int DISABLE_KHONGCODENNGAY = 3;
    private int Enable_Ghiso= 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guianhchup);
        Mappping_Layout();
        // GET DATA
        Json_table_ghiso_chitiet = getIntent().getStringExtra("CHUPHINH");
        // SET DATA
        try {
            if (Get_Table_GhiSo_Chitiet(Json_table_ghiso_chitiet)) {
                tv_danhba.setText(Danhba);
                tv_sost.setText(So_st);
                tv_hoten.setText(Hoten);
                tv_diachi.setText(Diachi);
                switch (Dongbo) {
                    case 2: {
                        // timeout
                        imv_dongbo.setImageResource(R.drawable.synced_2);
                        break;
                    }
                    case 1: {
                        // dang do bo
                        imv_dongbo.setImageResource(R.drawable.synced_1);
                        break;
                    }
                    case 0: {
                        // da dong bo
                        imv_dongbo.setImageResource(R.drawable.synced_0);
                        break;
                    }
                }
            }
        }catch (Exception e){

        }
        //Setup_test();
        Arraylist_data_chupanh = new ArrayList<data_Chupanh>();
        Adapter_chupanh = new adapter_Chupanh(activity_Chuphinh.this,R.layout.dong_hinhanh,Arraylist_data_chupanh);
        lv_anhs.setAdapter(Adapter_chupanh);
        lv_anhs.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

        // ACTION : CAMERA
        btn_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             //   captureImage();
                /*
                if(Enable_Ghiso==DISABLE_GHISO_CHUADENGIO){
                   Dialog_App dialog_app =new Dialog_App(activity_Chuphinh.this);
                   String notification = "Chưa đến thời gian ghi sô."+"\r\n"+"Không thể mở camera."+"\r\n"+"Xin cám ơn";
                   dialog_app.Dialog_Notification(notification);
                }else {
                    dispatchTakePictureIntent();
                }
                */
                dispatchTakePictureIntent();
            }
        });

        permissions.add(CAMERA);
        permissions.add(READ_EXTERNAL_STORAGE);
        permissions.add(WRITE_EXTERNAL_STORAGE);

        permissionsToRequest = findUnAskedPermissions(permissions);



        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (permissionsToRequest.size() > 0) {
                requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]),ALL_PERMISSIONS_RESULT);
            }
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Display_HinhAnh();
                }
            });

        }

        btn_xoaanh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text_n = "Bạn muốn xóa ảnh đã chọn";
                Dialog_Notification_xoaanh(text_n);
            }
        });
        btn_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(Enable_Ghiso==DISABLE_GHISO_CHUADENGIO){
                    Dialog_App dialog_app =new Dialog_App(activity_Chuphinh.this);
                    String notification = "Chưa đến thời gian ghi sô."+"\r\n"+"Không thể gửi ảnh về server."+"\r\n"+"Xin cám ơn";
                    dialog_app.Dialog_Notification(notification);
                }else {
                    GuiAnh();
                }


              //  GuiAnh();
            }
        });
        btn_anhlocal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*
                if(Enable_Ghiso==DISABLE_GHISO_CHUADENGIO) {
                    Dialog_App dialog_app =new Dialog_App(activity_Chuphinh.this);
                    String notification = "Chưa đến thời gian ghi sô."+"\r\n"+"Không thể lấy ảnh trong máy."+"\r\n"+"Xin cám ơn";
                    dialog_app.Dialog_Notification(notification);
                }else {
                    Intent photoPickerIntent = new Intent(Intent.ACTION_PICK,MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                    startActivityForResult(photoPickerIntent,RESULT_LOAD_IMG);
                }
                 */
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK,MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                startActivityForResult(photoPickerIntent,RESULT_LOAD_IMG);
            }
        });
        btn_xemanh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(activity_Chuphinh.this,activity_Xemanh.class);
                intent.putExtra("XEMHINH",Json_table_ghiso_chitiet);
                startActivity(intent);
            }
        });


    }
    private void Setup_test(){





        Dongbo = 0;
        Dot = "2";
        SoMay = "10";
        Danhba = "06066670090";
        So_st = "1100015";
        Ky = "6";
        Nam ="2021";
        tv_danhba.setText("06066670090");
        tv_sost.setText("1100015");
        tv_hoten.setText("LE THI NGOC BICH");
        tv_diachi.setText("");

    }
    private ArrayList<String> findUnAskedPermissions(ArrayList<String> wanted) {
        ArrayList<String> result = new ArrayList<String>();

        for (String perm : wanted) {
            if (!hasPermission(perm)) {
                result.add(perm);
            }
        }

        return result;
    }
    private boolean hasPermission(String permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }
    private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case ALL_PERMISSIONS_RESULT:
                for (String perms : permissionsToRequest) {
                    if (!hasPermission(perms)) {
                        permissionsRejected.add(perms);
                    }
                }
                if (permissionsRejected.size() > 0) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(permissionsRejected.get(0))) {
                            showMessageOKCancel("These permissions are mandatory for the application. Please allow access.",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions(permissionsRejected.toArray(new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    });
                            return;
                        }
                    }

                }

                break;
        }

    }
    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new android.support.v7.app.AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }
    String currentPhotoPath ="";

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }
    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File

            }
            // Continue only if the File was successfully created
            if (photoFile != null) {

                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.example.android.fileprovider",
                        photoFile);
                Log.e("Uri",String.valueOf(photoURI));
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_ID_IMAGE_CAPTURE);
            }
        }
    }
    private void captureImage() {
        // Create an implicit intent, for image capture.
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        try {
            startActivityForResult(takePictureIntent, REQUEST_ID_IMAGE_CAPTURE);
        } catch (ActivityNotFoundException e) {
            // display error state to the user
        }
        // Start camera and wait for the results.
    }
    public String getImageFilePath(Intent data) {
        return getImageFromFilePath(data);
    }
    private String getImageFromFilePath(Intent data) {
        boolean isCamera = data == null || data.getData() == null;

        if (isCamera) return getCaptureImageOutputUri().getPath();
        else return getPathFromURI(data.getData());

    }
    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = getExternalFilesDir("");
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getAbsolutePath(), ""));
        }
        return outputFileUri;
    }
    private String getPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);
        if(resultCode == activity_Chuphinh.RESULT_OK) {

            String filePath = getImageFilePath(data);
            Log.e("filePath:" ,filePath);

            switch (requestCode) {
                case REQUEST_ID_IMAGE_CAPTURE: {
                    try {
                        if (resultCode == RESULT_OK) {
                          //  bitmap_anhsave = (Bitmap) data.getExtras().get("data");
                           // Log.e("currentPhotoPath",currentPhotoPath);
                            if(!currentPhotoPath.isEmpty()) {
                                Seve_Picture_Local();
                            }
                        }
                    }catch (Exception ee){

                    }
                    break;
                }
                case  RESULT_LOAD_IMG: {
                    Uri selectedImage = data.getData();
                  //  Log.e("URL",String.valueOf(selectedImage));

                 //   Dialog_Notification(filePath);
                  //  Toast.makeText(activity_Chuphinh.this,filePath,Toast.LENGTH_SHORT).show();
                        if (resultCode == RESULT_OK) {
                            currentPhotoPath = filePath ;
                       //     Log.e("currentPhotoPath",currentPhotoPath);
                            //  bitmap_anhsave = MediaStore.Images.Media.getBitmap(activity_Chuphinh.this.getContentResolver(), selectedImage);
                          if(!currentPhotoPath.isEmpty()) {
                              Seve_Picture_Local();
                          }
                        }

                    break;
                }

            }
        } else if (resultCode == RESULT_CANCELED) {
            currentPhotoPath = "";
            Toast.makeText(this,"Action canceled",Toast.LENGTH_LONG).show();
        } else {
            currentPhotoPath ="";
            Toast.makeText(this,"Action Failed",Toast.LENGTH_LONG).show();
        }
    }
    private void Mappping_Layout() {
        tv_danhba =(TextView) findViewById(R.id.textview_danhba_chuphinh);
        tv_sost = ( TextView) findViewById(R.id.textview_sost_chuphinh);
        imv_dongbo =( ImageView) findViewById(R.id.imageView_dongbo_chuphinh);
        tv_hoten = ( TextView) findViewById(R.id.textview_hoten_chuphinh);
        tv_diachi = ( TextView) findViewById(R.id.textview_diachi_chuphinh);
        btn_camera = (Button) findViewById(R.id.button_camera_chuphinh);
        lv_anhs =( ListView) findViewById(R.id.listview_anh_chuphinh);
        btn_xoaanh =(Button) findViewById(R.id.button_xoanh_chupanh);
        btn_upload = (Button) findViewById(R.id.button_upload_chupanh);
        btn_anhlocal = (Button) findViewById(R.id.button_anhlocal_chupanh);
        btn_xemanh = (Button) findViewById(R.id.button_xemanh_chupanh);
    }
    private boolean Get_Table_GhiSo_Chitiet(String json){
        boolean status = false;
        try{
            Log.e("14",json);
            JSONObject table = new JSONObject(json);
            Danhba = table.getString("danhba");
            So_st = table.getString("so_st");
            Hoten = table.getString("hoten");
            Diachi = table.getString("diachi") ;
            String sync = table.getString("sync");
            Dongbo = Integer.parseInt(sync);
            Nam = table.getString("nam");
            Ky = table.getString("ky");
            Dot = table.getString("dot");
            SoMay = table.getString("somay");
            String enable_ghiso = Objects.toString(table.getString("enable_ghiso"),"");
            if(!enable_ghiso.isEmpty()){
                Enable_Ghiso = Integer.parseInt(enable_ghiso);
            }
            if(!sync.isEmpty()){
                Dongbo = Integer.parseInt(sync);
            }
            status = true;
        }catch (JSONException e){

        }
        return  status;
    }


    private void Seve_Picture_Local(){
        if(!currentPhotoPath.isEmpty()) {
            sqlite_Hinhanh sqlite_hinhanh = new sqlite_Hinhanh(activity_Chuphinh.this,Nam,Ky,Dot,SoMay);
            if (sqlite_hinhanh.getStatus_Table_ListHinhAnh_Exists()) {
                List<data_Hinhanh.data_anh> list_data_anh = new ArrayList<>();
                data_Hinhanh.data_anh value_anh = new data_Hinhanh.data_anh(currentPhotoPath);
                value_anh.setType("chupanh");
                list_data_anh.add(value_anh);
                data_Hinhanh data_anhsave = new data_Hinhanh(Danhba);
                data_anhsave.setHinhanhs(list_data_anh);
                sqlite_hinhanh.Insert_Table_ListHinhAnh(data_anhsave);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Display_HinhAnh();
                    }
                });
            }
        }
    }
    public Bitmap getResizedBitmap(Bitmap bm) {
      //  Log.e("Size anh:",String.valueOf(bm.getByteCount()));
        int width = bm.getWidth();
        int height = bm.getHeight();
      //  Log.e("width anh:",String.valueOf(width));
       // Log.e("height anh:",String.valueOf(height));
        double ratio = (float) (1000000.0/(bm.getByteCount()));
        float ratio_scale =(float) Math.sqrt(ratio);
      //  Log.e("Size anh:",String.valueOf(ratio_scale));
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(ratio_scale, ratio_scale);
        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        bm.recycle();
       // Log.e("Size anh n:",String.valueOf(resizedBitmap.getByteCount()));
        return resizedBitmap;
    }
    private String BitmapToString (Bitmap bitmap){
        String databse64 ="";
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG,100,byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            databse64 = Base64.encodeToString(byteArray,Base64.DEFAULT);
        }catch (Exception e){

        }
        return  databse64;
    }
    private Bitmap StringToBitmap(String base64){
        byte[] decodedString = Base64.decode(base64, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        return decodedByte;
    }
    private void  Display_HinhAnh (){
        try {
            Arraylist_data_chupanh.clear();
        }catch (Exception ee){

        }
        sqlite_Hinhanh sqlite_hinhanh = new sqlite_Hinhanh(activity_Chuphinh.this,Nam,Ky,Dot,SoMay);
        if(sqlite_hinhanh.getStatus_Table_ListHinhAnh_Exists()){
            data_Hinhanh data_hinhanh_khachhang = sqlite_hinhanh.Get_Table_ListHinhAnh(Danhba,"chupanh");
            List<data_Hinhanh.data_anh> list_anh = data_hinhanh_khachhang.getHinhanhs();
            if(list_anh !=null) {
                for (int i = 0; i < list_anh.size(); i++) {
                    try {
                        int id = list_anh.get(i).getId();
                        String anh = list_anh.get(i).getAnh();
                        String sync_anh = list_anh.get(i).getSync_anh();
                        String type = list_anh.get(i).getType();
                        if (type.equals("chupanh")) {
                            File imgfile = new File(anh);
                            if (imgfile.exists()) {
                                //Dialog_App dialog_app = new Dialog_App(activity_Chuphinh.this);
                                //  dialog_app.Dialog_Notification(imgfile.getAbsolutePath());
                                // BitmapFactory.Options options;
                                Bitmap myBitmap = BitmapFactory.decodeFile(imgfile.getAbsolutePath());

                                Bitmap bMapScaled = Bitmap.createScaledBitmap(myBitmap,1024,768,true);

                                if (bMapScaled != null) {
                                /*    if (myBitmap.getByteCount() > 1000000) {
                                        //   myBitmap = getResizedBitmap(myBitmap);
                                    }

                                 */
                                    data_Chupanh data_chupanh = new data_Chupanh(id,bMapScaled,false,sync_anh);
                                    Arraylist_data_chupanh.add(data_chupanh);
                                }
                            }
                        }
                    }catch (Exception e){

                    }
                    //   Bitmap bitmap = StringToBitmap(anh);
                    //   img_anhchup.setImageBitmap(bitmap);
                }
                Adapter_chupanh.notifyDataSetChanged();

            }
        }
    }
    private boolean Xoa_Anh(){

        boolean status = false;
        sqlite_Hinhanh sqlite_hinhanh = new sqlite_Hinhanh(activity_Chuphinh.this, Nam,Ky, Dot,SoMay);
        if(sqlite_hinhanh.getStatus_Table_ListHinhAnh_Exists()) {
            for(int i = Arraylist_data_chupanh.size() -1 ;  i>= 0; i--){
                boolean selete = Arraylist_data_chupanh.get(i).isSelete();
                if(selete){
                    int id = Arraylist_data_chupanh.get(i).getId();
                    sqlite_hinhanh.Delete_Anh_ListDataAnh(id,Danhba);
                    Arraylist_data_chupanh.remove(i);
                }
            }

        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Display_HinhAnh();
            }
        });

        return  status ;
    }
    private  AlertDialog dialog_notification_xoaanh ;
    public void Dialog_Notification_xoaanh(final String text){
        AlertDialog.Builder alert_update_version =new AlertDialog.Builder(activity_Chuphinh.this);
        alert_update_version.setMessage(text);
        // dong y
        alert_update_version.setPositiveButton("Xác nhận",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog,int which) {
                // xu ly anh

                Xoa_Anh();
                dialog_notification_xoaanh.dismiss();
            }
        });
        // bo qua
        alert_update_version.setNegativeButton("Hủy",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog,int which) {
                dialog_notification_xoaanh.dismiss();
            }
        });

        dialog_notification_xoaanh = alert_update_version.create();
        dialog_notification_xoaanh.show();
    }




    List<SendPicture_AsyncTask> list_myAsyncTask ;
    private void GuiAnh(){
        try {
            //  SendPicture_AsyncTask myAsyncTask = new SendPicture_AsyncTask(activity_Chuphinh.this,Danhba,Nam,Ky,Dot,SoMay);
            //  myAsyncTask.execute();
            Boolean status_countdown = false;
            list_myAsyncTask = new ArrayList<SendPicture_AsyncTask>();
            sqlite_Hinhanh sqlite_hinhanh = new sqlite_Hinhanh(activity_Chuphinh.this,Nam,Ky,Dot,SoMay);
            if(sqlite_hinhanh.getStatus_Table_ListHinhAnh_Exists()) {
                for(int i = Arraylist_data_chupanh.size() -1 ;  i>= 0; i--){
                    boolean selete = Arraylist_data_chupanh.get(i).isSelete();
                    if(selete){
                        String sync  = Arraylist_data_chupanh.get(i).getSync_anh();
                        if(!sync.equals("Rồi")){
                            status_countdown = true;
                            int id_anh = Arraylist_data_chupanh.get(i).getId();
                            Bitmap bitmap_send = Arraylist_data_chupanh.get(i).getAnh();
                           // String link_anh = sqlite_hinhanh.Get_Table_1HinhAnh(Danhba,String.valueOf(id_anh));
                            SendPicture_AsyncTask myAsyncTask = new SendPicture_AsyncTask(activity_Chuphinh.this,Danhba,Nam,Ky,Dot,SoMay,String.valueOf(id_anh),bitmap_send,false);
                            list_myAsyncTask.add(myAsyncTask);
                        }
                    }
                }
                /*
                data_Hinhanh data_hinhanh_khachhang = sqlite_hinhanh.Get_Table_ListHinhAnh(Danhba);
                List<data_Hinhanh.data_anh> list_anh = data_hinhanh_khachhang.getHinhanhs();
                int count = 0;
                for (data_Hinhanh.data_anh data_anh : list_anh) {
                    count++;
                    String sync_server = data_anh.getSync_anh();
                    if (!sync_server.equals("1")) {
                        String id_anh = String.valueOf(data_anh.getId());
                        String link_anh = data_anh.getAnh();
                        if(count == list_anh.size()) {
                            SendPicture_AsyncTask myAsyncTask = new SendPicture_AsyncTask(activity_Chuphinh.this,Danhba,Nam,Ky,Dot,SoMay,id_anh,link_anh,true);
                            list_myAsyncTask.add(myAsyncTask);
                        }else {
                            SendPicture_AsyncTask myAsyncTask = new SendPicture_AsyncTask(activity_Chuphinh.this,Danhba,Nam,Ky,Dot,SoMay,id_anh,link_anh,false);
                            list_myAsyncTask.add(myAsyncTask);
                        }

                    }

                }
                */
                for(int i =0 ; i < list_myAsyncTask.size() ; i++){
                    SendPicture_AsyncTask myAsyncTask = list_myAsyncTask.get(i);
                    if(i == list_myAsyncTask.size()-1){
                        myAsyncTask.setOn_dialog(true);
                    }
                    myAsyncTask.execute();
                }
               if(status_countdown) {
                   countDownTimer.start();
               }
            }


            //  GuiAnh();
        }catch (Exception e){}
    }

    CountDownTimer countDownTimer = new CountDownTimer(30000,1000) {
        @Override
        public void onTick(long millisUntilFinished) {

            List<Integer> list_status = new ArrayList<>();
            for(int i = 0 ;  i<list_myAsyncTask.size(); i++){
                SendPicture_AsyncTask myAsyncTask=list_myAsyncTask.get(i);
                list_status.add(myAsyncTask.Status_Send());
            }
            // kiemtra gui thanh cong
            boolean status_ok = true;
            for (int status : list_status){
                if(status==0 || status ==2){
                    status_ok = false;
                    break;
                }
            }


            // kiemtra gui loi
            boolean status_error = true;
            for (int status : list_status){
                if(status==2){

                }else {
                    status_error = false;
                    break;
                }
            }
            if(status_error){
                countDownTimer.cancel();
            }
            // kiemtra gui hoan thanh
            boolean status_finish = true;
            for (int status : list_status){
                if(status!=0){

                }else {
                    status_finish = false;
                    break;
                }
            }
            if(status_finish||status_ok){
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Display_HinhAnh();
                    }
                });
                countDownTimer.cancel();
            }
            Log.e("Picture",String.valueOf(status_finish)+","+String.valueOf(status_ok)+","+String.valueOf(status_error));

        }

        @Override
        public void onFinish() {
            countDownTimer.cancel();
        }
    };

}
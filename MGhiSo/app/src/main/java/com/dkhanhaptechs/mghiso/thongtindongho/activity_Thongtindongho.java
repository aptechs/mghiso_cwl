package com.dkhanhaptechs.mghiso.thongtindongho;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.dkhanhaptechs.mghiso.R;
import com.dkhanhaptechs.mghiso.Share.DanhSach_DocSo.sqlite_DanhSachKhackHang_DocSo;
import com.dkhanhaptechs.mghiso.Share.ThongTinChiTietDH.Api_Thongtinchitietdh;
import com.dkhanhaptechs.mghiso.Share.ThongTinChiTietDH.data_Thongtinchitetdh_response;
import com.dkhanhaptechs.mghiso.Share.ThongTinChiTietDH.data_Thongtinchitietdh_send;
import com.dkhanhaptechs.mghiso.Share.ThongTinChiTietDH.*;


import com.dkhanhaptechs.mghiso.Share.User.data_Http;
import com.dkhanhaptechs.mghiso.danhsachdocso_chitiet.data_Danhsachdocso_chitiet;
import com.dkhanhaptechs.mghiso.danhsachdocso_chitiet_khachhang.activity_Danhsachdocso_chitiet_khachhang;
import com.dkhanhaptechs.mghiso.danhsachdocso_chitiet_khachhang.data_danhsachdocso_chitiet_khachhang;
import com.dkhanhaptechs.mghiso.thongketieuthu.activity_Thongketieuthu;
import com.google.gson.Gson;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class activity_Thongtindongho extends AppCompatActivity {
    // BIEN LAYOUT
    private TextView tv_danhba, tv_sost, tv_hoten,tv_diachi;
    private ImageView imv_dongbo;
    private Toolbar toolbar ;
    private ListView lv_thongtinchitietdh ;
    private String Json_table_ghiso_chitiet = "";



    private ArrayList<data_Thongtinchitietdh> Arraylist_data_thongtinchitietdh;
    private adapter_Thongtinchitietdh Adapter_thongtinchitietdh;

    private String  So_st, Hoten, Diachi;
    private String Danhba, Nam, Ky, Dot, SoMay;
    private int Dongbo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thongtinchitietdhn);
        Mappping_Layout();

        Arraylist_data_thongtinchitietdh = new ArrayList<data_Thongtinchitietdh>();
        Adapter_thongtinchitietdh = new adapter_Thongtinchitietdh(activity_Thongtindongho.this,R.layout.dong_thongtinchitietdh,Arraylist_data_thongtinchitietdh);
        lv_thongtinchitietdh.setAdapter(Adapter_thongtinchitietdh);
        // GET DATA
        Json_table_ghiso_chitiet =getIntent().getStringExtra("THONGTINDONGHO");
        // SET DATA
        if(Get_Table_GhiSo_Chitiet(Json_table_ghiso_chitiet)){
            tv_danhba.setText(Danhba);
            tv_sost.setText(So_st);
            tv_hoten.setText(Hoten);
            tv_diachi.setText(Diachi);
            switch (Dongbo){
                case 2:{
                    // timeout
                    imv_dongbo.setImageResource(R.drawable.synced_2);
                    break;
                }
                case 1:{
                    // dang do bo
                    imv_dongbo.setImageResource(R.drawable.synced_1);
                    break;
                }
                case 0:{
                    // da dong bo
                    imv_dongbo.setImageResource(R.drawable.synced_0);
                    break;
                }
            }
        }
        Display_ThongTinDH_Chitiet();
    }

    private void Mappping_Layout() {
        tv_danhba =(TextView) findViewById(R.id.textview_danhba_thongtindongho);
        tv_sost = ( TextView) findViewById(R.id.textview_sost_thongtindongho);
        imv_dongbo =( ImageView) findViewById(R.id.imageView_dongbo_thongtindongho);
        tv_hoten = ( TextView) findViewById(R.id.textview_hoten_thongtindongho);
        tv_diachi = ( TextView) findViewById(R.id.textview_diachi_thongtindongho);
        lv_thongtinchitietdh = (ListView) findViewById(R.id.listview_thongtinchitietdh);
    }
    private boolean Get_Table_GhiSo_Chitiet(String json){
        boolean status = false;
        try{
            JSONObject table = new JSONObject(json);
            Danhba = table.getString("danhba");
            So_st = table.getString("so_st");
            Hoten = table.getString("hoten");
            Diachi = table.getString("diachi") ;
            String sync = table.getString("sync");
            Dongbo = Integer.parseInt(sync);
            Nam = table.getString("nam");
            Ky = table.getString("ky");
            Dot = table.getString("dot");

            status = true;
        }catch (JSONException e){

        }
        return  status;
    }
    private void Display_ThongTinDH_Chitiet(){
        String danhba = Danhba;
        Log.e("Danhba",Danhba);
        data_Thongtinchitietdh_send data_lichsuthaydh_send = new data_Thongtinchitietdh_send(danhba);
        Gson gson_request = new Gson();
        String json_send = gson_request.toJson(data_lichsuthaydh_send);
        Api_Thongtinchitietdh.get(activity_Thongtindongho.this, json_send,
                null,
                new TextHttpResponseHandler() {
                    @Override
                    public void onFailure(int statusCode,Header[] headers,String responseString,Throwable throwable) {
                        String text = "Lỗi lay thong tin chi tiet dh"+"\r\n"+"Vui lòng kiểm tra lại.";
                        Toast.makeText(activity_Thongtindongho.this,text,Toast.LENGTH_SHORT).show();
                        data_Http data_http = new data_Http(activity_Thongtindongho.this);
                        data_http.switch_Url_run();
                    }

                    @Override
                    public void onSuccess(int statusCode,Header[] headers,String responseString) {
                        Gson gson_reponse = new Gson();
                        data_Thongtinchitetdh_response data_thongtinchitetdh_response = gson_reponse.fromJson(responseString,data_Thongtinchitetdh_response.class);
                        Log.e("data_LSTDH_response",gson_reponse.toJson( data_thongtinchitetdh_response));
                        data_Thongtinchitetdh_response.data_thongtin_dhn []  all_data_thongtindh =data_thongtinchitetdh_response.getResult().getThongtin_dhn();
                        try{
                            Arraylist_data_thongtinchitietdh.clear();
                        }catch (Exception e){

                        }
                        for (int i =0 ; i < all_data_thongtindh.length ;i++){
                            try{
                                Object solenh = all_data_thongtindh[i].getSolenh();
                                Object ngaythay = all_data_thongtindh[i].getNgaythay();
                                Object chiso = all_data_thongtindh[i].getChiso();
                                Object bandoi  = all_data_thongtindh[i].getBandoi();
                                Object loaidh = all_data_thongtindh[i].getLoai_dh();
                                Object noidung = all_data_thongtindh[i].getNoidung();
                                if(solenh == null) {
                                    solenh = "";
                                }
                                if(ngaythay == null) {
                                    ngaythay = "";
                                }
                                if(chiso ==null) {
                                    chiso = "";
                                }
                                if(bandoi ==null) {
                                    bandoi = "";
                                }
                                if( loaidh == null) {
                                    loaidh = "";
                                }
                                if(noidung == null) {
                                    noidung = "";
                                }
                                data_Thongtinchitietdh data_thongtinchitietdh = new data_Thongtinchitietdh(i,
                                        solenh.toString(),ngaythay.toString(),chiso.toString(),bandoi.toString(),loaidh.toString(),noidung.toString());
                                Arraylist_data_thongtinchitietdh.add(data_thongtinchitietdh);
                            }catch (Exception e) {

                            }
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Adapter_thongtinchitietdh.notifyDataSetChanged();
                            }
                        });
                    }
               });
    }

}
package com.dkhanhaptechs.mghiso.thongtindongho;

class data_Thongtinchitietdh {
    private  int id;
    private String solenh ;
    private  String ngaythay;
    private  String cscu;
    private String bandoi ;
    private String loaidh ;
    private String noidung;

    public data_Thongtinchitietdh(int id,String solenh,String ngaythay,String cscu,String bandoi,String loaidh,String noidung) {
        this.id = id;
        this.solenh = solenh;
        this.ngaythay = ngaythay;
        this.cscu = cscu;
        this.bandoi = bandoi;
        this.loaidh = loaidh;
        this.noidung = noidung;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSolenh() {
        return solenh;
    }

    public void setSolenh(String solenh) {
        this.solenh = solenh;
    }

    public String getNgaythay() {
        return ngaythay;
    }

    public void setNgaythay(String ngaythay) {
        this.ngaythay = ngaythay;
    }

    public String getCscu() {
        return cscu;
    }

    public void setCscu(String cscu) {
        this.cscu = cscu;
    }

    public String getBandoi() {
        return bandoi;
    }

    public void setBandoi(String bandoi) {
        this.bandoi = bandoi;
    }

    public String getLoaidh() {
        return loaidh;
    }

    public void setLoaidh(String loaidh) {
        this.loaidh = loaidh;
    }

    public String getNoidung() {
        return noidung;
    }

    public void setNoidung(String noidung) {
        this.noidung = noidung;
    }
}

package com.dkhanhaptechs.mghiso.chuphinh;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;


import com.dkhanhaptechs.mghiso.R;
import com.github.chrisbanes.photoview.PhotoView;

import java.util.List;

public class adapter_Chupanh extends BaseAdapter {
    private Context Context_ ;
    private int Layout_ ;
    private List<data_Chupanh> List_data_chupanh ;
    public adapter_Chupanh(Context context, int layout, List<data_Chupanh> list_data_chupanh){
        this.Context_ = context;
        this.Layout_ = layout;
        this.List_data_chupanh = list_data_chupanh;
    }
    @Override
    public int getCount() {
        return List_data_chupanh.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
    private  class  Viewholder_data_chupanh{
        CheckBox cb_chon ;
        TextView tv_id ;
        PhotoView img_anh ;
        TextView tv_sync_anh ;

    }
    @Override
    public View getView(final int position,View convertView,ViewGroup parent) {
        final adapter_Chupanh.Viewholder_data_chupanh viewholder ;
        if(convertView == null){
            viewholder = new adapter_Chupanh.Viewholder_data_chupanh();
            LayoutInflater inflater = (LayoutInflater) Context_.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(Layout_,null);
            // Map
            viewholder.cb_chon = (CheckBox) convertView.findViewById(R.id.checkbox_chon_chupanh);
            viewholder.tv_id = (TextView) convertView.findViewById(R.id.textview_id_dongchuphinh);
            viewholder.img_anh =(PhotoView) convertView.findViewById(R.id.imageView_anh_dongchuphinh);
            viewholder.tv_sync_anh = (TextView) convertView.findViewById(R.id.textview_sync_dongchuphinh);
            //////////
            convertView.setTag(viewholder);
        }else {
            viewholder = (adapter_Chupanh.Viewholder_data_chupanh) convertView.getTag();
        }
        //
        viewholder.cb_chon.setFocusable(false);
        viewholder.cb_chon.setFocusableInTouchMode(false);

        // lay du lieu
        final data_Chupanh data_chupanh = List_data_chupanh.get(position);
        if(data_chupanh !=null && data_chupanh.getAnh() !=null) {
            viewholder.img_anh.setImageBitmap(data_chupanh.getAnh());
            // gan du lieu

            String id = String.valueOf(data_chupanh.getId());
            viewholder.tv_id.setText(id);
            //

            // xu ly du lieu
            boolean chon = data_chupanh.isSelete();
            if (!chon) {
                viewholder.cb_chon.setChecked(false);
            } else {
                viewholder.cb_chon.setChecked(true);
            }
            // chon
            viewholder.cb_chon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (viewholder.cb_chon.isChecked()) {
                        data_chupanh.setSelete(true);

                    } else {
                        data_chupanh.setSelete(false);
                    }
                    List_data_chupanh.set(position, data_chupanh);
                }
            });

            if (data_chupanh.getSync_anh().trim().equals("1")) {
                viewholder.tv_sync_anh.setText("Rồi");
            } else {
                viewholder.tv_sync_anh.setText("Chưa");
            }
        }
        return convertView;
    }
    private Bitmap StringToBitmap(String base64){
        byte[] decodedString = Base64.decode(base64, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        return decodedByte;
    }
}

package com.dkhanhaptechs.mghiso.thongketieuthu;

class data_Thongketieuthu {
    private int Id ;
    private String Datetime ;
    private String Code ;
    private String CSC;
    private String CSM;
    private String Tieuthu;
    private String Ghichu;
    private String Ghichuky;

    public data_Thongketieuthu(int id,String datetime,String code,String CSC,String CSM,String tieuthu,String ghichu,String ghichuky) {
        Id = id;
        Datetime = datetime;
        Code = code;
        this.CSC = CSC;
        this.CSM = CSM;
        Tieuthu = tieuthu;
        Ghichu = ghichu;
        this.Ghichuky = ghichuky;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getDatetime() {
        return Datetime;
    }

    public void setDatetime(String datetime) {
        Datetime = datetime;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public String getCSC() {
        return CSC;
    }

    public void setCSC(String CSC) {
        this.CSC = CSC;
    }

    public String getCSM() {
        return CSM;
    }

    public void setCSM(String CSM) {
        this.CSM = CSM;
    }

    public String getTieuthu() {
        return Tieuthu;
    }

    public void setTieuthu(String tieuthu) {
        Tieuthu = tieuthu;
    }

    public String getGhichu() {
        return Ghichu;
    }

    public void setGhichu(String ghichu) {
        Ghichu = ghichu;
    }

    public String getGhichuky() {
        return Ghichuky;
    }

    public void setGhichuky(String ghichuky) {
        Ghichuky = ghichuky;
    }
}

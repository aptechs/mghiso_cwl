package com.dkhanhaptechs.mghiso.mayin;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import com.dkhanhaptechs.mghiso.R;
import com.dkhanhaptechs.mghiso.Share.Dialog_App;
import com.starmicronics.starioextension.ICommandBuilder;
import com.starmicronics.starioextension.StarIoExt;
import com.woosim.printer.WoosimCmd;
import com.woosim.printer.WoosimImage;
import com.woosim.printer.WoosimService;
import com.dkhanhaptechs.mghiso.Share.Bluetooth.*;
import com.dkhanhaptechs.mghiso.Share.localizereceipts.*;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.util.List;

import static com.starmicronics.starioextension.StarIoExt.Emulation;


public class activity_Mayin extends AppCompatActivity {
    // Debugging
    private static final String TAG = "MainActivity";
    private static final boolean D = true;

    // Message types sent from the BluetoothPrintService Handler
    public static final int MESSAGE_DEVICE_NAME = 1;
    public static final int MESSAGE_TOAST = 2;
    public static final int MESSAGE_READ = 3;

    // Key names received from the BluetoothPrintService Handler
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";

    // Intent request codes
    private static final int REQUEST_CONNECT_DEVICE_SECURE = 1;
    private static final int REQUEST_CONNECT_DEVICE_INSECURE = 2;
    private static final int REQUEST_ENABLE_BT = 3;

    private static final int PERMISSION_DEVICE_SCAN_SECURE = 11;
    private static final int PERMISSION_DEVICE_SCAN_INSECURE = 12;

    // Layout Views
    private boolean mEmphasis = false;
    private boolean mUnderline = false;
    private int mCharsize = 1;
    private int mJustification = WoosimCmd.ALIGN_LEFT;

    // Local Bluetooth adapter
    private BluetoothAdapter mBluetoothAdapter = null;
    // Member object for the print services
    private BluetoothPrintService mPrintService = null;
    private WoosimService mWoosim = null;


    private Button btn_connectbt , btn_save, btn_testbt;
    private EditText edt_seriral;
    private CheckBox cb_sudung_woosim ;
    private CheckBox cb_sudung_star ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mayin);
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        Mappping_Layout();
        btn_connectbt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int permissionCheck = ContextCompat.checkSelfPermission(activity_Mayin.this, Manifest.permission.ACCESS_FINE_LOCATION);
                if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                    // Launch the DeviceListActivity to see devices and do scan
                     Intent serverIntent = new Intent(activity_Mayin.this, activity_DeviceList.class);
                    startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE_INSECURE);
                } else {
                    ActivityCompat.requestPermissions(activity_Mayin.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_DEVICE_SCAN_INSECURE);
                }
            }
        });

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String serial = edt_seriral.getText().toString().trim();
                if(!serial.equals("") ){
                    data_AddBluetooth data_addBluetooth_woosim = new data_AddBluetooth();
                    data_AddBluetooth data_addBluetooth_star = new data_AddBluetooth();
                    //current 0 ||"": chua cau hinh
                    // 1 : run
                    sqlite_AddBluetooth sqlite_addBluetooth = new sqlite_AddBluetooth(activity_Mayin.this);
                    if(sqlite_addBluetooth.getStatus_Table_AddrBluetooth_Exists()) {
                        if (cb_sudung_woosim.isChecked()) {
                            data_addBluetooth_woosim.setType("WOOSIM");
                            data_addBluetooth_woosim.setCurrent("1");
                            data_addBluetooth_woosim.setAdd(serial);
                        } else {
                            data_AddBluetooth data_addBluetooths = sqlite_addBluetooth.Get_Table_AddrBluetooth_Type("WOOSIM");
                            data_addBluetooth_woosim.setType("WOOSIM");
                            data_addBluetooth_woosim.setCurrent("0");
                            if(data_addBluetooths.getCurrent() != null && !data_addBluetooths.getAdd().isEmpty()) {
                                if (!data_addBluetooths.getAdd().isEmpty()) {
                                    data_addBluetooth_star.setAdd(data_addBluetooths.getAdd());
                                }else {
                                    data_addBluetooth_star.setAdd("");
                                }
                            }else {
                                data_addBluetooth_star.setAdd("");
                            }
                        }
                        if (cb_sudung_star.isChecked()) {
                            data_addBluetooth_star.setType("STAR");
                            data_addBluetooth_star.setCurrent("1");
                            data_addBluetooth_star.setAdd(serial);
                        } else {
                            data_AddBluetooth data_addBluetooths = sqlite_addBluetooth.Get_Table_AddrBluetooth_Type("STAR");
                            data_addBluetooth_star.setType("STAR");
                            data_addBluetooth_star.setCurrent("0");
                            if(data_addBluetooths.getCurrent() != null ) {
                                if (!data_addBluetooths.getAdd().isEmpty()) {
                                    data_addBluetooth_star.setAdd(data_addBluetooths.getAdd());
                                }else {
                                    data_addBluetooth_star.setAdd("");
                                }
                            }else {
                                data_addBluetooth_star.setAdd("");
                            }
                        }


                        sqlite_addBluetooth.Insert_Update_Table_AddrBluetooth(data_addBluetooth_woosim);
                        sqlite_addBluetooth.Insert_Update_Table_AddrBluetooth(data_addBluetooth_star);
                        Toast.makeText(activity_Mayin.this,"Bạn lưu thành công",Toast.LENGTH_SHORT).show();
                    }

                }else {
                    Dialog_App dialog_app = new Dialog_App(activity_Mayin.this);
                    String text_thongbao = "Vui lòng nhập số serial"+"\r\n"+" hoặc kết nối bluetôth để lấy.";
                    dialog_app.Dialog_Notification(text_thongbao);
                }
            }
        });
        btn_testbt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (cb_sudung_woosim.isChecked()) {
                        printReceipt();
                    } else if (cb_sudung_star.isChecked()) {
                        printReceipt_Star();
                    }
                }catch (Exception e){}
            }
        });
        cb_sudung_woosim.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
                if(isChecked){
                    cb_sudung_star.setChecked(false);
                    sqlite_AddBluetooth sqlite_addBluetooth = new sqlite_AddBluetooth(activity_Mayin.this);
                    if(sqlite_addBluetooth.getStatus_Table_AddrBluetooth_Exists()) {
                        data_AddBluetooth data_addBluetooths = sqlite_addBluetooth.Get_Table_AddrBluetooth_Type("WOOSIM");
                        if(data_addBluetooths.getCurrent() !=null ){
                            if(!data_addBluetooths.getAdd().isEmpty()) {
                                edt_seriral.setText(data_addBluetooths.getAdd());
                            }
                        }
                    }
                }
            }
        });
        cb_sudung_star.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
                if(isChecked){
                    cb_sudung_woosim.setChecked(false);
                    sqlite_AddBluetooth sqlite_addBluetooth = new sqlite_AddBluetooth(activity_Mayin.this);
                    if(sqlite_addBluetooth.getStatus_Table_AddrBluetooth_Exists()) {
                        data_AddBluetooth data_addBluetooths = sqlite_addBluetooth.Get_Table_AddrBluetooth_Type("STAR");
                        if(data_addBluetooths.getCurrent() !=null ){
                            if(!data_addBluetooths.getAdd().isEmpty()) {
                                edt_seriral.setText(data_addBluetooths.getAdd());
                            }
                        }
                    }
                }
            }
        });

        Display_serial();

    }
    private void  Mappping_Layout(){
        edt_seriral = (EditText) findViewById(R.id.edittext_serail_bluetooth);
        btn_connectbt = (Button) findViewById(R.id.button_connect_bluetooth);
        btn_testbt = (Button) findViewById(R.id.button_test_bluetooth);
        btn_save = ( Button) findViewById(R.id.button_save_bluetooth);
        cb_sudung_woosim = (CheckBox) findViewById(R.id.checkbox_sudung_woosim_bluetooth);
        cb_sudung_star = (CheckBox) findViewById(R.id.checkbox_sudung_star_bluetooth);
    }
    private void Display_serial(){
        sqlite_AddBluetooth sqlite_addBluetooth = new sqlite_AddBluetooth(activity_Mayin.this);
        if(sqlite_addBluetooth.getStatus_Table_AddrBluetooth_Exists()){
            List<data_AddBluetooth> data_addBluetooths = sqlite_addBluetooth.Get_All_Table_AddrBluetooth();
            for (data_AddBluetooth data_addBluetooth :data_addBluetooths){
                if(data_addBluetooth.getType().equals( "WOOSIM")){
                    if(data_addBluetooth.getCurrent().trim().equals("1")){
                        edt_seriral.setText(data_addBluetooth.getAdd().trim());
                        cb_sudung_woosim.setChecked(true);
                        break;
                    }
                }else if(data_addBluetooth.getType().equals( "STAR")){
                    if(data_addBluetooth.getCurrent().trim().equals("1")){
                        edt_seriral.setText(data_addBluetooth.getAdd().trim());
                        cb_sudung_star.setChecked(true);
                        break;
                    }
                }
            }
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,@NonNull String[] permissions,@NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_DEVICE_SCAN_SECURE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(this, activity_DeviceList.class);
                    startActivityForResult(intent, REQUEST_CONNECT_DEVICE_SECURE);
                }
                break;
            case PERMISSION_DEVICE_SCAN_INSECURE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(this, activity_DeviceList.class);
                    startActivityForResult(intent, REQUEST_CONNECT_DEVICE_INSECURE);
                }
                break;
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(D) Log.d(TAG, "onActivityResult " + resultCode);
        switch (requestCode) {
            case REQUEST_CONNECT_DEVICE_SECURE:
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    connectDevice(data, true);
                }
                break;
            case REQUEST_CONNECT_DEVICE_INSECURE:
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    connectDevice(data, false);
                }
                break;
            case REQUEST_ENABLE_BT:
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    // Bluetooth is now enabled, so set up a print
                    setupPrint();
                } else {
                    // User did not enable Bluetooth or an error occurred
                    if(D) Log.d(TAG, "BT not enabled");
                    Toast.makeText(this, R.string.bt_not_enabled_leaving, Toast.LENGTH_SHORT).show();
                    finish();
                }
        }
    }
    @Override
    public void onStart() {
        super.onStart();
        if(D) Log.i(TAG, "++ ON START ++");

        // If BT is not on, request that it be enabled.
        // setupPrint() will then be called during onActivityResult
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
            // Otherwise, setup the chat session
        } else {
            if (mPrintService == null)  setupPrint();
        }
    }

    @Override
    public synchronized void onResume() {
        super.onResume();
      //  if(D) Log.i(TAG, "+ ON RESUME +");

        // Performing this check in onResume() covers the case in which BT was
        // not enabled during onStart(), so we were paused to enable it...
        // onResume() will be called when ACTION_REQUEST_ENABLE activity returns.
        if (mPrintService != null) {
            // Only if the state is STATE_NONE, do we know that we haven't started already
            if (mPrintService.getState() == BluetoothPrintService.STATE_NONE) {
                // Start the Bluetooth print services
                mPrintService.start();
            }
        }
    }

    @Override
    protected void onDestroy() {
        if (mPrintService != null) {
            mPrintService.stop();
        }
        super.onDestroy();
    }

    private void setupPrint() {
        /*
        Spinner spinner = findViewById(R.id.spn_charsize);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                this, R.array.char_size_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        if (spinner != null) {
            spinner.setAdapter(adapter);
            spinner.setOnItemSelectedListener(
                    new AdapterView.OnItemSelectedListener() {
                        public void onItemSelected(AdapterView<?> parent,View view,int position,long id) {
                            if (position == 1) mCharsize = 2;
                            else if (position == 2) mCharsize = 3;
                            else if (position == 3) mCharsize = 4;
                            else if (position == 4) mCharsize = 5;
                            else if (position == 5) mCharsize = 6;
                            else if (position == 6) mCharsize = 7;
                            else if (position == 7) mCharsize = 8;
                            else mCharsize = 1;
                        }
                        public void onNothingSelected(AdapterView<?> parent) { }
                    }
            );
        }

         */
        mCharsize = 3;


        // Initialize the BluetoothPrintService to perform bluetooth connections
        mPrintService = new BluetoothPrintService(mHandler);
        mWoosim = new WoosimService(mHandler);
    }
    private final MyHandler mHandler = new MyHandler(activity_Mayin.this);

    private static class MyHandler extends Handler {
        private final WeakReference<activity_Mayin> mActivity;

        MyHandler(activity_Mayin activity) {
            mActivity = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            activity_Mayin activity = mActivity.get();
            if (activity != null) {
                activity.handleMessage(msg);
            }
        }
    }
    private void handleMessage(Message msg) {
        switch (msg.what) {
            case MESSAGE_DEVICE_NAME:
                // save the connected device's name
                String mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
                Toast.makeText(getApplicationContext(), "Connected to " + mConnectedDeviceName, Toast.LENGTH_SHORT).show();
                redrawMenu();
                break;
            case MESSAGE_TOAST:
                Toast.makeText(getApplicationContext(), msg.getData().getInt(TOAST), Toast.LENGTH_SHORT).show();
                break;
            case MESSAGE_READ:
                mWoosim.processRcvData((byte[])msg.obj, msg.arg1);
                break;
            case WoosimService.MESSAGE_PRINTER:
                if (msg.arg1 == WoosimService.MSR) {
                    if (msg.arg2 == 0) {
                        Toast.makeText(getApplicationContext(), "MSR reading failure", Toast.LENGTH_SHORT).show();
                    } else {
                        byte[][] track = (byte[][]) msg.obj;
                        if (track[0] != null) {
                            String str = new String(track[0]);
                           // mTrack1View.setText(str);
                        }
                        if (track[1] != null) {
                            String str = new String(track[1]);
                          //  mTrack2View.setText(str);
                        }
                        if (track[2] != null) {
                            String str = new String(track[2]);
                           // mTrack3View.setText(str);
                        }
                    }
                }
                break;
        }
    }
    private void redrawMenu() {
        /*
      //  MenuItem itemSecureConnect = mMenu.findItem(R.id.secure_connect_scan);
      //  MenuItem itemInsecureConnect = mMenu.findItem(R.id.insecure_connect_scan);
      //  MenuItem itemDisconnect = mMenu.findItem(R.id.disconnect);

        // Context sensitive option menu
        if (mPrintService.getState() != BluetoothPrintService.STATE_CONNECTED) {
            if (!itemSecureConnect.isVisible()) itemSecureConnect.setVisible(true);
            if (!itemInsecureConnect.isVisible()) itemInsecureConnect.setVisible(true);
            if (itemDisconnect.isVisible()) itemDisconnect.setVisible(false);
        } else {
            if (itemSecureConnect.isVisible()) itemSecureConnect.setVisible(false);
            if (itemInsecureConnect.isVisible()) itemInsecureConnect.setVisible(false);
            if (!itemDisconnect.isVisible()) itemDisconnect.setVisible(true);
        }

         */
    }
    private void connectDevice(Intent data, boolean secure) {
         String  address = null;
        // Get the device MAC address
        if (data.getExtras() != null)
            address = data.getExtras().getString(activity_DeviceList.EXTRA_DEVICE_ADDRESS);

                edt_seriral.setText(address.toString().trim());


        // Get the BLuetoothDevice object
        BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        // Attempt to connect to the device
        mPrintService.connect(device, secure);
    }

    private void sendData(byte[] data) {
        // Check that we're actually connected before trying printing
        if (mPrintService.getState() != BluetoothPrintService.STATE_CONNECTED) {
            Toast.makeText(this, R.string.not_connected, Toast.LENGTH_SHORT).show();
            return;
        }
        // Check that there's actually something to send
        if (data.length > 0)
            mPrintService.write(data);
    }

    /**
     * On click function for sample print button.
     */
    public void printReceipt() {
        InputStream inStream = getResources().openRawResource(R.raw.receipt2);
        sendData(WoosimCmd.initPrinter());
        try {
            byte[] data = new byte[inStream.available()];
            while (inStream.read(data) != -1)
            {
                sendData(data);
            }
        } catch (IOException e) {
            Log.e(TAG, "sample 2inch receipt print fail.", e);
        } finally {
            try {
                inStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void printImage(View v) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;
        Bitmap bmp = BitmapFactory.decodeResource(getResources(), R.drawable.logo, options);
        if (bmp == null) {
            Log.e(TAG, "resource decoding is failed");
            return;
        }
        byte[] data = WoosimImage.printBitmap(0, 0, 384, 200, bmp);
        bmp.recycle();

        sendData(WoosimCmd.setPageMode());
        sendData(data);
        sendData(WoosimCmd.PM_setStdMode());
    }


    // STAR
    private void printReceipt_Star(){
        int     mModelIndex =12 ;
        String  mMacAddress =edt_seriral.getText().toString().trim();
        String  mPortName ="BT:"+mMacAddress;
        String  mPortSettings ="Portable";
        String  mModelName = "STAR L200-00001";
        boolean mCashDrawerOpenActiveHigh = true;
        int     mPaperSize = 384;
        PrinterSettings       settings       = new PrinterSettings(mModelIndex,mPortName,mPortSettings,mMacAddress,mModelName,mCashDrawerOpenActiveHigh,mPaperSize);
        Emulation emulation = ModelCapability.getEmulation(settings.getModelIndex());

        byte [] data_print = createTextReceiptData(emulation);
        sendData(data_print);
    }
    public static byte[] createTextReceiptData(Emulation emulation) {
        ICommandBuilder builder = StarIoExt.createCommandBuilder(emulation);

        builder.beginDocument();
        builder.appendAlignment(ICommandBuilder.AlignmentPosition.Center);
        builder.appendFontStyle(ICommandBuilder.FontStyleType.A);
        builder.append(("CÔNG TY CỔ PHẦN CẤP NƯỚC CHỢ LỚN\r\n" +
                "97 Phạm Hữu Chí P.12 Q.5\r\n" +
                "Tổng đài : 0865.851.088\r\n" +
                "-------------------\r\n " +
                "Ngày gửi giấy báo : 01/03/2021\r\n").getBytes());
        builder.appendMultiple(("GIẤY BÁO TIỀN NƯỚC\r\n" +
                "(KHÔNG THAY THẾ HÓA ĐƠN)\r\n").getBytes(),1,2);

        builder.appendAlignment(ICommandBuilder.AlignmentPosition.Left);
        builder.append(("Kỳ: 3/2021\r\n".getBytes()));

        builder.append(("Từ ngày: 28/1/2021 -> ".getBytes()));
        builder.append(("1/3/2021\r\n".getBytes()));

        builder.appendAbsolutePosition(("Danh bạ:").getBytes(),0);
        builder.appendAbsolutePosition(200);
        builder.appendMultiple(("0610 851 0032\r\n").getBytes(),1,2);

        builder.appendAlignment(ICommandBuilder.AlignmentPosition.Left);
        builder.append(("KH: NGUYEN THI TUYET\r\n".getBytes()));

        builder.append(("Đ/C: 130/68 NGUYỄN VĂN LƯỢNG\r\n".getBytes()));

        builder.appendAbsolutePosition(("GB: 11").getBytes(),0);
        builder.appendAbsolutePosition(("ĐM: 36").getBytes(),128);
        builder.appendAbsolutePosition(("Code: 4\r\n").getBytes(),256);

        builder.appendAlignment(ICommandBuilder.AlignmentPosition.Center);
        builder.append(("-------------------------------\"\r\n".getBytes()));

        builder.appendAlignment(ICommandBuilder.AlignmentPosition.Left);
        builder.appendAbsolutePosition(("CS mới: 3448 - ").getBytes(),0);
        builder.appendAbsolutePosition(("CS cũ: 3310\r\n").getBytes(),200);

        builder.appendAlignment(ICommandBuilder.AlignmentPosition.Right);
        String value_tieuthu = "138 m3 ";
        builder.append(("Tiêu thụ:").getBytes());
        int lenght = 32 - value_tieuthu.length();
        builder.appendAbsolutePosition(lenght*12);
        builder.appendMultiple((value_tieuthu+"\r\n").getBytes(),1,2);

        String value_tiennuoc = "1.587.000 đ ";
        builder.append(("Tiền nước: ").getBytes());
        lenght = 32 - value_tiennuoc.length();
        builder.appendAbsolutePosition(lenght*12);
        builder.append((value_tiennuoc+"\r\n").getBytes());

        String value_tienthue = "79.350 đ ";
        builder.append(("Tiền thuế:").getBytes());
        lenght = 32 - value_tienthue.length();
        builder.appendAbsolutePosition(lenght*12);
        builder.append((value_tienthue+"\r\n").getBytes());

        String value_pjibvmt = "158.700 đ ";
        builder.append(("Phí BVMT:").getBytes());
        lenght = 32 - value_pjibvmt.length();
        builder.appendAbsolutePosition(lenght*12);
        builder.append((value_pjibvmt+"\r\n").getBytes());

        String value_tong = "1.825.050 đ ";
        builder.append(("Tổng cộng:").getBytes());
        lenght = 32 - value_tong.length();
        builder.appendAbsolutePosition(lenght*12);
        builder.appendMultiple((value_tong+"\r\n").getBytes(),1,2);

        builder.appendAlignment(ICommandBuilder.AlignmentPosition.Center);
        builder.append(("-------------------------------\"\r\n".getBytes()));

        builder.appendAlignment(ICommandBuilder.AlignmentPosition.Left);
        builder.appendAbsolutePosition(("Nhân viên: ").getBytes(),0);
        builder.appendAbsolutePosition(("TẠ HỮU PHƯỚC\r\n").getBytes(),200);


        builder.appendAlignment(ICommandBuilder.AlignmentPosition.Left);
        builder.append(("Zalo:".getBytes()));
        builder.appendAbsolutePosition(200);
        builder.appendMultiple(("0783672187\r\n").getBytes(),1,2);

        builder.appendAlignment(ICommandBuilder.AlignmentPosition.Center);
        builder.append((("Quý khách vui lòng thanh toán\r\b" +
                         "trong vòng 07 ngày kể từ ngày:\r\n" ).getBytes()));
        builder.appendFontStyle(ICommandBuilder.FontStyleType.B);

        builder.appendMultiple((" 04/03/2021\r\n").getBytes(),2,1);
        builder.appendFontStyle(ICommandBuilder.FontStyleType.A);
        builder.append((("để biết thông tin chi tiết tiền\r\n" +
                         "nước. Quý khách vui lòng liên \r\n" ).getBytes()));
        builder.appendAbsolutePosition(("hệ tổng đài: ").getBytes(),0);
        builder.appendFontStyle(ICommandBuilder.FontStyleType.B);
        builder.appendMultiple(("0865.851.088\r\n").getBytes(),2,1) ;
        builder.appendFontStyle(ICommandBuilder.FontStyleType.A);
        builder.append((  "hoặc truy cập vào website:\r\n" ).getBytes());
        builder.appendFontStyle(ICommandBuilder.FontStyleType.B);
        builder.appendMultiple(("http://capnuoccholon.com.vn\r\n").getBytes(),2,1);
        builder.appendFontStyle(ICommandBuilder.FontStyleType.A);
        builder.append((  "ứng dụng trên thiết bị di động\r\n" +
                "Android hoặc IOS.\r\n").getBytes());
        builder.append(("-------------------------------\"\r\n".getBytes()));
        builder.appendAlignment(ICommandBuilder.AlignmentPosition.Left);
        builder.append(("Lưu ý:\r\n" +
                "1. Nếu số điẹn thoại khách hàng\r\n").getBytes());
        builder.appendFontStyle(ICommandBuilder.FontStyleType.B);
        builder.appendMultiple(("0772649486 \r\n").getBytes(),1,2) ;
        builder.appendFontStyle(ICommandBuilder.FontStyleType.A);
        builder.append((       "không dùng xin vui lòng đang ký\r\n" +
                "lại qua số tổng đài : \r\n" ).getBytes());
        builder.appendFontStyle(ICommandBuilder.FontStyleType.B);
        builder.appendMultiple(("0865.851.088\r\n").getBytes(),2,1);

        //  localizeReceipts.appendTextReceiptData(builder, utf8);
        //  localizeReceipts.appendte

        builder.appendCutPaper(ICommandBuilder.CutPaperAction.PartialCutWithFeed);

        builder.endDocument();

        return builder.getCommands();
    }


}
package com.dkhanhaptechs.mghiso.thongketieuthu;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


import com.dkhanhaptechs.mghiso.R;


import java.util.List;

class adapter_Thongketieuthu extends BaseAdapter {
    private Context Context_ ;
    private int Layout_ ;
    private List<data_Thongketieuthu> List_data_thongketieuthu_ ;
    public adapter_Thongketieuthu(Context context, int layout, List<data_Thongketieuthu> list_data_thongketieuthu){
        this.Context_ = context;
        this.Layout_ = layout;
        this.List_data_thongketieuthu_ = list_data_thongketieuthu;
    }
    @Override
    public int getCount() {
        return List_data_thongketieuthu_.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    private  class  Viewholder_data_thongketieuthu{
        TextView tv_datetime ;
        TextView tv_code ;
        TextView tv_csc ;
        TextView tv_csm ;
        TextView tv_tieuthu ;
        TextView tv_ghichu;
        TextView tv_ghichuky;
        Button btn_in ;

    }
    @Override
    public View getView(int position,View convertView,ViewGroup parent) {
        adapter_Thongketieuthu.Viewholder_data_thongketieuthu viewholder ;
        if(convertView == null){
            viewholder = new adapter_Thongketieuthu.Viewholder_data_thongketieuthu();
            LayoutInflater inflater = (LayoutInflater) Context_.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(Layout_,null);
            // Map
            viewholder.tv_datetime = (TextView) convertView.findViewById(R.id.textview_datetime_dongthongketieuthu);
            viewholder.tv_code = (TextView) convertView.findViewById(R.id.textview_code_dongthongketieuthu);
            viewholder.tv_csc = (TextView) convertView.findViewById(R.id.textview_chisocu_dongthongketieuthu);
            viewholder.tv_csm = (TextView) convertView.findViewById(R.id.textview_chisomoi_dongthongketieuthu);
            viewholder.tv_tieuthu = (TextView) convertView.findViewById(R.id.textview_tieuthu_dongthongketieuthu);
            viewholder.tv_ghichu = (TextView) convertView.findViewById(R.id.textview_ghichu_dongthongketieuthu);
            viewholder.tv_ghichuky = (TextView) convertView.findViewById(R.id.textview_ghichuky_dongthongketieuthu);
        //    viewholder.btn_in =(Button) convertView.findViewById(R.id.button_in_dongthongketieuthu);
            //////////
            convertView.setTag(viewholder);
        }else {
            viewholder = (adapter_Thongketieuthu.Viewholder_data_thongketieuthu) convertView.getTag();
        }
        // GET DATA
        data_Thongketieuthu data_thongketieuthu = List_data_thongketieuthu_.get(position);
        // SET DATA
        viewholder.tv_datetime.setText(data_thongketieuthu.getDatetime());
        viewholder.tv_code.setText(data_thongketieuthu.getCode());
        viewholder.tv_csc.setText(data_thongketieuthu.getCSC());
        viewholder.tv_csm.setText(data_thongketieuthu.getCSM());
        viewholder.tv_tieuthu.setText(data_thongketieuthu.getTieuthu());
        viewholder.tv_ghichu.setText(data_thongketieuthu.getGhichu().trim());
        viewholder.tv_ghichuky.setText(data_thongketieuthu.getGhichuky().trim());
        /*
        viewholder.btn_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
*/

        return convertView;
    }
}

package com.dkhanhaptechs.mghiso.thongtindongho;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.dkhanhaptechs.mghiso.R;


import java.util.List;

class adapter_Thongtinchitietdh extends BaseAdapter {
    private Context Context_ ;
    private int Layout_ ;
    private List<data_Thongtinchitietdh> List_data_thongtinchitietdh ;
    public adapter_Thongtinchitietdh(Context context, int layout, List<data_Thongtinchitietdh> list_data_thongtinchitietdh){
        this.Context_ = context;
        this.Layout_ = layout;
        this.List_data_thongtinchitietdh = list_data_thongtinchitietdh;
    }
    @Override
    public int getCount() {
        return List_data_thongtinchitietdh.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
    private  class  Viewholder_data_thongtinchitietdh{
        TextView tv_solenh ;
        TextView tv_ngaythay ;
        TextView tv_csc ;
        TextView tv_bandoi ;
        TextView tv_loaidh;
        TextView tv_noidung ;

    }
    @Override
    public View getView(int position,View convertView,ViewGroup parent) {
        Viewholder_data_thongtinchitietdh viewholder ;
        if(convertView == null){
            viewholder = new Viewholder_data_thongtinchitietdh();
            LayoutInflater inflater = (LayoutInflater) Context_.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(Layout_,null);
            // Map
            viewholder.tv_solenh = (TextView) convertView.findViewById(R.id.textview_solenh_thongtinchitietdh);
            viewholder.tv_ngaythay = (TextView) convertView.findViewById(R.id.textview_ngaythay_thongtinchitietdh);
            viewholder.tv_csc = (TextView) convertView.findViewById(R.id.textview_chiso_thongtinchitietdh);
            viewholder.tv_bandoi = (TextView) convertView.findViewById(R.id.textview_bandoi_thongtinchitietdh);
            viewholder.tv_loaidh = (TextView) convertView.findViewById(R.id.textview_loaidh_thongtinchitietdh);
            viewholder.tv_noidung = (TextView) convertView.findViewById(R.id.textview_noidung_thongtinchitietdh);

            //////////
            convertView.setTag(viewholder);
        }else {
            viewholder = (adapter_Thongtinchitietdh.Viewholder_data_thongtinchitietdh) convertView.getTag();
        }
        // GET DATA
        data_Thongtinchitietdh data_thongtinchitietdh = List_data_thongtinchitietdh.get(position);

        // SET DATA
        viewholder.tv_solenh.setText(data_thongtinchitietdh.getSolenh().toString());
        viewholder.tv_ngaythay.setText(data_thongtinchitietdh.getNgaythay());
        viewholder.tv_csc.setText(data_thongtinchitietdh.getCscu());
        viewholder.tv_bandoi.setText(data_thongtinchitietdh.getBandoi());
        viewholder.tv_loaidh.setText(data_thongtinchitietdh.getLoaidh());
        viewholder.tv_noidung.setText(data_thongtinchitietdh.getNoidung());

        return convertView;
    }
}

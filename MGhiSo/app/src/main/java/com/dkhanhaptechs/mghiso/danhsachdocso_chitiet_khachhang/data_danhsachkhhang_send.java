package com.dkhanhaptechs.mghiso.danhsachdocso_chitiet_khachhang;

public class data_danhsachkhhang_send {

        private  String DanhBa ;
        private  String HoTen;
        private  String SoNha;
        private  String SoNhaMoi;
        private  String TinhTrangChi;
        private  String ViTriDHN;
        private  String dienthoai1;
        private  String dienthoai2 ;
        private String code;
        private String csmoi;
        private  String TinhTrang;
        private  String TieuThu;
        private  String ghichu1 ;
        private  String ghichu2;
        private  String Lat;
        private  String Lng ;

        public data_danhsachkhhang_send(String danhBa,String hoTen,String soNha,String soNhaMoi,
                              String tinhTrangChi,String viTriDHN,String dienthoai1,
                              String dienthoai2,String code,String csmoi,String tinhTrang,
                              String tieuThu,String ghichu1,String ghichu2,String lat,String lng) {
            DanhBa = danhBa;
            HoTen = hoTen;
            SoNha = soNha;
            SoNhaMoi = soNhaMoi;
            TinhTrangChi = tinhTrangChi;
            ViTriDHN = viTriDHN;
            this.dienthoai1 = dienthoai1;
            this.dienthoai2 = dienthoai2;
            this.code = code;
            this.csmoi = csmoi;
            TinhTrang = tinhTrang;
            TieuThu = tieuThu;
            this.ghichu1 = ghichu1;
            this.ghichu2 = ghichu2;
            Lat = lat;
            Lng = lng;
        }

        public String getDanhBa() {
            return DanhBa;
        }

        public void setDanhBa(String danhBa) {
            DanhBa = danhBa;
        }

        public String getHoTen() {
            return HoTen;
        }

        public void setHoTen(String hoTen) {
            HoTen = hoTen;
        }

        public String getSoNha() {
            return SoNha;
        }

        public void setSoNha(String soNha) {
            SoNha = soNha;
        }

        public String getSoNhaMoi() {
            return SoNhaMoi;
        }

        public void setSoNhaMoi(String soNhaMoi) {
            SoNhaMoi = soNhaMoi;
        }

        public String getTinhTrangChi() {
            return TinhTrangChi;
        }

        public void setTinhTrangChi(String tinhTrangChi) {
            TinhTrangChi = tinhTrangChi;
        }

        public String getViTriDHN() {
            return ViTriDHN;
        }

        public void setViTriDHN(String viTriDHN) {
            ViTriDHN = viTriDHN;
        }

        public String getDienthoai1() {
            return dienthoai1;
        }

        public void setDienthoai1(String dienthoai1) {
            this.dienthoai1 = dienthoai1;
        }

        public String getDienthoai2() {
            return dienthoai2;
        }

        public void setDienthoai2(String dienthoai2) {
            this.dienthoai2 = dienthoai2;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getCsmoi() {
            return csmoi;
        }

        public void setCsmoi(String csmoi) {
            this.csmoi = csmoi;
        }

        public String getTinhTrang() {
            return TinhTrang;
        }

        public void setTinhTrang(String tinhTrang) {
            TinhTrang = tinhTrang;
        }

        public String getTieuThu() {
            return TieuThu;
        }

        public void setTieuThu(String tieuThu) {
            TieuThu = tieuThu;
        }

        public String getGhichu1() {
            return ghichu1;
        }

        public void setGhichu1(String ghichu1) {
            this.ghichu1 = ghichu1;
        }

        public String getGhichu2() {
            return ghichu2;
        }

        public void setGhichu2(String ghichu2) {
            this.ghichu2 = ghichu2;
        }

        public String getLat() {
            return Lat;
        }

        public void setLat(String lat) {
            Lat = lat;
        }

        public String getLng() {
            return Lng;
        }

        public void setLng(String lng) {
            Lng = lng;
        }

}

package com.dkhanhaptechs.mghiso.Share.Location_Device;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.dkhanhaptechs.mghiso.dangnhap.activity_DangNhap;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

public class get_Location_Device  {
    private FusedLocationProviderClient fusedLocationProviderClient;
    private String lat;
    private String lng ;
    private Context mcontext;

    public get_Location_Device(Context context) {
        this.mcontext =context ;
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(mcontext);
        if (ActivityCompat.checkSelfPermission(mcontext,Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mcontext,Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        fusedLocationProviderClient.getLastLocation().addOnSuccessListener((Activity) mcontext,new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if(location!=null){
                    Log.e("Location","lat:" +String.valueOf(location.getLatitude()) +"\r\n"+"Lng:" +String.valueOf(location.getLongitude()));
                    lat =String.valueOf(location.getLatitude()) ;
                    lng =String.valueOf(location.getLongitude()) ;
                }
            }
        });
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }
}

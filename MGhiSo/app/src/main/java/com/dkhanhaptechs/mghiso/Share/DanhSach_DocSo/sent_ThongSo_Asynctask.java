package com.dkhanhaptechs.mghiso.Share.DanhSach_DocSo;

import android.content.Context;
import android.os.AsyncTask;

import com.dkhanhaptechs.mghiso.Share.Dialog_App;
import com.loopj.android.http.HttpGet;

import java.net.URI;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.utils.URIBuilder;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import cz.msebera.android.httpclient.util.EntityUtils;
import  com.dkhanhaptechs.mghiso.Share.User.*;

public  class sent_ThongSo_Asynctask extends AsyncTask<Void,Integer,Void> {

    Context contextParent;
    Dialog_App dialog_app ;
    private String Nam;
    private String Ky;
    private String Dot;
    private String Danhba;
    private String SoMay;
    private Boolean Status_Send =false;
    private String Type;
    private String Thamso1;
    private String Thamso2;
    private String BASE_URL_API ="/apikhachhang/api/DocSoNhanMotThamSo?";
    private String BASE_URL = "";
    public sent_ThongSo_Asynctask(Context contextParent,String nam,String ky,String dot,String danhba,String soMay,String type,String thamso1,String thamso2) {
        this.contextParent = contextParent;
        Nam = nam;
        Ky = ky;
        Dot = dot;
        Danhba = danhba;
        SoMay = soMay;
        dialog_app = new Dialog_App(contextParent);
        Type = type;
        Thamso1 =thamso1;
        if(Thamso1.isEmpty()){
            Thamso1 ="-";
        }
        Thamso2 =thamso2;
        if(Thamso2.isEmpty()){
            Thamso2 ="-";
        }
        data_Http data_http = new data_Http(contextParent);
            String url_run = data_http.get_Url_run();
            if(!url_run.isEmpty()){
                BASE_URL = url_run +BASE_URL_API;
            }else {
                BASE_URL = "http://viettel.capnuoccholon.com.vn"+BASE_URL_API;
            }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog_app.Dialog_Progress_Wait_Open("Đang gửi dữ liệu","Vui lòng chờ");
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        dialog_app.Dialog_Progress_Wait_Close();

        String notification = "";
            if(Status_Send) {
                if(Type.equals("diachimoi")){
                    notification = "Đồng bộ địa chỉ mới thành công." + "\r\n" + "Danh bạ: " + Danhba;
                } else if(Type.equals("ghichu")){
                    notification = "Đồng bộ ghi chú thành công." + "\r\n" + "Danh bạ: " + Danhba;
                }else if(Type.equals("sodienthoai")){
                    notification = "Đồng bộ điện thoại thành công." + "\r\n" + "Danh bạ: " + Danhba;
                }
            }else {
                if(Type.equals("diachimoi")){
                    notification = "Đồng bộ địa chỉ mới lỗi." + "\r\n" + "Danh bạ: " + Danhba+"\r\n"+"Vui lòng kiểm tra lại server";
                } else if(Type.equals("ghichu")){
                    notification = "Đồng bộ ghi chú lỗi." + "\r\n" + "Danh bạ: " + Danhba+"\r\n"+"Vui lòng kiểm tra lại server";
                }else if(Type.equals("sodienthoai")){
                    notification = "Đồng bộ điện thoại lỗi." + "\r\n" + "Danh bạ: " + Danhba+"\r\n"+"Vui lòng kiểm tra lại server";
                }
            }
        dialog_app.Dialog_Notification(notification);

    }
    @Override
    protected Void doInBackground(Void... voids) {
        try {
            HttpClient httpclient = new DefaultHttpClient();
            String [] com_may = SoMay.split("_");
            URI uri_ = new URIBuilder(BASE_URL)
                    .addParameter("loai",Type)
                    .addParameter("manv",com_may[0])
                    .addParameter("nam",Nam)
                    .addParameter("ky",Ky)
                    .addParameter("thamso1",Thamso1.toUpperCase())
                    .addParameter("thamso2",Thamso2.toLowerCase())
                    .addParameter("Danhba",Danhba)
                    .build();

            HttpGet httpget = new HttpGet(uri_);
            //  httppost.setHeader("Accept", "application/json");
            httpget.setHeader("Content-type","text/plain;charset=utf-8");
            //     httpget.setEntity(stringEntity_send);
            HttpResponse response = httpclient.execute(httpget);
            if (response.getStatusLine().getStatusCode() == 200) {
                String responseString = EntityUtils.toString(response.getEntity());
                String result = responseString.trim().replace("\"","");
                if(result.trim().equals("1")){
                    Status_Send = true;
                }
            }
        }catch (Exception e){

        }
        return null;
    }
}

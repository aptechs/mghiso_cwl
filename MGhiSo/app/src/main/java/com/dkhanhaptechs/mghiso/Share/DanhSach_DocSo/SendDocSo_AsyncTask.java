package com.dkhanhaptechs.mghiso.Share.DanhSach_DocSo;


import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;

import com.dkhanhaptechs.mghiso.Share.Dialog_App;

import com.dkhanhaptechs.mghiso.Share.HinhAnh.sqlite_Hinhanh;
import com.dkhanhaptechs.mghiso.Share.SyncGhiSo.data_syncghiso_response;
import com.dkhanhaptechs.mghiso.Share.SyncGhiSo.data_syncghiso_send;

import com.dkhanhaptechs.mghiso.danhsachdocso_chitiet_khachhang.activity_Danhsachdocso_chitiet_khachhang;
import com.google.gson.Gson;


import java.io.IOException;

import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;


import cz.msebera.android.httpclient.HttpResponse;

import cz.msebera.android.httpclient.client.HttpClient;

import cz.msebera.android.httpclient.client.methods.HttpGet;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.client.utils.URIBuilder;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;

import cz.msebera.android.httpclient.util.EntityUtils;
import  com.dkhanhaptechs.mghiso.Share.User.*;

public class SendDocSo_AsyncTask extends AsyncTask<Void,Integer,Void> {
    Context contextParent;
    Dialog_App dialog_app ;
    private String Nam;
    private String Ky;
    private String Dot;
    private String Danhba;
    private String SoMay;
    Dialog_App dialog_app_notification ;
    private Boolean Status_Send =false;
    private String Type;
    private String All_ok ="";
    private String All_erro ="";
    private String Erro ="";
    private String Erro_Ttruong ="";
    private String BASE_URL_API_1 ="/apikhachhang/api/DocSoKiemTraToTruongDieuChinh?";
    private String BASE_URL_1 ="";
    private String BASE_URL_API_2 ="/apikhachhang/api/DocSoNhanThongTinNhieuKhachHang?";
    private String BASE_URL_2 ="";
    public SendDocSo_AsyncTask(Context contextParent,String nam,String ky,String dot,String danhba,String soMay,String type) {
        this.contextParent = contextParent;
        Nam = nam;
        Ky = ky;
        Dot = dot;
        Danhba = danhba;
        SoMay = soMay;
        dialog_app = new Dialog_App(contextParent);
        dialog_app_notification = new Dialog_App(contextParent);
        Type = type;
        data_Http data_http = new data_Http(contextParent);
            String url_run = data_http.get_Url_run();
            if(!url_run.isEmpty()){
                BASE_URL_1 = url_run +BASE_URL_API_1;
                BASE_URL_2 = url_run +BASE_URL_API_2;
            }else {
                BASE_URL_1 = "http://viettel.capnuoccholon.com.vn"+BASE_URL_API_1;
                 BASE_URL_2 = "http://viettel.capnuoccholon.com.vn"+BASE_URL_API_2;
            }
    }

    public Boolean getStatus_Send() {
        return Status_Send;
    }

    public void setStatus_Send(Boolean status_Send) {
        Status_Send = status_Send;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
            dialog_app.Dialog_Progress_Wait_Open("Đang gửi dữ liệu","Vui lòng chờ");
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
            dialog_app.Dialog_Progress_Wait_Close();

            String notification = "";
            if(Type.equals("1")) {
                if(Status_Send) {
                    notification = "Đồng bộ dữ liệu thành công." + "\r\n" + "Danh bạ: " + Danhba;
                }else {
                    if (Erro_Ttruong.isEmpty()) {
                        notification = "Đồng bộ dữ liệu thất bại danh bạ: " + Danhba + "\r\n" + "Vui lòng kiểm tra lại server, và thử lại" + "\r\n" + Erro;
                    }else {
                        notification = Erro_Ttruong;

                    }
                }
            }else if(Type.equals("all")){
                notification = "Đồng bộ dữ liệu thành công."+"\r\n"+"Kỳ: "+Ky +" - Đợt: "+Dot +" - Năm: "+Nam +" - Số máy: "+SoMay + " Danh bạ:"+"\r\n"
                +All_ok +" Danh bạ thất bại vui lòng thử lại"+"\r\n"+All_erro;
            }
            dialog_app_notification.Dialog_Notification(notification);

    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected Void doInBackground(Void... voids) {
        /*
        sqlite_Hinhanh sqlite_hinhanh = new sqlite_Hinhanh(contextParent,Nam,Ky,Dot,SoMay);
        if (sqlite_hinhanh.getStatus_Table_ListHinhAnh_Exists()) {
            List<String> list_id_nosync = sqlite_hinhanh.Get_TableID_ListHinhAnh_NoSync(Danhba);

        }
        */
        try {
            if(Type.equals("1")) {
                Gui_DuLieuDocSo();
            }else if(Type.equals("all") ){
                Gui_DuLieuDocSoAll();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
   private void  Gui_DuLieuDocSo() throws IOException {
       try {
           sqlite_DanhSachKhackHang_DocSo sqlite_danhSachKhackHang_docSo = new sqlite_DanhSachKhackHang_DocSo(contextParent,Nam,Ky,Dot,SoMay);
           if (sqlite_danhSachKhackHang_docSo.getStatus_Table_DanhSachKhachHang_Exists()) {

               data_syncghiso_send.data_khachhang data_khanghangsend = sqlite_danhSachKhackHang_docSo.Get_KhachHang_Send(Danhba);


               if (!data_khanghangsend.getDanhBa().trim().isEmpty()) {

                   // Kiem tra
                   String tieuthu = Objects.toString(data_khanghangsend.getTieuThu(),"").trim();
                   if (!tieuthu.trim().isEmpty()) {
                       data_syncghiso_send.data_khachhang[] khachhangs = new data_syncghiso_send.data_khachhang[1];
                       data_khanghangsend.setSoNhaMoi(Objects.toString(data_khanghangsend.getSoNhaMoi(),"").toUpperCase());
                       data_khanghangsend.setGhichu1(Objects.toString(data_khanghangsend.getGhichu1(),"").toUpperCase());
                       data_khanghangsend.setGhichu2(Objects.toString(data_khanghangsend.getGhichu2(),"").toUpperCase());
                       data_khanghangsend.setCongDung(Objects.toString(data_khanghangsend.getCongDung(),"").toUpperCase());
                       ///
                       khachhangs[0] = data_khanghangsend;
                       String[] somay = SoMay.split("_");
                       List<String> namkydot =  sqlite_danhSachKhackHang_docSo.Get_KhachHang_NAMKYDOT(Danhba);
                       String nam_send = namkydot.get(0).trim();
                       String ky_send = namkydot.get(1).trim();
                       String dot_send = namkydot.get(2).trim();
                       String somay_send = namkydot.get(3).trim();
                       data_syncghiso_send.Content content = new data_syncghiso_send.Content(nam_send,ky_send,dot_send,somay_send,khachhangs);
                       data_syncghiso_send data_syncghiso_send = new data_syncghiso_send(content);
                       Gson gson_send = new Gson();
                       String json_send = gson_send.toJson(data_syncghiso_send);

                       // Kiem tra chinh sửa của tỏ chưởng
                       HttpClient httpclient_ttruong = new DefaultHttpClient();
                       URI uri_ttruong = new URIBuilder(BASE_URL_1)
                               .addParameter("nam",nam_send)
                               .addParameter("ky",ky_send)
                               .addParameter("dot",dot_send)
                               .addParameter("somay",somay_send)
                               .addParameter("danhba",Danhba)
                               .build();
                       HttpGet httpget_ttruong = new HttpGet(uri_ttruong);
                       httpget_ttruong.setHeader("Content-type","application/json;charset=utf-8");
                       HttpResponse response_tostruong = httpclient_ttruong.execute(httpget_ttruong);
                       if (response_tostruong.getStatusLine().getStatusCode() == 200) {
                           String responseString_ttruong = EntityUtils.toString(response_tostruong.getEntity()).replace('"',' ');
                           if(responseString_ttruong.trim().equals("0")) {

                               /////////
                               HttpClient httpclient = new DefaultHttpClient();
                               URI uri_ = new URIBuilder(BASE_URL_2)
                                       .addParameter("thamso",json_send)
                                       .addParameter("kieu","string")
                                       .build();
                               HttpPost httppost = new HttpPost(uri_);
                               httppost.setHeader("Content-type","application/json;charset=utf-8");
                               HttpResponse response = httpclient.execute(httppost);

                               if (response.getStatusLine().getStatusCode() == 200) {
                                   String responseString = EntityUtils.toString(response.getEntity());
                                   Gson gson_reponse = new Gson();
                                   data_syncghiso_response data_syncghiso_response = gson_reponse.fromJson(responseString,data_syncghiso_response.class);
                                   String danhbas = data_syncghiso_response.getResult().getDanhbas();
                                   String[] com_danhba = danhbas.split(",");
                                   if (com_danhba.length > 0) {
                                       for (String danhba : com_danhba) {
                                           if (sqlite_danhSachKhackHang_docSo.getStatus_Table_DanhSachKhachHang_Exists() && !danhba.trim().isEmpty()) {
                                               sqlite_danhSachKhackHang_docSo.Update_Sync_DanhSachDocSo_KhachHang(danhba.trim());
                                               String tinhtrang = data_khanghangsend.getTinhTrang();
                                               if (tinhtrang.trim().equals("Đóng cửa")) {
                                                   sqlite_danhSachKhackHang_docSo.Update_TinhTrang_KH_DanhSachDocSo_KhachHang(danhba,"Đóng cửa");
                                               }
                                           }
                                       }
                                       Status_Send = true;
                                   }
                               } else {
                                   String responseString = EntityUtils.toString(response.getEntity());
                                   Erro = " Status: " + String.valueOf(response.getStatusLine().getStatusCode()) + " " + responseString + "\r\n";
                                   Status_Send = false;

                               }
                           }else {
                               Erro_Ttruong = "Lỗi không điều chỉnh được "+"\r\n"+
                                       "do đã điều chỉnh dữ liệu trước đó"+ "\r\n"+
                                       "Không thể gửi dữ liệu .";
                               Status_Send = false;
                           }
                       }
                   }
               }
           }
       } catch (Exception e) {

       }

   }

    private void  Gui_DuLieuDocSoAll() throws IOException {
        try {
            sqlite_DanhSachKhackHang_DocSo sqlite_danhSachKhackHang_docSo = new sqlite_DanhSachKhackHang_DocSo(contextParent,Nam,Ky,Dot,SoMay);
            sqlite_Hinhanh sqlite_hinhanh = new sqlite_Hinhanh(contextParent,Nam,Ky,Dot,SoMay);
            if (sqlite_danhSachKhackHang_docSo.getStatus_Table_DanhSachKhachHang_Exists()) {
                List<data_syncghiso_send.data_khachhang> khs_no_sync = sqlite_danhSachKhackHang_docSo.Get_KhachHangChua_Sync();
                for(data_syncghiso_send.data_khachhang data_khanghangsend : khs_no_sync) {
                    String danhbacheck =data_khanghangsend.getDanhBa();
                        if (!data_khanghangsend.getDanhBa().trim().isEmpty()) {
                            // Kiem tra

                            if (!Objects.toString(data_khanghangsend.getTieuThu(),"").trim().isEmpty()) {
                                data_syncghiso_send.data_khachhang[] khachhangs = new data_syncghiso_send.data_khachhang[1];
                                data_khanghangsend.setSoNhaMoi(Objects.toString(data_khanghangsend.getSoNhaMoi(),"").toUpperCase());
                                data_khanghangsend.setGhichu1(Objects.toString(data_khanghangsend.getGhichu1(),"").toUpperCase());
                                data_khanghangsend.setGhichu2(Objects.toString(data_khanghangsend.getGhichu2(),"").toUpperCase());
                                data_khanghangsend.setCongDung(Objects.toString(data_khanghangsend.getCongDung(),"").toUpperCase());
                                ///
                                khachhangs[0] = data_khanghangsend;
                                if(sqlite_hinhanh.Get_Count_HinhAnh_Gui(danhbacheck) >0) {
                                    List<String> namkydot =  sqlite_danhSachKhackHang_docSo.Get_KhachHang_NAMKYDOT(danhbacheck);
                                    String nam_send = namkydot.get(0).trim();
                                    String ky_send = namkydot.get(1).trim();
                                    String dot_send = namkydot.get(2).trim();
                                    String somay_send = namkydot.get(3).trim();
                                    data_syncghiso_send.Content content = new data_syncghiso_send.Content(nam_send,ky_send,dot_send,somay_send,khachhangs);
                                    data_syncghiso_send data_syncghiso_send = new data_syncghiso_send(content);
                                    Gson gson_send = new Gson();
                                    String json_send = gson_send.toJson(data_syncghiso_send);
                                    //    Log.e("Json_send",json_send);
                                    StringEntity stringEntity_send = new StringEntity(json_send,"utf-8");
                                    // Kiem tra chinh sửa của tỏ chưởng
                                    HttpClient httpclient_ttruong = new DefaultHttpClient();
                                    URI uri_ttruong = new URIBuilder(BASE_URL_1)
                                            .addParameter("nam",nam_send)
                                            .addParameter("ky",ky_send)
                                            .addParameter("dot",dot_send)
                                            .addParameter("somay",somay_send)
                                            .addParameter("danhba",Danhba)
                                            .build();
                                    HttpGet httpget_ttruong = new HttpGet(uri_ttruong);
                                    httpget_ttruong.setHeader("Content-type","application/json;charset=utf-8");
                                    HttpResponse response_tostruong = httpclient_ttruong.execute(httpget_ttruong);
                                    if (response_tostruong.getStatusLine().getStatusCode() == 200) {
                                        String responseString_ttruong = EntityUtils.toString(response_tostruong.getEntity()).replace('"',' ');
                                        if (responseString_ttruong.trim().equals("0")) {
                                            ////////////////////////
                                            HttpClient httpclient = new DefaultHttpClient();
                                            HttpPost httppost = new HttpPost(BASE_URL_2);
                                            //  httppost.setHeader("Accept", "application/json");
                                            httppost.setHeader("Content-type","application/json;charset=utf-8");
                                            httppost.setEntity(stringEntity_send);
                                            HttpResponse response = httpclient.execute(httppost);

                                            if (response.getStatusLine().getStatusCode() == 200) {
                                                String responseString = EntityUtils.toString(response.getEntity());
                                                Gson gson_reponse = new Gson();
                                                data_syncghiso_response data_syncghiso_response = gson_reponse.fromJson(responseString,data_syncghiso_response.class);
                                                String danhbas = data_syncghiso_response.getResult().getDanhbas();
                                                String[] com_danhba = danhbas.split(",");
                                                if (com_danhba.length > 0) {
                                                    for (String danhba : com_danhba) {
                                                        if (sqlite_danhSachKhackHang_docSo.getStatus_Table_DanhSachKhachHang_Exists() && !danhba.trim().isEmpty()) {
                                                            sqlite_danhSachKhackHang_docSo.Update_Sync_DanhSachDocSo_KhachHang(danhba.trim());
                                                        }
                                                    }
                                                    Status_Send = true;
                                                    All_ok = All_ok + khachhangs[0].getDanhBa() + "\r\n";
                                                }
                                            } else {
                                                String responseString = EntityUtils.toString(response.getEntity());
                                                Status_Send = false;
                                                All_erro = All_erro + khachhangs[0].getDanhBa() + " Status: " + String.valueOf(response.getStatusLine().getStatusCode()) + " " + responseString + "\r\n";
                                            }
                                        }else {
                                            All_erro = All_erro + khachhangs[0].getDanhBa()+"\r\n"+
                                                    "Lỗi không điều chỉnh được "+"\r\n"+
                                                    "do đã điều chỉnh dữ liệu trước đó"+ "\r\n";
                                            Status_Send = false;
                                        }
                                    }
                                }else {
                                    Status_Send = false;
                                    All_erro = All_erro + khachhangs[0].getDanhBa() + " Status: Chưa gửi hinh"+ "\r\n";
                                }
                            }
                        }

                }

            }
        }catch (Exception e){

        }


    }
}

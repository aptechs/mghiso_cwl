package com.dkhanhaptechs.mghiso.danhsachdocso;

class data_Danhsachdocso {
    private int Id ;
    private boolean Chon;
    private  String Tieude ;
    private  int Dongbo ;
    private  int So_dh;
    private  int So_dh_doc ;
    private  int Sanluong ;
    private int So_hinh;

    public data_Danhsachdocso(int id,boolean chon,String tieude,int dongbo,int so_dh,int so_dh_doc,int sanluong,int so_hinh) {
        Id = id;
        Chon = chon;
        Tieude = tieude;
        Dongbo = dongbo;
        So_dh = so_dh;
        So_dh_doc = so_dh_doc;
        Sanluong = sanluong;
        So_hinh = so_hinh;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public boolean getChon() {
        return Chon;
    }

    public void setChon(boolean chon) {
        Chon = chon;
    }

    public String getTieude() {
        return Tieude;
    }

    public void setTieude(String tieude) {
        Tieude = tieude;
    }

    public int getDongbo() {
        return Dongbo;
    }

    public void setDongbo(int dongbo) {
        Dongbo = dongbo;
    }

    public int getSo_dh() {
        return So_dh;
    }

    public void setSo_dh(int so_dh) {
        So_dh = so_dh;
    }

    public int getSo_dh_doc() {
        return So_dh_doc;
    }

    public void setSo_dh_doc(int so_dh_doc) {
        So_dh_doc = so_dh_doc;
    }

    public int getSanluong() {
        return Sanluong;
    }

    public void setSanluong(int sanluong) {
        Sanluong = sanluong;
    }

    public int getSo_hinh() {
        return So_hinh;
    }

    public void setSo_hinh(int so_hinh) {
        So_hinh = so_hinh;
    }
}

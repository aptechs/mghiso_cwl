package com.dkhanhaptechs.mghiso.Share.User;

import android.content.Context;
import android.database.Cursor;

import com.dkhanhaptechs.mghiso.Share.Database_SQLite;


import java.util.ArrayList;
import java.util.List;

public class sqlite_User {
    private Database_SQLite database_sqLite ;
    private String Database ="Database_User.sqlite";
    private String Table_User = "Table_User";
    private boolean Status_Table_User_Exists = false;
    public sqlite_User (Context context){
        database_sqLite = new Database_SQLite(context, Database, null, 1);
        if(Create_Table_User()){
            Status_Table_User_Exists = true;
        }
    }

    public boolean getStatus_Table_User_Exists() {
        return Status_Table_User_Exists;
    }

    private   boolean Create_Table_User(){
        boolean ok =false;
        try {
            String querry = "CREATE TABLE IF NOT EXISTS "+Table_User +
                    " (" +
                    " id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                    " mac nvarchar(30)," +
                    " tennhanvien nvarchar(50)," +
                    " somay nvarchar(20)," +
                    " dienthoai nvarchar(20)" +
                    ");";
            if(database_sqLite.QueryDatabase(querry)) {
                ok = true;
            }
        }catch (Exception e){

        }
        return ok;
    }
    public  boolean Insert_Update_User(data_User user){
        boolean ok =false;
        try {
            String querry = "SELECT mac FROM "+Table_User+" WHERE id =1;";
            Cursor data_check = database_sqLite.GetDatabase(querry);
            String mac = "";
            while (data_check.moveToNext()) {
                mac = data_check.getString(0);

            }
            if(!mac.isEmpty()){
                // update
                querry = "UPDATE "+Table_User+" SET mac ='" + user.getMac() + "',"  +
                        " tennhanvien ='" + user.getTennhanvien() + "',"  +
                        " somay ='" + user.getSomay() + "',"  +
                        " dienthoai ='" + user.getdienthoai() + "'"  +
                        " WHERE id =1;" ;
            }else {
                // insert
                querry = "INSERT INTO "+Table_User+" (mac,tennhanvien,somay,dienthoai)" +
                        " VALUES ('" + user.getMac() + "','" + user.getTennhanvien() + "','"+user.getSomay()+"','"+user.getdienthoai()+"');";
            }
            if(database_sqLite.QueryDatabase(querry)) {
                ok = true;
            }
        }catch (Exception e){

        }
        return ok;
    }
    public boolean Delete_User(String id){
        boolean ok =false;
        try {
            String querry = "DELETE FROM "+Table_User+"  WHERE id = "+id+";";
            if(database_sqLite.QueryDatabase(querry)) {
                ok = true;
            }
        }catch (Exception e){

        }
        return ok;
    }
    public List<data_User> Get_All_User(){
        List<data_User> all_user = new ArrayList<data_User>();
        try {
            String querry = "SELECT * FROM "+Table_User;
            Cursor data_check = database_sqLite.GetDatabase(querry);
            while (data_check.moveToNext()) {
                String id = data_check.getString(0);
                String mac = data_check.getString(1);
                String tennhanvien= data_check.getString(2);
                String somay = data_check.getString(3);
                String dienthoai = data_check.getString(4);
                data_User user = new data_User(
                        Integer.parseInt(id),
                        mac,
                        tennhanvien,
                        somay,
                        dienthoai
                );
                all_user.add(user);
            }

        }catch (Exception e){

        }
        return  all_user;
    }
    public  data_User Get_User(String id){
        data_User user = new data_User(1,"","","","");
        try {
            String querry = "SELECT * FROM "+Table_User+" WHERE id="+id;
            Cursor data_check = database_sqLite.GetDatabase(querry);
            while (data_check.moveToNext()) {

                String mac = data_check.getString(1);
                String tennhanvien= data_check.getString(2);
                String somay = data_check.getString(3);
                String dienthoai = data_check.getString(4);

                user.setId(Integer.parseInt(id));
                user.setMac(mac);
                user.setTennhanvien(tennhanvien);
                user.setSomay(somay);
                user.setdienthoai(dienthoai);
            }

        }catch (Exception e){

        }
        return user ;
    }
}


package com.dkhanhaptechs.mghiso.Share;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class Database_SQLite extends SQLiteOpenHelper {
    public Database_SQLite(Context context,String name,SQLiteDatabase.CursorFactory factory,int version) {
        super(context, name, factory, version);
    }
    // truy van khong tra ket qua : Create, insert, updat, delete .....
    public  boolean  QueryDatabase(String sql){
        boolean ok =false;
        try {
            SQLiteDatabase database = getWritableDatabase();
            database.execSQL(sql);
            ok = true;
        }catch (Exception e){
            Log.e("Database_SQLite",e.toString());
        }
        return  ok;
    }
    // truy van co tra ket qua: Selecte
    public Cursor GetDatabase(String sql){
        SQLiteDatabase database = getReadableDatabase();
        return  database.rawQuery(sql,null);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }


}

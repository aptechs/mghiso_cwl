package com.dkhanhaptechs.mghiso.Share.SyncGhiSo;

import android.content.Context;

public class data_syncghiso_send {
    private String type;
    private Content content;

    public data_syncghiso_send(Content content) {
        this.type = "dong bo du lieu khach hang";
        this.content = content;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Content getContent() {
        return content;
    }

    public void setContent(Content content) {
        this.content = content;
    }

    public static class  Content{
        private String nam;
        private String ky;
        private  String dot;
        private  String may ;
        private  data_khachhang [] khachhangs;


        public Content(String nam,String ky,String dot,String may,data_khachhang[] khachhangs) {
            this.nam = nam;
            this.ky = ky;
            this.dot = dot;
            this.may = may;
            this.khachhangs = khachhangs;
        }

        public String getNam() {
            return nam;
        }

        public void setNam(String nam) {
            this.nam = nam;
        }

        public String getKy() {
            return ky;
        }

        public void setKy(String ky) {
            this.ky = ky;
        }

        public String getDot() {
            return dot;
        }

        public void setDot(String dot) {
            this.dot = dot;
        }

        public String getMay() {
            return may;
        }

        public void setMay(String may) {
            this.may = may;
        }

        public data_khachhang[] getKhachhangs() {
            return khachhangs;
        }

        public void setKhachhangs(data_khachhang[] khachhangs) {
            this.khachhangs = khachhangs;
        }
    }
    public static class  data_khachhang{
        private  String DanhBa ;
        private  String HoTen;
        private  String SoNha;
        private  String SoNhaMoi;
        private  String TinhTrangChi;
        private  String ViTriDHN;
        private  String dienthoai1;
        private  String dienthoai2 ;
        private String code;
        private String csmoi;
        private  String TinhTrang;
        private  String TieuThu;
        private  String ghichu1 ;
        private  String ghichu2;
        private  String Lat;
        private  String Lng ;
        private String CongDung;

        public data_khachhang(String danhBa,String hoTen,String soNha,String soNhaMoi,String tinhTrangChi,String viTriDHN,String dienthoai1,String dienthoai2,String code,String csmoi,String tinhTrang,String tieuThu,String ghichu1,String ghichu2,String lat,String lng,String congDung) {
            DanhBa = danhBa;
            HoTen = hoTen;
            SoNha = soNha;
            SoNhaMoi = soNhaMoi;
            TinhTrangChi = tinhTrangChi;
            ViTriDHN = viTriDHN;
            this.dienthoai1 = dienthoai1;
            this.dienthoai2 = dienthoai2;
            this.code = code;
            this.csmoi = csmoi;
            TinhTrang = tinhTrang;
            TieuThu = tieuThu;
            this.ghichu1 = ghichu1;
            this.ghichu2 = ghichu2;
            Lat = lat;
            Lng = lng;
            CongDung = congDung;
        }

        public String getDanhBa() {
            return DanhBa;
        }

        public void setDanhBa(String danhBa) {
            DanhBa = danhBa;
        }

        public String getHoTen() {
            return HoTen;
        }

        public void setHoTen(String hoTen) {
            HoTen = hoTen;
        }

        public String getSoNha() {
            return SoNha;
        }

        public void setSoNha(String soNha) {
            SoNha = soNha;
        }

        public String getSoNhaMoi() {
            return SoNhaMoi;
        }

        public void setSoNhaMoi(String soNhaMoi) {
            SoNhaMoi = soNhaMoi;
        }

        public String getTinhTrangChi() {
            return TinhTrangChi;
        }

        public void setTinhTrangChi(String tinhTrangChi) {
            TinhTrangChi = tinhTrangChi;
        }

        public String getViTriDHN() {
            return ViTriDHN;
        }

        public void setViTriDHN(String viTriDHN) {
            ViTriDHN = viTriDHN;
        }

        public String getDienthoai1() {
            return dienthoai1;
        }

        public void setDienthoai1(String dienthoai1) {
            this.dienthoai1 = dienthoai1;
        }

        public String getDienthoai2() {
            return dienthoai2;
        }

        public void setDienthoai2(String dienthoai2) {
            this.dienthoai2 = dienthoai2;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getCsmoi() {
            return csmoi;
        }

        public void setCsmoi(String csmoi) {
            this.csmoi = csmoi;
        }

        public String getTinhTrang() {
            return TinhTrang;
        }

        public void setTinhTrang(String tinhTrang) {
            TinhTrang = tinhTrang;
        }

        public String getTieuThu() {
            return TieuThu;
        }

        public void setTieuThu(String tieuThu) {
            TieuThu = tieuThu;
        }

        public String getGhichu1() {
            return ghichu1;
        }

        public void setGhichu1(String ghichu1) {
            this.ghichu1 = ghichu1;
        }

        public String getGhichu2() {
            return ghichu2;
        }

        public void setGhichu2(String ghichu2) {
            this.ghichu2 = ghichu2;
        }

        public String getLat() {
            return Lat;
        }

        public void setLat(String lat) {
            Lat = lat;
        }

        public String getLng() {
            return Lng;
        }

        public void setLng(String lng) {
            Lng = lng;
        }

        public String getCongDung() {
            return CongDung;
        }

        public void setCongDung(String congDung) {
            CongDung = congDung;
        }
    }
}

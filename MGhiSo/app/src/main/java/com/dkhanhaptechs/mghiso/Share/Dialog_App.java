package com.dkhanhaptechs.mghiso.Share;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.dkhanhaptechs.mghiso.dangnhap.activity_DangNhap;

public class Dialog_App {
    private Context context ;
    private  AlertDialog dialog_notification ;
    private  ProgressDialog progressDialog_wait ;
    private  Boolean Status_dismiss_Dialog_Notification = false;
    public Dialog_App (Context context){
        this.context = context;
    }
    public void Dialog_Notification(final String text){
        AlertDialog.Builder alert_notìication =new AlertDialog.Builder(context);
        alert_notìication.setMessage(text);
        // dong y
        alert_notìication.setPositiveButton("Xác nhận",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog,int which) {
                dialog_notification.dismiss();
                Status_dismiss_Dialog_Notification = true;
            }
        });
        // bo qua
        dialog_notification = alert_notìication.create();
        dialog_notification.setCancelable(false);
        dialog_notification.show();
    }

    public Boolean getStatus_dismiss_Dialog_Notification() {
        return Status_dismiss_Dialog_Notification;
    }

    public void setStatus_dismiss_Dialog_Notification(Boolean status_dismiss_Dialog_Notification) {
        Status_dismiss_Dialog_Notification = status_dismiss_Dialog_Notification;
    }

    public void Dialog_Progress_Wait_Open(String title,String message){
        progressDialog_wait = new ProgressDialog(context);
        progressDialog_wait.setTitle(title);
        progressDialog_wait.setMessage(message);
        progressDialog_wait.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog_wait.setCancelable(false);
        progressDialog_wait.show();

    }


    public void Dialog_Progress_Wait_Close(){
        progressDialog_wait.dismiss();
    }
}

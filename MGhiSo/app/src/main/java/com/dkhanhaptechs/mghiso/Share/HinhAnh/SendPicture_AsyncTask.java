package com.dkhanhaptechs.mghiso.Share.HinhAnh;


import android.content.Context;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.util.Log;

import com.dkhanhaptechs.mghiso.Share.Dialog_App;

import java.io.IOException;


import cz.msebera.android.httpclient.HttpResponse;

import cz.msebera.android.httpclient.client.HttpClient;

import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.impl.client.HttpClients;
import cz.msebera.android.httpclient.util.EntityUtils;
import  com.dkhanhaptechs.mghiso.Share.User.*;

public class SendPicture_AsyncTask  extends AsyncTask<Void,Integer,Void> {
    private final Context contextParent;
    private final String  Nam;
    private final String Ky ;
    private  final String Dot ;
    private  final String SoMay;
    private  final String Danhba;
    private final String Id_anh ;
    private final Bitmap Bitmap_send;
    Dialog_App dialog_app ;
    private  Boolean On_dialog ;
    private  int Send_Ok  ;
    private int status = 0;
    private String BASE_URL_API ="/wsnhanhinhanh/services.asmx?op=NhanHinhMotAnh";
    private String BASE_URL ="";
    public SendPicture_AsyncTask(Context contextParent,String danhba ,String nam,String ky,String dot,String somay,String id_anh,Bitmap bitmap_send,Boolean on_dialog){
        this.contextParent = contextParent;
        this.Danhba = danhba;
        this.Nam = nam;
        this.Ky = ky;
        this.Dot = dot;
        this.SoMay = somay;
        dialog_app = new Dialog_App(contextParent);
        this.Id_anh = id_anh;
        this.Bitmap_send = bitmap_send;
        this.On_dialog = on_dialog;
        Send_Ok = 0;
        data_Http data_http = new data_Http(contextParent);
            String url_run = data_http.get_Url_run();
            if(!url_run.isEmpty()){
                BASE_URL = url_run +BASE_URL_API;
            }else {
                BASE_URL = "http://viettel.capnuoccholon.com.vn"+BASE_URL_API;
            }

    }
    public  int  Status_Send (){
        return Send_Ok;
    }

    public Boolean getOn_dialog() {
        return On_dialog;
    }

    public void setOn_dialog(Boolean on_dialog) {
        On_dialog = on_dialog;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if(On_dialog) {
            dialog_app.Dialog_Progress_Wait_Open("Đang gửi anh","Vui lòng chờ");
        }
        Log.e("SYNC Start",Danhba);
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        if(On_dialog) {
            dialog_app.Dialog_Progress_Wait_Close();
        }
       Send_Ok = status ;
        if(Send_Ok == 0){
            dialog_app.Dialog_Notification("Gửi ảnh lỗi"+"\r\n"+"Không kết nối được server"+"\r\n"+"Vui lòng thử lại hoặc liên hệ server");
        }
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected Void doInBackground(Void... voids) {

        try {
            GuiAnh();
            SystemClock.sleep(500);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    private void GuiAnh() throws IOException {

        data_Api_Webservice_Hinhanh_send data_api_webservice_hinhanh_send = new data_Api_Webservice_Hinhanh_send(Id_anh,Danhba,Nam,Ky,Dot,Bitmap_send);
        String data_send = data_api_webservice_hinhanh_send.get_Data_Send_Webservice();
        //Log.e("data_send 1: ",data_send);
        if (data_send.trim().isEmpty()) {
            dialog_app.Dialog_Notification("Không có ảnh để gửi");
            return;
        }
        StringEntity stringEntity_send = new StringEntity(data_send);
        HttpClient httpclient = HttpClients.createDefault();
        HttpPost httppost = new HttpPost(BASE_URL);
        httppost.setHeader("Content-type", "text/xml;charset=utf-8");
        httppost.setEntity(stringEntity_send);
        HttpResponse response = httpclient.execute(httppost);
        if(response.getStatusLine().getStatusCode() ==200) {

            String responseString = EntityUtils.toString(response.getEntity());
            // Log.v("log_tag", "In the try Loop" + st);
            String[] com_responseString = responseString.split("<NhanHinhMotAnhResult>");
            if (com_responseString.length > 1) {
                String[] com_responseString1 = com_responseString[1].split("</NhanHinhMotAnhResult>");
                if (com_responseString1.length > 1) {
                    String[] id_danhba = com_responseString1[0].split("-");
                    sqlite_Hinhanh sqlite_hinhanh = new sqlite_Hinhanh(contextParent,Nam,Ky,Dot,SoMay);
                    sqlite_hinhanh.Update_Table_HinhAnh(id_danhba[1],id_danhba[0]);
                }
            }
            status = 1;
        }else {
            status = 2;
        }

    }

}

package com.dkhanhaptechs.mghiso.Share.DanhSach_DocSo;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.dkhanhaptechs.mghiso.Share.Database_SQLite;


import java.util.ArrayList;
import java.util.List;

public class sqlite_DanhSachDocSo {
    private Database_SQLite database_sqLite ;
    private String Database ="Database_DanhSachDocSo.sqlite";
    private String Table_DanhSachDocSo = "DanhSachDocSo";
    private boolean Status_DanhSachDocSo_Exists = false;
    public sqlite_DanhSachDocSo (Context context){
        database_sqLite = new Database_SQLite(context, Database, null, 1);
        if(Create_Table_DanhSachDocSo()){
            Status_DanhSachDocSo_Exists = true;
        }else {
            Status_DanhSachDocSo_Exists = false;
        }
    }

    public boolean getStatus_Table_DanhSachDocSo_Exists() {
        return Status_DanhSachDocSo_Exists;
    }

    private boolean Create_Table_DanhSachDocSo (){
        boolean ok = false;
        try{

            String querry = "CREATE TABLE IF NOT EXISTS "+Table_DanhSachDocSo +
                    " (" +
                    " id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                    " nam nvarchar(5)," +
                    " ky nvarchar(5)," +
                    " dot nvarchar(5)," +
                    " somay nvarchar(10)," +
                    " sum_dh nvarchar(20)," +
                    " sum_dadoc nvarchar(20)," +
                    " sum_sanluong nvarchar(10)" +
                    ");";
            if(database_sqLite.QueryDatabase(querry)) {
                ok = true;
            }
        }catch (Exception e){
            Log.e("Create DanhSachDocSo", e.toString());
        }
        return  ok;
    }
    public  boolean Insert_Update_Table_DanhSachDocSo(data_DanhSachDocSo data_danhsachdocso){
        boolean ok =false;
        try {
            String querry = "SELECT * FROM "+Table_DanhSachDocSo+" WHERE " +
                    "  nam ='"+data_danhsachdocso.getNam().toString()+"'" +
                    "  AND ky ='"+data_danhsachdocso.getKy().toString()+"'" +
                    "  AND dot ='"+data_danhsachdocso.getDot().toString()+"'" +
                    "  AND somay ='"+data_danhsachdocso.getSomay().toString()+"'" +
                    ";";
            Cursor data_check = database_sqLite.GetDatabase(querry);
            String id = "";
            while (data_check.moveToNext()) {
                id = data_check.getString(0);

            }
            if(!id.isEmpty()){
                // update
                querry = "UPDATE "+Table_DanhSachDocSo+
                        " SET " +
                        " sum_dh ='" + data_danhsachdocso.getSum_dh().toString() + "', "  +
                        " sum_dadoc ='" + data_danhsachdocso.getDadoc().toString() + "', "  +
                        " sum_sanluong ='" + data_danhsachdocso.getSanluong().toString() + "' "  +
                        " WHERE " +
                        "  nam ='"+data_danhsachdocso.getNam().toString()+"'" +
                        "  AND ky ='"+data_danhsachdocso.getKy().toString()+"'" +
                        "  AND dot ='"+data_danhsachdocso.getDot().toString()+"'" +
                        "  AND somay ='"+data_danhsachdocso.getSomay().toString()+"' ;" ;
            }else {
                // insert
                querry = "INSERT INTO "+Table_DanhSachDocSo+" (nam,ky,dot,somay,sum_dh,sum_dadoc,sum_sanluong)" +
                        " VALUES (" +
                        "'" + data_danhsachdocso.getNam() + "'," +
                        "'" + data_danhsachdocso.getKy() + "'," +
                        "'" + data_danhsachdocso.getDot() + "'," +
                        "'" + data_danhsachdocso.getSomay() + "'," +
                        "'" + data_danhsachdocso.getSum_dh() + "'," +
                        "'" + data_danhsachdocso.getDadoc() + "'," +
                        "'" + data_danhsachdocso.getSanluong() + "'" +
                        ");";
            }
            if(database_sqLite.QueryDatabase(querry)) {
                ok = true;
            }
        }catch (Exception e){

        }
        return ok;
    }
    public boolean Delete_DanhSachDocSo(String nam, String ky, String dot, String somay){
        boolean ok =false;
        try {
            String querry = "DELETE FROM "+Table_DanhSachDocSo+"  " +
                    " WHERE " +
                    " nam = '"+nam+"' AND " +
                    " ky = '"+ky+"' AND" +
                    " dot = '"+dot+"' AND" +
                    " somay = '"+somay+"' ;" ;
            if(database_sqLite.QueryDatabase(querry)) {
                ok = true;
            }
        }catch (Exception e){

        }
        return ok;
    }

    public  data_DanhSachDocSo Get_DanhSachDocSo(data_DanhSachDocSo  data_danhsachdocso){

        try {
            String querry = "SELECT * FROM "+Table_DanhSachDocSo +
                    " WHERE " +
                    "  nam ='"+data_danhsachdocso.getNam().toString()+"'" +
                    "  AND ky ='"+data_danhsachdocso.getKy().toString()+"'" +
                    "  AND dot ='"+data_danhsachdocso.getDot().toString()+"'" +
                    "  AND somay ='"+data_danhsachdocso.getSomay().toString()+"' ;" ;
            data_danhsachdocso.setId(0);
            Cursor data_check = database_sqLite.GetDatabase(querry);
            while (data_check.moveToNext()) {
                String id = data_check.getString(0);
                data_danhsachdocso.setId(Integer.parseInt(id));

                String sum_dh = data_check.getString(5);
                String sum_dadoc = data_check.getString(6);
                String sum_sanluong = data_check.getString(7);

                data_danhsachdocso.setSum_dh(sum_dh);
                data_danhsachdocso.setDadoc(sum_dadoc);
                data_danhsachdocso.setSanluong(sum_sanluong);

            }

        }catch (Exception e){

        }
        return data_danhsachdocso ;
    }
    public  List<data_DanhSachDocSo> Get_All_DanhSachDocSo(){
        List< data_DanhSachDocSo > data_all_danhsachdocso = new ArrayList<>();
        try {
            String querry = "SELECT * FROM "+Table_DanhSachDocSo + ";";

            Cursor data_check = database_sqLite.GetDatabase(querry);
            while (data_check.moveToNext()) {
                data_DanhSachDocSo  data_danhsachdocso = new data_DanhSachDocSo();

                String id = data_check.getString(0);

                String nam = data_check.getString(1);
                String ky = data_check.getString(2);
                String dot = data_check.getString(3);
                String somay = data_check.getString(4);

                String sum_dh = data_check.getString(5);
                String sum_dadoc = data_check.getString(6);
                String sum_sanluong = data_check.getString(7);

                data_danhsachdocso.setId(Integer.parseInt(id));
                data_danhsachdocso.setNam(nam);
                data_danhsachdocso.setKy(ky);
                data_danhsachdocso.setDot(dot);
                data_danhsachdocso.setSomay(somay);
                data_danhsachdocso.setSum_dh(sum_dh);
                data_danhsachdocso.setDadoc(sum_dadoc);
                data_danhsachdocso.setSanluong(sum_sanluong);
                data_all_danhsachdocso.add(data_danhsachdocso);

            }

        }catch (Exception e){

        }
        return data_all_danhsachdocso ;
    }
}

package com.dkhanhaptechs.mghiso.Share.ThongTinChiTietDH;

public class data_Thongtinchitetdh_response {
    private  data_result result ;
    private String status;
    private String message ;

    public data_result getResult() {
        return result;
    }

    public void setResult(data_result result) {
        this.result = result;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public class  data_result {
     private data_thongtin_dhn [] thongtin_dhn;

        public data_thongtin_dhn[] getThongtin_dhn() {
            return thongtin_dhn;
        }

        public void setThongtin_dhn(data_thongtin_dhn[] thongtin_dhn) {
            this.thongtin_dhn = thongtin_dhn;
        }
    }
    public class data_thongtin_dhn {
        private String solenh;
        private String ngaythay;
        private String chiso;
        private String bandoi;
        private String loai_dh;
        private String noidung;

        public String getSolenh() {
            return solenh;
        }

        public void setSolenh(String solenh) {
            this.solenh = solenh;
        }

        public String getNgaythay() {
            return ngaythay;
        }

        public void setNgaythay(String ngaythay) {
            this.ngaythay = ngaythay;
        }

        public String getChiso() {
            return chiso;
        }

        public void setChiso(String chiso) {
            this.chiso = chiso;
        }

        public String getBandoi() {
            return bandoi;
        }

        public void setBandoi(String bandoi) {
            this.bandoi = bandoi;
        }

        public String getLoai_dh() {
            return loai_dh;
        }

        public void setLoai_dh(String loai_dh) {
            this.loai_dh = loai_dh;
        }

        public String getNoidung() {
            return noidung;
        }

        public void setNoidung(String noidung) {
            this.noidung = noidung;
        }
    }
}

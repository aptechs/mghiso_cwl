package com.dkhanhaptechs.mghiso.danhsachdocso_chitiet;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;

import com.dkhanhaptechs.mghiso.MainActivity;
import com.dkhanhaptechs.mghiso.R;
import com.dkhanhaptechs.mghiso.Share.Dialog_App;
import com.dkhanhaptechs.mghiso.dangnhap.activity_DangNhap;
import com.dkhanhaptechs.mghiso.danhsachbatthuong.activity_Danhsachbatthuong;
import com.dkhanhaptechs.mghiso.danhsachdocso.activity_Danhsachdocso;
import com.dkhanhaptechs.mghiso.danhsachdocso_chitiet_khachhang.activity_Danhsachdocso_chitiet_khachhang;
import com.dkhanhaptechs.mghiso.Share.DanhSach_DocSo.sqlite_DanhSachKhackHang_DocSo;
import com.dkhanhaptechs.mghiso.mayin.activity_Mayin;



import org.json.JSONException;
import org.json.JSONObject;



import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;



public class activity_Danhsachdocso_chitiet extends AppCompatActivity {
    // BIEN LAYOUT
    private Toolbar toolbar;
    TextView tv_Tieude ,tv_sumdh, tv_sum_dadoc, tv_sum_sanluong,tv_per;
    ListView lv_Danhsachdocso_chitiet;
    SearchView sv_timkiem ;
    Spinner spn_muctimkiem;

    // BIEN CHUONG TRINH
    private String Json_table_ghiso ="";
    private String Nam ="";
    private String Ky = "";
    private String Dot ="";
    private String SoMay ="";
    private int Sum_Dh =0 ;
    private  int Sum_DaDoc = 0;
    private  int Sum_SanLuong =0 ;


    private ArrayList<String> array_muctimkiem ;
    private ArrayAdapter adapter_muctimkiem;
    private String [] data_Muctimkiem = {"Danh bạ","Số thân","Địa chỉ","Họ tên"};
    private int num_Selete_muctimkiem = 0 ;
    // BIEN
    private ArrayList<data_Danhsachdocso_chitiet> Arraylist_data_danhsachdocso_chitiet;
    private ArrayList<data_Danhsachdocso_chitiet> Arraylist_data_danhsachdocso_chitiet_temp;
    private adapter_Danhsachdocso_chitiet Adapter_danhsachdocso_chitiet;
    //NavigationView
    ImageView imv_user;
    TextView tv_tennv_navi , tv_somay_navi , tv_sodienthoai_navi;
    ImageButton imbt_dangxuat;

    private String Json_EnableGhiso ="";
    //
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_danhsachdocso_chitiet);
        // INIT
        Mappping_Layout();
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this,drawer,toolbar,R.string.navigation_drawer_open,R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View header = Mappping_Layout_NavigationView(navigationView);


        Arraylist_data_danhsachdocso_chitiet = new ArrayList<data_Danhsachdocso_chitiet>();
        Arraylist_data_danhsachdocso_chitiet_temp = new ArrayList<data_Danhsachdocso_chitiet>();
        Adapter_danhsachdocso_chitiet = new adapter_Danhsachdocso_chitiet(activity_Danhsachdocso_chitiet.this,R.layout.dong_danhsachdocso_chitiet,Arraylist_data_danhsachdocso_chitiet);
        lv_Danhsachdocso_chitiet.setAdapter(Adapter_danhsachdocso_chitiet);

        // GET DATA TABLE_DANHSACHDOCSO
        Json_table_ghiso =getIntent().getStringExtra("TABLE_DANHSACHDOCSO");

        // SET DATA
        if(Get_Table_GhiSo(Json_table_ghiso)){
            String tieude = "Kỳ "+Ky+"/"+Nam+" - Đ"+Dot +" - M"+SoMay;
            tv_Tieude.setText(tieude);
            tv_sumdh.setText(String.valueOf(Sum_Dh));
            tv_sum_dadoc.setText(String.valueOf(Sum_DaDoc));
            tv_sum_sanluong.setText(String.valueOf(Sum_SanLuong));
            double per =0;
            if(Sum_Dh!=0) {
                per = Sum_DaDoc * 100 / Sum_Dh;
            }
            String per_new = String.valueOf(per);
            tv_per.setText(per_new +"%");
        }

        array_muctimkiem = new ArrayList<String>();
        adapter_muctimkiem = new ArrayAdapter(activity_Danhsachdocso_chitiet.this, android.R.layout.simple_spinner_item,array_muctimkiem);
        adapter_muctimkiem.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
        spn_muctimkiem.setAdapter(adapter_muctimkiem);
        array_muctimkiem.addAll(Arrays.asList(data_Muctimkiem));
        adapter_muctimkiem.notifyDataSetChanged();
        spn_muctimkiem.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent,View view,int position,long id) {
                switch (position) {
                    case 0:
                    sv_timkiem.setInputType(InputType.TYPE_CLASS_NUMBER);
                    break;
                    case 1:
                        sv_timkiem.setInputType(InputType.TYPE_CLASS_NUMBER);
                        break;
                    case 2:
                        sv_timkiem.setInputType(InputType.TYPE_CLASS_TEXT);
                        break;
                    case 3:
                        sv_timkiem.setInputType(InputType.TYPE_CLASS_TEXT);
                        break;
                }
                num_Selete_muctimkiem = position;
                sv_timkiem.setQuery("",false);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
       // Test_GanData_Listview();

        // ACTION
        lv_Danhsachdocso_chitiet.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent,View view,int position,long id) {
                data_Danhsachdocso_chitiet data_danhsachdocso_chitiet = Arraylist_data_danhsachdocso_chitiet.get(position);
                String danhba =data_danhsachdocso_chitiet.getDanhba();
                String nam = String.valueOf(Nam);
                String ky = String.valueOf(Ky);
                String dot = String.valueOf(Dot);
                String somay = String.valueOf(SoMay);
                String tinhtrang = "all";
                String json_send = "{\"danhba\":\""+danhba+"\","+
                        "\"nam\":\""+nam+"\","+
                        "\"ky\":\""+ky+"\","+
                        "\"dot\":\""+dot+"\","+
                        "\"somay\":\""+somay+"\","+
                        "\"tinhtrang\":\""+tinhtrang+"\"}";

             //   Log.e("12",json_send);
                Intent intent = new Intent(activity_Danhsachdocso_chitiet.this,activity_Danhsachdocso_chitiet_khachhang.class);
                intent.putExtra("TABLE_DANHSACHDOCSO_CHITIET",json_send);
                startActivity(intent);
            }
        });

        sv_timkiem.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
        sv_timkiem.setSelected(false);
                    ListTimKiem(newText.toString().trim());
               // String text = newText;
               // Adapter_danhsachdocso_chitiet.filter(text);

                return true;
            }
        });
        // ACTION : Click menu
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                int id = menuItem.getItemId();
                if(id == R.id.nav_mainmenu){
                    Intent intent = new Intent(activity_Danhsachdocso_chitiet.this,MainActivity.class);
                    //intent.putExtra("SODIENTHOAI",com_reponse[1]);
                    finish();
                    startActivity(intent);
                }
                else if (id == R.id.nav_danhsachdocso) {
                    // danh sach bat thuong
                    Intent intent = new Intent(activity_Danhsachdocso_chitiet.this,activity_Danhsachdocso.class);
                    //intent.putExtra("SODIENTHOAI",com_reponse[1]);
                      finish();
                    startActivity(intent);
                    // Toast.makeText(MainActivity.this,"BB",Toast.LENGTH_SHORT).show();
                }
                else if (id == R.id.nav_danhsachbatthuong) {
                    // danh sach bat thuong
                    Intent intent = new Intent(activity_Danhsachdocso_chitiet.this,activity_Danhsachbatthuong.class);
                    String json_send = "{\"Nam\":\"" + Nam + "\"," +
                            "\"Ky\":\"" + Ky + "\"," +
                            "\"Dot\":\"" + Dot + "\"," +
                            "\"SoMay\":\"" + SoMay + "\"" +
                            "}";
             //       Log.e("11",json_send);
                    intent.putExtra("TABLE_DANHSACHDOCSO_BATTHUONG",json_send);
                    //intent.putExtra("SODIENTHOAI",com_reponse[1]);
                    //  finish();
                    startActivity(intent);
                    // Toast.makeText(MainActivity.this,"BB",Toast.LENGTH_SHORT).show();
                }else if(id == R.id.nav_bandoghiso){
                    Dialog_App dialog_app = new Dialog_App(activity_Danhsachdocso_chitiet.this);
                    String notification = "Trang bản đồ ghi số đang phát triển."+"\r\b"+"Xin vui lòng thông cảm";
                    dialog_app.Dialog_Notification(notification);

                }else if(id == R.id.nav_bandodaghiso){
                    Dialog_App dialog_app = new Dialog_App(activity_Danhsachdocso_chitiet.this);
                    String notification = "Trang bản đồ đã ghi số đang phát triển."+"\r\b"+"Xin vui lòng thông cảm";
                    dialog_app.Dialog_Notification(notification);
                }

                else if (id == R.id.nav_mayinbluetooth) {
                    // danh sach bat thuong
                    Intent intent = new Intent(activity_Danhsachdocso_chitiet.this,activity_Mayin.class);
                    //intent.putExtra("SODIENTHOAI",com_reponse[1]);
                     finish();
                    startActivity(intent);
                    // Toast.makeText(MainActivity.this,"BB",Toast.LENGTH_SHORT).show();
                }else if(id == R.id.nav_dangxuat){
                    String notìication ="Bạn muốn đăng xuất tài khoản này.";
                    Dialog_Notification_Dangxuat(notìication);
                }

                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
                return false;
            }
        });


      //  Display_DanhSachKhachHang();
       // Display_User();
    }




    private void  Init_Nam_Ky_Dot(){

    }
    /*
    private void Display_User(){
        sqlite_User sqlite_user = new sqlite_User(activity_Danhsachdocso_chitiet.this);
        if(sqlite_user.getStatus_Table_User_Exists()) {
            try {

                data_User user_dangnhap = sqlite_user.Get_User("1");
                String somay = user_dangnhap.getSomay().trim();
                String tennv = user_dangnhap.getTennhanvien().toString();
                String sodienthoai = user_dangnhap.getdienthoai().toString();

                tv_tennv_navi.setText("  Nhân viên: "+tennv);
                tv_sodienthoai_navi.setText("Điện thoại: "+sodienthoai);
                tv_somay_navi.setText("Số máy: "+somay);

            }catch (Exception e){

            }
        }
    }

     */
    AlertDialog dialog_notification_dangxuat ;
    private void Dialog_Notification_Dangxuat(final String text){
        AlertDialog.Builder dialog_notification =new AlertDialog.Builder(activity_Danhsachdocso_chitiet.this);
        dialog_notification.setMessage(text);
        // dong y
        dialog_notification.setPositiveButton("Đồng ý",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog,int which) {
                Intent intent = new Intent(activity_Danhsachdocso_chitiet.this,activity_DangNhap.class);
                //intent.putExtra("SODIENTHOAI",com_reponse[1]);
                finish();
                startActivity(intent);
                dialog_notification_dangxuat.dismiss();
            }
        });
        // bo qua
        dialog_notification.setNegativeButton("Hủy",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog,int which) {
                dialog_notification_dangxuat.dismiss();
            }
        });
        dialog_notification_dangxuat = dialog_notification.create();
        dialog_notification_dangxuat.show();
    }
    private void ListTimKiem(String text_check){
        text_check = text_check.toLowerCase(Locale.getDefault());
        try{
            Arraylist_data_danhsachdocso_chitiet.clear();
            Adapter_danhsachdocso_chitiet.notifyDataSetChanged();

        }catch (Exception e) {

        }
        if(text_check.equals("")) {
            Arraylist_data_danhsachdocso_chitiet.addAll(Arraylist_data_danhsachdocso_chitiet_temp);
        }
        else {
            switch (num_Selete_muctimkiem) {
                case 0: {
                    for (data_Danhsachdocso_chitiet data : Arraylist_data_danhsachdocso_chitiet_temp) {
                        try {
                            if (data.getDanhba().toLowerCase(Locale.getDefault()).contains(text_check)) {
                                Arraylist_data_danhsachdocso_chitiet.add(data);
                            }
                        }catch (Exception e){

                        }
                    }
                    break;
                }
                case 1:{
                    for (data_Danhsachdocso_chitiet data : Arraylist_data_danhsachdocso_chitiet_temp) {
                        try {
                            if (data.getSo_st().toLowerCase(Locale.getDefault()).contains(text_check)) {
                                Arraylist_data_danhsachdocso_chitiet.add(data);
                            }
                        }catch (Exception e){

                        }
                    }
                    break;
                }
                case 2:{
                    for (data_Danhsachdocso_chitiet data : Arraylist_data_danhsachdocso_chitiet_temp) {
                        try {
                            if (data.getDiaChi().toLowerCase(Locale.getDefault()).contains(text_check)) {
                                Arraylist_data_danhsachdocso_chitiet.add(data);
                            }
                        }catch (Exception e){

                        }
                    }
                    break;
                }
                case 3:{
                    for (data_Danhsachdocso_chitiet data : Arraylist_data_danhsachdocso_chitiet_temp) {
                        try {
                            if (data.getHoten().toLowerCase(Locale.getDefault()).contains(text_check)) {
                                Arraylist_data_danhsachdocso_chitiet.add(data);
                            }
                        }catch ( Exception e){

                        }
                    }
                    break;
                }
            }
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Adapter_danhsachdocso_chitiet.notifyDataSetChanged();
                }
            });
        }
    }
    private String Check_Enable_Ghiso(data_Enable_Ghiso data_enable_ghiso){
        String status = "0";
        try{
            SimpleDateFormat format_datetime_chukyds = new SimpleDateFormat("dd/MM/yyyy'T'HH:mm:ss");
            Date currenttime = Calendar.getInstance().getTime();
            String h_start = Objects.toString(data_enable_ghiso.getGiodoctruoc(),"").trim();
            String date_start = Objects.toString(data_enable_ghiso.getNgaydoctruoc(),"").trim();
            String h_end = Objects.toString(data_enable_ghiso.getGiokhoadoc(),"").trim();
            String date_end = Objects.toString(data_enable_ghiso.getNgaykhoadoc(),"").trim();
            if(!h_start.isEmpty()
                    && !date_start.isEmpty()
                    && !h_end.isEmpty()
                    && !date_end.isEmpty()){
                String date_enable_start = date_start+"T"+h_start+":00:00";
                String date_enable_end = date_end+"T"+h_end+":00:00";
                Date date_start_ghiso = format_datetime_chukyds.parse(date_enable_start);
                Date date_end_ghiso = format_datetime_chukyds.parse(date_enable_end);

                long diff_start = date_start_ghiso.getTime();
                long diff_end = date_end_ghiso.getTime();
                long diff_current = currenttime.getTime();
                if((diff_current >= diff_start) && (diff_current <= diff_end)){
                    status ="1";
                    return  status;
                }

            }else if(  !h_start.isEmpty()
                    && !date_start.isEmpty()){

                String date_enable_start = date_start+"T"+h_start+":00:00";
                Date date_start_ghiso = format_datetime_chukyds.parse(date_enable_start);

                long diff_start = date_start_ghiso.getTime();
                long diff_current = currenttime.getTime();
                if((diff_current >= diff_start) ){
                    status ="1";
                    return  status;
                }
            }
            else if(  !h_end.isEmpty()
                    && !date_end.isEmpty()){

                String date_enable_end = date_end+"T"+h_end+":00:00";
                Date date_end_ghiso = format_datetime_chukyds.parse(date_enable_end);

                long diff_end = date_end_ghiso.getTime();
                long diff_current = currenttime.getTime();
                if( (diff_current <= diff_end)){
                    status ="1";
                    return  status;
                }
            }
        }catch (Exception e){

        }
        return  status;
    }

    @Override
    protected void onResume() {

            Display_DanhSachKhachHang();
            sv_timkiem.setQuery("",true);


        super.onResume();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),0);
        }
        sv_timkiem.setSelected(true);
        return super.dispatchTouchEvent(ev);
    }
/*
    private void Test_GanData_Listview() {
        data_Danhsachdocso_chitiet data1 = new data_Danhsachdocso_chitiet(1,"456787454530","34532520",0,"Nguyen Van A","113 Nguyen Trai P3 Q1");
        Arraylist_data_danhsachdocso_chitiet.add(data1);
        data_Danhsachdocso_chitiet data2 = new data_Danhsachdocso_chitiet(2,"456787454531","34532521",1,"Nguyen Van B","114 Nguyen Trai P3 Q1");
        Arraylist_data_danhsachdocso_chitiet.add(data2);
        data_Danhsachdocso_chitiet data3 = new data_Danhsachdocso_chitiet(3,"456787454532","34532522",2,"Nguyen Van C","115 Nguyen Trai P3 Q1");
        Arraylist_data_danhsachdocso_chitiet.add(data3);
        data_Danhsachdocso_chitiet data4 = new data_Danhsachdocso_chitiet(4,"456787454533","34532523",0,"Nguyen Van D","116 Nguyen Trai P3 Q1");
        Arraylist_data_danhsachdocso_chitiet.add(data4);
        data_Danhsachdocso_chitiet data5 = new data_Danhsachdocso_chitiet(5,"456787454534","34532524",1,"Nguyen Van E","117 Nguyen Trai P3 Q1");
        Arraylist_data_danhsachdocso_chitiet.add(data5);
        data_Danhsachdocso_chitiet data6 = new data_Danhsachdocso_chitiet(6,"456787454535","34532525",2,"Nguyen Van F","118 Nguyen Trai P3 Q1");
        Arraylist_data_danhsachdocso_chitiet.add(data6);
    }

 */
    private void Display_DanhSachKhachHang(){
        String nam = String.valueOf(Nam);
        String ky = String.valueOf(Ky);
        String dot = String.valueOf(Dot);
        String somay = String.valueOf(SoMay);
        sqlite_DanhSachKhackHang_DocSo sqlite_danhSachKhackHang_docSo = new sqlite_DanhSachKhackHang_DocSo(activity_Danhsachdocso_chitiet.this,nam,ky,dot,somay);
        Arraylist_data_danhsachdocso_chitiet.clear();
        Arraylist_data_danhsachdocso_chitiet_temp.clear();
        if(sqlite_danhSachKhackHang_docSo.getStatus_Table_DanhSachKhachHang_Exists()){
            List<data_Danhsachdocso_chitiet> khachhangs = sqlite_danhSachKhackHang_docSo.Get_DanhSachDocSo_Chitiet();
            Arraylist_data_danhsachdocso_chitiet.addAll(khachhangs);

        }
        Adapter_danhsachdocso_chitiet.notifyDataSetChanged();
        Arraylist_data_danhsachdocso_chitiet_temp.addAll(Arraylist_data_danhsachdocso_chitiet);

    }

    private void Mappping_Layout() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tv_Tieude = (TextView) findViewById(R.id.textview_tieude_dongdanhsachdocso_chitiet);
        tv_sumdh = (TextView) findViewById(R.id.textview_sumdh_danhsachdocso_chitiet);
        tv_sum_dadoc = (TextView) findViewById(R.id.textview_sumdadoc_danhsachdocso_chitiet);
        tv_sum_sanluong = (TextView) findViewById(R.id.textview_sumsanluong_danhsachdocso_chitiet);
        tv_per = (TextView) findViewById(R.id.textview_per_danhsachdocso_chitiet);
        lv_Danhsachdocso_chitiet = (ListView) findViewById(R.id.listview_danhsach_danhsachdocso_chitiet);
        sv_timkiem = (SearchView) findViewById(R.id.searchView_timkiem_danhsachdocso_chitiet);
        spn_muctimkiem =(Spinner) findViewById(R.id.spinner_muctimkiem_danhsachdocso_chitiet);
    }
    private boolean Get_Table_GhiSo(String json){
        boolean status = false;
        try{
            JSONObject table = new JSONObject(json);
            Nam = table.getString("Nam");
            Ky = table.getString("Ky");
            Dot = table.getString("Dot");
            SoMay = table.getString("SoMay");
            Sum_Dh = table.getInt("Sum_DH");
            Sum_DaDoc = table.getInt("Sum_DaDoc");
            Sum_SanLuong = table.getInt("Sum_SanLuong");
            status = true;
        }catch (JSONException e){

        }
        return  status;
    }
    private View  Mappping_Layout_NavigationView(NavigationView navigationView ){
        View header = navigationView.getHeaderView(0);
/*
        // NavigationView
        imv_user = (ImageView) header.findViewById(R.id.imageview_user_NavigationView);
        tv_tennv_navi =(TextView) header.findViewById(R.id.textview_tennv_NavigationView);
        tv_somay_navi = (TextView) header.findViewById(R.id.textview_somay_NavigationView);
        tv_sodienthoai_navi = (TextView) header.findViewById(R.id.textview_sodienthoai_NavigationView);
        imbt_dangxuat =(ImageButton) header.findViewById(R.id.imagebutton_dangxuat_NavigationView);

 */
        return  header;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



}
package com.dkhanhaptechs.mghiso.Share.LichSuThayDH;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.TabHost;

import com.dkhanhaptechs.mghiso.Share.DanhSach_DocSo.sqlite_DanhSachKhackHang_DocSo;
import com.dkhanhaptechs.mghiso.Share.Dialog_App;

import com.dkhanhaptechs.mghiso.danhsachdocso_chitiet_khachhang.activity_Danhsachdocso_chitiet_khachhang;
import com.google.gson.Gson;
import com.loopj.android.http.HttpGet;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.client.utils.URIBuilder;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import cz.msebera.android.httpclient.util.EntityUtils;
import  com.dkhanhaptechs.mghiso.Share.User.*;

public class get_LSthayDH_Asynctask extends AsyncTask<Void,Integer,Void> {
    Context contextParent;
    private  String Danhba;
    private Dialog_App dialog_app ;
    private SimpleDateFormat format_datetime = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    private Date Date_tungay;
    private Date Date_denngay;
    private String Nam;
    private String Ky ;
    private String Dot;
    private String Somay;
    private String Thongbao;
    private String BASE_URL_API ="/apikhachhang/api/DocSoLichSuThayDHN?";
    private String BASE_URL ="";
    public get_LSthayDH_Asynctask(Context contextParent,String danhba, String tungay,String denngay, String nam,String ky,String dot, String somay ) throws ParseException {
        this.contextParent = contextParent;
        Danhba = danhba;
        dialog_app = new Dialog_App(contextParent);
        SimpleDateFormat format_datetime_thaydh = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date_tungay = format_datetime_thaydh.parse(tungay);
        Date_denngay = format_datetime_thaydh.parse(denngay);
        Nam =nam ;
        Ky = ky;
        Dot = dot;
        Somay = somay;
        Thongbao ="";
        data_Http data_http = new data_Http(contextParent);
            String url_run = data_http.get_Url_run();
            if(!url_run.isEmpty()){
                BASE_URL = url_run +BASE_URL_API;
            }else {
                BASE_URL = "http://viettel.capnuoccholon.com.vn"+BASE_URL_API;
            }

    }
    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    //    dialog_app.Dialog_Progress_Wait_Open("Đang lấy ảnh","Vui lòng chờ");
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        if(!Thongbao.isEmpty()){
            dialog_app.Dialog_Notification(Thongbao);
        }
     //   dialog_app.Dialog_Progress_Wait_Close();

    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected Void doInBackground(Void... voids) {
        try {
        data_Lichsuthaydh_send data_lichsuthaydh_send = new data_Lichsuthaydh_send(Danhba);
        Gson gson_request = new Gson();
        String json_send = gson_request.toJson(data_lichsuthaydh_send);
        StringEntity stringEntity_send = new StringEntity(json_send,"utf-8");
        HttpClient httpclient = new DefaultHttpClient();
            URI uri_ = new URIBuilder(BASE_URL)
                    .addParameter("thamso",json_send)
                    .build();
        HttpGet httpget = new HttpGet(uri_);
        //  httppost.setHeader("Accept", "application/json");
            httpget.setHeader("Content-type","application/json;charset=utf-8");
       //     httpget.setEntity(stringEntity_send);
            HttpResponse response = httpclient.execute(httpget);
            if (response.getStatusLine().getStatusCode() == 200) {
                sqlite_DanhSachKhackHang_DocSo sqlite_danhSachKhackHang_docSo = new sqlite_DanhSachKhackHang_DocSo(contextParent,Nam,Ky,Dot,Somay);

                SimpleDateFormat format_datetime_thay = new SimpleDateFormat("dd/MM/yyyy");
                String responseString = EntityUtils.toString(response.getEntity());
                Gson gson_reponse = new Gson();
                data_Lichsuthaydong_response data_lichsuthaydong_response = gson_reponse.fromJson(responseString,data_Lichsuthaydong_response.class);
                data_Lichsuthaydong_response.data_lichsudongho[]  all_data_thongtindh =data_lichsuthaydong_response.getResult().getLichsudonghos();
                String status ="no";
                for (int i =all_data_thongtindh.length-1 ; i >=0 ;i--){
                    try{
                        String ngaythay = all_data_thongtindh[i].getNgaythay();
                        Date date_thay = format_datetime_thay.parse(ngaythay);
                        long l_tungay = Date_tungay.getTime()/1000;
                        long l_denngay = Date_denngay.getTime()/1000;
                        long l_thay = date_thay.getTime()/1000;
                        if((l_thay>=l_tungay) && (l_thay <=l_denngay)) {
                            String lydo = all_data_thongtindh[i].getLydo();
                            String csgo = all_data_thongtindh[i].getCSGo();
                            String csgan = all_data_thongtindh[i].getCSGan();
                            sqlite_danhSachKhackHang_docSo.Update_ThayDH_DanhSachDocSo_KhachHang(Danhba,csgo,csgan,ngaythay,lydo);
                           Thongbao ="THAY ĐỒNG HỒ NƯỚC: "+Danhba+"\r\n"+
                                    "ĐHN thay ngày: "+ ngaythay +"\r\n"+
                                    "Lý do: "+ lydo +"\r\n"+
                                    "Chỉ số gỡ: "+csgo +"\r\n"+
                                    "Chỉ số gắn: "+ csgan;
                            status = "yes";
                            break;
                        }

                    }catch (Exception e) {

                    }
                }
                if(status.equals("no")){
                    sqlite_danhSachKhackHang_docSo.Update_ThayDH_DanhSachDocSo_KhachHang(Danhba,"","","no","");
                }
            }
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
        return null;
    }

}

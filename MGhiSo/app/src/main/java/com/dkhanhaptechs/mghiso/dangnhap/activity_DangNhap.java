package com.dkhanhaptechs.mghiso.dangnhap;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.dkhanhaptechs.mghiso.MainActivity;
import com.dkhanhaptechs.mghiso.R;
import com.dkhanhaptechs.mghiso.Share.Config_CongDung.Api_Config_CongDung;
import com.dkhanhaptechs.mghiso.Share.Config_CongDung.data_Api_Config_reponse;
import com.dkhanhaptechs.mghiso.Share.Config_CongDung.data_Api_Config_send;
import com.dkhanhaptechs.mghiso.Share.Config_CongDung.data_Config_CongDung;
import com.dkhanhaptechs.mghiso.Share.Config_CongDung.sqlite_Config_CongDung;
import com.dkhanhaptechs.mghiso.Share.Dialog_App;
import com.dkhanhaptechs.mghiso.Share.CheckServer.data_Api_Checkserver_send;
import com.dkhanhaptechs.mghiso.Share.CheckServer.data_Api_Checkserver_response;
import com.dkhanhaptechs.mghiso.Share.CheckServer.Api_Checkserver;
import com.dkhanhaptechs.mghiso.Share.LyDoTangGiam.Api_lydotanggiam;
import com.dkhanhaptechs.mghiso.Share.LyDoTangGiam.data_lydotanggiam;
import com.dkhanhaptechs.mghiso.Share.LyDoTangGiam.data_lydotanggiam_response;
import com.dkhanhaptechs.mghiso.Share.LyDoTangGiam.sqlite_config_lydotanggiam;
import com.dkhanhaptechs.mghiso.Share.User.sqlite_User;
import com.dkhanhaptechs.mghiso.Share.User.data_User;
import com.dkhanhaptechs.mghiso.Share.CheckVersion.*;
import com.dkhanhaptechs.mghiso.Share.MAC_SMID.*;
import com.google.android.gms.location.FusedLocationProviderClient;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.gson.Gson;

import com.loopj.android.http.TextHttpResponseHandler;



import java.net.SocketException;


import cz.msebera.android.httpclient.Header;
import  com.dkhanhaptechs.mghiso.Share.User.*;

public class activity_DangNhap extends AppCompatActivity {
    // BIEN LAYOUT
    EditText edt_Mac_simd;
    Button btn_Dangnhap;
    CheckBox cb_LuuMac_simd;
    TextView tv_version ;

    public static final int MESSAGE_READ = 3;

    // Key names received from the BluetoothPrintService Handler


    // BIEN CHUONG TRINH
    sqlite_User sqlite_user;
    sqlite_Checkversion sqlite_checkversion;
    private boolean Status_Server = false;
    private FusedLocationProviderClient fusedLocationProviderClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dangnhap);
        // INIT
        Mappping_Layout();
    //    Check_Server();
        sqlite_user = new sqlite_User(activity_DangNhap.this);
        // ACTION : Dangnhap
        btn_Dangnhap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /////////

                String mac_smid = edt_Mac_simd.getText().toString().trim();
                if (!mac_smid.isEmpty()) {
                    if(mac_smid.toUpperCase().equals("CNCL")){
                        Intent intent = new Intent(activity_DangNhap.this,MainActivity.class);
                        //intent.putExtra("SODIENTHOAI",com_reponse[1]);
                        finish();
                        startActivity(intent);
                    }else {
                        Dangnhap(mac_smid);
                    }
                } else {
                    String text_nontification = "Lỗi đăng nhặp"+"\r\n"+"Vui lòng nhập số MAC/SMID" ;
                    Dialog_App dialog_app_notidfication = new Dialog_App(activity_DangNhap.this);
                    dialog_app_notidfication.Dialog_Notification(text_nontification);

                }

                 /*
                get_Location_Device get_location_device = new get_Location_Device(activity_DangNhap.this);
                Log.e("Location get","lat:" +get_location_device.getLat() +"\r\n"+"Lng:" +get_location_device.getLng());

                  */

            }
        });

        // LUU SO MAC
        cb_LuuMac_simd.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
                if (cb_LuuMac_simd.isChecked()) {
                    edt_Mac_simd.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    String mac = edt_Mac_simd.getText().toString().trim();
                    if (mac.isEmpty()) {
                        if (sqlite_user.getStatus_Table_User_Exists()) {
                            data_User user = sqlite_user.Get_User("1");
                            edt_Mac_simd.setText(user.getMac());
                        }
                    }
                }else {


                    edt_Mac_simd.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
            }
        });

        // Kiem tra ket noi
        /*
        get_MAC_SMID get_mac_smid = null;
        try {
            get_mac_smid = new get_MAC_SMID();
        } catch (SocketException e) {
            e.printStackTrace();
        }
        String mac_smid = get_mac_smid.getMac_mid();
        edt_Mac_simd.setText(mac_smid);
*/
        // kiem tra version hien tai cua app
        Check_Version();
        //Check_Server();

        if (ContextCompat.checkSelfPermission(activity_DangNhap.this,Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},1);
        }

        Display_User();
       // get_Location();
    }
    private void Display_User(){
        sqlite_user = new sqlite_User(activity_DangNhap.this);
        if(sqlite_user.getStatus_Table_User_Exists()) {
            try {
                data_User user_dangnhap = sqlite_user.Get_User("1");
                String mamax = user_dangnhap.getMac().trim();
                if(!mamax.isEmpty()) {
                    edt_Mac_simd.setText(mamax);
                }
            }catch (Exception e){

            }
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),0);
        }
        return super.dispatchTouchEvent(ev);
    }

    private void get_Location() {
        if (ActivityCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        fusedLocationProviderClient.getLastLocation().addOnSuccessListener(this,new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if(location!=null){
                  //  Log.e("Location","lat:" +String.valueOf(location.getLatitude()) +"\r\n"+"Lng:" +String.valueOf(location.getLongitude()));
                }
            }
        });
    }

    // ANH XA LAYOUT
    private void  Mappping_Layout(){
        edt_Mac_simd = (EditText) findViewById(R.id.edittext_Mac_Smid_Dangnhap);
        btn_Dangnhap = (Button) findViewById(R.id.button_DangNhap_Dangnhap);
        cb_LuuMac_simd = (CheckBox) findViewById(R.id.checkbox_LuuMac_Smid_Dangnhap);
        tv_version = (TextView) findViewById(R.id.textview_version_Dangnhap);
    }



    // DANG NHAP
    private void Dangnhap(String mac_smid){

        try {
            final String mac = edt_Mac_simd.getText().toString().trim();
            data_Dangnhap_send data_send = new data_Dangnhap_send(mac);
            Gson gson_request = new Gson();
            String json_send = gson_request.toJson(data_send);
           // Toast.makeText(this,json_send,Toast.LENGTH_SHORT).show();
       //     Log.e("Json Send Dang nhap",json_send);
            Api_DangNhap.get(activity_DangNhap.this ,json_send,
                    null,
                    new TextHttpResponseHandler() {
                        @Override
                        public void onFailure(int statusCode,Header[] headers,String responseString,Throwable throwable) {

                            String text_nontification = "Lỗi đăng nhập"+"\r\n"+"Vui lòng kiểm tra lại.";
                            Dialog_App dialog_app_notidfication = new Dialog_App(activity_DangNhap.this);
                            dialog_app_notidfication.Dialog_Notification(text_nontification);
                            data_Http data_http = new data_Http(activity_DangNhap.this);
                            data_http.switch_Url_run();
                        }
                        @Override
                        public void onSuccess(int statusCode,Header[] headers,String responseString) {
                            Gson gson_reponse = new Gson();
                            data_Dangnhap_response data_dangnhap_response = gson_reponse.fromJson(responseString,data_Dangnhap_response.class);
                       //     Log.e("data_dangnhap_response",gson_reponse.toJson( data_dangnhap_response));
                            if(data_dangnhap_response.getStatus().equals("1")){

                                    data_User user_dangnhap = new data_User(
                                            1,
                                            mac,
                                            data_dangnhap_response.getResult().getTennhanvien(),
                                            data_dangnhap_response.getResult().getSomay(),
                                            data_dangnhap_response.getResult().getDienthoai()
                                    );
                                    if (sqlite_user.Insert_Update_User(user_dangnhap)) {
                                    }else {
                                   //     Log.e("Insert_Update_User","error");
                                    }

                                Intent intent = new Intent(activity_DangNhap.this,MainActivity.class);
                                //intent.putExtra("SODIENTHOAI",com_reponse[1]);
                                finish();
                                startActivity(intent);

                            }else {
                                String text_nontification = "Mã MAX/SMID không tồn tại."+"\r\n"+
                                        "Status server: "+data_dangnhap_response.getStatus() + "\r\n"+
                                        "Message server: " +data_dangnhap_response.getMessage() +"\r\n"+
                                        "Vui lòng kiểm tra lại mã MAX/SMID hoặc server.";
                                Dialog_App dialog_app_notidfication = new Dialog_App(activity_DangNhap.this);
                                dialog_app_notidfication.Dialog_Notification(text_nontification);
                            }
                        }
                    });
           // status = true;
        }catch (Exception e){
            String aa = e.toString() ;
        }

    }

    private void Check_Version(){
        // get version hien tai
       sqlite_checkversion = new sqlite_Checkversion(activity_DangNhap.this);
        final data_Checkversion version_current = sqlite_checkversion.Get_InfoVersion("1");
        //
        data_Checkversion_send data_checkversion_send = new data_Checkversion_send();
        Gson gson_send = new Gson();
        String data_send = gson_send.toJson(data_checkversion_send);
        Api_Checkversion.get(activity_DangNhap.this,data_send,null,new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode,Header[] headers,String responseString,Throwable throwable) {
                String text = "Lỗi từ server"+"\r\n"+"Vui lòng kiểm tra lại.";
                //Toast.makeText(activity_DangNhap.this,text,Toast.LENGTH_SHORT).show();
                Dialog_Notification(text);
                data_Http data_http = new data_Http(activity_DangNhap.this);
                data_http.switch_Url_run();
            }

            @Override
            public void onSuccess(int statusCode,Header[] headers,String responseString) {
                try {
                    Gson gson_reponse = new Gson();
                    data_Checkversion_response data_checkversion_response = gson_reponse.fromJson(responseString,data_Checkversion_response.class);
                    //   Log.e("checkversion response",gson_reponse.toJson( data_checkversion_response));
                    if (data_checkversion_response.getStatus().equals("1")) {

                        // kiem tra version
                        String version_api = data_checkversion_response.getResult().getVersion();
                        if (!version_current.getVersion().equals(version_api)) {
                            // hien thi podup thông báo cập nhật thông tin
                            // Toast.makeText(activity_DangNhap.this,"version hien tai api: " +version_api,Toast.LENGTH_SHORT).show();
                            //down load thong tin version moi
                            version_current.setVersion(version_api);
                            Dialog_Update_Version(version_current);
                            // luu version xuong sqlite

                        } else {
                            //  Toast.makeText(activity_DangNhap.this,"version hien tai app: " +version_current.getVersion().toString(),Toast.LENGTH_SHORT).show();

                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                tv_version.setText("Version: " + version_current.getVersion());
                            }
                        });

                    }
                }catch (Exception e){

                }
            }
        });

    }
    private void TaiCongDung(){
        // lay version
        sqlite_checkversion = new sqlite_Checkversion(activity_DangNhap.this);
        data_Checkversion version_current = sqlite_checkversion.Get_InfoVersion("1");
        sqlite_Config_CongDung sqlite_config_congDung = new sqlite_Config_CongDung(activity_DangNhap.this);
        if(sqlite_config_congDung.getStatus_Table_ListCongDung_Exists()) {
            if(!sqlite_config_congDung.Delete_Table_ListCongDung()){
                return;
            }
        }
        String version  = version_current.getVersion();
        data_Api_Config_send data_api_config_send = new data_Api_Config_send(version);
        Gson gson_request = new Gson();
        String json_send = gson_request.toJson(data_api_config_send);
        Api_Config_CongDung.get(activity_DangNhap.this,json_send,
                null,new TextHttpResponseHandler() {
                    @Override
                    public void onFailure(int statusCode,Header[] headers,String responseString,Throwable throwable) {
                        data_Http data_http = new data_Http(activity_DangNhap.this);
                        data_http.switch_Url_run();
                    }

                    @Override
                    public void onSuccess(int statusCode,Header[] headers,String responseString) {
                        Gson gson_reponse = new Gson();
                        data_Api_Config_reponse data_api_config_reponse = gson_reponse.fromJson(responseString,data_Api_Config_reponse.class);
                   //     Log.e("data_dangnhap_response",gson_reponse.toJson( data_api_config_reponse));
                        data_Api_Config_reponse.data_congdung [] congdungs = data_api_config_reponse.getResult().getCongdungs();
                        if(data_api_config_reponse !=null) {
                            sqlite_Config_CongDung  sqlite_config_congDung = new sqlite_Config_CongDung(activity_DangNhap.this);
                            if(sqlite_config_congDung.getStatus_Table_ListCongDung_Exists()) {
                                for (int i = 0; i < congdungs.length; i++) {
                                    String doituong = congdungs[i].getDoiTuong();
                                    data_Api_Config_reponse.data_ListCongDung[] list_congdung = congdungs[i].getListCongDung();
                                    for (int j = 0; j < list_congdung.length; j++) {
                                        String congdung = list_congdung[j].getCong_dung();
                                        // luu sql
                                        data_Config_CongDung data_config_congDung = new data_Config_CongDung(doituong,congdung);
                                        sqlite_config_congDung.Insert_Table_ListCongDung(data_config_congDung);
                                    }
                                }
                            }
                        }
                        // update data

                    }
                });
    }
    private void TaiLyDoTangGiam(){
        // lay version
        /*
        sqlite_checkversion = new sqlite_Checkversion(MainActivity.this);
        data_Checkversion version_current = sqlite_checkversion.Get_InfoVersion("1");

         */
        sqlite_config_lydotanggiam sqlite_config_lydotanggiam = new sqlite_config_lydotanggiam(activity_DangNhap.this);
        if(sqlite_config_lydotanggiam.getStatus_Table_ListLyDoTangGiam_Exists()) {
            if(!sqlite_config_lydotanggiam.Delete_Table_ListLyDoTangGiam()){
                return;
            }
        }
        /*
        String version  = version_current.getVersion();
        data_Api_Config_send data_api_config_send = new data_Api_Config_send(version);
        Gson gson_request = new Gson();
        String json_send = gson_request.toJson(data_api_config_send);
        *
         */
        Api_lydotanggiam.get(activity_DangNhap.this,"",
                null,new TextHttpResponseHandler() {
                    @Override
                    public void onFailure(int statusCode,Header[] headers,String responseString,Throwable throwable) {
                        data_Http data_http = new data_Http(activity_DangNhap.this);
                        data_http.switch_Url_run();
                    }

                    @Override
                    public void onSuccess(int statusCode,Header[] headers,String responseString) {
                        Gson gson_reponse = new Gson();
                        data_lydotanggiam_response data_lydotanggiam_response = gson_reponse.fromJson(responseString,data_lydotanggiam_response.class);
                      //  Log.e("data_lydoTG_response",gson_reponse.toJson( data_lydotanggiam_response));
                        data_lydotanggiam_response.data_lydotanggiam [] all_data = data_lydotanggiam_response.getResult().getLydotanggiam();
                        if(all_data !=null) {
                            sqlite_config_lydotanggiam  sqlite_config_lydotanggiam = new sqlite_config_lydotanggiam(activity_DangNhap.this);
                            if(sqlite_config_lydotanggiam.getStatus_Table_ListLyDoTangGiam_Exists()) {
                                for (int i = 0; i < all_data.length; i++) {
                                    String ma = all_data[i].getMa();
                                    String noidung = all_data[i].getNoidung();
                                    data_lydotanggiam data_lydotanggiam = new data_lydotanggiam(ma,noidung);
                                    sqlite_config_lydotanggiam.Insert_Table_ListLyDoTangGiam(data_lydotanggiam);
                                }
                            }
                        }
                        // update data

                    }
                });
    }

    AlertDialog alertDialog_update ;
    private void Dialog_Update_Version(final data_Checkversion version){
        AlertDialog.Builder alert_update_version =new AlertDialog.Builder(activity_DangNhap.this);
        alert_update_version.setMessage("Có version mới :"+version.getVersion().toString() +"\r\n" + "Bạn có muốn cập nhật không ?");
        // dong y
        alert_update_version.setPositiveButton("Đồng ý",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog,int which) {
                alertDialog_update.dismiss();
                try {
                    Dialog_Progress_Updateversion(version);
                    TaiCongDung();
                    TaiLyDoTangGiam();
                    dialog_progress_updateversion.dismiss();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        });
        // bo qua
        alert_update_version.setNegativeButton("Bỏ qua",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog,int which) {
                finish();
            }
        });
        alertDialog_update = alert_update_version.create();
        alertDialog_update.show();
    }
    ProgressDialog dialog_progress_updateversion ;
    private void Dialog_Progress_Updateversion(final data_Checkversion version_current) throws InterruptedException {
        dialog_progress_updateversion = new ProgressDialog(activity_DangNhap.this);
        dialog_progress_updateversion.setTitle("Cập nhật phiên bản dữ liệu:" +version_current.getVersion().toString());
        dialog_progress_updateversion.setMessage("Dang Cập nhật");
        dialog_progress_updateversion.setMax(100);
        dialog_progress_updateversion.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        dialog_progress_updateversion.setCancelable(false);
        dialog_progress_updateversion.show();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    while (dialog_progress_updateversion.getProgress() <= dialog_progress_updateversion.getMax()){
                        Thread.sleep(10);
                        handler_update.handleMessage(handler_update.obtainMessage());

                        if (dialog_progress_updateversion.getProgress() == dialog_progress_updateversion.getMax()) {
                            if(sqlite_checkversion.getStatus_Table_InfoVersion_Exists()){
                                sqlite_checkversion.Insert_Update_Table_InfoVersion(version_current);
                            }

                        }
                    }
                }catch (Exception e){

                }
            }
        }).start();



    }
    Handler handler_update = new Handler(){
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            dialog_progress_updateversion.incrementProgressBy(1);

        }
    };


    private void Check_Server(){
        data_Api_Checkserver_send data_api_checkserver_send = new data_Api_Checkserver_send();
        Gson gson_send = new Gson();
        String data_send = gson_send.toJson(data_api_checkserver_send);
        Api_Checkserver.get(activity_DangNhap.this,data_send,
                null ,
             new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode,Header[] headers,String responseString,Throwable throwable) {
                String text = "Server MGhi số không thể kết nối"+"\r\n"+"Vui lòng kiểm tra lại.";
                Toast.makeText(activity_DangNhap.this,text,Toast.LENGTH_SHORT).show();
                data_Http data_http = new data_Http(activity_DangNhap.this);
                data_http.switch_Url_run();
            }

            @Override
            public void onSuccess(int statusCode,Header[] headers,String responseString) {
                Gson gson_reponse = new Gson();
                data_Api_Checkserver_response data_api_checkserver_response = gson_reponse.fromJson(responseString,data_Api_Checkserver_response.class);
              //  Log.e("checkserver response",gson_reponse.toJson( data_api_checkserver_response));
                if(data_api_checkserver_response.getStatus().equals("1")) {
                    Status_Server = true;
                    String text_notification = "Kết nối đến server :" +  data_api_checkserver_response.getResult().getNetwork().toString();
                    Dialog_Notification(text_notification);
                }else {
                    String text_notification = "Mất kết nối với server." +"\r\n"+"Vui lòng liên hệ với Cấp Nước đẻ kiểm tra lại servẻ";
                    Dialog_Notification(text_notification);
                }

            }
        });
    }
    AlertDialog dialog_notification ;
    private void Dialog_Notification(final String text){
        AlertDialog.Builder alert_update_version =new AlertDialog.Builder(activity_DangNhap.this);
        alert_update_version.setMessage(text);
        // dong y
        alert_update_version.setPositiveButton("Đồng ý",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog,int which) {
                dialog_notification.dismiss();
            }
        });
        // bo qua
        alert_update_version.setNegativeButton("Bỏ qua",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog,int which) {
                finish();
            }
        });
        dialog_notification = alert_update_version.create();
        dialog_notification.show();
    }


}
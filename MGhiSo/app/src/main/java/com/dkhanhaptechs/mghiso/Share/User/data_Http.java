package com.dkhanhaptechs.mghiso.Share.User;

import android.content.Context;

public class data_Http {
    private int id ;
    private String Http_run ;
    private Context Context_;
    private String Url_1 ="http://viettel3.capnuoccholon.com.vn";
    private String Url_2 ="http://viettel.capnuoccholon.com.vn";

    public data_Http(int id, String http_run) {
        this.id = id;
        Http_run = http_run;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getHttp_run() {
        return Http_run;
    }

    public void setHttp_run(String http_run) {
        Http_run = http_run;
    }

    public data_Http(Context context){
        Context_ = context;
    }
    public String get_Url_run(){
        String url_run = "";
        try{
            sqlite_Http sqlite_http = new sqlite_Http(Context_);
            if(sqlite_http.get_Status_Table_Http_Run()){
                url_run = sqlite_http.get_Table_Http_Run();
            }
        }catch (Exception e){

        }
        return  url_run;
    }
    public boolean switch_Url_run(){
        Boolean status = false;
        try{

            sqlite_Http sqlite_http = new sqlite_Http(Context_);
            if(sqlite_http.get_Status_Table_Http_Run()) {
                String url_run = sqlite_http.get_Table_Http_Run();
                if(url_run.equals(Url_1)){
                    url_run = Url_2;
                }else {
                    url_run = Url_1;
                }
                sqlite_http.Insert_Update_Table_Http_Run(url_run);
            }
        }catch (Exception e){

        }
        return  status;
    }

}

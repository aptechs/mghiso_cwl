package com.dkhanhaptechs.mghiso.Share.TienNo;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.dkhanhaptechs.mghiso.Share.Database_SQLite;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class sqlite_TienNo {
    private Database_SQLite database_sqLite ;
    private String Database ="Database_TienNo";
    private String Table_DanhSachHangHang_TienNo = "DanhSachHangHang_TienNo";
    private boolean Status_DanhSachHangHang_TienNo_Exists = false;
    public  sqlite_TienNo(Context context,String nam,String ky,String dot,String somay){
        this.Database = Database +"_"+nam+".sqlite";
        this.Table_DanhSachHangHang_TienNo = Table_DanhSachHangHang_TienNo +"_"+nam+"_"+ky+"_"+dot+"_"+somay ;
        database_sqLite = new Database_SQLite(context, Database, null, 1);
        if(Create_Table_DanhSachHangHang_TienNo()){
            Status_DanhSachHangHang_TienNo_Exists = true;
        }else {
            Status_DanhSachHangHang_TienNo_Exists =false;
        }
    }
    public boolean getStatus_Table_DanhSachHangHang_TienNo_Exists() {
        return Status_DanhSachHangHang_TienNo_Exists;
    }
    private boolean Create_Table_DanhSachHangHang_TienNo () {
        boolean ok = false;
        try {
            String querry = "CREATE TABLE IF NOT EXISTS "+Table_DanhSachHangHang_TienNo +
                    " (" +
                    " id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                    " danhba nvarchar(50)," +
                    " nam nvarchar(5)," +
                    " ky nvarchar(5)," +
                    " tienno nvarchar(20)" +
                    ");";
            if(database_sqLite.QueryDatabase(querry)) {
                ok = true;
            }

        } catch (Exception e) {

        }
        return  ok;
    }
    public boolean Insert_Update_Table_DanhSachHangHang_TienNo(data_tiennos data_tiennos){
        boolean ok = false;
        try{
            for(data_tiennos.data_tienno data_tienno: data_tiennos.getTiennos()) {

                try {

                    String querry_chẹck = "SELECT * FROM "+Table_DanhSachHangHang_TienNo +
                            " WHERE " +
                            "  danhba ='"+data_tiennos.getDanhba()+"' AND nam ='"+data_tienno.getNam() +"' AND ky ='"+data_tienno.getKy()+"' " +
                            ";";
                    Cursor data_check = database_sqLite.GetDatabase(querry_chẹck);
                    String id = "";
                    while (data_check.moveToNext()) {
                        id = data_check.getString(0);
                    }
                    String querry ="";
                    if(!id.isEmpty()){
                        querry = "UPDATE "+Table_DanhSachHangHang_TienNo+
                                " SET " +
                                " tienno ='"+ Objects.toString(data_tienno.getTienNo(),"")+"'" +
                                " WHERE " +
                                "  danhba ='"+data_tiennos.getDanhba()+"' AND nam ='"+data_tienno.getNam() +"' AND ky ='"+data_tienno.getKy()+"' " +
                                ";";
                    }else {

                        querry = "INSERT INTO " + Table_DanhSachHangHang_TienNo + " (danhba,nam,ky,tienno)" +
                                " VALUES (" +
                                "'" + data_tiennos.getDanhba() + "'," +
                                "'" + data_tienno.getNam() + "'," +
                                "'" + data_tienno.getKy() + "'," +
                                "'" + data_tienno.getTienNo() + "'" +

                                ");";
                    }
                    if (database_sqLite.QueryDatabase(querry)) {
                        ok = true;
                    }
                }catch (Exception e){

                }
            }
        }catch (Exception e){
            Log.e(" Update Sync", e.toString());
        }
        return  ok;
    }
    public boolean Delete_Table_DanhSachHangHang_TienNo(){
        boolean ok =false;
        try {
            String querry = "DROP TABLE IF EXISTS "+Table_DanhSachHangHang_TienNo+"; ";

            if(database_sqLite.QueryDatabase(querry)) {
                ok = true;
                Log.e("D T DSDS_HackHang ","ok");
            }
        }catch (Exception e){

        }
        return ok;
    }
    public List<data_tiennos > Get_All_Data_TienNo(){
        List<data_tiennos > list_data_tiennos = new ArrayList<>();
        try {
            String querry = "SELECT DISTINCT (danhba) FROM "+Table_DanhSachHangHang_TienNo + ";";
            Cursor data_danhba = database_sqLite.GetDatabase(querry);
            while (data_danhba.moveToNext()) {
                String danhba = data_danhba.getString(0);
                String querry_tienno = "SELECT nam,ky,tienno FROM "+Table_DanhSachHangHang_TienNo + " WHERE danhba ='"+danhba+"' ORDERBY nam DESC , ky DESC;";
                Cursor data_tienno = database_sqLite.GetDatabase(querry_tienno);
                List<data_tiennos.data_tienno> list_tieno = new ArrayList<>();
                while (data_tienno.moveToNext()) {
                    String nam = data_tienno.getString(0);
                    String ky = data_tienno.getString(1);
                    String tienno = data_tienno.getString(2);
                    data_tiennos.data_tienno tiennos = new data_tiennos.data_tienno(nam,ky,tienno);
                    list_tieno.add(tiennos);
                }
                data_tiennos.data_tienno [] tiennos  = new data_tiennos.data_tienno[list_tieno.size()];
                for (int i = 0 ; i < list_tieno.size(); i++){
                    tiennos [i] = list_tieno.get(i);
                }
                data_tiennos data_tiennos = new data_tiennos(danhba,tiennos);
                list_data_tiennos.add(data_tiennos);
            }
        }catch (Exception e){

        }
        return list_data_tiennos;
    }
    public  data_tiennos Get_Data_TienNo(String danhba){
        data_tiennos data_tiennos = new data_tiennos(danhba);
        try {
            String querry_tienno = "SELECT nam,ky,tienno FROM "+Table_DanhSachHangHang_TienNo + " WHERE danhba ='"+danhba+"' ORDER BY CAST(nam AS int) DESC , CAST(ky AS int )DESC;";
            Cursor data_tienno = database_sqLite.GetDatabase(querry_tienno);
            List<data_tiennos.data_tienno> list_tieno = new ArrayList<>();
            while (data_tienno.moveToNext()) {
                String nam = data_tienno.getString(0);
                String ky = data_tienno.getString(1);
                String tienno = data_tienno.getString(2);
                data_tiennos.data_tienno tiennos = new data_tiennos.data_tienno(nam,ky,tienno);
                list_tieno.add(tiennos);
            }
            data_tiennos.data_tienno [] tiennos  = new data_tiennos.data_tienno[list_tieno.size()];
            for (int i = 0 ; i < list_tieno.size(); i++){
                tiennos [i] = list_tieno.get(i);
            }
            data_tiennos.setTiennos(tiennos);
        }catch (Exception e){

        }
        return  data_tiennos;

    }
}

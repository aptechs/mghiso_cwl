package com.dkhanhaptechs.mghiso.Share.ThongTinChiTietDH;

public class data_Thongtinchitietdh_send {
    private String type;
    private Content content ;

    public data_Thongtinchitietdh_send(String  danhba) {
        this.type = "thong tin chi tiet dong ho nuoc";
        this.content = new Content(danhba);
    }

    class  Content {
      private String danhba ;

        public Content(String danhba) {
            this.danhba = danhba;
        }

        public String getDanhba() {
            return danhba;
        }

        public void setDanhba(String danhba) {
            this.danhba = danhba;
        }
    }
}

package com.dkhanhaptechs.mghiso.Share.DanhSach_DocSo;

public class data_DanhSachDocSo {
    private int id ;
    private String nam;
    private String ky ;
    private  String dot ;
    private  String somay;
    private  String sum_dh ;
    private  String dadoc ;
    private String sanluong ;

    public data_DanhSachDocSo(String nam,String ky,String dot,String somay,String sum_dh,String dadoc,String sanluong) {
        this.nam = nam;
        this.ky = ky;
        this.dot = dot;
        this.somay = somay;
        this.sum_dh = sum_dh;
        this.dadoc = dadoc;
        this.sanluong = sanluong;
    }
    public data_DanhSachDocSo() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNam() {
        return nam;
    }

    public void setNam(String nam) {
        this.nam = nam;
    }

    public String getKy() {
        return ky;
    }

    public void setKy(String ky) {
        this.ky = ky;
    }

    public String getDot() {
        return dot;
    }

    public void setDot(String dot) {
        this.dot = dot;
    }

    public String getSomay() {
        return somay;
    }

    public void setSomay(String somay) {
        this.somay = somay;
    }

    public String getSum_dh() {
        return sum_dh;
    }

    public void setSum_dh(String sum_dh) {
        this.sum_dh = sum_dh;
    }

    public String getDadoc() {
        return dadoc;
    }

    public void setDadoc(String dadox) {
        this.dadoc= dadox;
    }

    public String getSanluong() {
        return sanluong;
    }

    public void setSanluong(String sanluong) {
        this.sanluong = sanluong;
    }
}

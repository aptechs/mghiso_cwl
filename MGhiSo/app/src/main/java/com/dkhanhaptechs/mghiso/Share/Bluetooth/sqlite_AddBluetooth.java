package com.dkhanhaptechs.mghiso.Share.Bluetooth;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.dkhanhaptechs.mghiso.Share.CheckVersion.data_Checkversion;
import com.dkhanhaptechs.mghiso.Share.Database_SQLite;

import java.util.ArrayList;
import java.util.List;

public class sqlite_AddBluetooth {
    private Database_SQLite database_sqLite ;
    private String Database ="Database_Bluetooth.sqlite";
    private String Table_AddrBluetooth = "AddrBluetooth";
    private boolean Status_AddrBluetooth_Exists = false;
    public sqlite_AddBluetooth (Context context){
        database_sqLite = new Database_SQLite(context, Database, null, 1);
        if(Create_Table_AddrBluetooth()){
            Status_AddrBluetooth_Exists = true;
        }
    }
    public boolean getStatus_Table_AddrBluetooth_Exists() {
        return Status_AddrBluetooth_Exists;
    }

    private   boolean Create_Table_AddrBluetooth(){
        boolean ok =false;
        try {
            String querry = "CREATE TABLE IF NOT EXISTS "+Table_AddrBluetooth +
                    " (" +
                    " id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                    " type nvarchar(100)," +
                    " addr nvarchar(50)," +
                    " current nvarchar(5)" +
                    ");";
            if(database_sqLite.QueryDatabase(querry)) {
                ok = true;
            }
        }catch (Exception e){

        }
        return ok;
    }
    public  boolean Insert_Update_Table_AddrBluetooth(data_AddBluetooth data_addBluetooth){
        boolean ok =false;
        try {
            String querry = "SELECT id FROM "+Table_AddrBluetooth+" WHERE type = '"+data_addBluetooth.getType()+"';";
            Cursor data_check = database_sqLite.GetDatabase(querry);
            String id = "";
            while (data_check.moveToNext()) {
                id = data_check.getString(0);

            }
            if(!id.isEmpty()){
                // update
                querry = "UPDATE "+Table_AddrBluetooth+" SET " +
                        " addr ='" + data_addBluetooth.getAdd() + "', current ='"+data_addBluetooth.getCurrent()+"'"  +
                        " WHERE type = '"+data_addBluetooth.getType()+"';";
            }else {
                // insert
                querry = "INSERT INTO "+Table_AddrBluetooth+" (type,addr,current)" +
                        " VALUES ('" + data_addBluetooth.getType()+ "','"+data_addBluetooth.getAdd()+"','"+data_addBluetooth.getCurrent()+"');";
            }
            Log.e("SQL BL",querry);
            if(database_sqLite.QueryDatabase(querry)) {
                ok = true;
            }
        }catch (Exception e){

        }
        return ok;
    }


    public List<data_AddBluetooth> Get_All_Table_AddrBluetooth(){
        List<data_AddBluetooth> list_data_AddBluetooths = new ArrayList<>();
        try {
            String querry = "SELECT * FROM "+Table_AddrBluetooth+";";
            Cursor data_check = database_sqLite.GetDatabase(querry);
            while (data_check.moveToNext()) {
                data_AddBluetooth data_addBluetooth = new data_AddBluetooth();
                int id = data_check.getInt(0);
                String type = data_check.getString(1);
                String add = data_check.getString(2);
                String current = data_check.getString(3);
                data_addBluetooth.setId(id);
                data_addBluetooth.setType(type);
                data_addBluetooth.setAdd(add);
                data_addBluetooth.setCurrent(current);
                list_data_AddBluetooths.add(data_addBluetooth);

            }

        }catch (Exception e){

        }
        return list_data_AddBluetooths ;
    }
    public data_AddBluetooth Get_Table_AddrBluetooth_Type(String type){
        data_AddBluetooth data_addBluetooth = new data_AddBluetooth();
        try {
            String querry = "SELECT * FROM "+Table_AddrBluetooth+" WHERE type = '"+type+"';";
            Cursor data_check = database_sqLite.GetDatabase(querry);
            while (data_check.moveToNext()) {
                int id = data_check.getInt(0);
                String add = data_check.getString(2);
                String current = data_check.getString(3);
                data_addBluetooth.setId(id);
                data_addBluetooth.setType(type);
                data_addBluetooth.setAdd(add);
                data_addBluetooth.setCurrent(current);

            }
        }catch (Exception e){

        }
        return data_addBluetooth ;
    }
}

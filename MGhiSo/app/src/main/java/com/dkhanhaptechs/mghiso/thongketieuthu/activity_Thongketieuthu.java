package com.dkhanhaptechs.mghiso.thongketieuthu;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.dkhanhaptechs.mghiso.R;
import com.dkhanhaptechs.mghiso.Share.DanhSach_DocSo.data_Api_DanhSachDocSo_response;
import com.dkhanhaptechs.mghiso.Share.DanhSach_DocSo.data_Api_DanhSachDocSo_send;
import com.dkhanhaptechs.mghiso.Share.DanhSach_DocSo.data_DanhSachDocSo;
import com.dkhanhaptechs.mghiso.Share.DanhSach_DocSo.sqlite_DanhSachDocSo;
import com.dkhanhaptechs.mghiso.Share.DanhSach_DocSo.sqlite_DanhSachKhackHang_DocSo;
import com.dkhanhaptechs.mghiso.Share.Dialog_App;
import com.dkhanhaptechs.mghiso.Share.ThongKeTieuThu.*;
import com.dkhanhaptechs.mghiso.dangnhap.activity_DangNhap;
import com.dkhanhaptechs.mghiso.danhsachdocso.activity_Danhsachdocso;
import com.dkhanhaptechs.mghiso.danhsachdocso_chitiet_khachhang.activity_Danhsachdocso_chitiet_khachhang;
import com.google.gson.Gson;
import com.loopj.android.http.HttpGet;
import com.loopj.android.http.TextHttpResponseHandler;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Objects;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.utils.URIBuilder;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import cz.msebera.android.httpclient.util.EntityUtils;
import  com.dkhanhaptechs.mghiso.Share.User.*;

public class activity_Thongketieuthu extends AppCompatActivity {
    // BIEN LAYOUT
    private TextView tv_danhba, tv_sost, tv_hoten,tv_diachi;
    private ImageView imv_dongbo;
    private Toolbar toolbar ;
    private String Json_table_ghiso_chitiet = "";
    private ListView lv_danhsach_thongketieuthu;
    private Button btn_laydulieumaychu ;

    private String Danhba, So_st, Hoten, Diachi;
    private String Nam, Ky, Dot, Somay;
    private int Dongbo,Status_GhiSo;

    // BIEN
    private ArrayList<data_Thongketieuthu> Arraylist_data_thongketieuthu;
    private adapter_Thongketieuthu Adapter_thongketieuthu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thongketieuthu);
        // INIT
        Mappping_Layout();
        Arraylist_data_thongketieuthu = new ArrayList<data_Thongketieuthu>();
        Adapter_thongketieuthu = new adapter_Thongketieuthu(activity_Thongketieuthu.this,R.layout.dong_thongketieuthu,Arraylist_data_thongketieuthu);
        lv_danhsach_thongketieuthu.setAdapter(Adapter_thongketieuthu);
        // GET DATA
        Json_table_ghiso_chitiet =getIntent().getStringExtra("THONGKETIEUTHU");

        // SET DATA
        if(Get_Table_GhiSo_Chitiet(Json_table_ghiso_chitiet)){
            tv_danhba.setText(Danhba);
            tv_sost.setText(So_st);
            tv_hoten.setText(Hoten);
            tv_diachi.setText(Diachi);
            switch (Dongbo){
                case 2:{
                    // timeout
                    imv_dongbo.setImageResource(R.drawable.synced_2);
                    break;
                }
                case 1:{
                    // dang do bo
                    imv_dongbo.setImageResource(R.drawable.synced_1);
                    break;
                }
                case 0:{
                    // da dong bo
                    imv_dongbo.setImageResource(R.drawable.synced_0);
                    break;
                }
            }
        }
        btn_laydulieumaychu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               String notification= "Bạn muốn đồng bộ dữ liệu máy chủ."+"\r\n"+"Danh bạ: "+Danhba+" năm: "+
                       Nam+" Kỳ: "+Ky +" Đợt: "+Dot+" không?";

                Dialog_Notification_Taidulieu(notification);
               // Display_Thongketieuthu_khachhang();
            }
        });

       // Test_GanData_Listview();
        //Adapter_thongketieuthu.notifyDataSetChanged();
        Display_Thongketieuthu_khachhang();


    }

    private void Mappping_Layout() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tv_danhba =(TextView) findViewById(R.id.textview_danhba_thongketieuthu);
        tv_sost = ( TextView) findViewById(R.id.textview_sost_thongketieuthu);
        imv_dongbo =( ImageView) findViewById(R.id.imageView_dongbo_thongketieuthu);
        tv_hoten = ( TextView) findViewById(R.id.textview_hoten_thongketieuthu);
        tv_diachi = ( TextView) findViewById(R.id.textview_diachi_thongketieuthu);
        lv_danhsach_thongketieuthu =( ListView) findViewById(R.id.listview_danhsach_thongketieuthu);
        btn_laydulieumaychu = ( Button) findViewById(R.id.button_laydulieumaychu_thongketieuthu);
    }
    private boolean Get_Table_GhiSo_Chitiet(String json){
        boolean status = false;
        try{
            JSONObject table = new JSONObject(json);
            Danhba = table.getString("danhba");
            So_st = table.getString("so_st");
            Hoten = table.getString("hoten");
            Diachi = table.getString("diachi");
            Nam = table.getString("nam");
            Ky = table.getString("ky");
            Dot = table.getString("dot");
            Somay = table.getString("somay");
            String ghiso = table.getString("ghiso");
            Status_GhiSo = Integer.parseInt(ghiso);
            String dongbo = table.getString("sync");
            if(!dongbo.isEmpty()){
                Dongbo = Integer.parseInt(dongbo);
            }
            status = true;
        }catch (JSONException e){

        }
        return  status;
    }
    /*
    private void Test_GanData_Listvew() {
        data_Thongketieuthu data1 = new data_Thongketieuthu(1,"1/2020","A","2000","1500","500","Dong cua");
        Arraylist_data_thongketieuthu.add(data1);
        data_Thongketieuthu data2 = new data_Thongketieuthu(2,"2/2020","B","1000","1200","200","Dong cua 1");
        Arraylist_data_thongketieuthu.add(data2);
        data_Thongketieuthu data3 = new data_Thongketieuthu(3,"3/2020","C","2010","3010","1000","Dong cua 2");
        Arraylist_data_thongketieuthu.add(data3);
        data_Thongketieuthu data4 = new data_Thongketieuthu(4,"4/2020","D","4000","5500","1500","Dong cua 3");
        Arraylist_data_thongketieuthu.add(data4);
        data_Thongketieuthu data5 = new data_Thongketieuthu(5,"5/2020","E","6000","9500","3500","Dong cua 4");
        Arraylist_data_thongketieuthu.add(data5);

    }

     */
    AlertDialog dialog_notification_dongbomaychu ;
    private void Dialog_Notification_dongbomaychu(final String text){
        AlertDialog.Builder dialog_notification =new AlertDialog.Builder(activity_Thongketieuthu.this);
        dialog_notification.setMessage(text);
        // dong y
        dialog_notification.setPositiveButton("Đồng ý",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog,int which) {
                Display_Thongketieuthu_khachhang();
                dialog_notification_dongbomaychu.dismiss();
            }
        });
        // bo qua
        dialog_notification.setNegativeButton("Hủy",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog,int which) {
                dialog_notification_dongbomaychu.dismiss();
            }
        });
        dialog_notification_dongbomaychu = dialog_notification.create();
        dialog_notification_dongbomaychu.show();
    }
    private void Display_Thongketieuthu_khachhang(){
        String danhba = Danhba;
      //  Log.e("Danhba",Danhba);
        data_thongketieuthu_send data_thongketieuthu_send = new data_thongketieuthu_send(danhba);
        Gson gson_request = new Gson();
        String json_send = gson_request.toJson(data_thongketieuthu_send);
    //    Log.e("TKTT ",json_send);
        Api_ThongKeTieuThu.get(activity_Thongketieuthu.this,json_send,
                null,
                new TextHttpResponseHandler() {
                    @Override
                    public void onFailure(int statusCode,Header[] headers,String responseString,Throwable throwable) {
                        String text = "Lỗi lay thong ke tieu thu khach hang"+"\r\n"+"Vui lòng kiểm tra lại.";
                        Toast.makeText(activity_Thongketieuthu.this,text,Toast.LENGTH_SHORT).show();
                        data_Http data_http = new data_Http(activity_Thongketieuthu.this);
                        data_http.switch_Url_run();
                    }

                    @Override
                    public void onSuccess(int statusCode,Header[] headers,String responseString) {
                        try {
                            Gson gson_reponse = new Gson();
                            data_thongketieuthu_response data_thongketieuthu_response = gson_reponse.fromJson(responseString,data_thongketieuthu_response.class);
                            //   Log.e("data_TKTT",gson_reponse.toJson( data_thongketieuthu_response));
                            if (data_thongketieuthu_response.getResult() != null) {
                                data_thongketieuthu_response.data_thongketieuthu[] all_data = data_thongketieuthu_response.getResult().getData_thongketieuthus();
                                try {
                                    Arraylist_data_thongketieuthu.clear();
                                } catch (Exception e) {

                                }
                                for (int i = 0; i < all_data.length; i++) {
                                    try {
                                        String ky = Objects.toString(all_data[i].getKy(),"").trim();
                                        String nam = Objects.toString(all_data[i].getNam(),"").trim();
                                        String dot = Objects.toString(all_data[i].getDot(),"").trim();
                                        String code = Objects.toString(all_data[i].getCode(),"").trim();
                                        String csc = Objects.toString(all_data[i].getCscu(),"").trim();
                                        String csm = Objects.toString(all_data[i].getCsmoi(),"").trim();
                                        String tieuthu = Objects.toString(all_data[i].getTieuthu(),"").trim();
                                        String ghichu = Objects.toString(all_data[i].getGhichu(),"").trim();
                                        String ghichuky = Objects.toString(all_data[i].getGhichuky(),"").trim();
                                        if(!ky.isEmpty() && !nam.isEmpty()) {
                                            String kyyear = ky + "/" + nam;
                                            data_Thongketieuthu data_thongketieuthu = new data_Thongketieuthu(i,kyyear,code,csc,csm,tieuthu,ghichu,ghichuky);
                                            Arraylist_data_thongketieuthu.add(data_thongketieuthu);
                                        }
                                    } catch (Exception e) {

                                    }
                                }
                            }
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Adapter_thongketieuthu.notifyDataSetChanged();
                                }
                            });
                        }catch (Exception e){

                        }
                    }
                });
    }
    private AlertDialog dialog_notification_taidulieu;
    public void Dialog_Notification_Taidulieu(final String title){
        AlertDialog.Builder alert_taidulieu = new AlertDialog.Builder(activity_Thongketieuthu.this);
        alert_taidulieu.setMessage(title);
        // dong y
        alert_taidulieu.setPositiveButton("Xác nhận",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog,int which) {
                dialog_notification_taidulieu.dismiss();
                get_Danhsachdulieumaychu get_danhsachdulieumaychu =new get_Danhsachdulieumaychu(Nam,Ky,Dot,Somay,Danhba,Status_GhiSo);
                get_danhsachdulieumaychu.execute();
            }
        });

        alert_taidulieu.setNegativeButton("Hủy",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog,int which) {
                dialog_notification_taidulieu.dismiss();
            }
        });
        // bo qua

        dialog_notification_taidulieu = alert_taidulieu.create();
        dialog_notification_taidulieu.show();
    }
    private class get_Danhsachdulieumaychu extends AsyncTask<Void,Void,String> {
        private Dialog_App dialog_app ;
        private String Nam;
        private String Ky ;
        private String Dot;
        private String Somay;
        private String Danhba;
        private String ResponseString;
        private int GhiSo;
        private String BASE_URL_API ="/apikhachhang/api/DocSoDanhSachDocSo?";
        private String BASE_URL="";
        public get_Danhsachdulieumaychu(String nam,String ky,String dot, String somay , String danhba  ,int ghiso){
            dialog_app = new Dialog_App(activity_Thongketieuthu.this);
            Nam =nam ;
            Ky = ky;
            Dot = dot;
            Somay = somay;

            Danhba = danhba;
            ResponseString = "";
            GhiSo = ghiso;
            data_Http data_http = new data_Http(activity_Thongketieuthu.this);
            String url_run = data_http.get_Url_run();
            if(!url_run.isEmpty()){
                BASE_URL = url_run +BASE_URL_API;
            }else {
                BASE_URL = "http://viettel.capnuoccholon.com.vn"+BASE_URL_API;
            }
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
                dialog_app.Dialog_Progress_Wait_Open("Đang tải dữ liệu từ máy chủ.","Vui lòng chờ");
        }

        @Override
        protected String doInBackground(Void... voids) {
            try {

                data_Api_DanhSachDocSo_send data_api_danhSachDocSo_send = new data_Api_DanhSachDocSo_send(Nam,Ky,Dot,Somay);
                Gson gson_send = new Gson();
                String json_send = gson_send.toJson(data_api_danhSachDocSo_send);
                HttpClient httpclient = new DefaultHttpClient();


                URI   uri_ = new URIBuilder(BASE_URL)
                            .addParameter("thamso",json_send)
                            .addParameter("manhanvien",Somay)
                            .addParameter("danhba",Danhba)
                            .build();

                HttpGet httpget = new HttpGet(uri_);
                //  httppost.setHeader("Accept", "application/json");
                httpget.setHeader("Content-type","application/json;charset=utf-8");
                //     httpget.setEntity(stringEntity_send);
                HttpResponse response = httpclient.execute(httpget);
                if (response.getStatusLine().getStatusCode() == 200) {
                    String responseString = EntityUtils.toString(response.getEntity());
                    ResponseString =responseString;

                }
            } catch (IOException | URISyntaxException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            dialog_app.Dialog_Progress_Wait_Close();
            if(!ResponseString.isEmpty()) {
                Gson gson_reponse = new Gson();
                data_Laydulieumaychu data_laydulieumaychu = gson_reponse.fromJson(ResponseString,data_Laydulieumaychu.class);
                if (data_laydulieumaychu.getResult().getKhachhangs().length > 0) {
                    String date_sync = Ky + "/" + Nam;
                    for (int i = 0; i < Arraylist_data_thongketieuthu.size(); i++) {
                        data_Thongketieuthu data_thongketieuthu = Arraylist_data_thongketieuthu.get(i);
                        String date = data_thongketieuthu.getDatetime();
                        if (date_sync.equals(date)) {
                            data_thongketieuthu.setCode(data_laydulieumaychu.getResult().getKhachhangs()[0].getCODE());
                            data_thongketieuthu.setCSM(data_laydulieumaychu.getResult().getKhachhangs()[0].getCSMOI());
                            data_thongketieuthu.setCSC(data_laydulieumaychu.getResult().getKhachhangs()[0].getCSCU());
                            data_thongketieuthu.setTieuthu(data_laydulieumaychu.getResult().getKhachhangs()[0].getTIEUTHU());
                            //  data_thongketieuthu.setGhichu("ok");
                            Arraylist_data_thongketieuthu.set(i,data_thongketieuthu);
                            break;
                        }

                    }
                    data_Laydulieumaychu.KhachHang khachhang_update = data_laydulieumaychu.getResult().getKhachhangs()[0];
                    Update_thongtin(khachhang_update);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Adapter_thongketieuthu.notifyDataSetChanged();
                        }
                    });


                    // Dialog_Progress_TaiDuLieu(data_api_danhSachDocSo_response);
                } else {
                    dialog_app.Dialog_Notification("Không có dữ liệu đọc số" + "\r\n" + "Năm: " + Nam + " Kỳ: " + Ky + " Đợt: " + Dot + " Số máy: " + Somay + "\r\n" + "Danh bạ: " + Danhba);
                }
            }else {
                dialog_app.Dialog_Notification("Không có dữ liệu."+"\r\n"+"Vui lòng thử lại hoặc liên hệ về server");

            }
        }
        private boolean Update_thongtin(data_Laydulieumaychu.KhachHang khachhang){
            Boolean status = false;
            try{

                if(GhiSo != 1) {
                    sqlite_DanhSachKhackHang_DocSo sqlite_danhSachKhackHang_docSo = new sqlite_DanhSachKhackHang_DocSo(activity_Thongketieuthu.this,Nam,Ky,Dot,Somay);
                    if (sqlite_danhSachKhackHang_docSo.getStatus_Table_DanhSachKhachHang_Exists()) {
                        sqlite_danhSachKhackHang_docSo.Update_Table_DanhSachHangHang_Maychu(khachhang,"0");
                        status = true;
                    }
                }
            }catch (Exception e){

            }
            return  status;
        }
    }
}
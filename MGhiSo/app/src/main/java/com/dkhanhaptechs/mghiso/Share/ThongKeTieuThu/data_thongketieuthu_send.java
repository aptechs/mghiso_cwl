package com.dkhanhaptechs.mghiso.Share.ThongKeTieuThu;

public class data_thongketieuthu_send
{
    private String type;
    private Content content ;

    public data_thongketieuthu_send(String  danhba) {
        this.type = "lich su thay dong ho nuoc";
        this.content = new Content(danhba);
    }

    class  Content {
        private String danhba ;

        public Content(String danhba) {
            this.danhba = danhba;
        }

        public String getDanhba() {
            return danhba;
        }

        public void setDanhba(String danhba) {
            this.danhba = danhba;
        }
    }
}

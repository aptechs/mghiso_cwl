package com.dkhanhaptechs.mghiso.Share.SyncGhiSo;

public class data_syncghiso_response {
    private data_result result;
    private String status;
    private String message;

    public data_result getResult() {
        return result;
    }

    public void setResult(data_result result) {
        this.result = result;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public class data_result {
        private String danhbas;

        public String getDanhbas() {
            return danhbas;
        }

        public void setDanhbas(String danhbas) {
            this.danhbas = danhbas;
        }
    }
}

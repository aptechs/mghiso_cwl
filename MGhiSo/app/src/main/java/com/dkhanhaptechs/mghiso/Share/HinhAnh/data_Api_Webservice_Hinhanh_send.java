package com.dkhanhaptechs.mghiso.Share.HinhAnh;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.util.Base64;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.List;

public class data_Api_Webservice_Hinhanh_send {
    private String Id ;
    private String Danhba;
    private String Nam ;
    private String Ky ;
    private String Dot ;
    private List<String> List_anh;
    private Bitmap Bitmap_Anh;

    public data_Api_Webservice_Hinhanh_send(String danhba,String nam,String ky,String dot,List<String> list_anh) {
        Danhba = danhba;
        Nam = nam;
        Ky = ky;
        Dot = dot;
        List_anh = list_anh;
    }



    public data_Api_Webservice_Hinhanh_send(String id,String danhba,String nam,String ky,String dot,Bitmap bitmap_anh) {
        Id = id;
        Danhba = danhba;
        Nam = nam;
        Ky = ky;
        Dot = dot;
        Bitmap_Anh = bitmap_anh;
    }

    public String get_Data_Send_NhieuAnh_Webservice(){

        String data_send = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
                "  <soap:Body>\n" +
                "    <NhanHinhAnh xmlns=\"http://tempuri.org/\">\n" +
                "      <nam>"+Nam+"</nam>\n" +
                "      <ky>"+Ky+"</ky>\n" +
                "      <dot>"+Dot+"</dot>\n" +
                "      <danhba>"+Danhba+"</danhba>\n" +
                "      <hinhanhs>\n" ;
        for(int i = 0 ; i < List_anh.size() ; i++){
            String anh =List_anh.get(i);
            String data_anh = PathFileToStringData(anh);
            data_send = data_send+ "<csHinhAnh>\n" ;
            data_send = data_send+ " <id>"+String.valueOf(i)+"</id>\n" ;
            data_send = data_send+ "<chuoihinh>"+data_anh+"</chuoihinh>\n" ;
            data_send = data_send+ "</csHinhAnh>\n" ;
        }
        data_send = data_send + "      </hinhanhs>\n" +
                "    </NhanHinhAnh>\n" +
                "  </soap:Body>\n" +
                "</soap:Envelope>" ;
                return  data_send;
    }
    public String get_Data_Send_Webservice(){
        String data_anh = BimapToStringData(Bitmap_Anh);
        String data_send = "";
        if(!data_anh.isEmpty()) {
            data_send = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                    "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
                    "  <soap:Body>\n" +
                    "    <NhanHinhMotAnh xmlns=\"http://tempuri.org/\">\n" +
                    "      <nam>" + Nam + "</nam>\n" +
                    "      <ky>" + Ky + "</ky>\n" +
                    "      <dot>" + Dot + "</dot>\n" +
                    "      <danhba>" + Danhba + "</danhba>\n" +
                    "      <id>" + Id + "</id>\n" +
                    "      <hinhanh>" + data_anh + "</hinhanh>\n" +
                    "    </NhanHinhMotAnh>\n" +
                    "  </soap:Body>\n" +
                    "</soap:Envelope>";
        }

        return  data_send;
    }
    private String PathFileToStringData (String path_file){
        String databse64 = "";
        File imgfile = new File(path_file);
        if(imgfile.exists()) {

            Bitmap bitmap = BitmapFactory.decodeFile(imgfile.getAbsolutePath());

            Bitmap bitmap_scale = Bitmap.createScaledBitmap(bitmap,1024,768,true);
            try {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap_scale.compress(Bitmap.CompressFormat.PNG,100,byteArrayOutputStream);
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                databse64 = Base64.encodeToString(byteArray,Base64.DEFAULT);
            } catch (Exception e) {

            }
        }
        return  databse64;
    }
    private String BimapToStringData (Bitmap bitmap_anh){
        String databse64 = "";
/*
            int w =bitmap_anh.getWidth();
            int h =bitmap_anh.getHeight();
            int l = bitmap_anh.getByteCount();

 */
            Bitmap bitmap_scale = Bitmap.createScaledBitmap(bitmap_anh,1024,768,true);
            try {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap_scale.compress(Bitmap.CompressFormat.PNG,100,byteArrayOutputStream);
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                databse64 = Base64.encodeToString(byteArray,Base64.DEFAULT);
            } catch (Exception e) {

            }

        return  databse64;
    }
    public Bitmap getResizedBitmap(Bitmap bm) {

        int width = bm.getWidth();
        int height = bm.getHeight();
        double ratio = (float) (850000.0/(bm.getByteCount()));
        float ratio_scale =(float) Math.sqrt(ratio);
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(ratio_scale, ratio_scale);
        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        //Log.e("Size anh n:",String.valueOf(resizedBitmap.getByteCount()));
        return resizedBitmap;
    }
}

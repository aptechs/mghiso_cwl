package com.dkhanhaptechs.mghiso.Share.HinhAnh;

public class data_Api_Webservice_Hinhanh_get {
    private String Danhba;
    private String Nam ;
    private String Ky ;

    public data_Api_Webservice_Hinhanh_get(String danhba,String nam,String ky) {
        Danhba = danhba;
        Nam = nam;
        Ky = ky;
    }
    public String  get_data_get_HinhAnh(){
        String data_send ="<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
                "  <soap:Body>\n" +
                "    <DanhSachHinhAnh xmlns=\"http://tempuri.org/\">\n" +
                "      <nam>"+Nam+"</nam>\n" +
                "      <ky>"+Ky+"</ky>\n" +
                "      <danhba>"+Danhba+"</danhba>\n" +
                "    </DanhSachHinhAnh>\n" +
                "  </soap:Body>\n" +
                "</soap:Envelope>";
        return  data_send;
    }

    public String getDanhba() {
        return Danhba;
    }

    public void setDanhba(String danhba) {
        Danhba = danhba;
    }

    public String getNam() {
        return Nam;
    }

    public void setNam(String nam) {
        Nam = nam;
    }

    public String getKy() {
        return Ky;
    }

    public void setKy(String ky) {
        Ky = ky;
    }


}

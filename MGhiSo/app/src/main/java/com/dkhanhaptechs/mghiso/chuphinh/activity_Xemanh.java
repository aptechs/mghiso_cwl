package com.dkhanhaptechs.mghiso.chuphinh;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.dkhanhaptechs.mghiso.R;
import com.dkhanhaptechs.mghiso.Share.HinhAnh.*;

import org.json.JSONException;
import org.json.JSONObject;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;


public class activity_Xemanh extends AppCompatActivity {

    private TextView tv_danhba, tv_sost, tv_hoten,tv_diachi;
    private ImageView imv_dongbo;
    private Toolbar toolbar ;
    private String Json_table_ghiso_chitiet = "";
    private Button btn_xemanhkytruoc,btn_xemanhkysau;
    private ListView lv_anhs;

    private String  So_st, Hoten, Diachi;
    private String Danhba, Nam, Ky, Dot, SoMay;
    private int Dongbo;
    private Bitmap bitmap_anhsave;
    private EditText edt_nam, edt_ky;

    private ArrayList<data_Chupanh> Arraylist_data_chupanh;
    private adapter_Chupanh Adapter_chupanh;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_xemanh);
        Mappping_Layout();
        Json_table_ghiso_chitiet = getIntent().getStringExtra("XEMHINH");
        //

        // SET DATA
        try {
            if (Get_Table_GhiSo_Chitiet(Json_table_ghiso_chitiet)) {
                tv_danhba.setText(Danhba);
                tv_sost.setText(So_st);
                tv_hoten.setText(Hoten);
                tv_diachi.setText(Diachi);
                edt_ky.setText(Ky);
                edt_nam.setText(Nam);
                switch (Dongbo) {
                    case 2: {
                        // timeout
                        imv_dongbo.setImageResource(R.drawable.synced_2);
                        break;
                    }
                    case 1: {
                        // dang do bo
                        imv_dongbo.setImageResource(R.drawable.synced_1);
                        break;
                    }
                    case 0: {
                        // da dong bo
                        imv_dongbo.setImageResource(R.drawable.synced_0);
                        break;
                    }
                }
            }
        }catch (Exception e){

        }

        btn_xemanhkytruoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Arraylist_data_chupanh.clear();
                Adapter_chupanh.notifyDataSetChanged();
                String nam = edt_nam.getText().toString();
                String ky = edt_ky.getText().toString();
                try {
                    int i_ky = Integer.parseInt(ky);
                    if(i_ky ==1){
                        Ky = String.valueOf(12);
                        int i_nam = Integer.parseInt(nam);
                        Nam = String.valueOf(i_nam -1);
                    }else {
                        Ky = String.valueOf(i_ky-1);
                    }
                    edt_nam.setText(Nam);
                    edt_ky.setText(Ky);
                    Get_HinhAnh_Server();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        });
        btn_xemanhkysau.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Arraylist_data_chupanh.clear();
                Adapter_chupanh.notifyDataSetChanged();
                String nam = edt_nam.getText().toString();
                String ky = edt_ky.getText().toString();
                try {
                    int i_ky = Integer.parseInt(ky);
                    if(i_ky ==12){
                        Ky = String.valueOf(1);
                        int i_nam = Integer.parseInt(nam);
                        Nam = String.valueOf(i_nam +1);
                    }else {
                        Ky = String.valueOf(i_ky+1);
                    }
                    edt_nam.setText(Nam);
                    edt_ky.setText(Ky);
                    Get_HinhAnh_Server();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        });

        Arraylist_data_chupanh = new ArrayList<data_Chupanh>();
        Adapter_chupanh = new adapter_Chupanh(activity_Xemanh.this,R.layout.dong_hinhanh,Arraylist_data_chupanh);
        lv_anhs.setAdapter(Adapter_chupanh);
        lv_anhs.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        try {
            Get_HinhAnh_Server();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    private void Mappping_Layout() {
        tv_danhba =(TextView) findViewById(R.id.textview_danhba_xemanh);
        tv_sost = ( TextView) findViewById(R.id.textview_sost_xemanh);
        imv_dongbo =( ImageView) findViewById(R.id.imageView_dongbo_xemanh);
        tv_hoten = ( TextView) findViewById(R.id.textview_hoten_xemanh);
        tv_diachi = ( TextView) findViewById(R.id.textview_diachi_xemanh);
        lv_anhs =( ListView) findViewById(R.id.listview_anh_xemanh);
        btn_xemanhkytruoc = (Button) findViewById(R.id.button_xemanhkytruoc_xemanh);
        btn_xemanhkysau = (Button) findViewById(R.id.button_xemanhkysau_xemanh);
        edt_nam = (EditText) findViewById(R.id.edittext_nam_xemanh);
        edt_ky = (EditText) findViewById(R.id.edittext_ky_xemanh);
      //  edt_dot = (EditText) findViewById(R.id.edittext_dot_xemanh);
    }
    private boolean Get_Table_GhiSo_Chitiet(String json){
        boolean status = false;
        try{
            Log.e("14",json);
            JSONObject table = new JSONObject(json);
            Danhba = table.getString("danhba");
            So_st = table.getString("so_st");
            Hoten = table.getString("hoten");
            Diachi = table.getString("diachi") ;
            String sync = table.getString("sync");
            Dongbo = Integer.parseInt(sync);
            Nam = table.getString("nam");
            Ky = table.getString("ky");
            Dot = table.getString("dot");
            SoMay = table.getString("somay");
            if(!sync.isEmpty()){
                Dongbo = Integer.parseInt(sync);
            }
            status = true;
        }catch (JSONException e){

        }
        return  status;
    }
    Getpicture_Asynctask getpicture_asynctask ;
    private void Get_HinhAnh_Server() throws UnsupportedEncodingException {

        getpicture_asynctask = new Getpicture_Asynctask(activity_Xemanh.this,Nam,Ky,Danhba);
        getpicture_asynctask.execute();
        countDownTimer_getpicture.start();

    }
   CountDownTimer countDownTimer_getpicture =new CountDownTimer(30000,1000) {
       @Override
       public void onTick(long millisUntilFinished) {
           try {
               int status = getpicture_asynctask.getStatus_Send_Finish();
               if (status ==1) {
                   List<Bitmap> list_bimat = getpicture_asynctask.getList_bitmap_anh();
                   if (list_bimat.size() > 0) {
                       countDownTimer_getpicture.cancel();
                       for (int i = 0; i < list_bimat.size(); i++) {
                           Bitmap myBitmap = list_bimat.get(i);
                           data_Chupanh data_chupanh = new data_Chupanh(i,myBitmap,true,"1");
                           Arraylist_data_chupanh.add(data_chupanh);
                       }
                       runOnUiThread(new Runnable() {
                           @Override
                           public void run() {
                               Adapter_chupanh.notifyDataSetChanged();
                           }
                       });
                   }
                   countDownTimer_getpicture.cancel();
               }else if (status ==2){
                   countDownTimer_getpicture.cancel();
               }

           } catch (Exception e) {

           }
       }

       @Override
       public void onFinish() {

       }
   };
    private Bitmap StringToBitmap(String base64){
        Bitmap decodedByte = null;
        try {
            byte[] decodedString = Base64.decode(base64,Base64.DEFAULT);
            decodedByte = BitmapFactory.decodeByteArray(decodedString,0,decodedString.length);

        }catch (Exception e){

        }
        return decodedByte;
    }
}
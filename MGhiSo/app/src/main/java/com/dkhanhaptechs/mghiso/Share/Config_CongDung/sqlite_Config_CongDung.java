package com.dkhanhaptechs.mghiso.Share.Config_CongDung;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.dkhanhaptechs.mghiso.Share.CheckVersion.data_Checkversion;
import com.dkhanhaptechs.mghiso.Share.Database_SQLite;

import java.util.ArrayList;
import java.util.List;

public class sqlite_Config_CongDung {
    private Database_SQLite database_sqLite ;
    private String Database ="Database_CongDung.sqlite";
    private String Table_ListCongDung = "ListCongDung";
    private boolean Status_ListCongDung_Exists = false;
    public sqlite_Config_CongDung (Context context){
        database_sqLite = new Database_SQLite(context, Database, null, 1);
        if(Create_Table_ListCongDung()){
            Status_ListCongDung_Exists = true;
        }else {
            Status_ListCongDung_Exists = false;
        }
    }
    public boolean getStatus_Table_ListCongDung_Exists() {
        return Status_ListCongDung_Exists;
    }

    private   boolean Create_Table_ListCongDung(){
        boolean ok =false;
        try {
            String querry = "CREATE TABLE IF NOT EXISTS "+Table_ListCongDung +
                    " (" +
                    " id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                    " doituong nvarchar(50)," +
                    " congdung TEXT NOT NULL" +
                    ");";
            if(database_sqLite.QueryDatabase(querry)) {
                ok = true;
            }
        }catch (Exception e){

        }
        return ok;
    }
    public  boolean Insert_Table_ListCongDung (data_Config_CongDung data_config_congDung){
        boolean ok =false;
        try {

                // insert
            String  querry = "INSERT INTO "+Table_ListCongDung+" (doituong,congdung)" +
                        " VALUES ('" + data_config_congDung.getDoituong().toString().trim() + "','"+data_config_congDung.getCongdung().toString().trim()+"');";
            if(database_sqLite.QueryDatabase(querry)) {
                ok = true;
            }
        }catch (Exception e){
            Log.e("INSERT ListCongDung",e.toString());
        }
        return ok;
    }

    public boolean Delete_Table_ListCongDung() {
        boolean ok = false;
        try {
            String querry = "DROP TABLE IF EXISTS " + Table_ListCongDung + "; ";

            if (database_sqLite.QueryDatabase(querry)) {
                ok = true;
                Log.e("D T Table_ListCongDung ","ok");
            }
        } catch (Exception e) {

        }
        return ok ;
    }
     public List<String> Get_DoiTuong(){
         List<String> doituons = new ArrayList<>();
         try {

             String querry = "SELECT DISTINCT doituong FROM "+Table_ListCongDung +" ORDER BY doituong;" ;
             Cursor data_check = database_sqLite.GetDatabase(querry);
             while (data_check.moveToNext()) {
                 String doituong = data_check.getString(0);
                 doituons.add(doituong);
             }

         }catch (Exception e){

         }
         return doituons;
     }
    public List<String> Get_CongDung(String doituong){
        List<String> congdungs = new ArrayList<>();
        try {

            String querry = "SELECT DISTINCT congdung FROM "+Table_ListCongDung +" WHERE doituong = '"+doituong+"';" ;
            Cursor data_check = database_sqLite.GetDatabase(querry);
            while (data_check.moveToNext()) {
                String congdung = data_check.getString(0);
                congdungs.add(congdung);
            }
        }catch (Exception e){
        }
        return congdungs;
    }
    public String Check_DoiTuong_CongDung(String doituong,String congdung){
        String id ="";
        try {

            String querry = "SELECT id FROM "+Table_ListCongDung +" WHERE doituong = '"+doituong+"' AND congdung ='"+congdung+"';" ;
            Cursor data_check = database_sqLite.GetDatabase(querry);
            while (data_check.moveToNext()) {
                id = data_check.getString(0);
            }
        }catch (Exception e){
        }
        return id;
    }
}

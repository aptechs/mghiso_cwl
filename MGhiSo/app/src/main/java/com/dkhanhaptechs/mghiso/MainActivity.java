package com.dkhanhaptechs.mghiso;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Menu;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.NavigationView;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.dkhanhaptechs.mghiso.Share.CheckServer.Api_Checkserver;
import com.dkhanhaptechs.mghiso.Share.CheckServer.data_Api_Checkserver_response;
import com.dkhanhaptechs.mghiso.Share.CheckServer.data_Api_Checkserver_send;
import com.dkhanhaptechs.mghiso.Share.CheckVersion.data_Checkversion;
import com.dkhanhaptechs.mghiso.Share.CheckVersion.sqlite_Checkversion;
import com.dkhanhaptechs.mghiso.Share.Dialog_App;
import com.dkhanhaptechs.mghiso.danhsachbatthuong.*;
import com.dkhanhaptechs.mghiso.Share.Config_CongDung.* ;
import com.dkhanhaptechs.mghiso.dangnhap.activity_DangNhap;
import com.dkhanhaptechs.mghiso.danhsachdocso.activity_Danhsachdocso ;
import com.dkhanhaptechs.mghiso.Share.User.*;
import com.dkhanhaptechs.mghiso.Share.LyDoTangGiam.*;
import com.dkhanhaptechs.mghiso.lichsuthaydongho.activity_Lichsuthaydongho;
import com.dkhanhaptechs.mghiso.mayin.*;
import com.google.gson.Gson;
import com.loopj.android.http.TextHttpResponseHandler;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class MainActivity extends AppCompatActivity {

    // BIEN LAYOUT
    private  Button btn_mainmenu_ghiso , btn_kiemtraduongtruyen, btn_taicongdung, btn_tailydotanggiam, btn_quaylai;
    private  Toolbar toolbar ;
    TextView tv_tennv , tv_somay , tv_sodienthoai;
    private Spinner spn_doituong, spn_congdung, spn_lydotanggiam;

    //NavigationView
    ImageView imv_user;
    TextView tv_tennv_navi , tv_somay_navi , tv_sodienthoai_navi;
    ImageButton imbt_dangxuat;


    // BIEN CHUONG TRINH
    sqlite_User sqlite_user ;
    sqlite_Checkversion sqlite_checkversion ;
    ArrayList<String> array_doituong ;
    ArrayList<String> array_congdung ;
    ArrayList<String> array_lydotanggiam ;
    ArrayAdapter adapter_doituong;
    ArrayAdapter adapter_congdung;
    ArrayAdapter adapter_lydotanggiam;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mainmenu);
        // INIT

        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        Mappping_Layout();
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View header = Mappping_Layout_NavigationView(navigationView);

        // INIT
        array_doituong = new ArrayList<String>();
        adapter_doituong = new ArrayAdapter(MainActivity.this, android.R.layout.simple_spinner_item,array_doituong);
        adapter_doituong.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
        spn_doituong.setAdapter(adapter_doituong);

        array_congdung = new ArrayList<String>();
        adapter_congdung = new ArrayAdapter(MainActivity.this, android.R.layout.simple_spinner_item,array_congdung);
        adapter_congdung.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
        spn_congdung.setAdapter(adapter_congdung);

        array_lydotanggiam = new ArrayList<String>();
        adapter_lydotanggiam = new ArrayAdapter(MainActivity.this, android.R.layout.simple_spinner_item,array_lydotanggiam);
        adapter_lydotanggiam.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
        spn_lydotanggiam.setAdapter(adapter_lydotanggiam);

        sqlite_user = new sqlite_User(MainActivity.this);
        Display_InFor_User();
        ///
        Display_DoiTuong();
        Display_LyDoTangGiam ();



        ///

        // ACTION : Click menu
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                int id = menuItem.getItemId();
                if (id == R.id.nav_danhsachdocso) {
                    // danh sach doc so
                    Intent intent = new Intent(MainActivity.this,activity_Danhsachdocso.class);
                    //intent.putExtra("SODIENTHOAI",com_reponse[1]);
                    //  finish();
                    startActivity(intent);
                    //Toast.makeText(MainActivity.this,"AAA",Toast.LENGTH_SHORT).show();
                } else if (id == R.id.nav_danhsachbatthuong) {
                    // danh sach bat thuong
                    Intent intent = new Intent(MainActivity.this,activity_Danhsachbatthuong.class);
                    String json_send = "";
                  //  Log.e("11",json_send);
                    intent.putExtra("TABLE_DANHSACHDOCSO_BATTHUONG",json_send);
                    //intent.putExtra("SODIENTHOAI",com_reponse[1]);
                    //  finish();
                    startActivity(intent);
                   // Toast.makeText(MainActivity.this,"BB",Toast.LENGTH_SHORT).show();
                } else if(id == R.id.nav_bandoghiso){
                    Dialog_App dialog_app = new Dialog_App(MainActivity.this);
                    String notification = "Trang bản đồ ghi số đang phát triển."+"\r\b"+"Xin vui lòng thông cảm";
                    dialog_app.Dialog_Notification(notification);

                }else if(id == R.id.nav_bandodaghiso){
                    Dialog_App dialog_app = new Dialog_App(MainActivity.this);
                    String notification = "Trang bản đồ đã ghi số đang phát triển."+"\r\b"+"Xin vui lòng thông cảm";
                    dialog_app.Dialog_Notification(notification);
                }

                else if (id == R.id.nav_mayinbluetooth) {
                    // danh sach bat thuong
                    Intent intent = new Intent(MainActivity.this,activity_Mayin.class);
                    //intent.putExtra("SODIENTHOAI",com_reponse[1]);
                    //  finish();
                    startActivity(intent);
                    // Toast.makeText(MainActivity.this,"BB",Toast.LENGTH_SHORT).show();
                }else if(id == R.id.nav_dangxuat){
                    String notìication ="Bạn muốn đăng xuất tài khoản này.";
                    Dialog_Notification_Dangxuat(notìication);

                }

                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
                return false;
            }
        });

        // ACTION : Click ghi chi so
        btn_mainmenu_ghiso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,activity_Danhsachdocso.class);
                //intent.putExtra("SODIENTHOAI",com_reponse[1]);
              //  finish();
                startActivity(intent);
            }
        });
        // ACTION : Kiem tra duong truyen
        btn_kiemtraduongtruyen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Check_Server();
            }
        });
/*
        // ACTION : Dang xuat
        imbt_dangxuat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,activity_DangNhap.class);
                //intent.putExtra("SODIENTHOAI",com_reponse[1]);
                 finish();
                startActivity(intent);
            }
        });

 */

        // ACTION : Tai Cong Dung
        btn_taicongdung.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TaiCongDung();

            }
        });
        spn_doituong.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent,View view,int position,long id) {
                String doituong_selete = array_doituong.get(position).toString().trim();
                Display_CongDung(doituong_selete);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // ACTION : Tai LyDoTaiGiam
        btn_tailydotanggiam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TaiLyDoTangGiam();
            }
        });
        btn_quaylai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,activity_DangNhap.class);
                //intent.putExtra("SODIENTHOAI",com_reponse[1]);
                finish();
                startActivity(intent);
            }
        });

    }

    // ANH XA LAYOUT
    private void  Mappping_Layout(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        btn_mainmenu_ghiso = (Button) findViewById(R.id.button_Ghichiso_Mainmenu);
        tv_tennv =(TextView) findViewById(R.id.textview_tennv_Mainmenu);
        tv_somay = (TextView)findViewById(R.id.textview_somay_Mainmenu);
        tv_sodienthoai = (TextView) findViewById(R.id.textview_sodienthoai_Mainmenu);
        btn_kiemtraduongtruyen = (Button) findViewById(R.id.button_Kiemtraduongtruyen_Mainmenu);
        btn_taicongdung = (Button) findViewById(R.id.button_Taicongdung_Mainmenu);
        spn_doituong =( Spinner) findViewById(R.id.spinner_doituong_Mainmenu);
        spn_congdung =(Spinner) findViewById(R.id.spinner_congdung_Mainmenu);
        btn_tailydotanggiam = (Button) findViewById(R.id.button_Tailydotanggiam_Mainmenu);
        spn_lydotanggiam = (Spinner) findViewById(R.id.spinner_lydotanggiam_Mainmenu);
        btn_quaylai = (Button) findViewById(R.id.button_Quaylai_Mainmenu);

    }
    private View  Mappping_Layout_NavigationView(NavigationView navigationView ){
        View header = navigationView.getHeaderView(0);
        /*
        // NavigationView
        imv_user = (ImageView) header.findViewById(R.id.imageview_user_NavigationView);
        tv_tennv_navi =(TextView) header.findViewById(R.id.textview_tennv_NavigationView);
        tv_somay_navi = (TextView) header.findViewById(R.id.textview_somay_NavigationView);
        tv_sodienthoai_navi = (TextView) header.findViewById(R.id.textview_sodienthoai_NavigationView);
        imbt_dangxuat =(ImageButton) header.findViewById(R.id.imagebutton_dangxuat_NavigationView);
         */
        return  header;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void Display_InFor_User(){
        sqlite_user = new sqlite_User(MainActivity.this);
        if(sqlite_user.getStatus_Table_User_Exists()){
            data_User data_user = sqlite_user.Get_User("1");

            String tennv = data_user.getTennhanvien().toString();
            String sodienthoai = data_user.getdienthoai().toString();
            String somay =data_user.getSomay().toString();
         //   tv_tennv_navi.setText("  Nhân viên: "+tennv);
      //      tv_sodienthoai_navi.setText("Điện thoại: "+sodienthoai);
        //    tv_somay_navi.setText("Số máy: "+somay);
            tv_tennv.setText("Nhân viên: "+tennv);
            tv_sodienthoai.setText("Điện thoại: "+sodienthoai);
            tv_somay.setText("Số máy: "+somay);
        }

    }
    private void Check_Server() {
        data_Api_Checkserver_send data_api_checkserver_send = new data_Api_Checkserver_send();
        Gson gson_send = new Gson();
        String data_send = gson_send.toJson(data_api_checkserver_send);
        Api_Checkserver.get(MainActivity.this, data_send,
                null,
                new TextHttpResponseHandler() {
                    @Override
                    public void onFailure(int statusCode,Header[] headers,String responseString,Throwable throwable) {
                        String text = "Server MGhi số không thể kết nối" + "\r\n" + "Vui lòng kiểm tra lại.";
                        Toast.makeText(MainActivity.this,text,Toast.LENGTH_SHORT).show();
                        data_Http data_http = new data_Http(MainActivity.this);
                        data_http.switch_Url_run();
                    }

                    @Override
                    public void onSuccess(int statusCode,Header[] headers,String responseString) {
                        Gson gson_reponse = new Gson();
                        data_Api_Checkserver_response data_api_checkserver_response = gson_reponse.fromJson(responseString,data_Api_Checkserver_response.class);
                      //  Log.e("checkserver response",gson_reponse.toJson(data_api_checkserver_response));
                        if (data_api_checkserver_response.getStatus().equals("1")) {
                            String text_notification = "Kết nối đến server :" + data_api_checkserver_response.getResult().getNetwork().toString();
                            Dialog_Notification(text_notification);
                        } else {
                            String text_notification = "Mất kết nối với server." + "\r\n" + "Vui lòng liên hệ với Cấp Nước đẻ kiểm tra lại servẻ";
                            Dialog_Notification(text_notification);
                        }

                    }
                });
    }
    private void TaiCongDung(){
        // lay version
        sqlite_checkversion = new sqlite_Checkversion(MainActivity.this);
        data_Checkversion version_current = sqlite_checkversion.Get_InfoVersion("1");
        sqlite_Config_CongDung  sqlite_config_congDung = new sqlite_Config_CongDung(MainActivity.this);
        if(sqlite_config_congDung.getStatus_Table_ListCongDung_Exists()) {
            if(!sqlite_config_congDung.Delete_Table_ListCongDung()){
                return;
            }
        }
        String version  = version_current.getVersion();
        data_Api_Config_send data_api_config_send = new data_Api_Config_send(version);
        Gson gson_request = new Gson();
        String json_send = gson_request.toJson(data_api_config_send);
        Api_Config_CongDung.get(MainActivity.this,json_send,
                null,new TextHttpResponseHandler() {
                    @Override
                    public void onFailure(int statusCode,Header[] headers,String responseString,Throwable throwable) {
                        data_Http data_http = new data_Http(MainActivity.this);
                        data_http.switch_Url_run();
                    }

                    @Override
                    public void onSuccess(int statusCode,Header[] headers,String responseString) {
                        try {
                            Gson gson_reponse = new Gson();
                            data_Api_Config_reponse data_api_config_reponse = gson_reponse.fromJson(responseString,data_Api_Config_reponse.class);
                            //     Log.e("data_dangnhap_response",gson_reponse.toJson( data_api_config_reponse));
                            data_Api_Config_reponse.data_congdung[] congdungs = data_api_config_reponse.getResult().getCongdungs();
                            if (data_api_config_reponse != null) {
                                sqlite_Config_CongDung sqlite_config_congDung = new sqlite_Config_CongDung(MainActivity.this);
                                if (sqlite_config_congDung.getStatus_Table_ListCongDung_Exists()) {
                                    for (int i = 0; i < congdungs.length; i++) {
                                        String doituong = congdungs[i].getDoiTuong();
                                        data_Api_Config_reponse.data_ListCongDung[] list_congdung = congdungs[i].getListCongDung();
                                        for (int j = 0; j < list_congdung.length; j++) {
                                            String congdung = list_congdung[j].getCong_dung();
                                            // luu sql
                                            data_Config_CongDung data_config_congDung = new data_Config_CongDung(doituong,congdung);
                                            sqlite_config_congDung.Insert_Table_ListCongDung(data_config_congDung);
                                        }
                                    }
                                }
                            }
                            // update data
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Display_DoiTuong();
                                }
                            });
                        }catch (Exception e){

                        }
                    }
                });
    }

    private void  Display_DoiTuong(){
        sqlite_Config_CongDung  sqlite_config_congDung = new sqlite_Config_CongDung(MainActivity.this);
        if(sqlite_config_congDung.getStatus_Table_ListCongDung_Exists()) {
            List<String> doituongs = sqlite_config_congDung.Get_DoiTuong();
            array_doituong.addAll(doituongs);
            adapter_doituong.notifyDataSetChanged();

        }
    }
    private  void Display_CongDung(String doituong){
        try{
            array_congdung.clear();
            adapter_congdung.clear();
            adapter_congdung.notifyDataSetChanged();
        }catch (Exception e){

        }
        sqlite_Config_CongDung  sqlite_config_congDung = new sqlite_Config_CongDung(MainActivity.this);
        if(sqlite_config_congDung.getStatus_Table_ListCongDung_Exists()) {
            List<String> congdungs = sqlite_config_congDung.Get_CongDung(doituong);
            array_congdung.addAll(congdungs);
            adapter_congdung.notifyDataSetChanged();
        }
    }
    AlertDialog dialog_notification ;
    private void Dialog_Notification(final String text){
        AlertDialog.Builder alert_update_version =new AlertDialog.Builder(MainActivity.this);
        alert_update_version.setMessage(text);
        // dong y
        alert_update_version.setPositiveButton("Đồng ý",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog,int which) {
                dialog_notification.dismiss();
            }
        });
        // bo qua
        alert_update_version.setNegativeButton("Bỏ qua",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog,int which) {
                finish();
            }
        });
        dialog_notification = alert_update_version.create();
        dialog_notification.show();
    }

    AlertDialog dialog_notification_dangxuat ;
    private void Dialog_Notification_Dangxuat(final String text){
        AlertDialog.Builder dialog_notification =new AlertDialog.Builder(MainActivity.this);
        dialog_notification.setMessage(text);
        // dong y
        dialog_notification.setPositiveButton("Đồng ý",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog,int which) {
                Intent intent = new Intent(MainActivity.this,activity_DangNhap.class);
                //intent.putExtra("SODIENTHOAI",com_reponse[1]);
                finish();
                startActivity(intent);
                dialog_notification_dangxuat.dismiss();
            }
        });
        // bo qua
        dialog_notification.setNegativeButton("Hủy",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog,int which) {
                dialog_notification_dangxuat.dismiss();
            }
        });
        dialog_notification_dangxuat = dialog_notification.create();
        dialog_notification_dangxuat.show();
    }

    private void TaiLyDoTangGiam(){
        // lay version
        /*
        sqlite_checkversion = new sqlite_Checkversion(MainActivity.this);
        data_Checkversion version_current = sqlite_checkversion.Get_InfoVersion("1");

         */
        sqlite_config_lydotanggiam  sqlite_config_lydotanggiam = new sqlite_config_lydotanggiam(MainActivity.this);
        if(sqlite_config_lydotanggiam.getStatus_Table_ListLyDoTangGiam_Exists()) {
            if(!sqlite_config_lydotanggiam.Delete_Table_ListLyDoTangGiam()){
                return;
            }
        }
        /*
        String version  = version_current.getVersion();
        data_Api_Config_send data_api_config_send = new data_Api_Config_send(version);
        Gson gson_request = new Gson();
        String json_send = gson_request.toJson(data_api_config_send);
        *
         */
        Api_lydotanggiam.get(MainActivity.this,"",
                null,new TextHttpResponseHandler() {
                    @Override
                    public void onFailure(int statusCode,Header[] headers,String responseString,Throwable throwable) {
                        data_Http data_http = new data_Http(MainActivity.this);
                        data_http.switch_Url_run();
                    }

                    @Override
                    public void onSuccess(int statusCode,Header[] headers,String responseString) {
                        try {
                            Gson gson_reponse = new Gson();
                            data_lydotanggiam_response data_lydotanggiam_response = gson_reponse.fromJson(responseString,data_lydotanggiam_response.class);
                            //   Log.e("data_lydoTG_response",gson_reponse.toJson( data_lydotanggiam_response));
                            data_lydotanggiam_response.data_lydotanggiam[] all_data = data_lydotanggiam_response.getResult().getLydotanggiam();
                            if (all_data != null) {
                                sqlite_config_lydotanggiam sqlite_config_lydotanggiam = new sqlite_config_lydotanggiam(MainActivity.this);
                                if (sqlite_config_lydotanggiam.getStatus_Table_ListLyDoTangGiam_Exists()) {
                                    for (int i = 0; i < all_data.length; i++) {
                                        String ma = all_data[i].getMa();
                                        String noidung = all_data[i].getNoidung();
                                        data_lydotanggiam data_lydotanggiam = new data_lydotanggiam(ma,noidung);
                                        sqlite_config_lydotanggiam.Insert_Table_ListLyDoTangGiam(data_lydotanggiam);
                                    }
                                }
                            }
                            // update data
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Display_LyDoTangGiam();
                                }
                            });
                        }catch (Exception e){

                        }
                    }
                });
    }
    private void  Display_LyDoTangGiam(){
        sqlite_config_lydotanggiam  sqlite_config_lydotanggiam = new sqlite_config_lydotanggiam(MainActivity.this);
        if(sqlite_config_lydotanggiam.getStatus_Table_ListLyDoTangGiam_Exists()) {
            List<data_lydotanggiam> data_lydotanggiams = sqlite_config_lydotanggiam.Get_Table_ListLyDoTangGiam();
            for (data_lydotanggiam data : data_lydotanggiams){
                String ma = data.getMa();
                String noidung = data.getNoidung();
                String lydotanggiam  = ma +" - " +noidung ;
                adapter_lydotanggiam.add(lydotanggiam);
            }

            adapter_lydotanggiam.notifyDataSetChanged();

        }
    }



}
package com.dkhanhaptechs.mghiso.lichsuthaydongho;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.dkhanhaptechs.mghiso.R;
import java.util.List;

class adapter_Lichsuthaydongho extends BaseAdapter {
    private Context Context_ ;
    private int Layout_ ;
    private List<data_Lichsuthaydongho> List_data_lichsuthaydongho_ ;
    public adapter_Lichsuthaydongho(Context context, int layout, List<data_Lichsuthaydongho> list_data_lichsuthaydongho){
        this.Context_ = context;
        this.Layout_ = layout;
        this.List_data_lichsuthaydongho_ = list_data_lichsuthaydongho;
    }
    @Override
    public int getCount() {
        return List_data_lichsuthaydongho_.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
    private  class  Viewholder_data_lichsuthaydongho{
        TextView tv_datetime ;
        TextView tv_hieu ;
        TextView tv_co ;
        TextView tv_sothan ;
        TextView tv_lydo;
        TextView tv_csgo ;
        TextView tv_csgan;

    }
    @Override
    public View getView(int position,View convertView,ViewGroup parent) {
        adapter_Lichsuthaydongho.Viewholder_data_lichsuthaydongho viewholder ;
        if(convertView == null){
            viewholder = new adapter_Lichsuthaydongho.Viewholder_data_lichsuthaydongho();
            LayoutInflater inflater = (LayoutInflater) Context_.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(Layout_,null);
            // Map
            viewholder.tv_datetime = (TextView) convertView.findViewById(R.id.textview_datetime_donglichsuthaydongho);
            viewholder.tv_hieu = (TextView) convertView.findViewById(R.id.textview_hieu_donglichsuthaydongho);
            viewholder.tv_co = (TextView) convertView.findViewById(R.id.textview_co_donglichsuthaydongho);
            viewholder.tv_sothan = (TextView) convertView.findViewById(R.id.textview_sothan_donglichsuthaydongho);
            viewholder.tv_lydo = (TextView) convertView.findViewById(R.id.textview_lydo_donglichsuthaydongho);
            viewholder.tv_csgo = (TextView) convertView.findViewById(R.id.textview_chisogo_donglichsuthaydongho);
            viewholder.tv_csgan = (TextView) convertView.findViewById(R.id.textview_chisogan_donglichsuthaydongho);
            //////////
            convertView.setTag(viewholder);
        }else {
            viewholder = (adapter_Lichsuthaydongho.Viewholder_data_lichsuthaydongho) convertView.getTag();
        }
        // GET DATA
        data_Lichsuthaydongho data_lichsuthaydongho = List_data_lichsuthaydongho_.get(position);
        // SET DATA
        viewholder.tv_datetime.setText(data_lichsuthaydongho.getDatetime());
        viewholder.tv_hieu.setText(data_lichsuthaydongho.getHieu());
        viewholder.tv_co.setText(data_lichsuthaydongho.getCo());
        viewholder.tv_sothan.setText(data_lichsuthaydongho.getSothan());
        viewholder.tv_csgo.setText(data_lichsuthaydongho.getCSGo());
        viewholder.tv_csgan.setText(data_lichsuthaydongho.getCSGan());
        viewholder.tv_lydo.setText(data_lichsuthaydongho.getLydo());
        return convertView;
    }
}

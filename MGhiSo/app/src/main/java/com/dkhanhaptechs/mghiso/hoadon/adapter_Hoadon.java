package com.dkhanhaptechs.mghiso.hoadon;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.dkhanhaptechs.mghiso.R;
import com.dkhanhaptechs.mghiso.Share.TienNo.*;


import java.text.DecimalFormat;
import java.util.List;

class adapter_Hoadon  extends BaseAdapter {
    private Context Context_ ;
    private int m_Layout ;
    private List<data_tiennos.data_tienno> List_data_tienno ;
    public adapter_Hoadon(Context context, int layout, List<data_tiennos.data_tienno> list_data_tienno){
        this.Context_ = context;
        this.m_Layout = layout;
        this.List_data_tienno = list_data_tienno;
    }

    @Override
    public int getCount() {
        return List_data_tienno.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    private  class  Viewholder_data_tieno{
        TextView tv_nam ;
        TextView tv_ky ;
        TextView tv_tienno ;


    }

    @Override
    public View getView(int position,View convertView,ViewGroup parent) {
        adapter_Hoadon.Viewholder_data_tieno viewholder ;
        if(convertView == null){

            viewholder = new adapter_Hoadon.Viewholder_data_tieno();
            LayoutInflater inflater = (LayoutInflater) Context_.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(m_Layout,null);
            // Map
            viewholder.tv_ky = (TextView) convertView.findViewById(R.id.textview_ky_dongtienno);
            viewholder.tv_nam = (TextView) convertView.findViewById(R.id.textview_nam_dongtienno);
            viewholder.tv_tienno = (TextView) convertView.findViewById(R.id.textview_tienno_dongtienno);


            //////////
            convertView.setTag(viewholder);
        }else {
            viewholder = (adapter_Hoadon.Viewholder_data_tieno) convertView.getTag();
        }
        DecimalFormat formatter = new DecimalFormat("###,###,###");
        data_tiennos.data_tienno tienno = List_data_tienno.get(position);
        viewholder.tv_ky.setText(tienno.getKy());
        viewholder.tv_nam.setText(tienno.getNam());
        int tienno_ = Integer.parseInt(tienno.getTienNo());
        viewholder.tv_tienno.setText(formatter.format(tienno_));
        return convertView;
    }
}

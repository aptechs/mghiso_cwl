package com.dkhanhaptechs.mghiso.Share.Config_CongDung;

import com.dkhanhaptechs.mghiso.Share.DanhSach_DocSo.data_Api_DanhSachDocSo_send;

public class data_Api_Config_send {
    private String type;
    private Content content ;

    public data_Api_Config_send(String  version) {
        this.type = "update version app";
        this.content = new Content(version);
    }

    class  Content {
        private String update_version ;

        public Content(String update_version) {
            this.update_version = update_version;
        }

        public String getUpdate_version() {
            return update_version;
        }

        public void setUpdate_version(String update_version) {
            this.update_version = update_version;
        }
    }

}

package com.dkhanhaptechs.mghiso.Share.MAC_SMID;

import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Collections;
import java.util.List;

public class get_MAC_SMID {
    private  String mac_mid ;

    public get_MAC_SMID() throws SocketException {
        String mac ="";
        List<NetworkInterface> networkInterfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
        for( NetworkInterface networkInterface: networkInterfaces){
            if(networkInterface.getDisplayName().equalsIgnoreCase("wlan0")){
                for (int i =0 ; i < networkInterface.getHardwareAddress().length; i++){
                    String string_macbyte = Integer.toHexString(networkInterface.getHardwareAddress()[i]&0xFF);
                    if(string_macbyte.length() == 1){
                        string_macbyte = "0"+string_macbyte;

                    }
                    mac = mac + string_macbyte.toUpperCase()+";";
                }

                break;
            }
        }
        mac_mid = mac.substring(0,mac.length()-1);

    }

    public String getMac_mid() {
        return mac_mid;
    }

    public void setMac_mid(String mac_mid) {
        this.mac_mid = mac_mid;
    }
}

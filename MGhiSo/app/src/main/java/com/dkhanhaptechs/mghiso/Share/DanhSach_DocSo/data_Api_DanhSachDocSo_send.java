package com.dkhanhaptechs.mghiso.Share.DanhSach_DocSo;

public class data_Api_DanhSachDocSo_send {
    private String type;
    private Content  content ;

    public data_Api_DanhSachDocSo_send(String nam, String ky, String dot, String may) {
        this.type = "lay danh sach doc so";

        String [] com_somay = may.split("_");
        if(com_somay.length >1) {
            may = com_somay[1].trim();
        }
        this.content = new Content(nam,ky,dot,may);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Content getContent() {
        return content;
    }

    public void setContent(Content content) {
        this.content = content;
    }

    class  Content {
        private String nam;
        private String ky;
        private String dot;
        private String may;

        public Content(String nam,String ky,String dot,String may) {
            this.nam = nam;
            this.ky = ky;
            this.dot = dot;
            this.may = may;
        }

        public String getNam() {
            return nam;
        }

        public void setNam(String nam) {
            this.nam = nam;
        }

        public String getKy() {
            return ky;
        }

        public void setKy(String ky) {
            this.ky = ky;
        }

        public String getDot() {
            return dot;
        }

        public void setDot(String dot) {
            this.dot = dot;
        }

        public String getMay() {
            return may;
        }

        public void setMay(String may) {
            this.may = may;
        }
    }
}

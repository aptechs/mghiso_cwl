package com.dkhanhaptechs.mghiso.Share.HinhAnh;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.dkhanhaptechs.mghiso.Share.Database_SQLite;
import com.dkhanhaptechs.mghiso.Share.LyDoTangGiam.data_lydotanggiam;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class sqlite_Hinhanh {
    private Database_SQLite database_sqLite ;
    private String Database ="Database_HinhAnh.sqlite";
    private String Table_ListHinhAnh = "ListListHinhAnh";
    private boolean Status_Table_ListHinhAnh_Exists = false;
    public sqlite_Hinhanh (Context context, String nam,String ky, String dot, String somay){
        this.Database = Database +"_"+nam+".sqlite";
        this.Table_ListHinhAnh = Table_ListHinhAnh +"_"+nam+"_"+ky+"_"+dot+"_"+somay ;
        database_sqLite = new Database_SQLite(context, Database, null, 1);
        if(Create_Table_ListHinhAnh()){
            Status_Table_ListHinhAnh_Exists = true;
        }else {
            Status_Table_ListHinhAnh_Exists = false;
        }
    }
    public boolean getStatus_Table_ListHinhAnh_Exists() {
        return Status_Table_ListHinhAnh_Exists;
    }

    private   boolean Create_Table_ListHinhAnh(){
        boolean ok =false;
        try {
            String querry = "CREATE TABLE IF NOT EXISTS "+Table_ListHinhAnh +
                    " (" +
                    " id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                    " danhba nvarchar(50)," +
                    " anh TEXT," +
                    " sync_anh nvarchar(5)," +
                    " type nvarchar(50)" +
                    ");";
            if(database_sqLite.QueryDatabase(querry)) {
                ok = true;
            }
        }catch (Exception e){

        }
        return ok;
    }
    public int Get_Sum_AnhSync(){
        int sum_anh = 0 ;
        try {

            String querry = "SELECT COUNT(id) FROM "+Table_ListHinhAnh +" WHERE sync_anh ='1';" ;
            Cursor data_check = database_sqLite.GetDatabase(querry);
            while (data_check.moveToNext()) {
                sum_anh= data_check.getInt(0);
            }

        }catch (Exception e){

        }
        return   sum_anh;
    }
    public  boolean Insert_Table_ListHinhAnh(data_Hinhanh data_anhs){
        boolean ok =false;
        try {

            // insert
            for (data_Hinhanh.data_anh data_anh:data_anhs.getHinhanhs()) {
                String querry = "INSERT INTO " + Table_ListHinhAnh + " (danhba,anh,sync_anh,type)" +
                        " VALUES ('" + data_anhs.getDanhba().toString().trim() + "','" + data_anh.getAnh().trim() + "','0','"+data_anh.getType()+"');";
                database_sqLite.QueryDatabase(querry);
            }
            ok = true;
        }catch (Exception e){
            Log.e("INSERT ListHinhAnh",e.toString());
        }
        return ok;
    }
    public  boolean Update_Table_ListHinhAnh(String danhba, List<String> list_id){
        boolean ok =false;
        try {

            // insert
            for(String id : list_id) {
                String querry = "UPDATE " + Table_ListHinhAnh +
                        " SET " +
                        " sync_anh = '1'" +
                        " WHERE danhba='" + danhba + "' AND id = "+id +";";
                if (database_sqLite.QueryDatabase(querry)) {
                    ok = true;
                }
            }
        }catch (Exception e){
            Log.e("Update ListHinhAnh",e.toString());
        }
        return ok;
    }
    public  boolean Update_Table_HinhAnh(String danhba, String id){
        boolean ok =false;
        try {

            // insert

                String querry = "UPDATE " + Table_ListHinhAnh +
                        " SET " +
                        " sync_anh = '1'" +
                        " WHERE danhba='" + danhba + "' AND id = "+id +";";
                if (database_sqLite.QueryDatabase(querry)) {
                    ok = true;
                }

        }catch (Exception e){
            Log.e("Update ListHinhAnh",e.toString());
        }
        return ok;
    }

    public boolean Delete_Table_ListHinhAnh() {
        boolean ok = false;
        try {
            String querry = "DROP TABLE IF EXISTS " + Table_ListHinhAnh + "; ";

            if (database_sqLite.QueryDatabase(querry)) {
                ok = true;
                Log.e("Dl Table_ListHinhAnh ","ok");
            }
        } catch (Exception e) {

        }
        return ok ;
    }
    public data_Hinhanh Get_Table_ListHinhAnh(String danhba , String type){
        data_Hinhanh data_danhbaanh= new data_Hinhanh(danhba);
        List<data_Hinhanh.data_anh> list_anh = new ArrayList<>();
        try {

            String querry = "SELECT id,anh,sync_anh,type FROM "+Table_ListHinhAnh +" WHERE danhba ='"+danhba+"' AND type='"+type+"' ORDER BY id DESC ;" ;
            Cursor data_check = database_sqLite.GetDatabase(querry);
            while (data_check.moveToNext()) {
                int id = data_check.getInt(0);
                String value_anh = data_check.getString(1);
                String sync_anh = data_check.getString(2);
                type = data_check.getString(3);
                data_Hinhanh.data_anh data_anh = new data_Hinhanh.data_anh(value_anh);
                data_anh.setId(id);
                data_anh.setSync_anh(sync_anh);
                data_anh.setType(type);
                list_anh.add(data_anh);
            }

            data_danhbaanh.setHinhanhs(list_anh);

        }catch (Exception e){

        }
        return data_danhbaanh;
    }
    public List<String> Get_Table_ListDanhBaCoAnh(){

        List<String> list_danhba = new ArrayList<String>();
        try {

            String querry = "SELECT DISTINCT danhba FROM "+Table_ListHinhAnh +" ;" ;
            Cursor data_check = database_sqLite.GetDatabase(querry);
            while (data_check.moveToNext()) {
                String danhba = data_check.getString(0);
                list_danhba.add(danhba);
            }



        }catch (Exception e){

        }
        return list_danhba;
    }
    public List<String> Get_TableID_ListHinhAnh_NoSync(String danhba){
        List<String> list_id = new ArrayList<>();
        try {

            String querry = "SELECT id,anh,sync_anh FROM "+Table_ListHinhAnh +" WHERE danhba ='"+danhba+"' AND sync_anh !='1'  ;" ;
            Cursor data_check = database_sqLite.GetDatabase(querry);
            while (data_check.moveToNext()) {
                String id = data_check.getString(0);
                list_id.add(id);
            }
        }catch (Exception e){

        }
        return list_id;
    }
    public String Get_Table_1HinhAnh(String danhba, String id){
        String value_anh ="";
        try {

            String querry = "SELECT anh FROM "+Table_ListHinhAnh +" WHERE danhba ='"+danhba+"' AND id ="+id+" ;" ;
            Cursor data_check = database_sqLite.GetDatabase(querry);
            while (data_check.moveToNext()) {

                value_anh = data_check.getString(0);
            }
        }catch (Exception e){

        }
        return value_anh;
    }
    public int Get_Count_HinhAnh_Gui(String danhba){
        int count =0;
        try {

            String querry = "SELECT COUNT(id) FROM "+Table_ListHinhAnh +" WHERE danhba ='"+danhba+"'  AND sync_anh ='1';" ;
            Cursor data_check = database_sqLite.GetDatabase(querry);
            while (data_check.moveToNext()) {
                count = data_check.getInt(0);
            }
        }catch (Exception e){

        }
        return count;
    }
    public int Get_Count_Table_ListHinhAnh(String danhba){
        int count =0;
        try {

            String querry = "SELECT COUNT(id) FROM "+Table_ListHinhAnh +" WHERE danhba ='"+danhba+"'  ;" ;
            Cursor data_check = database_sqLite.GetDatabase(querry);
            while (data_check.moveToNext()) {
               count = data_check.getInt(0);

            }
        }catch (Exception e){

        }
        return count;
    }
    public boolean Delete_Anh_ListDataAnh(int id, String danhba){
        boolean ok = false;
        try {
            String querry = "DELETE FROM "+Table_ListHinhAnh+"  " +
                    " WHERE " +
                    "  danhba ='"+danhba+"'AND id = "+String.valueOf(id) +
                    " ;" ;
            if(database_sqLite.QueryDatabase(querry)) {
                ok = true;
            }
        }catch (Exception e){

        }
        return  ok;
    }
}

package com.dkhanhaptechs.mghiso.danhsachdocso_chitiet_khachhang;


import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;


import android.util.Log;
import android.view.KeyEvent;

import android.view.View;
import android.view.inputmethod.EditorInfo;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;

import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.dkhanhaptechs.mghiso.R;
import com.dkhanhaptechs.mghiso.Share.Bluetooth.BluetoothPrintService;
import com.dkhanhaptechs.mghiso.Share.Bluetooth.data_AddBluetooth;
import com.dkhanhaptechs.mghiso.Share.Bluetooth.sqlite_AddBluetooth;
import com.dkhanhaptechs.mghiso.Share.Config_CongDung.sqlite_Config_CongDung;
import com.dkhanhaptechs.mghiso.Share.Dialog_App;

import com.dkhanhaptechs.mghiso.Share.HinhAnh.sqlite_Hinhanh;
import com.dkhanhaptechs.mghiso.Share.LyDoTangGiam.data_lydotanggiam;
import com.dkhanhaptechs.mghiso.Share.LyDoTangGiam.sqlite_config_lydotanggiam;

import com.dkhanhaptechs.mghiso.Share.Location_Device.*;
import com.dkhanhaptechs.mghiso.Share.LichSuThayDH.*;
import com.dkhanhaptechs.mghiso.Share.ThanhToan.*;
import com.dkhanhaptechs.mghiso.Share.TienNo.data_tiennos;
import com.dkhanhaptechs.mghiso.Share.TienNo.sqlite_TienNo;
import com.dkhanhaptechs.mghiso.Share.User.data_User;
import com.dkhanhaptechs.mghiso.Share.User.sqlite_User;
import com.dkhanhaptechs.mghiso.Share.localizereceipts.ModelCapability;
import com.dkhanhaptechs.mghiso.Share.localizereceipts.PrinterSettings;
import com.dkhanhaptechs.mghiso.danhsachdocso_chitiet.data_Enable_Ghiso;

import com.dkhanhaptechs.mghiso.congdung.*;

import com.dkhanhaptechs.mghiso.danhsachdocso_chitiet.data_Danhsachdocso_chitiet;

import com.dkhanhaptechs.mghiso.mayin.activity_DeviceList;

import com.dkhanhaptechs.mghiso.thongketieuthu.activity_Thongketieuthu;
import com.dkhanhaptechs.mghiso.lichsuthaydongho.activity_Lichsuthaydongho;
import com.dkhanhaptechs.mghiso.thongtindongho.activity_Thongtindongho;
import com.dkhanhaptechs.mghiso.chuphinh.activity_Chuphinh;
import com.dkhanhaptechs.mghiso.hoadon.activity_Hoadon;
import com.dkhanhaptechs.mghiso.Share.DanhSach_DocSo.*;
import com.google.gson.Gson;

import com.starmicronics.starioextension.ICommandBuilder;
import com.starmicronics.starioextension.StarIoExt;
import com.woosim.printer.WoosimCmd;
import com.woosim.printer.WoosimImage;
import com.woosim.printer.WoosimService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.net.URI;
import java.net.URISyntaxException;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;


import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.client.utils.URIBuilder;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import cz.msebera.android.httpclient.util.EntityUtils;

import  com.dkhanhaptechs.mghiso.Share.User.*;
public class activity_Danhsachdocso_chitiet_khachhang extends AppCompatActivity {
    // BIEN LAYOUT
    private TextView  tv_mlt, tv_hoten,tv_diachi,tv_tieude;
    private ImageView imv_dongbo;
  //  private Toolbar toolbar ;
    private String Json_table_ghiso_chitiet = "";
    private EditText edt_diachinew , edt_congdung;
    private EditText edt_dienthoai1, edt_dienthoai2,edt_csm;
    private TextView tv_hieuconamdh,tv_code,tv_csc,tv_ttcu, tv_binhquan,tv_giabieu, tv_dinhmuc,tv_tieuthu, tv_machigoc;
    private Spinner  spn_tinhtrangchi,spn_vitridh, spn_khachhang, spn_tinhtrang;
    private CheckBox cb_diachinew, cb_doituongcongdung, cb_dongho, cb_dienthoai ;
    private EditText edt_ghichu1,edt_ghichu2;
    private AutoCompleteTextView auto_danhba,auto_sothan;
    //
    private Button btn_xacnhan;
    private Button btn_hoadon , btn_in ;
    private Button btn_tieuthu, btn_thaydongho, btn_thongtindongho, btn_chuphinh;
    private Button btn_next , btn_back;
    // BIEN CHUONG TRINH
    private String Danhba, So_St, Hoten, Diachi, Nam, Ky, Dot, SoMay;
    private String TinhTrang ;
    private  int DongBo = 0 ;

    private ArrayList<String> array_tinhtrangchi ;
    private ArrayList<String> array_vitridongho ;
    private ArrayList<String> array_khachhang ;
    private ArrayList<String> array_tinhtrang ;


    private ArrayAdapter adapter_tinhtrangchi;
    private ArrayAdapter adapter_vitridongho;
    private ArrayAdapter adapter_khachhang;
    private ArrayAdapter adapter_tinhtrang;
    private String [] data_TinhTrangChi = {"Đủ chì","Đứt chì góc","Đứt chì MS","Đứt 2 chì"};
    private String [] data_ViTrisDh = {"Giữa","Trái","Phải"};
    private String [] data_ViTrisDh_Gui = {"G","T","P"};
    private String Danhba_current ="";
    private String SoThan_Current ="";
    private String [] data_TinhTrang = {
            "B.Thường",
            "Chủ ghi",
            "Chủ báo",
            "Đóng cửa",
            "Thay Đ.Kỳ",
            "Thay B.Thường",
            "Mất",
            "Đ/C K.ở",
            "N.C.N",
            "Chất đồ",
            "Giải tỏa",
            "Kính mờ",
            "Kẹt khóa",
            "Lệch số",
            "Kẹt số",
            "Chạy ngược",
            "Ngập nước",
            "Gắn mới",
            "TLDB",
            "Retour4",
            "Retour5",
            "Lấp",
            "Đóng Nước",
            "Bấm Chì"
    };

    private String Code_Tinhtrang = "4";
    private String Doituong_Current ="";
    private String Congdung_Current = "";



    // May In
    public static final int STATE_NONE = 0;               // we're doing nothing
    private static final int STATE_LISTEN = 1;     // now listening for incoming connections
    private static final int STATE_CONNECTING = 2; // now initiating an outgoing connection
    public static final int STATE_CONNECTED = 3;     private static final String TAG = "MainActivity";
    private static final boolean D = true;

    // Message types sent from the BluetoothPrintService Handler
    public static final int MESSAGE_DEVICE_NAME = 1;
    public static final int MESSAGE_TOAST = 2;
    public static final int MESSAGE_READ = 3;

    // Key names received from the BluetoothPrintService Handler
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";

    // Intent request codes
    private static final int REQUEST_CONNECT_DEVICE_SECURE = 1;
    private static final int REQUEST_CONNECT_DEVICE_INSECURE = 2;
    private static final int REQUEST_ENABLE_BT = 3;

    private static final int PERMISSION_DEVICE_SCAN_SECURE = 11;
    private static final int PERMISSION_DEVICE_SCAN_INSECURE = 12;

    // Layout Views
    private boolean mEmphasis = false;
    private boolean mUnderline = false;
    private int mCharsize = 1;
    private int mJustification = WoosimCmd.ALIGN_LEFT;


    // Local Bluetooth adapter
    private BluetoothAdapter mBluetoothAdapter = null;
    // Member object for the print services
    private BluetoothPrintService mPrintService = null;
    private WoosimService mWoosim = null;
    private String Add_BLT = "";
    private String Type_BLT_Run = "";
///
    private int per_tieuthu_alarm = 30;
    private String CongDung_Server ="";
    private Boolean Status_Action_Checkbox = true;
    private Boolean Change_Screen = false;
    private Boolean Change_Screen_CongDung = false;
    private String Ngay_ThayDH ="";
    private String Date_Tungay ="";
    private String Date_Denngay ="";
    private String TinhTrang_DHMoi = "" ;
    private String Code_old = "";
    private Boolean Status_TTTthu = false;
    private int Enable_Ghiso = 0;
    private int Enable_Chuphinh = 0;
    private String Json_EnableGhiso_Server ="";
    private static final int NORMAL_GHISO = 0;
    private static final int DISABLE_GHISO_CHUADENGIO = 1;
    private static final int DISABLE_GHISO_QUAGIO = 2;
    private static final int DISABLE_KHONGCODENNGAY = 3;
    private String Status_Sync = "2";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_danhsachdocso_chitiet_khachhang);
        Mappping_Layout();
        Display_serial();
        // Bluetooth
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        array_tinhtrangchi = new ArrayList<String>();
        adapter_tinhtrangchi = new ArrayAdapter(activity_Danhsachdocso_chitiet_khachhang.this, R.layout.text_color_spinner,array_tinhtrangchi);
        adapter_tinhtrangchi.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
        spn_tinhtrangchi.setAdapter(adapter_tinhtrangchi);
        array_tinhtrangchi.addAll(Arrays.asList(data_TinhTrangChi));
        adapter_tinhtrangchi.notifyDataSetChanged();

        array_vitridongho = new ArrayList<String>();
        adapter_vitridongho = new ArrayAdapter(activity_Danhsachdocso_chitiet_khachhang.this, R.layout.text_color_spinner,array_vitridongho);
        adapter_vitridongho.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
        spn_vitridh.setAdapter(adapter_vitridongho);
        array_vitridongho.addAll(Arrays.asList(data_ViTrisDh));
        adapter_vitridongho.notifyDataSetChanged();


        array_khachhang = new ArrayList<String>();
        adapter_khachhang = new ArrayAdapter(activity_Danhsachdocso_chitiet_khachhang.this,R.layout.text_color_spinner,array_khachhang);
        adapter_khachhang.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
        spn_khachhang.setAdapter(adapter_khachhang);

        array_tinhtrang = new ArrayList<String>();
        adapter_tinhtrang = new ArrayAdapter(activity_Danhsachdocso_chitiet_khachhang.this, R.layout.text_color_spinner,array_tinhtrang);
        adapter_tinhtrang.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
        spn_tinhtrang.setAdapter(adapter_tinhtrang);
        array_tinhtrang.addAll(Arrays.asList(data_TinhTrang));
        adapter_tinhtrang.notifyDataSetChanged();



        // GET DATA
        Json_table_ghiso_chitiet =getIntent().getStringExtra("TABLE_DANHSACHDOCSO_CHITIET");
     //   Display_DoiTuong();
        Display_KhachHang();
        // SET DATA
        if(Get_Table_GhiSo_Chitiet(Json_table_ghiso_chitiet)){
            auto_danhba.setText(Danhba,true);
        }

        spn_tinhtrang.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent,View view,int position,long id) {
                String tinhtrang  = data_TinhTrang[position];

                String nam = String.valueOf(Nam);
                String ky = String.valueOf(Ky);
                String dot = String.valueOf(Dot);
                String somay = String.valueOf(SoMay);
                sqlite_DanhSachKhackHang_DocSo sqlite_danhSachKhackHang_docSo = new sqlite_DanhSachKhackHang_DocSo(activity_Danhsachdocso_chitiet_khachhang.this,nam,ky,dot,somay);


               // String code = get_Codetinhtrang(tinhtrang,Code_old);
               // tv_code.setText(code);
                String tieuthu ="0";
                String csm ="";
                String binhquan = tv_binhquan.getText().toString().trim();
                if(tinhtrang.trim().equals("Đóng cửa") ||
                        tinhtrang.trim().equals("Mất") ||
                        tinhtrang.trim().equals("N.C.N") ||
                        tinhtrang.trim().equals("Chất đồ") ||
                        tinhtrang.trim().equals("Kính mờ") ||
                        tinhtrang.trim().equals("Kẹt khóa") ||
                        tinhtrang.trim().equals("Lệch số") ||
                        tinhtrang.trim().equals("Kẹt số") ||
                        tinhtrang.trim().equals("Chạy ngược") ||
                        tinhtrang.trim().equals("Ngập nước") ||
                        tinhtrang.trim().equals("Lấp") ){
                        tieuthu = binhquan ;
                        tv_tieuthu.setText(tieuthu);
                    String csc = tv_csc.getText().toString().trim();
                    if(!csc.isEmpty()){
                        int csm_i = Integer.parseInt(binhquan)+ Integer.parseInt(csc);
                        csm = String.valueOf(csm_i);
                    }else {
                        csm =binhquan ;
                    }
                    edt_csm.setText(csm);
                    // tinh tieu thu trung binh
                }else if( tinhtrang.trim().equals("TLDB")||
                        tinhtrang.trim().equals("Gắn mới")){
                    csm = edt_csm.getText().toString().trim();
                    if(!csm.isEmpty()){
                        tieuthu = csm;
                        tv_tieuthu.setText(tieuthu);
                    }else {
                        tieuthu = "0";
                        tv_tieuthu.setText(tieuthu);
                    }

                }
                else if( tinhtrang.trim().equals("Đ/C K.ở")||
                        tinhtrang.trim().equals("Đóng Nước")||
                        tinhtrang.trim().equals("Bấm Chì")||
                        tinhtrang.trim().equals("Giải tỏa")){
                    String csc = tv_csc.getText().toString().trim();
                    if(!csc.isEmpty()){
                        csm = csc;
                    }else {
                        csm = "0";
                    }
                    tieuthu = "0";
                    edt_csm.setText(csm);
                    tv_tieuthu.setText(tieuthu);

                }else if(tinhtrang.trim().equals("Retour4")){
                    csm = edt_csm.getText().toString().trim();
                    if(!csm.isEmpty()){
                        String csc = tv_csc.getText().toString().trim();
                        int tieuthu_tinh =10000-Integer.parseInt(csc)+Integer.parseInt(csm);
                        tieuthu = String.valueOf(tieuthu_tinh);
                        tv_tieuthu.setText(tieuthu);
                    }
                }
                else if(tinhtrang.trim().equals("Retour5")){
                    csm = edt_csm.getText().toString().trim();
                    if(!csm.isEmpty()){
                        String csc = tv_csc.getText().toString().trim();
                        int tieuthu_tinh = 100000-Integer.parseInt(csc)+Integer.parseInt(csm);
                        tieuthu = String.valueOf(tieuthu_tinh);
                        tv_tieuthu.setText(tieuthu);
                    }
                }
                else if(tinhtrang.trim().equals("Thay Đ.cửa")){
                    csm = edt_csm.getText().toString().trim();
                    tieuthu = binhquan;
                    tv_tieuthu.setText(tieuthu);
                }
                //Thay Bất Thường
             else if(tinhtrang.trim().equals("Thay B.Thường")) {
                    csm = edt_csm.getText().toString().trim();
                    List<String> tinhtrangthaydh = sqlite_danhSachKhackHang_docSo.Get_TinhTrangThayDH(Danhba);
                    String csgo = tinhtrangthaydh.get(0);
                    String csgan = tinhtrangthaydh.get(1);
                    String lydo = tinhtrangthaydh.get(4).trim();
                    String ngaythay_dh =tinhtrangthaydh.get(5).trim();
                    if (!csm.isEmpty() && !(csgan.trim().isEmpty())) {
                        String[] com_lydo = lydo.split("-");
                        if (com_lydo.length >= 2) {
                            String tinhtrangthay = com_lydo[1];
                            if (tinhtrangthay.trim().equals("NCN") ||
                                    tinhtrangthay.trim().equals("Lệch số") ||
                                    tinhtrangthay.trim().equals("Kẹt số") ||
                                    tinhtrangthay.trim().equals("MẤT") ||
                                    tinhtrangthay.trim().equals("Chạy ngược") ||
                                    (tinhtrangthay.trim().equals("KM") && (csgo.trim().equals("0")))
                            ) {
                                tieuthu = TinhTieuThu_TH2(csm,Date_Tungay,Date_Denngay,csgan,ngaythay_dh);
                            }
                        } else if(com_lydo[0].trim().equals("BTH")&& (csgo.trim().equals("0"))){
                            tieuthu = TinhTieuThu_TH2(csm,Date_Tungay,Date_Denngay,csgan,ngaythay_dh);
                        }
                        tv_tieuthu.setText(tieuthu);
                    }
                }
                else if( tinhtrang.trim().equals("Thay Đ.Kỳ")){
                    csm = edt_csm.getText().toString().trim();
                    List<String> tinhtrangthaydh = sqlite_danhSachKhackHang_docSo.Get_TinhTrangThayDH(Danhba);
                    String csgo = tinhtrangthaydh.get(0);
                    String csgan = tinhtrangthaydh.get(1);
                    if(!csm.isEmpty()&& !csgan.trim().isEmpty()){
                        String csc = tv_csc.getText().toString().trim();

                            if(!csgo.isEmpty() && !csgan.isEmpty()) {
                                try {
                                    int i_csgo = Integer.parseInt(csgo);
                                    int i_csgan = Integer.parseInt(csgan);
                                    if (i_csgo >= Integer.parseInt(csc)) {
                                        int tieuthu_tinh = i_csgo - Integer.parseInt(csc) + Integer.parseInt(csm) - i_csgan;
                                        tieuthu = String.valueOf(tieuthu_tinh);
                                        try {
                                            if (Integer.parseInt(tieuthu) < 0) {
                                                tieuthu = "0";
                                            }
                                        } catch (Exception e) {
                                            tieuthu = "0";
                                        }
                                    } else {
                                        tieuthu = "0";
                                    }
                                }catch (Exception e){

                                }
                            }else {
                                tieuthu ="0";
                            }
                    }else {
                        tieuthu ="0";
                    }
                    tv_tieuthu.setText(tieuthu);
                }else {
                    csm = edt_csm.getText().toString().trim();
                    if(!csm.isEmpty()) {
                        try {
                            String csc = tv_csc.getText().toString().trim();
                            int tieuthu_i = Integer.parseInt(csm) - Integer.parseInt(csc);
                            if (tieuthu_i < 0) {
                                tieuthu_i = 0;
                            }
                            tieuthu = String.valueOf(tieuthu_i);
                            tv_tieuthu.setText(tieuthu);
                            int binhquan_i = Integer.parseInt(binhquan);
                            if (binhquan_i > 1 && binhquan_i <= 100) {
                                per_tieuthu_alarm = 30;
                            } else if (binhquan_i > 100 && binhquan_i <= 1000) {
                                per_tieuthu_alarm = 20;
                            } else {
                                per_tieuthu_alarm = 10;
                            }
                            int thr_tieuthu_nho = (int)Math.round(binhquan_i - binhquan_i * per_tieuthu_alarm *0.01);
                            int thr_tieuthu_lon =(int)Math.round(binhquan_i + binhquan_i * per_tieuthu_alarm *0.01);
                            tv_tieuthu.setText(String.valueOf(tieuthu_i));
                            Dialog_App dialog_app = new Dialog_App(activity_Danhsachdocso_chitiet_khachhang.this);
                            if (tieuthu_i < thr_tieuthu_nho) {
                                String notification = "Tiêu thụ nhỏ hơn " + String.valueOf(per_tieuthu_alarm) + " % bình quân hàng tháng" + "\r\n" +
                                        "Vui lòng kiểm tra kỹ lại chỉ số ghi.";
                                dialog_app.Dialog_Notification(notification);
                                // Dialog_Notification_alarm_luu(notification);
                            } else if (tieuthu_i > thr_tieuthu_lon) {
                                String notification = "Tiêu thụ lớn hơn " + String.valueOf(per_tieuthu_alarm) + " % bình quân hàng tháng" + "\r\n" +
                                        "Vui lòng kiểm tra kỹ lại chỉ số ghi.";
                                dialog_app.Dialog_Notification(notification);
                                //Dialog_Notification_alarm_luu(notification);
                            } else {
                                // XacNhan_Ghiso();
                            }
                        }catch (Exception e){

                        }
                    }
                }

                //dieu chinh vi tri
                /*
                if(tinhtrang.equals("Gắn mới") || tinhtrang.equals("TLDB")){
                    spn_vitridh.setEnabled(true);
                }else {
                    spn_vitridh.setEnabled(false);
                }

                 */
                /*
                if(!tieuthu.isEmpty()) {

                    if (sqlite_danhSachKhackHang_docSo.getStatus_Table_DanhSachKhachHang_Exists()) {
                        sqlite_danhSachKhackHang_docSo.Update_Tinhtrang_Code_DanhSachDocSo_KhachHang(Danhba,tinhtrang,code);
                        if (!tieuthu.isEmpty() && !csm.isEmpty()) {
                            sqlite_danhSachKhackHang_docSo.Update_CSM_TieuThu_DanhSachDocSo_KhachHang(Danhba,csm,tieuthu);
                        }
                    }
                }
                 */
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        edt_congdung.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Enable_Ghiso = Enable_GhiSo(Danhba,Date_Denngay,TinhTrang_DHMoi);
                String json_send = "{\"danhba\":\""+Danhba+"\","+
                        "\"so_st\":\""+So_St+"\","+
                        "\"hoten\":\""+Hoten+"\","+
                        "\"diachi\":\""+Diachi+"\","+
                        "\"sync\":\""+String.valueOf(DongBo)+"\","+
                        "\"nam\":\""+Nam+"\","+
                        "\"ky\":\""+Ky+"\","+
                        "\"dot\":\""+Dot+"\","+
                        "\"congdung\":\""+CongDung_Server+"\","+
                        "\"enable_ghiso\":\""+String.valueOf(Enable_Ghiso)+"\","+
                        "\"somay\":\""+SoMay+"\"}";
                Intent intent = new Intent(activity_Danhsachdocso_chitiet_khachhang.this,activity_Congdung.class);
                intent.putExtra("CONGDUNG",json_send);
                startActivity(intent);
                Change_Screen = true;
                Change_Screen_CongDung =true;
            }
        });






        spn_khachhang.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent,View view,int position,long id) {
                /*
                String tinhtrang_khachhang  = array_khachhang.get(position);
              //  edt_ghichu1.setText(lydo_khachhang);
                String nam = String.valueOf(Nam);
                String ky = String.valueOf(Ky);
                String dot = String.valueOf(Dot);
                String somay = String.valueOf(SoMay);
                sqlite_DanhSachKhackHang_DocSo sqlite_danhSachKhackHang_docSo = new sqlite_DanhSachKhackHang_DocSo(activity_Danhsachdocso_chitiet_khachhang.this,nam,ky,dot,somay);
                if(sqlite_danhSachKhackHang_docSo.getStatus_Table_DanhSachKhachHang_Exists()){
                    sqlite_danhSachKhackHang_docSo.Update_TinhtrangKhachHang_DanhSachDocSo_KhachHang(Danhba,tinhtrang_khachhang);
                }

                 */
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        btn_tieuthu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Status_TTTthu =true;
                String json_send = "{\"danhba\":\""+Danhba+"\","+
                        "\"so_st\":\""+So_St+"\","+
                        "\"hoten\":\""+Hoten+"\","+
                        "\"diachi\":\""+Diachi+"\","+
                        "\"sync\":\""+String.valueOf(DongBo)+"\","+
                        "\"nam\":\""+Nam+"\","+
                        "\"ky\":\""+Ky+"\","+
                        "\"dot\":\""+Dot+"\","+
                        "\"somay\":\""+SoMay+"\","+
                        "\"ghiso\":\""+String.valueOf(Enable_Ghiso)+"\"}";
                Intent intent = new Intent(activity_Danhsachdocso_chitiet_khachhang.this,activity_Thongketieuthu.class);
                intent.putExtra("THONGKETIEUTHU",json_send);
                startActivity(intent);
                Change_Screen = true;
            }
        });
        // ACTION: THAY DONG HO
        btn_thaydongho.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  Toast.makeText(activity_Danhsachdocso_chitiet_khachhang.this,"btn_thaydongho",Toast.LENGTH_SHORT).show();
                String json_send = "{\"danhba\":\""+Danhba+"\","+
                        "\"so_st\":\""+So_St+"\","+
                        "\"hoten\":\""+Hoten+"\","+
                        "\"diachi\":\""+Diachi+"\","+
                        "\"sync\":\""+String.valueOf(DongBo)+"\","+
                        "\"nam\":\""+Nam+"\","+
                        "\"ky\":\""+Ky+"\","+
                        "\"dot\":\""+Dot+"\","+
                        "\"somay\":\""+SoMay+"\"}";
                Intent intent = new Intent(activity_Danhsachdocso_chitiet_khachhang.this,activity_Lichsuthaydongho.class);
                intent.putExtra("LICHSUTHAYDONGHO",json_send);
                startActivity(intent);
                Change_Screen = true;
            }
        });


        // ACTION: THONG TIN DONG HO
        btn_thongtindongho.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             //   Toast.makeText(activity_Danhsachdocso_chitiet_khachhang.this,"btn_thongtindongho",Toast.LENGTH_SHORT).show();
            //    Toast.makeText(activity_Danhsachdocso_chitiet_khachhang.this,"btn_thaydongho",Toast.LENGTH_SHORT).show();
                String json_send = "{\"danhba\":\""+Danhba+"\","+
                        "\"so_st\":\""+So_St+"\","+
                        "\"hoten\":\""+Hoten+"\","+
                        "\"diachi\":\""+Diachi+"\","+
                        "\"sync\":\""+String.valueOf(DongBo)+"\","+
                        "\"nam\":\""+Nam+"\","+
                        "\"ky\":\""+Ky+"\","+
                        "\"dot\":\""+Dot+"\","+
                        "\"somay\":\""+SoMay+"\"}";
                Intent intent = new Intent(activity_Danhsachdocso_chitiet_khachhang.this,activity_Thongtindongho.class);
                intent.putExtra("THONGTINDONGHO",json_send);
                startActivity(intent);
                Change_Screen = true;
            }
        });

        // ACTION: CHUP HINH
        btn_chuphinh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             //   Toast.makeText(activity_Danhsachdocso_chitiet_khachhang.this,"btn_thongtindongho",Toast.LENGTH_SHORT).show();
             //   Toast.makeText(activity_Danhsachdocso_chitiet_khachhang.this,"btn_thaydongho",Toast.LENGTH_SHORT).show();
                Enable_Ghiso = Enable_GhiSo(Danhba,Date_Denngay,TinhTrang_DHMoi);
                String json_send = "{\"danhba\":\""+Danhba+"\","+
                        "\"so_st\":\""+So_St+"\","+
                        "\"hoten\":\""+Hoten+"\","+
                        "\"diachi\":\""+Diachi+"\","+
                        "\"sync\":\""+String.valueOf(DongBo)+"\","+
                        "\"nam\":\""+Nam+"\","+
                        "\"ky\":\""+Ky+"\","+
                        "\"dot\":\""+Dot+"\","+
                        "\"enable_ghiso\":\""+String.valueOf(Enable_Ghiso)+"\","+
                        "\"somay\":\""+SoMay+"\"}";
                Intent intent = new Intent(activity_Danhsachdocso_chitiet_khachhang.this,activity_Chuphinh.class);
                intent.putExtra("CHUPHINH",json_send);
                startActivity(intent);
                Change_Screen = true;
            }
        });
        // ACTION: HOADON
        btn_hoadon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             //   Toast.makeText(activity_Danhsachdocso_chitiet_khachhang.this,"btn_hoadon",Toast.LENGTH_SHORT).show();
                String json_send = "{\"danhba\":\""+Danhba+"\","+
                        "\"so_st\":\""+So_St+"\","+
                        "\"hoten\":\""+Hoten+"\","+
                        "\"diachi\":\""+Diachi+"\","+
                        "\"sync\":\""+String.valueOf(DongBo)+"\","+
                        "\"nam\":\""+Nam+"\","+
                        "\"ky\":\""+Ky+"\","+
                        "\"dot\":\""+Dot+"\","+
                        "\"somay\":\""+SoMay+"\"}";
                Intent intent = new Intent(activity_Danhsachdocso_chitiet_khachhang.this,activity_Hoadon.class);
                intent.putExtra("HOADON",json_send);
                startActivity(intent);
                Change_Screen = true;
            }
        });
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Status_Action_Checkbox = true;
                Display_DocSoChiTiet_KhachHang_Next();
            }
        });
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Status_Action_Checkbox = true;
                Display_DocSoChiTiet_KhachHang_Back();
            }
        });
        btn_xacnhan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    Dialog_App dialog_app = new Dialog_App(activity_Danhsachdocso_chitiet_khachhang.this);
                    Enable_Ghiso = Enable_GhiSo(Danhba,Date_Denngay,TinhTrang_DHMoi);
                    if(Enable_Ghiso == NORMAL_GHISO  ) {

                            sqlite_DanhSachKhackHang_DocSo sqlite_danhSachKhackHang_docSo = new sqlite_DanhSachKhackHang_DocSo(activity_Danhsachdocso_chitiet_khachhang.this,Nam,Ky,Dot,SoMay);
                            if (sqlite_danhSachKhackHang_docSo.getStatus_Table_DanhSachKhachHang_Exists()) {
                                String tinhtrang = spn_tinhtrang.getSelectedItem().toString().trim();
                                String csm = edt_csm.getText().toString().trim();
                                String tieuthu = tv_tieuthu.getText().toString().trim();
                                String code = get_Codetinhtrang(tinhtrang,Code_old);
                                sqlite_danhSachKhackHang_docSo.Update_CSM_TieuThu_DanhSachDocSo_KhachHang(Danhba,csm,tieuthu);
                                sqlite_danhSachKhackHang_docSo.Update_Tinhtrang_Code_DanhSachDocSo_KhachHang(Danhba,tinhtrang,code);
                                XacNhan_Ghiso();
                            }

                    }else  if(Enable_Ghiso == DISABLE_GHISO_CHUADENGIO) {
                        String notification = "Chưa đến thời gian ghi số." + "\r\n" + "Các thao tác ghi số sẽ không được lưu lại" + "\r\n" + "Xin cám ơn !!!";
                        dialog_app.Dialog_Notification(notification);
                    }else if(Enable_Ghiso == DISABLE_GHISO_QUAGIO) {
                        String notification = "Quá thời gian ghi số và điều chỉnh." + "\r\n" + "Các thao tác ghi số sẽ không được lưu lại" + "\r\n" + "Xin cám ơn !!!";
                        dialog_app.Dialog_Notification(notification);
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

            }
        });


        edt_csm.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v,int actionId,KeyEvent event) {
               // Log.e("click","1");
                if (actionId == EditorInfo.IME_ACTION_DONE)
                {
                        Tinh_TieuThu();
                }

                return false;
            }
        });

        edt_ghichu2.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v,int actionId,KeyEvent event) {
               // Log.e("click","1");
                if (actionId == EditorInfo.IME_ACTION_DONE)
                {
                    String nam = String.valueOf(Nam);
                    String ky = String.valueOf(Ky);
                    String dot = String.valueOf(Dot);
                    String somay = String.valueOf(SoMay);
                    sqlite_DanhSachKhackHang_DocSo sqlite_danhSachKhackHang_docSo = new sqlite_DanhSachKhackHang_DocSo(activity_Danhsachdocso_chitiet_khachhang.this,nam,ky,dot,somay);
                    if(sqlite_danhSachKhackHang_docSo.getStatus_Table_DanhSachKhachHang_Exists()){
                        String ghichu1 = edt_ghichu1.getText().toString().trim();
                        String ghichu2 = edt_ghichu2.getText().toString().trim();
                        sqlite_danhSachKhackHang_docSo.Update_GhiChu2_DanhSachDocSo_KhachHangs_CongDung(Danhba,ghichu2);
                        sent_ThongSo_Asynctask sent_thongSo_asynctask =new sent_ThongSo_Asynctask(
                                activity_Danhsachdocso_chitiet_khachhang.this,
                                nam,ky,dot,Danhba,somay,"ghichu",ghichu1,ghichu2);
                        sent_thongSo_asynctask.execute();
                    }
                }
                return false;
            }
        });
        edt_ghichu1.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v,int actionId,KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE)
                {
                    String nam = String.valueOf(Nam);
                    String ky = String.valueOf(Ky);
                    String dot = String.valueOf(Dot);
                    String somay = String.valueOf(SoMay);
                    sqlite_DanhSachKhackHang_DocSo sqlite_danhSachKhackHang_docSo = new sqlite_DanhSachKhackHang_DocSo(activity_Danhsachdocso_chitiet_khachhang.this,nam,ky,dot,somay);
                    if(sqlite_danhSachKhackHang_docSo.getStatus_Table_DanhSachKhachHang_Exists()){
                        String ghichu1 = edt_ghichu1.getText().toString().trim();
                        String ghichu2 = edt_ghichu2.getText().toString().trim();
                        sqlite_danhSachKhackHang_docSo.Update_GhiChu1_DanhSachDocSo_KhachHangs_CongDung(Danhba,ghichu1);
                        sent_ThongSo_Asynctask sent_thongSo_asynctask =new sent_ThongSo_Asynctask(
                                activity_Danhsachdocso_chitiet_khachhang.this,
                                nam,ky,dot,Danhba,somay,"ghichu",ghichu1,ghichu2);
                        sent_thongSo_asynctask.execute();
                    }
                }
                return false;
            }
        });
        edt_dienthoai1.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v,int actionId,KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE)
                {
                    String nam = String.valueOf(Nam);
                    String ky = String.valueOf(Ky);
                    String dot = String.valueOf(Dot);
                    String somay = String.valueOf(SoMay);
                    sqlite_DanhSachKhackHang_DocSo sqlite_danhSachKhackHang_docSo = new sqlite_DanhSachKhackHang_DocSo(activity_Danhsachdocso_chitiet_khachhang.this,nam,ky,dot,somay);
                    if(sqlite_danhSachKhackHang_docSo.getStatus_Table_DanhSachKhachHang_Exists()){
                        String dienthoai1 = edt_dienthoai1.getText().toString().trim();
                        String dienthoai2 = edt_dienthoai2.getText().toString().trim();
                        sqlite_danhSachKhackHang_docSo.Update_Dienthoai1_DanhSachDocSo_KhachHang(Danhba,dienthoai1);
                        sent_ThongSo_Asynctask sent_thongSo_asynctask =new sent_ThongSo_Asynctask(
                                activity_Danhsachdocso_chitiet_khachhang.this,
                                nam,ky,dot,Danhba,somay,"sodienthoai",dienthoai1,dienthoai2);
                        sent_thongSo_asynctask.execute();
                    }
                }
                return false;
            }
        });
        edt_dienthoai2.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v,int actionId,KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE)
                {
                    String nam = String.valueOf(Nam);
                    String ky = String.valueOf(Ky);
                    String dot = String.valueOf(Dot);
                    String somay = String.valueOf(SoMay);
                    sqlite_DanhSachKhackHang_DocSo sqlite_danhSachKhackHang_docSo = new sqlite_DanhSachKhackHang_DocSo(activity_Danhsachdocso_chitiet_khachhang.this,nam,ky,dot,somay);
                    if(sqlite_danhSachKhackHang_docSo.getStatus_Table_DanhSachKhachHang_Exists()){
                        String dienthoai1 = edt_dienthoai1.getText().toString().trim();
                        String dienthoai2 = edt_dienthoai2.getText().toString().trim();
                        sqlite_danhSachKhackHang_docSo.Update_Dienthoai2_DanhSachDocSo_KhachHang(Danhba,dienthoai2);
                        sent_ThongSo_Asynctask sent_thongSo_asynctask =new sent_ThongSo_Asynctask(
                                activity_Danhsachdocso_chitiet_khachhang.this,
                                nam,ky,dot,Danhba,somay,"sodienthoai",dienthoai1,dienthoai2);
                        sent_thongSo_asynctask.execute();
                    }
                }
                return false;
            }
        });

        //
        edt_diachinew.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v,int actionId,KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE)
                {
                    String nam = String.valueOf(Nam);
                    String ky = String.valueOf(Ky);
                    String dot = String.valueOf(Dot);
                    String somay = String.valueOf(SoMay);
                    sqlite_DanhSachKhackHang_DocSo sqlite_danhSachKhackHang_docSo = new sqlite_DanhSachKhackHang_DocSo(activity_Danhsachdocso_chitiet_khachhang.this,nam,ky,dot,somay);
                    if(sqlite_danhSachKhackHang_docSo.getStatus_Table_DanhSachKhachHang_Exists()){
                        String sonhamoi = edt_diachinew.getText().toString().trim();
                        sqlite_danhSachKhackHang_docSo.Update_SoNhaMoi_DanhSachDocSo_KhachHang(Danhba,sonhamoi);
                        sent_ThongSo_Asynctask sent_thongSo_asynctask =new sent_ThongSo_Asynctask(
                                activity_Danhsachdocso_chitiet_khachhang.this,
                                nam,ky,dot,Danhba,somay,"diachimoi",sonhamoi,"");
                        sent_thongSo_asynctask.execute();
                    }
                }
                return false;
            }
        });
        cb_diachinew.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
                if(cb_diachinew.isChecked()){
                    edt_diachinew.setEnabled(false);
                    String nam = String.valueOf(Nam);
                    String ky = String.valueOf(Ky);
                    String dot = String.valueOf(Dot);
                    String somay = String.valueOf(SoMay);
                    sqlite_DanhSachKhackHang_DocSo sqlite_danhSachKhackHang_docSo = new sqlite_DanhSachKhackHang_DocSo(activity_Danhsachdocso_chitiet_khachhang.this,nam,ky,dot,somay);
                    if(sqlite_danhSachKhackHang_docSo.getStatus_Table_DanhSachKhachHang_Exists()){
                        String sonhamoi = edt_diachinew.getText().toString().trim();
                        sqlite_danhSachKhackHang_docSo.Update_SoNhaMoi_DanhSachDocSo_KhachHang(Danhba,sonhamoi);
                    }
                }else {
                    edt_diachinew.setEnabled(true);
                }
            }
        });
        cb_doituongcongdung.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
                if(cb_doituongcongdung.isChecked()){
                    edt_congdung.setEnabled(false);
                    String nam = String.valueOf(Nam);
                    String ky = String.valueOf(Ky);
                    String dot = String.valueOf(Dot);
                    String somay = String.valueOf(SoMay);
                    sqlite_DanhSachKhackHang_DocSo sqlite_danhSachKhackHang_docSo = new sqlite_DanhSachKhackHang_DocSo(activity_Danhsachdocso_chitiet_khachhang.this,nam,ky,dot,somay);
                    if(sqlite_danhSachKhackHang_docSo.getStatus_Table_DanhSachKhachHang_Exists()){

                        String congdung_save = edt_congdung.getText().toString().trim();
                        sqlite_danhSachKhackHang_docSo.Update_CongDung_DanhSachDocSo_KhachHang(Danhba,congdung_save);
                    }
                }else {
                    edt_congdung.setEnabled(true);
                }
            }
        });
        cb_dongho.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
                if(cb_dongho.isChecked()){
                    spn_tinhtrangchi.setEnabled(false);
                    spn_vitridh.setEnabled(false);

                    String nam = String.valueOf(Nam);
                    String ky = String.valueOf(Ky);
                    String dot = String.valueOf(Dot);
                    String somay = String.valueOf(SoMay);
                    sqlite_DanhSachKhackHang_DocSo sqlite_danhSachKhackHang_docSo = new sqlite_DanhSachKhackHang_DocSo(activity_Danhsachdocso_chitiet_khachhang.this,nam,ky,dot,somay);
                    if(sqlite_danhSachKhackHang_docSo.getStatus_Table_DanhSachKhachHang_Exists()){
                        int tinhtrangchi = spn_tinhtrangchi.getSelectedItemPosition();
                        int vitridh = spn_vitridh.getSelectedItemPosition();
                        String vitridhn = data_ViTrisDh_Gui[vitridh];

                        sqlite_danhSachKhackHang_docSo.Update_Chi_VTDH_DanhSachDocSo_KhachHang(Danhba,String.valueOf(tinhtrangchi),vitridhn);
                    }

                }else {
                    spn_tinhtrangchi.setEnabled(true);
                    /*
                    String tinhtrang = spn_tinhtrang.getSelectedItem().toString();
                    if(tinhtrang.trim().equals("Gắn mới") || tinhtrang.trim().equals("TLDB")){
                        spn_vitridh.setEnabled(true);
                    }else {
                        spn_vitridh.setEnabled(false);
                    }

                     */
                    spn_vitridh.setEnabled(true);
                }
            }
        });
        cb_dienthoai.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
                if(cb_dienthoai.isChecked()){
                    edt_dienthoai1.setEnabled(false);
                    edt_dienthoai2.setEnabled(false);

                    String nam = String.valueOf(Nam);
                    String ky = String.valueOf(Ky);
                    String dot = String.valueOf(Dot);
                    String somay = String.valueOf(SoMay);
                    sqlite_DanhSachKhackHang_DocSo sqlite_danhSachKhackHang_docSo = new sqlite_DanhSachKhackHang_DocSo(activity_Danhsachdocso_chitiet_khachhang.this,nam,ky,dot,somay);
                    if(sqlite_danhSachKhackHang_docSo.getStatus_Table_DanhSachKhachHang_Exists()){
                        String dienthoai1 = edt_dienthoai1.getText().toString().trim();
                        String dienthoai2 =  edt_dienthoai2.getText().toString().trim();
                        sqlite_danhSachKhackHang_docSo.Update_Dienthoai_DanhSachDocSo_KhachHang(Danhba,dienthoai1,dienthoai2);
                    }

                }else {
                    edt_dienthoai1.setEnabled(true);
                    edt_dienthoai2.setEnabled(true);
                }
            }
        });



        btn_in.setEnabled(false);
        btn_in.setBackgroundColor(getResources().getColor(R.color.btn_Red));
        btn_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             //   Toast.makeText(activity_Danhsachdocso_chitiet_khachhang.this,"btn_in",Toast.LENGTH_SHORT).show();
                try {
                    // Print_HoaDonCnCl();
                  //   Print_HoaDonCnCl_Woosim();

                    String tinhtrang = spn_tinhtrang.getSelectedItem().toString().trim();
                    if (Type_BLT_Run.equals("WOOSIM")) {

                        if (tinhtrang.trim().equals("Đóng cửa") ||
                                tinhtrang.trim().equals("Thay Đ.cửa") ||
                                tinhtrang.trim().equals("Chất đồ") ||
                                tinhtrang.trim().equals("Kẹt khóa") ||
                                tinhtrang.trim().equals("Ngập nước") ||
                                tinhtrang.trim().equals("Lấp")) {
                            Print_HoaDonCnCl_Woosim_Trongai(tinhtrang);
                        } else {
                            Print_HoaDonCnCl_Woosim();
                        }
                    } else if (Type_BLT_Run.equals("STAR") ) {

                        int mModelIndex = 12;
                        String mMacAddress = Add_BLT.toString().trim();
                        String mPortName = "BT:" + mMacAddress;
                        String mPortSettings = "Portable";
                        String mModelName = "STAR L200-00001";
                        boolean mCashDrawerOpenActiveHigh = true;
                        int mPaperSize = 408;
                        PrinterSettings settings = new PrinterSettings(mModelIndex, mPortName, mPortSettings, mMacAddress, mModelName, mCashDrawerOpenActiveHigh, mPaperSize);
                        StarIoExt.Emulation emulation = ModelCapability.getEmulation(settings.getModelIndex());

                        if (tinhtrang.trim().equals("Đóng cửa") ||
                                tinhtrang.trim().equals("Thay Đ.cửa") ||
                                tinhtrang.trim().equals("Chất đồ") ||
                                tinhtrang.trim().equals("Kẹt khóa") ||
                                tinhtrang.trim().equals("Ngập nước") ||
                                tinhtrang.trim().equals("Lấp")) {
                            //   Log.e("Star","Tro Ngai");
                            Print_HoaDonCnCl_Star_TroNgai(emulation, tinhtrang);
                        } else {
                            //   Log.e("Star","Binh Thuong");
                            Print_HoaDonCnCl_Star(emulation);
                        }

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                /*
                 try {
                   // Print_HoaDonCnCl();
                   // Print_HoaDonCnCl_Woosim();
                    String nam = String.valueOf(Nam);
                    String ky = String.valueOf(Ky);
                    String dot = String.valueOf(Dot);
                    String somay = String.valueOf(SoMay);
                    sqlite_DanhSachKhackHang_DocSo sqlite_danhSachKhackHang_docSo = new sqlite_DanhSachKhackHang_DocSo(activity_Danhsachdocso_chitiet_khachhang.this,nam,ky,dot,somay);
                    if (sqlite_danhSachKhackHang_docSo.getStatus_Table_DanhSachKhachHang_Exists()) {
                        String sync_server = sqlite_danhSachKhackHang_docSo.Get_Sync_DanhSachDocSo_KhachHang(Danhba);
                        String tieuthu = tv_tieuthu.getText().toString().trim();
                        String tieuthu_check = sqlite_danhSachKhackHang_docSo.Get_Tieuthu_DanhSachDocSo_KhachHang(Danhba);
                        if(sync_server.equals("0") ) {
                            if(tieuthu_check.equals( tieuthu)) {
                                String tinhtrang = spn_tinhtrang.getSelectedItem().toString().trim();
                                if (Type_BLT_Run.equals("WOOSIM")) {

                                    if (tinhtrang.trim().equals("Đóng cửa") ||
                                            tinhtrang.trim().equals("Thay Đ.cửa") ||
                                            tinhtrang.trim().equals("Chất đồ") ||
                                            tinhtrang.trim().equals("Kẹt khóa") ||
                                            tinhtrang.trim().equals("Ngập nước") ||
                                            tinhtrang.trim().equals("Lấp")) {
                                        Print_HoaDonCnCl_Woosim_Trongai(tinhtrang);
                                    } else {
                                        Print_HoaDonCnCl_Woosim();
                                    }
                                } else if (Type_BLT_Run.equals("STAR")) {

                                    int mModelIndex = 12;
                                    String mMacAddress = Add_BLT.toString().trim();
                                    String mPortName = "BT:" + mMacAddress;
                                    String mPortSettings = "Portable";
                                    String mModelName = "STAR L200-00001";
                                    boolean mCashDrawerOpenActiveHigh = true;
                                    int mPaperSize = 384;
                                    PrinterSettings settings = new PrinterSettings(mModelIndex, mPortName, mPortSettings, mMacAddress, mModelName, mCashDrawerOpenActiveHigh, mPaperSize);
                                    StarIoExt.Emulation emulation = ModelCapability.getEmulation(settings.getModelIndex());

                                    if (tinhtrang.trim().equals("Đóng cửa") ||
                                            tinhtrang.trim().equals("Thay Đ.cửa") ||
                                            tinhtrang.trim().equals("Chất đồ") ||
                                            tinhtrang.trim().equals("Kẹt khóa") ||
                                            tinhtrang.trim().equals("Ngập nước") ||
                                            tinhtrang.trim().equals("Lấp")) {
                                        //   Log.e("Star","Tro Ngai");
                                        Print_HoaDonCnCl_Star_TroNgai(emulation, tinhtrang);
                                    } else {
                                        //   Log.e("Star","Binh Thuong");
                                        Print_HoaDonCnCl_Star(emulation);
                                    }

                                }
                            }else {
                                Dialog_App dialog_app = new Dialog_App(activity_Danhsachdocso_chitiet_khachhang.this);
                                String notification = "Tiêu thụ đang lưu trong máy: "+tieuthu_check+"\r\n"+"và tiêu thụ đang hiển thị: "+tieuthu+", khác nhau." + "\r\n" + "Vui lòng gửi dữ liệu về server." + "\r\n" +"Sau đó mới in hóa đơn." + "\r\n" + "Xin cám ơn";
                                dialog_app.Dialog_Notification(notification);
                            }
                        }else {
                            Dialog_App dialog_app = new Dialog_App(activity_Danhsachdocso_chitiet_khachhang.this);
                            String notification = "Vui lòng gửi dữ liệu về server." + "\r\n" + "Sau đó mới in hóa đơn." + "\r\n" + "Xin cám ơn";
                            dialog_app.Dialog_Notification(notification);
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                 */
            }
        });

        Init_EditText();

        Get_List_Danhba();
        Get_List_SoThan();
     //   auto_sothan.setEnabled(false);
        auto_sothan.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent,View view,int position,long id) {
                String sothan = parent.getItemAtPosition(position).toString();
                if(!sothan.equals(SoThan_Current)) {
                    Display_DocSoChiTiet_KhachHang_Timkiem_SoThan(sothan);
                }else {
                    SoThan_Current = sothan;
                }
            }
        });

        auto_danhba.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent,View view,int position,long id) {
                String danhba = parent.getItemAtPosition(position).toString();
                if(!danhba.equals(Danhba_current)) {
                    Display_DocSoChiTiet_KhachHang_Timkiem_DanhBa(danhba);
                }else {
                    Danhba_current = danhba;
                }
            }
        });

    }
    private  void Get_List_SoThan(){

        String nam = String.valueOf(Nam);
        String ky = String.valueOf(Ky);
        String dot = String.valueOf(Dot);
        String somay = String.valueOf(SoMay);
        sqlite_DanhSachKhackHang_DocSo sqlite_danhSachKhackHang_docSo = new sqlite_DanhSachKhackHang_DocSo(activity_Danhsachdocso_chitiet_khachhang.this,nam,ky,dot,somay);
        if(sqlite_danhSachKhackHang_docSo.getStatus_Table_DanhSachKhachHang_Exists()){
            List<String> list_sothan  =  sqlite_danhSachKhackHang_docSo.Get_DanhSachDocSo_Chitiet_KhachHang_listsothan();
            ArrayAdapter<String> arrayAdapter_sothan = new ArrayAdapter<String>(activity_Danhsachdocso_chitiet_khachhang.this,android.R.layout.simple_list_item_1,list_sothan);
            auto_sothan.setAdapter(arrayAdapter_sothan);
        }
    }
    private  String TinhTieuThu_TH2(String csm,String tungay, String denngay, String csgan,String ngaythay_dh){
        String tieuthu="0";
        try{
            if(!ngaythay_dh.isEmpty()) {
                SimpleDateFormat format_datetime_chukyds = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                final Date date_tungay = format_datetime_chukyds.parse(tungay);
                final Date date_denngay = format_datetime_chukyds.parse(denngay);
                SimpleDateFormat format_datetime_thay = new SimpleDateFormat("dd/MM/yyyy");
                Date date_thay = format_datetime_thay.parse(ngaythay_dh);

                long l_tungay = date_tungay.getTime();
                long l_denngay = date_denngay.getTime();
                long l_thay = date_thay.getTime();
                long diff_chuky_ds = l_denngay - l_tungay;
                long diff_chuky_thay = l_denngay - l_thay;

                int songay_docso = (int) Math.round(diff_chuky_ds / 1000 / 60 / 60 / 24);
                int songay_thay = (int) Math.round(diff_chuky_thay / 1000 / 60 / 60 / 24);

                double i_csm = Double.parseDouble(csm);
                double i_csgan = Double.parseDouble(csgan);
                int i_tieuthu = (int) Math.round(((i_csm - i_csgan) * songay_docso) / songay_thay);
                tieuthu = String.valueOf(i_tieuthu);

            }
        }catch (Exception e){

        }
        return  tieuthu;
    }

    private  void Get_List_Danhba(){

        String nam = String.valueOf(Nam);
        String ky = String.valueOf(Ky);
        String dot = String.valueOf(Dot);
        String somay = String.valueOf(SoMay);
        sqlite_DanhSachKhackHang_DocSo sqlite_danhSachKhackHang_docSo = new sqlite_DanhSachKhackHang_DocSo(activity_Danhsachdocso_chitiet_khachhang.this,nam,ky,dot,somay);
        if(sqlite_danhSachKhackHang_docSo.getStatus_Table_DanhSachKhachHang_Exists()){
            List<String> list_danhba  =  sqlite_danhSachKhackHang_docSo.Get_DanhSachDocSo_Chitiet_KhachHang_listdanhba();
            ArrayAdapter<String> arrayAdapter_danhba = new ArrayAdapter<String>(activity_Danhsachdocso_chitiet_khachhang.this,android.R.layout.simple_list_item_1,list_danhba);
            auto_danhba.setAdapter(arrayAdapter_danhba);
        }
    }
    /*
    List<SendPicture_AsyncTask> list_myAsyncTask ;
    private void GuiAnh(){
        try {
            //  SendPicture_AsyncTask myAsyncTask = new SendPicture_AsyncTask(activity_Chuphinh.this,Danhba,Nam,Ky,Dot,SoMay);
            //  myAsyncTask.execute();
            list_myAsyncTask = new ArrayList<SendPicture_AsyncTask>();
            sqlite_Hinhanh sqlite_hinhanh = new sqlite_Hinhanh(activity_Danhsachdocso_chitiet_khachhang.this,Nam,Ky,Dot,SoMay);
            if(sqlite_hinhanh.getStatus_Table_ListHinhAnh_Exists()) {
                data_Hinhanh data_hinhanh_khachhang = sqlite_hinhanh.Get_Table_ListHinhAnh(Danhba);
                List<data_Hinhanh.data_anh> list_anh = data_hinhanh_khachhang.getHinhanhs();
                int count = 0;
                for (data_Hinhanh.data_anh data_anh : list_anh) {
                    count++;
                    String sync_server = data_anh.getSync_anh();
                    if (!sync_server.equals("1")) {
                        String id_anh = String.valueOf(data_anh.getId());
                        String link_anh = data_anh.getAnh();
                        if(count == list_anh.size()) {
                            SendPicture_AsyncTask myAsyncTask = new SendPicture_AsyncTask(activity_Danhsachdocso_chitiet_khachhang.this,Danhba,Nam,Ky,Dot,SoMay,id_anh,link_anh,true);
                            list_myAsyncTask.add(myAsyncTask);
                        }else {
                            SendPicture_AsyncTask myAsyncTask = new SendPicture_AsyncTask(activity_Danhsachdocso_chitiet_khachhang.this,Danhba,Nam,Ky,Dot,SoMay,id_anh,link_anh,false);
                            list_myAsyncTask.add(myAsyncTask);
                        }
                    }
                }
            }

            for(SendPicture_AsyncTask myAsyncTask:list_myAsyncTask){
                myAsyncTask.execute();
            }

            //  GuiAnh();
        }catch (Exception e){}
    }


     */
    private void Init_EditText(){
        edt_diachinew.setImeOptions(EditorInfo.IME_ACTION_DONE);
        edt_dienthoai1.setImeOptions(EditorInfo.IME_ACTION_DONE);
        edt_dienthoai2.setImeOptions(EditorInfo.IME_ACTION_DONE);
        edt_csm.setImeOptions(EditorInfo.IME_ACTION_DONE);
        edt_ghichu1.setImeOptions(EditorInfo.IME_ACTION_DONE);
        edt_ghichu2.setImeOptions(EditorInfo.IME_ACTION_DONE);
        edt_congdung.setImeOptions(EditorInfo.IME_ACTION_DONE);
        auto_danhba.setImeOptions(EditorInfo.IME_ACTION_DONE);
        auto_sothan.setImeOptions(EditorInfo.IME_ACTION_DONE);
    }


    AlertDialog dialog_notification_alarm_luu ;
    private void Dialog_Notification_alarm_luu(final String text){
        AlertDialog.Builder alert_update_version =new AlertDialog.Builder(activity_Danhsachdocso_chitiet_khachhang.this);
        alert_update_version.setMessage(text);
        // dong y
        alert_update_version.setPositiveButton("Bỏ qua và lưu",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog,int which) {
                try {
                    XacNhan_Ghiso();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                dialog_notification_alarm_luu.dismiss();
            }
        });
        // bo qua
        alert_update_version.setNegativeButton("Xem lại",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog,int which) {
                dialog_notification_alarm_luu.dismiss();
            }
        });
        dialog_notification_alarm_luu = alert_update_version.create();
        dialog_notification_alarm_luu.show();
    }



private String get_Codetinhtrang(String tinhtrang, String re_code){
    tinhtrang =tinhtrang.trim();
    re_code  = re_code.trim();
    if(tinhtrang.trim().equals("B.Thường") || tinhtrang.trim().equals("Chủ ghi") || tinhtrang.trim().equals("Chủ báo"))
    {
        if(  re_code.trim().equals("60") ||
                re_code.trim().equals("67") ||
                re_code.trim().equals("63")||
                re_code.trim().equals("F") ||
                re_code.trim().equals("K") ||
                re_code.trim().equals("N")
        )
        {
            return   "5";
        }else {
            return   "4";
        }

    }
    else if(tinhtrang.trim().equals("Đóng cửa"))
    {
        return   "F";
    }
    else if(tinhtrang.trim().equals("Gắn mới"))
    {
        return   "M";
    }
    else if(tinhtrang.trim().equals("TLDB"))
    {
        return   "5";
    }
    else if(tinhtrang.trim().equals("Đ/C K.ở"))
    {
        return   "67";
    }
    else if(tinhtrang.trim().equals("N.C.N") || tinhtrang.trim().equals("Chất đồ") || tinhtrang.trim().equals("Kính mờ") || tinhtrang.trim().equals("Kẹt khóa" ) || tinhtrang.trim().equals("Lấp"))
    {
        return   "60";
    }
    else if(tinhtrang.trim().equals("Mất"))
    {
        return   "63";
    }
    else if(tinhtrang.trim().equals("Giải tỏa") || tinhtrang.trim().equals("Đóng Nước") || tinhtrang.trim().equals("Bấm Chì"))
    {
        return   "N";
    }
    else if(tinhtrang.trim().equals("Thay Đ.cửa"))
    {
        return   "80";
    }
    else if(tinhtrang.trim().equals("Thay 82"))
    {
        return   "82";
    }
    else if(tinhtrang.trim().equals("Thay 81"))
    {
        return   "81";
    }
    else if(tinhtrang.trim().equals("Thay Đ.Kỳ"))
    {
        return   "82";
    }
    else if(tinhtrang.trim().equals("Retour4"))
    {
        if(  re_code.trim().equals("60") ||
                re_code.trim().equals("67") ||
                re_code.trim().equals("F") ||
                re_code.trim().equals("N")
        )
        {
            return   "5";
        }else {
            return   "4";
        }
    }
    else if(tinhtrang.trim().equals("Retour5"))
    {
        if(  re_code.trim().equals("60") ||
                re_code.trim().equals("67") ||
                re_code.trim().equals("F") ||
                re_code.trim().equals("N")
        )
        {
            return   "5";
        }else {
            return   "4";
        }
    }
    else if(tinhtrang.trim().equals("Lệch số")||tinhtrang.trim().equals("Kẹt số")||tinhtrang.trim().equals("Chạy ngược")||tinhtrang.trim().equals("Ngập nước"))
    {
        return   "60";
    }
    else if(tinhtrang.trim().equals("Thay B.Thường"))
    {
        return   "81";
    }
    //Thay Bất Thường
    return "";
}
    private void Mappping_Layout() {
       // tv_danhba =(TextView) findViewById(R.id.textview_danhba_dongdanhsachdocso_chitiet_khachhang);
        //sv_danhba =(SearchView) findViewById(R.id.searchview_danhba_dongdanhsachdocso_chitiet_khachhang);
        tv_tieude = (TextView) findViewById(R.id.textview_tieude_dongdanhsachdocso_chitiet);
        auto_danhba =(AutoCompleteTextView) findViewById(R.id.autoCompleteTextView_danhba_dongdanhsachdocso_chitiet_khachhang);
       // sv_sothan =(SearchView) findViewById(R.id.searchview_sothan_dongdanhsachdocso_chitiet_khachhang);
        auto_sothan =(AutoCompleteTextView) findViewById(R.id.autoCompleteTextView_sothan_dongdanhsachdocso_chitiet_khachhang);
        tv_mlt = ( TextView) findViewById(R.id.textview_mlt_dongdanhsachdocso_chitiet_khachhang);
        imv_dongbo =( ImageView) findViewById(R.id.imageView_dongbo_dongdanhsachdocso_chitiet_khachhang);
        tv_hoten = ( TextView) findViewById(R.id.textview_hoten_dongdanhsachdocso_chitiet_khachhang);
        tv_diachi = ( TextView) findViewById(R.id.textview_diachi_danhsachdocso_chitiet_khachhang);

        edt_diachinew = (EditText) findViewById(R.id.edittext_dianhinew_dongdanhsachdocso_chitiet_khachhang);
        edt_congdung = (EditText) findViewById(R.id.edittext_congdung_danhsachdocso_chitiet_khachhang);
        tv_hieuconamdh = (TextView) findViewById(R.id.textview_hieuconamdh_danhsachdocso_chitiet_khachhang);
        tv_giabieu = (TextView) findViewById(R.id.textview_giabieu_dongdanhsachdocso_chitiet_khachhang);
        tv_dinhmuc = (TextView) findViewById(R.id.textview_dinhmuc_dongdanhsachdocso_chitiet_khachhang);
        edt_dienthoai1 = (EditText) findViewById(R.id.edittext_dienthoai1_dongdanhsachdocso_chitiet_khachhang);
        edt_dienthoai2 = (EditText) findViewById(R.id.edittext_dienthoai2_dongdanhsachdocso_chitiet_khachhang);
        edt_csm = (EditText) findViewById(R.id.edittext_csm_dongdanhsachdocso_chitiet_khachhang);
        tv_tieuthu = (TextView) findViewById(R.id.textview_tieuthu_dongdanhsachdocso_chitiet_khachhang);

        edt_ghichu1 = (EditText) findViewById(R.id.edittext_ghichu1_dongdanhsachdocso_chitiet_khachhang);
        edt_ghichu2 = (EditText) findViewById(R.id.edittext_ghichu2_dongdanhsachdocso_chitiet_khachhang);

        tv_code =(TextView) findViewById(R.id.textview_code_dongdanhsachdocso_chitiet_khachhang);
        tv_csc =(TextView) findViewById(R.id.textview_csc_dongdanhsachdocso_chitiet_khachhang);
        tv_ttcu =(TextView) findViewById(R.id.textview_ttcu_dongdanhsachdocso_chitiet_khachhang);
        tv_binhquan =(TextView) findViewById(R.id.textview_binhquan_dongdanhsachdocso_chitiet_khachhang);
        btn_xacnhan = ( Button ) findViewById(R.id.button_back_xacnhan_danhsachdocso);
        btn_tieuthu =(Button) findViewById(R.id.button_tieuthu_danhsachdocso);
        btn_thaydongho =(Button) findViewById(R.id.button_thaydongho_danhsachdocso);
        btn_thongtindongho =(Button) findViewById(R.id.button_thongtindongho_danhsachdocso);
        btn_chuphinh =(Button) findViewById(R.id.button_chuphinh_danhsachdocso);
        btn_next =(Button) findViewById(R.id.button_next_dongdanhsachdocso_chitiet_khachhang);
        btn_back =(Button) findViewById(R.id.button_back_dongdanhsachdocso_chitiet_khachhang);

        spn_tinhtrangchi = (Spinner) findViewById(R.id.spinner_tinhtrangchi_danhsachdocso_chitiet_khachhang);
        spn_vitridh = (Spinner) findViewById(R.id.spinner_vitridongho_danhsachdocso_chitiet_khachhang);
        spn_khachhang = (Spinner) findViewById(R.id.spinner_khachhang_danhsachdocso_chitiet_khachhang);
        spn_tinhtrang = (Spinner) findViewById(R.id.spinner_tinhtrang_danhsachdocso_chitiet_khachhang);
        cb_diachinew =( CheckBox) findViewById(R.id.checkbox_diachimoi_danhsachdocso_chitiet_khachhang);
        cb_doituongcongdung =( CheckBox) findViewById(R.id.checkbox_doituongcongdung_danhsachdocso_chitiet_khachhang);
        cb_dongho =( CheckBox) findViewById(R.id.checkbox_dongho_danhsachdocso_chitiet_khachhang);
        cb_dienthoai =( CheckBox) findViewById(R.id.checkbox_dienthoai_danhsachdocso_chitiet_khachhang);

        btn_hoadon = ( Button ) findViewById(R.id.button_hoadon_danhsachdocso_chitiet_khachhang);
        btn_in = ( Button ) findViewById(R.id.button_in_danhsachdocso_chitiet_khachhang);
        tv_machigoc = (TextView) findViewById(R.id.textview_machi_dongdanhsachdocso_chitiet_khachhang);


    }

    private void Tinh_TieuThu(){
        String nam = String.valueOf(Nam);
        String ky = String.valueOf(Ky);
        String dot = String.valueOf(Dot);
        String somay = String.valueOf(SoMay);
        sqlite_DanhSachKhackHang_DocSo sqlite_danhSachKhackHang_docSo = new sqlite_DanhSachKhackHang_DocSo(activity_Danhsachdocso_chitiet_khachhang.this,nam,ky,dot,somay);
        String binhquan = tv_binhquan.getText().toString().trim();
        String csm = edt_csm.getText().toString().trim();
        String csc = tv_csc.getText().toString().trim();
        Dialog_App dialog_app = new Dialog_App(activity_Danhsachdocso_chitiet_khachhang.this);
        String tinhtrang = spn_tinhtrang.getSelectedItem().toString().trim();
        String tieuthu = "0";
        if(tinhtrang.trim().equals("Đóng cửa") ||
                tinhtrang.trim().equals("Mất") ||
                tinhtrang.trim().equals("N.C.N") ||
                tinhtrang.trim().equals("Chất đồ") ||
                tinhtrang.trim().equals("Kính mờ") ||
                tinhtrang.trim().equals("Kẹt khóa") ||
                tinhtrang.trim().equals("Lệch số") ||
                tinhtrang.trim().equals("Kẹt số") ||
                tinhtrang.trim().equals("Chạy ngược") ||
                tinhtrang.trim().equals("Ngập nước") ||
                tinhtrang.trim().equals("Lấp") ){
            tieuthu = binhquan ;
            tv_tieuthu.setText(tieuthu);
            if(!csc.isEmpty()){
                int csm_i = Integer.parseInt(binhquan)+ Integer.parseInt(csc);
                csm = String.valueOf(csm_i);
            }else {
                csm =binhquan ;
            }
            edt_csm.setText(csm);
            // tinh tieu thu trung binh
        }else if( tinhtrang.trim().equals("TLDB")||
                tinhtrang.trim().equals("Gắn mới")){
            if(!csm.isEmpty()){
                tieuthu = csm;
                tv_tieuthu.setText(tieuthu);
            }

        }else if( tinhtrang.trim().equals("Đ/C K.ở")||
                tinhtrang.trim().equals("Đóng Nước")||
                tinhtrang.trim().equals("Bấm Chì")||
                tinhtrang.trim().equals("Giải tỏa")){
            if(!csc.isEmpty()){
                csm = csc;
                tieuthu = "0";
            }
            edt_csm.setText(csm);
            tv_tieuthu.setText(tieuthu);

        }else if(tinhtrang.trim().equals("Retour4")){
            if(!csm.isEmpty()){
                int tieuthu_tinh = 10000-Integer.parseInt(csc)+Integer.parseInt(csm);
                tieuthu = String.valueOf(tieuthu_tinh);
                tv_tieuthu.setText(tieuthu);
            }
        }
        else if(tinhtrang.trim().equals("Retour5")){
            if(!csm.isEmpty()){
                int tieuthu_tinh = 100000-Integer.parseInt(csc)+Integer.parseInt(csm);
                tieuthu = String.valueOf(tieuthu_tinh);
                tv_tieuthu.setText(tieuthu);
            }
        } else if(tinhtrang.trim().equals("Thay Đ.cửa")){
            csm = edt_csm.getText().toString().trim();
            if(!csm.isEmpty()){
                tieuthu = binhquan;
                tv_tieuthu.setText(tieuthu);
            }
        }//Thay Bất Thường
        else if(tinhtrang.trim().equals("Thay B.Thường")){
            csm = edt_csm.getText().toString().trim();
            List<String> tinhtrangthaydh = sqlite_danhSachKhackHang_docSo.Get_TinhTrangThayDH(Danhba);
            String csgo = tinhtrangthaydh.get(0);
            String csgan = tinhtrangthaydh.get(1);
            String lydo = tinhtrangthaydh.get(4).trim();
            String ngaythay_dh =tinhtrangthaydh.get(5).trim();
            if(!csm.isEmpty()&& !(csgan.trim().isEmpty())){
                String [] com_lydo = lydo.split("-");

                if(com_lydo.length>=2) {
                String tinhtrangthay = com_lydo[1];
                    if (tinhtrangthay.trim().equals("NCN") ||
                            tinhtrangthay.trim().equals("Lệch số") ||
                            tinhtrangthay.trim().equals("Kẹt số") ||
                            tinhtrangthay.trim().equals("MẤT") ||
                            tinhtrangthay.trim().equals("Chạy ngược") ||
                            (tinhtrangthay.trim().equals("KM") && (csgo.trim().equals("0"))) ||
                            (com_lydo[0].trim().equals("BTH")&& (csgo.trim().equals("0")))
                    ) {
                        tieuthu = TinhTieuThu_TH2(csm,Date_Tungay,Date_Denngay,csgan,ngaythay_dh);
                    }
                }else if(com_lydo[0].trim().equals("BTH")&& (csgo.trim().equals("0"))){
                    tieuthu = TinhTieuThu_TH2(csm,Date_Tungay,Date_Denngay,csgan,ngaythay_dh);
                }
                        tv_tieuthu.setText(tieuthu);
            }
        }
        else if( tinhtrang.trim().equals("Thay Đ.Kỳ")){
            csm = edt_csm.getText().toString().trim();
            List<String> tinhtrangthaydh = sqlite_danhSachKhackHang_docSo.Get_TinhTrangThayDH(Danhba);
            String csgo = tinhtrangthaydh.get(0);
            String csgan = tinhtrangthaydh.get(1);
            if(!csm.isEmpty()&& !(csgan.trim().isEmpty())){
                try {
                    if (!csgo.isEmpty() && !csgan.isEmpty()) {
                        int i_csgo = Integer.parseInt(csgo);
                        int i_csgan = Integer.parseInt(csgan);
                        if (i_csgo >= Integer.parseInt(csc)) {
                            int tieuthu_tinh = i_csgo - Integer.parseInt(csc) + Integer.parseInt(csm) - i_csgan;
                            tieuthu = String.valueOf(tieuthu_tinh);
                            try {
                                if (Integer.parseInt(tieuthu) < 0) {
                                    tieuthu = "0";
                                }
                            } catch (Exception e) {
                                tieuthu = "0";
                            }
                        } else {
                            tieuthu = "0";
                        }
                    } else {
                        tieuthu = "0";
                    }
                }catch (Exception e){
                }

            }else {
                tieuthu ="0";
            }
            tv_tieuthu.setText(tieuthu);
        }
        else {
            if (!csm.isEmpty()) {
                try {
                    int tieuthu_i = Integer.parseInt(csm) - Integer.parseInt(csc);
                    if(tieuthu_i <0){
                        tieuthu_i =0;
                    }
                    tieuthu = (String.valueOf(tieuthu_i));
                    tv_tieuthu.setText(tieuthu);
                    int binhquan_i = Integer.parseInt(binhquan);
                    if (binhquan_i > 1 && binhquan_i <= 100) {
                        per_tieuthu_alarm = 30;
                    } else if (binhquan_i > 100 && binhquan_i <= 1000) {
                        per_tieuthu_alarm = 20;
                    } else {
                        per_tieuthu_alarm = 10;
                    }
                    int thr_tieuthu_nho = (int)Math.round(binhquan_i - binhquan_i * per_tieuthu_alarm *0.01);
                    int thr_tieuthu_lon = (int) Math.round(binhquan_i + binhquan_i * per_tieuthu_alarm *0.01);
                    tv_tieuthu.setText(String.valueOf(tieuthu_i));
                    if (tieuthu_i < thr_tieuthu_nho) {
                        String notification = "Tiêu thụ nhỏ hơn " + String.valueOf(per_tieuthu_alarm) + " % bình quân hàng tháng" + "\r\n" +
                                "Vui lòng kiểm tra kỹ lại chỉ số ghi.";
                        dialog_app.Dialog_Notification(notification);
                        // Dialog_Notification_alarm_luu(notification);
                    } else if (tieuthu_i > thr_tieuthu_lon) {
                        String notification = "Tiêu thụ lớn hơn " + String.valueOf(per_tieuthu_alarm) + " % bình quân hàng tháng" + "\r\n" +
                                "Vui lòng kiểm tra kỹ lại chỉ số ghi.";
                        dialog_app.Dialog_Notification(notification);
                        //Dialog_Notification_alarm_luu(notification);
                    } else {
                        // XacNhan_Ghiso();
                    }
                }catch (Exception e){

                }
            } else {
                String notification = "Vui lòng nhập chỉ số mới rồi hãy lưu lại" + "\r\n" + "Xin cám ơn";
                dialog_app.Dialog_Notification(notification);
            }
        }
        // luu
        if(!tieuthu.isEmpty()) {
           try {
               String lat = "";
               String lng = "";
               try {
                   get_Location_Device location_device = new get_Location_Device(activity_Danhsachdocso_chitiet_khachhang.this);
                   lat = location_device.getLat();
                   lng = location_device.getLng();
               } catch (Exception e) {
                   Dialog_App dialog_app_googlemap = new Dialog_App(activity_Danhsachdocso_chitiet_khachhang.this);
                   String notification = "Vui lòng mở ứng dụng Google Map." + "\r\b" + "Để lấy được tọa độ Google." + "\r\n" + "Xin cám ơn";
                   dialog_app_googlemap.Dialog_Notification(notification);
               }
               if (sqlite_danhSachKhackHang_docSo.getStatus_Table_DanhSachKhachHang_Exists()) {
                   String code = get_Codetinhtrang(tinhtrang,Code_old);
                   sqlite_danhSachKhackHang_docSo.Update_Tinhtrang_Code_DanhSachDocSo_KhachHang(Danhba,tinhtrang,code);
                   sqlite_danhSachKhackHang_docSo.Update_CSM_TieuThu_DanhSachDocSo_KhachHang(Danhba,csm,tieuthu);
                   sqlite_danhSachKhackHang_docSo.Update_lat_long_khachhang(Danhba,lat,lng);
               }
           }catch (Exception e){

           }
        }
    }

    private boolean Get_Table_GhiSo_Chitiet(String json){
        boolean status = false;
        try{
          //  Log.e("13",json);
            JSONObject table = new JSONObject(json);
            Danhba = table.getString("danhba");
            Nam = table.getString("nam");
            Ky = table.getString("ky");
            Dot = table.getString("dot");
            SoMay = table.getString("somay");
            TinhTrang = table.getString("tinhtrang");
            String tieude = "Kỳ "+Ky+"/"+Nam+" - Đ"+Dot +" - M"+SoMay;
            tv_tieude.setText(tieude);
            status = true;
        }catch (JSONException e){

        }
        return  status;
    }
    private void Display_DocSoChiTiet_KhachHang_Timkiem_DanhBa(String danhba_timkiem){
        Status_Action_Checkbox  = true;
       Display_DocSoChiTiet_KhachHang(danhba_timkiem);

    }
    private void Display_DocSoChiTiet_KhachHang_Timkiem_SoThan(String sothan_timkiem){
        Status_Action_Checkbox = true;
        String nam = String.valueOf(Nam);
        String ky = String.valueOf(Ky);
        String dot = String.valueOf(Dot);
        String somay = String.valueOf(SoMay);
        sqlite_DanhSachKhackHang_DocSo sqlite_danhSachKhackHang_docSo = new sqlite_DanhSachKhackHang_DocSo(activity_Danhsachdocso_chitiet_khachhang.this,nam,ky,dot,somay);
        if(sqlite_danhSachKhackHang_docSo.getStatus_Table_DanhSachKhachHang_Exists()){
            String danhba =  sqlite_danhSachKhackHang_docSo.Get_DanhSachDocSo_Chitiet_KhachHang_sothan(sothan_timkiem);
            if(!danhba.isEmpty()){
                Display_DocSoChiTiet_KhachHang(danhba);
            }
        }



    }

    private void Display_DocSoChiTiet_KhachHang(String danhba){
        try {
            get_Enable_Docso_Server get_enable_docso_server = new get_Enable_Docso_Server(Nam,Ky,Dot,SoMay);
            get_enable_docso_server.execute();
        }catch (Exception e){

        }
        String nam = String.valueOf(Nam);
        String ky = String.valueOf(Ky);
        String dot = String.valueOf(Dot);
        String somay = String.valueOf(SoMay);
        sqlite_DanhSachKhackHang_DocSo sqlite_danhSachKhackHang_docSo = new sqlite_DanhSachKhackHang_DocSo(activity_Danhsachdocso_chitiet_khachhang.this,nam,ky,dot,somay);
        if(sqlite_danhSachKhackHang_docSo.getStatus_Table_DanhSachKhachHang_Exists()){
            data_danhsachdocso_chitiet_khachhang khachhang =  sqlite_danhSachKhackHang_docSo.Get_DanhSachDocSo_Chitiet_KhachHang(danhba);
            if(khachhang.getId() != 0) {
                String tungay = khachhang.getTungay();
                String denngay =khachhang.getDenngay();
                Date_Tungay = tungay;
                Date_Denngay = denngay;


                /////////////////////

                ///////////////////////
                Danhba = danhba ;
                String mlt = khachhang.getSt();
                tv_mlt.setText(mlt);
                String tinhtrang_dhmoi = Objects.toString(khachhang.getTinhtrangmoi(),"");
                String tieuthu = khachhang.getTieuthu();
                if(tinhtrang_dhmoi.isEmpty() && !tieuthu.trim().isEmpty()){
                    tinhtrang_dhmoi = Objects.toString(khachhang.getTinhtrang_dh(),"");
                }

                // kiem tra rule
                TinhTrang_DHMoi = tinhtrang_dhmoi ;


               // Enable_Disable(Enable_Ghiso);
                //
                String sync = khachhang.getSync();
                Status_Sync = sync;
                if(sync.equals("3")){
                    sync ="0";
                }
                try {
                    String ngaythaydh = khachhang.getNgaythaydh().trim();
                    ngaythaydh = "";
                    if(ngaythaydh.isEmpty()) {
                        get_LSthayDH_Asynctask dh_asynctask = new get_LSthayDH_Asynctask(activity_Danhsachdocso_chitiet_khachhang.this,danhba,tungay,denngay,nam,ky,dot,somay);
                        dh_asynctask.execute();
                    }else if(!ngaythaydh.equals("no") ){
                        String lydo = khachhang.getLydo().trim();
                        String csgo = khachhang.getCsgo().trim();
                        String csgan =khachhang.getCsgan().trim();
                        try {

                        }catch (Exception e){

                        }
                       String thongbao ="THAY ĐỒNG HỒ NƯỚC: "+danhba+"\r\n"+
                                "ĐHN thay ngày: "+ ngaythaydh +"\r\n"+
                                "Lý do: "+ lydo +"\r\n"+
                                "Chỉ số gỡ: "+csgo +"\r\n"+
                                "Chỉ số gắn: "+ csgan;
                       Dialog_App dialog_app =new Dialog_App(activity_Danhsachdocso_chitiet_khachhang.this);
                       dialog_app.Dialog_Notification(thongbao);
                    }
                //    Display_LichSuThayDongHo(danhba,tungay,denngay);
                }catch (Exception e){

                }
                if(Status_Action_Checkbox) {
                    cb_diachinew.setChecked(false);
                    cb_doituongcongdung.setChecked(false);
                    cb_dongho.setChecked(false);
                    cb_dienthoai.setChecked(false);
                    edt_congdung.setText("");
                    tv_hieuconamdh.setText("");
                    edt_dienthoai1.setText("");
                    edt_dienthoai2.setText("");
                    edt_ghichu1.setText("");
                    edt_ghichu2.setText("");
                }

                String tinhtrang_dh = Objects.toString(khachhang.getTinhtrang_dh(),"");
                if(!tinhtrang_dhmoi.isEmpty()){
                    tinhtrang_dh = tinhtrang_dhmoi;
                }
                if(!tinhtrang_dh.trim().isEmpty()) {
                    try{
                        for (int i = 0; i < data_TinhTrang.length; i++) {
                            final  int selete = i ;

                            if (tinhtrang_dh.trim().equals(data_TinhTrang[i].trim())) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        //       cb_dongho.setChecked(true);
                                        spn_tinhtrang.setSelection(selete);
                                    }
                                });
                                break;
                            }
                        }
                    }catch (Exception e){}
                }else {
                    spn_tinhtrang.setSelection(0);
                }
                // kiem tra co thay DHN

                Danhba = danhba;
                Danhba_current = Danhba;
                auto_danhba.setText(Danhba,false);
                So_St = khachhang.getSt();
          //      tv_sost.setText("ST: " + So_St);
                Hoten = khachhang.getHoten() ;
                tv_hoten.setText(Hoten);
                String diachi = khachhang.getSonha() + " - " + khachhang.getDuong();
                Diachi = diachi;
                tv_diachi.setText(diachi);
               // edt_diachinew.setText(khachhang.getSonha());
                //edt_sothan.setText(khachhang.getSothan());

                String hieu_dh = khachhang.getHieudh();
                String co_dh = khachhang.getCo_dh();
               // String co_dh = "null";
                String nam_dh = khachhang.getNam_dh();
                String hieuconam_dh = hieu_dh+"-"+co_dh+"-"+nam_dh;
                tv_hieuconamdh.setText(hieuconam_dh);

                SoThan_Current  = khachhang.getSothan();
                auto_sothan.setText(SoThan_Current,false);
                tv_giabieu.setText(khachhang.getGiabieu());
                tv_dinhmuc.setText(khachhang.getDinhmuc());
                tv_machigoc.setText(khachhang.getMachigoc());
                String code = Objects.toString( khachhang.getCode(),"");
                Code_old= code;
                tv_code.setText(code);
                tv_ttcu.setText(khachhang.getTieuthucu());

                tv_csc.setText(khachhang.getCscu());
                edt_csm.setText(khachhang.getCsmoi());

                String binhquan = khachhang.getBinhquan();
                tv_binhquan.setText(binhquan);
                String sonha_moi =Objects.toString(khachhang.getSonha_moi(),"");

                edt_diachinew.setText(sonha_moi.trim());

                String dienthoai1 =Objects.toString(khachhang.getDienthoai1(),"");

                edt_dienthoai1.setText(dienthoai1.trim());

                String dienthoai2 =Objects.toString(khachhang.getDienthoai2(),"");

                edt_dienthoai2.setText(dienthoai2.trim());

                String congdung = Objects.toString(khachhang.getCongdung(),"");
                CongDung_Server = congdung;
                edt_congdung.setText(congdung);
                Boolean status_error_congdung = true;
                if(!congdung.trim().isEmpty()) {
                    try {
                        String[] com_congdung = congdung.split("-");

                        if(com_congdung.length>2) {
                            String doituong_check =com_congdung[0].trim();
                            String congdung_check =com_congdung[1].trim();
                            sqlite_Config_CongDung sqlite_config_congDung = new sqlite_Config_CongDung(activity_Danhsachdocso_chitiet_khachhang.this);
                            if (sqlite_config_congDung.getStatus_Table_ListCongDung_Exists()) {
                                String id = sqlite_config_congDung.Check_DoiTuong_CongDung(doituong_check,congdung_check).trim();
                                if(!id.isEmpty()){
                                    status_error_congdung = false;
                                }
                            }
                        }else if (com_congdung.length>1){
                            status_error_congdung = false;
                        }


                    }catch (Exception e){}
                }
                if(status_error_congdung){
                    edt_congdung.setBackgroundColor(getResources().getColor(R.color.spinner_Red));

                }else {
                    edt_congdung.setBackgroundColor(getResources().getColor(R.color.spinner_White));
                }
                String vitri_dh = Objects.toString(khachhang.getVitri_dh(),"");
                if(!vitri_dh.trim().isEmpty()) {
                    try {
                        for (int i = 0; i < data_ViTrisDh_Gui.length; i++) {
                            final  int selete = i ;
                            if (vitri_dh.trim().equals(data_ViTrisDh_Gui[i].trim())) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                spn_vitridh.setSelection(selete);
                                    }
                                });
                                break;
                            }
                        }
                    }catch (Exception e){}
                }else {
                    spn_vitridh.setSelection(0);

                }



                String tinhtrang_chi = Objects.toString(khachhang.getChi().trim(),"");
                if(!tinhtrang_chi.trim().isEmpty()) {
                    try{

                        if(!tinhtrang_chi.isEmpty()) {
                            final int selete = Integer.parseInt(tinhtrang_chi);

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        //       cb_dongho.setChecked(true);
                                        spn_tinhtrangchi.setSelection(selete);
                                    }
                                });

                        }

                    }catch (Exception e){}
                }else {
                    spn_tinhtrangchi.setSelection(0);
                }


                String tinhtrang_kh = Objects.toString(khachhang.getTinhtrangks(),"");
                if(!tinhtrang_kh.trim().isEmpty()) {
                    try{
                        for (int i = 0; i < array_khachhang.size(); i++) {
                            final  int selete = i ;

                            if (tinhtrang_kh.trim().equals(array_khachhang.get(i).trim())) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        //       cb_dongho.setChecked(true);
                                        spn_khachhang.setSelection(selete);
                                    }
                                });
                                break;
                            }
                        }
                    }catch (Exception e){}
                }else {
                    spn_khachhang.setSelection(0);
                }


                Sync_DuLieu(sync);
                String ghichu1 = Objects.toString(khachhang.getGhichu1(),"");
                edt_ghichu1.setText(ghichu1);
                String ghichu2 = Objects.toString(khachhang.getGhichu2(),"");
                edt_ghichu2.setText(ghichu2);
                Status_Action_Checkbox = false;

                //
                String tldv = khachhang.getTiledv();
                String tlhcsn = khachhang.getTilehcsn();
                String tlsh = khachhang.getTilesh();
                String tlsx = khachhang.getTilesx();

                try{
                    int i_tldv = Integer.parseInt(tldv);
                    int i_tlhcsn = Integer.parseInt(tlhcsn);
                    int i_tlsh = Integer.parseInt(tlsh);
                    int i_tlsx = Integer.parseInt(tlsx);
                    int tong = i_tldv +i_tlhcsn +i_tlsh +i_tlsx;
                    if((tong >0 && tong <100) || tong>100){
                        Dialog_App dialog_app = new Dialog_App(activity_Danhsachdocso_chitiet_khachhang.this);
                        String notification = "Tỉ lệ tính tiêu thụ bất thường :" + String.valueOf(tong)+" %"+ "\r\n" +
                                " - Tỉ lệ DV : "+tldv +"% " +"\r\n" +
                                " - Tỉ lệ HCSN : "+tlhcsn +"% " +"\r\n"+
                                " - Tỉ lệ SH : "+tlsh +"% " +"\r\n"+
                                " - Tỉ lệ SX : "+tlsx +"% " +"\r\n"+
                                "Vui lòng thông báo với bộ phận quản lý, để kiểm tra lại.";
                        dialog_app.Dialog_Notification(notification);
                    }
                }catch (Exception e_tl){

                }

                tv_tieuthu.setText(tieuthu);
                if(!tieuthu.trim().isEmpty() && !khachhang.getCsmoi().trim().isEmpty()) {
                    try {
                        int tieuthu_i = Integer.parseInt(tieuthu);
                        int binhquan_i = Integer.parseInt(binhquan);
                        if (binhquan_i > 1 && binhquan_i <= 100) {
                            per_tieuthu_alarm = 30;
                        } else if (binhquan_i > 100 && binhquan_i <= 1000) {
                            per_tieuthu_alarm = 20;
                        } else {
                            per_tieuthu_alarm = 10;
                        }
                        int thr_tieuthu_nho = binhquan_i - binhquan_i * per_tieuthu_alarm / 100;
                        int thr_tieuthu_lon = binhquan_i + binhquan_i * per_tieuthu_alarm / 100;
                        tv_tieuthu.setText(String.valueOf(tieuthu_i));

                        if (tieuthu_i < thr_tieuthu_nho) {
                            Dialog_App dialog_app = new Dialog_App(activity_Danhsachdocso_chitiet_khachhang.this);
                            String notification = "Tiêu thụ nhỏ hơn " + String.valueOf(per_tieuthu_alarm) + " % bình quân hàng tháng" + "\r\n" +
                                    "Vui lòng kiểm tra kỹ lại chỉ số ghi.";
                            dialog_app.Dialog_Notification(notification);
                            // Dialog_Notification_alarm_luu(notification);
                        } else if (tieuthu_i > thr_tieuthu_lon) {
                            Dialog_App dialog_app = new Dialog_App(activity_Danhsachdocso_chitiet_khachhang.this);
                            String notification = "Tiêu thụ lớn hơn " + String.valueOf(per_tieuthu_alarm) + " % bình quân hàng tháng" + "\r\n" +
                                    "Vui lòng kiểm tra kỹ lại chỉ số ghi.";
                            dialog_app.Dialog_Notification(notification);
                            //Dialog_Notification_alarm_luu(notification);
                        } else {
                            // XacNhan_Ghiso();
                        }
                    }catch (Exception e){

                    }
                }
            }
        }

    }
    private int Enable_GhiSo(String danhba,String denngay,String tinhtrang){
        int status =0;
        try{
            if(!denngay.isEmpty()) {

                Gson gson_reponse = new Gson();
                data_Enable_Ghiso data_enable_ghiso = new data_Enable_Ghiso("","","","","","","");
                String ngaykhoadoc = "";
                String ngay_h_doctruoc ="";
                int h_khoa = 15;
                int h_khoaf = 9;
                try {
                    data_enable_ghiso = gson_reponse.fromJson(Json_EnableGhiso_Server,data_Enable_Ghiso.class);
                  //  data_enable_ghiso.setNgaydoctruoc("18/06/2021");
                   // data_enable_ghiso.setGiodoctruoc("15");
                    if(!data_enable_ghiso.getNgaykhoadoc().isEmpty()) {
                        String[] com_ngaykhoadoc = data_enable_ghiso.getNgaykhoadoc().split("/");
                        String ngay =com_ngaykhoadoc[0];
                        String thang = com_ngaykhoadoc[1];
                        if( Integer.parseInt(com_ngaykhoadoc[2]) < 10){
                            ngay = "0"+String.valueOf(Integer.parseInt(com_ngaykhoadoc[2]));
                        }
                        if( Integer.parseInt(com_ngaykhoadoc[1]) < 10){
                            thang = "0"+String.valueOf(Integer.parseInt(com_ngaykhoadoc[1]));
                        }
                        ngaykhoadoc = com_ngaykhoadoc[2] + "-" + thang + "-"+ngay ;
                    }
                    if(!data_enable_ghiso.getGiokhoadoc().isEmpty()) {
                        h_khoa = Integer.parseInt(data_enable_ghiso.getGiokhoadoc());
                    }
                    if(!data_enable_ghiso.getGiokhoadocF().isEmpty()) {
                        h_khoaf = Integer.parseInt(data_enable_ghiso.getGiokhoadocF());
                    }
                    if(!data_enable_ghiso.getNgaydoctruoc().isEmpty() && !data_enable_ghiso.getGiodoctruoc().isEmpty()) {
                        String[] com_ngaydoctruoc = data_enable_ghiso.getNgaydoctruoc().split("/");
                        String ngay =com_ngaydoctruoc[0];
                        String thang = com_ngaydoctruoc[1];
                        String gio  = data_enable_ghiso.getGiodoctruoc() ;
                        if( Integer.parseInt(gio) < 10){
                            ngay = "0"+String.valueOf(Integer.parseInt(ngay));
                        }
                        if( Integer.parseInt(thang) < 10){
                            thang = "0"+String.valueOf(Integer.parseInt(thang));
                        }
                        if( Integer.parseInt(gio) < 10){
                            gio = "0"+String.valueOf(Integer.parseInt(gio));
                            ngay_h_doctruoc = com_ngaydoctruoc[2] + "-" + thang + "-"+ngay +
                                    "T0" + gio + ":00:00";
                        }else {
                            ngay_h_doctruoc = com_ngaydoctruoc[2] + "-" + thang + "-"+ngay +
                                    "T" + gio + ":00:00";
                        }

                    }

                    if(!data_enable_ghiso.getDuocphepkhongchuphinh().isEmpty()) {
                        Enable_Chuphinh = Integer.parseInt(data_enable_ghiso.getDuocphepkhongchuphinh());
                    }
                }catch (Exception e){

                }
                //
                SimpleDateFormat format_datetime_chukyds = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                String [] com_denngay = denngay.split("T");
                String start_ghiso = com_denngay[0] +"T05:00:00";
                if(!ngay_h_doctruoc.isEmpty()){
                    start_ghiso  = ngay_h_doctruoc;
                }
                Date date_start_ghiso = format_datetime_chukyds.parse(start_ghiso);
                Date date_denngay = format_datetime_chukyds.parse(denngay);
                Calendar calendar_denngay = Calendar.getInstance();
                calendar_denngay.setTime(date_denngay);
                int thu = calendar_denngay.get(Calendar.DAY_OF_WEEK);
                ////////////


                int hour_offset =0;
                if(thu== 6){
                    hour_offset = 72;
                }else if(thu == 7){
                    hour_offset = 48;
                }else {
                    hour_offset = 24;
                }
                Date currenttime = Calendar.getInstance().getTime();
                long diff_denngay = date_denngay.getTime();
                long diff_start_ghiso = date_start_ghiso.getTime();
                long diff_currenttime = currenttime.getTime();
                //kiem tra den hghi so chua
                long diff_start = diff_currenttime - diff_start_ghiso;
                //


                //
                if(diff_start>=0) {
                    long diff = diff_currenttime - diff_denngay;
                    long sonh_ttieuthu = (int) (diff / 1000 / 60 / 60);
                    String nam = String.valueOf(Nam);
                    String ky = String.valueOf(Ky);
                    String dot = String.valueOf(Dot);
                    String somay = String.valueOf(SoMay);
                    sqlite_DanhSachKhackHang_DocSo sqlite_danhSachKhackHang_docSo = new sqlite_DanhSachKhackHang_DocSo(activity_Danhsachdocso_chitiet_khachhang.this,nam,ky,dot,somay);
                    String tinhtrang_kh ="";
                    if (sqlite_danhSachKhackHang_docSo.getStatus_Table_DanhSachKhachHang_Exists()) {
                        tinhtrang_kh = sqlite_danhSachKhackHang_docSo.Get_DanhSachDocSo_TinhTrangKH(danhba);
                    }
                   // if (tinhtrang.trim().equals("Đóng cửa") || tinhtrang_kh.trim().equals("Đóng cửa")) {
                    if (tinhtrang_kh.trim().equals("Đóng cửa")) {

                        if (sonh_ttieuthu >= (h_khoaf + hour_offset)) {
                            status = DISABLE_GHISO_QUAGIO;
                        }else {
                            status = NORMAL_GHISO;
                        }
                    } else {
                        if (sonh_ttieuthu < h_khoa){
                            status = NORMAL_GHISO;

                        }else {
                            status = DISABLE_GHISO_QUAGIO;
                        }
                    }
                }else {
                    status = DISABLE_GHISO_CHUADENGIO ;
                }

            }else {
                status = DISABLE_KHONGCODENNGAY;
            }
        }catch (Exception e){

        }
        return status;
    }

    private void Display_DocSoChiTiet_KhachHang_TaiMayChu(String danhba){

        String nam = String.valueOf(Nam);
        String ky = String.valueOf(Ky);
        String dot = String.valueOf(Dot);
        String somay = String.valueOf(SoMay);
        sqlite_DanhSachKhackHang_DocSo sqlite_danhSachKhackHang_docSo = new sqlite_DanhSachKhackHang_DocSo(activity_Danhsachdocso_chitiet_khachhang.this,nam,ky,dot,somay);
        if(sqlite_danhSachKhackHang_docSo.getStatus_Table_DanhSachKhachHang_Exists()){
            data_danhsachdocso_chitiet_khachhang khachhang =  sqlite_danhSachKhackHang_docSo.Get_DanhSachDocSo_Chitiet_KhachHang(danhba);
            if(khachhang.getId() != 0) {

                // kiem tra co thay DHN
                Danhba = danhba;
                Danhba_current = Danhba;
                edt_csm.setText(khachhang.getCsmoi());
                String tieuthu = khachhang.getTieuthu();
                tv_tieuthu.setText(tieuthu);
                Status_Sync = Objects.toString(khachhang.getSync().trim(),"2");
            }
        }

    }
    /*
    private void Display_LichSuThayDongHo(final String danhba,String tungay,String denngay) throws ParseException {
        CSGan =-1;
        CSgo= -1;
        Ngay_ThayDH ="";
        final Dialog_App dialog_app = new Dialog_App(activity_Danhsachdocso_chitiet_khachhang.this);
        SimpleDateFormat format_datetime_thaydh = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        final Date date_tungay = format_datetime_thaydh.parse(tungay);
        final Date date_denngay = format_datetime_thaydh.parse(denngay);
        data_Lichsuthaydh_send data_lichsuthaydh_send = new data_Lichsuthaydh_send(danhba);
        Gson gson_request = new Gson();
        String json_send = gson_request.toJson(data_lichsuthaydh_send);
        Api_Lichsuthaydh.get(json_send,
                null,
                new TextHttpResponseHandler() {
                    @Override
                    public void onFailure(int statusCode,Header[] headers,String responseString,Throwable throwable) {
                    }
                    @Override
                    public void onSuccess(int statusCode,Header[] headers,String responseString) {
                        SimpleDateFormat format_datetime_thay = new SimpleDateFormat("dd/MM/yyyy");
                        Gson gson_reponse = new Gson();
                        data_Lichsuthaydong_response data_lichsuthaydong_response = gson_reponse.fromJson(responseString,data_Lichsuthaydong_response.class);
                        data_Lichsuthaydong_response.data_lichsudongho[]  all_data_thongtindh =data_lichsuthaydong_response.getResult().getLichsudonghos();

                        for (int i =all_data_thongtindh.length-1 ; i >=0 ;i--){
                            try{

                                String ngaythay = all_data_thongtindh[i].getNgaythay();
                                Date date_thay = format_datetime_thay.parse(ngaythay);

                                long l_tungay = date_tungay.getTime()/1000;
                                long l_denngay = date_denngay.getTime()/1000;
                                long l_thay = date_thay.getTime()/1000;

                                if((l_thay>l_tungay) && (l_thay <l_denngay)) {
                                    Ngay_ThayDH =ngaythay;
                                 //   String hieu = all_data_thongtindh[i].getHieu();
                                 //   String co = all_data_thongtindh[i].getCo();
                                 //   String sothan = all_data_thongtindh[i].getSoThan();
                                    String lydo = all_data_thongtindh[i].getLydo();
                                    String csgo = all_data_thongtindh[i].getCSGo();
                                    String csgan = all_data_thongtindh[i].getCSGan();
                                    String notification ="THAY ĐỒNG HỒ NƯỚC:"+danhba+"\r\n"+
                                            "ĐHN thay ngày: "+ ngaythay +"\r\n"+
                                            "Lý do: "+ lydo +"\r\n"+
                                            "Chỉ số gỡ: "+csgo +"\r\n"+
                                            "Chỉ số gắn: "+ csgan;
                                    dialog_app.Dialog_Notification(notification);
                                    CSGan =Integer.parseInt(csgan);
                                    CSgo = Integer.parseInt(csgo);
                                    break;
                                }

                            }catch (Exception e) {

                            }
                        }

                    }
                });
    }


     */
    private  void  Sync_DuLieu(String sync){
        int dongbo = Integer.parseInt(sync);

        switch (dongbo) {
            case 2: {
                // timeout
                imv_dongbo.setImageResource(R.drawable.synced_2);
                break;
            }
            case 1: {
                // dang do bo
                imv_dongbo.setImageResource(R.drawable.synced_1);
                break;
            }
            case 0: {
                // da dong bo
                imv_dongbo.setImageResource(R.drawable.synced_0);
                break;
            }
        }
    }
    private String ToDate_Display (String date){
        String date_new = "";
        String [] com_date = date.split("-");
        date_new = com_date[2]+"/"+com_date[1]+"/"+com_date[0];
        return date_new;
    }

    private void Display_DocSoChiTiet_KhachHang_Next(){
        String nam = String.valueOf(Nam);
        String ky = String.valueOf(Ky);
        String dot = String.valueOf(Dot);
        String somay = String.valueOf(SoMay);
        String querry_tinhtrang = "";
        if(!TinhTrang.equals("all")){
            String [] com_tinhtrang = TinhTrang.split("#");
            if(!com_tinhtrang[1].trim().isEmpty()){
                querry_tinhtrang =" danhba IN ("+com_tinhtrang[1]+")";
            }
        }

        sqlite_DanhSachKhackHang_DocSo sqlite_danhSachKhackHang_docSo = new sqlite_DanhSachKhackHang_DocSo(activity_Danhsachdocso_chitiet_khachhang.this,nam,ky,dot,somay);
        if(sqlite_danhSachKhackHang_docSo.getStatus_Table_DanhSachKhachHang_Exists()){
            String danhba =  sqlite_danhSachKhackHang_docSo.Get_DanhSachDocSo_Chitiet_KhachHang_Next(So_St,querry_tinhtrang,Danhba);
           if(!So_St.isEmpty()) {
               if (!danhba.isEmpty()) {
                   Display_DocSoChiTiet_KhachHang(danhba);
               } else {
                   String notification = "Danh bạ này đã là mã lộ trình cuối cùng." + "\r\n" + "Bạn có muốn quay về danh bạ có mã lộ trình đầu tiên";
                   Dialog_Notification_next(notification,sqlite_danhSachKhackHang_docSo,querry_tinhtrang);
               }
           }else {

               if (danhba.isEmpty()) {
                   So_St = "0";
                   danhba =  sqlite_danhSachKhackHang_docSo.Get_DanhSachDocSo_Chitiet_KhachHang_Next(So_St,querry_tinhtrang,Danhba);
               }
               Display_DocSoChiTiet_KhachHang(danhba);
           }
        }


    }
    AlertDialog dialog_notification_next ;
    private void Dialog_Notification_next(final String text,final sqlite_DanhSachKhackHang_DocSo sqlite_danhSachKhackHang_docSo,final String querry_tinhtrang){
        AlertDialog.Builder dialog_notification =new AlertDialog.Builder(activity_Danhsachdocso_chitiet_khachhang.this);
        dialog_notification.setMessage(text);
        // dong y
        dialog_notification.setPositiveButton("Đồng ý",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog,int which) {
                String danhba = sqlite_danhSachKhackHang_docSo.Get_DanhSachDocSo_Chitiet_KhachHang_NextMax(querry_tinhtrang);
                if (!danhba.isEmpty()) {
                    Display_DocSoChiTiet_KhachHang(danhba);
                }
                dialog_notification_next.dismiss();
            }
        });
        // bo qua
        dialog_notification.setNegativeButton("Hủy",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog,int which) {
                dialog_notification_next.dismiss();
            }
        });
        dialog_notification_next = dialog_notification.create();
        dialog_notification_next.show();
    }
    private void Display_DocSoChiTiet_KhachHang_Back(){
        String nam = String.valueOf(Nam);
        String ky = String.valueOf(Ky);
        String dot = String.valueOf(Dot);
        String somay = String.valueOf(SoMay);
        String querry_tinhtrang = "";
        if(!TinhTrang.equals("all")){
            String [] com_tinhtrang = TinhTrang.split("#");
            if(!com_tinhtrang[1].trim().isEmpty()){
                querry_tinhtrang =" danhba IN ("+com_tinhtrang[1]+")";
            }
        }
        sqlite_DanhSachKhackHang_DocSo sqlite_danhSachKhackHang_docSo = new sqlite_DanhSachKhackHang_DocSo(activity_Danhsachdocso_chitiet_khachhang.this,nam,ky,dot,somay);
        if(sqlite_danhSachKhackHang_docSo.getStatus_Table_DanhSachKhachHang_Exists()) {
            String danhba = sqlite_danhSachKhackHang_docSo.Get_DanhSachDocSo_Chitiet_KhachHang_Back(So_St,querry_tinhtrang,Danhba);

            if(!So_St.isEmpty()) {
                if (!danhba.isEmpty()) {
                    Display_DocSoChiTiet_KhachHang(danhba);
                } else {
                    String notification = "Danh bạ này đã là mã lộ trình đầu tiên." +"\r\n"+"Bạn có muốn quay về danh bạ có mã lộ trình cuối cùng";
                    Dialog_Notification_back(notification,sqlite_danhSachKhackHang_docSo,querry_tinhtrang);
                }
            }else
            {
                if (danhba.isEmpty()) {
                        String notification = "Danh bạ này đã là mã lộ trình đầu tiên." + "\r\n" + "Bạn có muốn quay về danh bạ có mã lộ trình cuối cùng";
                        Dialog_Notification_back(notification,sqlite_danhSachKhackHang_docSo,querry_tinhtrang);

                }else {
                    Display_DocSoChiTiet_KhachHang(danhba);
                }
            }

        }


    }
    AlertDialog dialog_notification_back ;
    private void Dialog_Notification_back(final String text,final sqlite_DanhSachKhackHang_DocSo sqlite_danhSachKhackHang_docSo,final String querry_tinhtrang){
        AlertDialog.Builder dialog_notification =new AlertDialog.Builder(activity_Danhsachdocso_chitiet_khachhang.this);
        dialog_notification.setMessage(text);
        // dong y
        dialog_notification.setPositiveButton("Đồng ý",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog,int which) {
                String danhba = sqlite_danhSachKhackHang_docSo.Get_DanhSachDocSo_Chitiet_KhachHang_BackMax(querry_tinhtrang);
                if (!danhba.isEmpty()) {
                    Display_DocSoChiTiet_KhachHang(danhba);
                }
                dialog_notification_back.dismiss();
            }
        });
        // bo qua
        dialog_notification.setNegativeButton("Hủy",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog,int which) {
                dialog_notification_back.dismiss();
            }
        });
        dialog_notification_back = dialog_notification.create();
        dialog_notification_back.show();
    }
    SendDocSo_AsyncTask sendDocSo_asyncTask ;
    private void XacNhan_Ghiso() throws UnsupportedEncodingException {
        Dialog_App dialog_app = new Dialog_App(activity_Danhsachdocso_chitiet_khachhang.this);
        if (!cb_diachinew.isChecked()) {
            String notìication = "Vui lòng kiểm tra có địa chỉ mới không ?" + "\r\n" + "Sau đó hãy tích đã vào địa chỉ để xác nhận đã kiểm tra.";
            dialog_app.Dialog_Notification(notìication);
            return;
        }

        if (!cb_doituongcongdung.isChecked()) {
            String notìication = "Vui lòng kiểm tra đối tượng va công dụng." + "\r\n" + "Sau đó hãy tích đã vào để xác nhận đã kiểm tra.";
            dialog_app.Dialog_Notification(notìication);
            return;
        }

        //String chi = spn_chi.getSelectedItem().toString();
        // String machigoc = spn_machigoc.getSelectedItem().toString();
        if (!cb_dongho.isChecked()) {
            String notìication = "Vui lòng kiểm tra chì và đồng hồ." + "\r\n" + "Sau đó hãy tích đã vào để xác nhận đã kiểm tra.";
            dialog_app.Dialog_Notification(notìication);
            return;
        }

        if (!cb_dienthoai.isChecked()) {
            String notìication = "Vui lòng kiểm tra điện thoại ." + "\r\n" + "Sau đó hãy tích đã vào để xác nhận đã kiểm tra.";
            dialog_app.Dialog_Notification(notìication);
            return;
        }
        String csm =edt_csm.getText().toString().trim();
        String tieuthu = tv_tieuthu.getText().toString().trim();
        if (csm.isEmpty()) {
            String notìication = "Vui lòng kiểm tra lại chỉ số mới ." + "\r\n"  ;
            dialog_app.Dialog_Notification(notìication);
            return;
        }
        if (tieuthu.isEmpty()) {
            String notìication = "Vui lòng kiểm tra lại tiêu thụ." + "\r\n"  ;
            dialog_app.Dialog_Notification(notìication);
            return;
        }

        try {

            sqlite_Hinhanh sqlite_hinhanh = new sqlite_Hinhanh(activity_Danhsachdocso_chitiet_khachhang.this,Nam,Ky,Dot,SoMay);
            if(sqlite_hinhanh.getStatus_Table_ListHinhAnh_Exists()) {
                if(sqlite_hinhanh.Get_Count_HinhAnh_Gui(Danhba) !=0 || (Enable_Chuphinh==1)){
               //if( (Enable_Chuphinh==0)){
                    sendDocSo_asyncTask = new SendDocSo_AsyncTask(activity_Danhsachdocso_chitiet_khachhang.this,Nam,Ky,Dot,Danhba,SoMay,"1");
                    sendDocSo_asyncTask.execute();
                    countDownTimer_scan.start();
                }else {
                    String notìication = "Vui lòng chụp hình và gửi ảnh về server." + "\r\n" + "Sau đó hãy gửi dữ liệu đọc số về";
                    dialog_app.Dialog_Notification(notìication);
                }
            }
        } catch (Exception ee) {

        }
    }
    CountDownTimer countDownTimer_scan  =new CountDownTimer(1000,15000) {
        @Override
        public void onTick(long millisUntilFinished) {
            if(sendDocSo_asyncTask.getStatus_Send()){
                Sync_DuLieu("0");
                countDownTimer_scan.cancel();
            }
        }

        @Override
        public void onFinish() {

        }
    };


    private void  Display_KhachHang(){
        sqlite_config_lydotanggiam sqlite_config_lydotanggiam = new sqlite_config_lydotanggiam(activity_Danhsachdocso_chitiet_khachhang.this);
        if(sqlite_config_lydotanggiam.getStatus_Table_ListLyDoTangGiam_Exists()) {
            List<data_lydotanggiam> data_lydotanggiams = sqlite_config_lydotanggiam.Get_Table_ListLyDoTangGiam();
            for (data_lydotanggiam data : data_lydotanggiams){
                String ma = data.getMa();
                String noidung = data.getNoidung();
                String lydotanggiam  = ma +" - " +noidung ;
                adapter_khachhang.add(lydotanggiam);
            }
            adapter_khachhang.notifyDataSetChanged();
        }
    }




    ///// MAY IN BLUETOOTH
    private void Display_serial(){
        try {
            sqlite_AddBluetooth sqlite_addBluetooth = new sqlite_AddBluetooth(activity_Danhsachdocso_chitiet_khachhang.this);
            if (sqlite_addBluetooth.getStatus_Table_AddrBluetooth_Exists()) {
                List<data_AddBluetooth> data_addBluetooths = sqlite_addBluetooth.Get_All_Table_AddrBluetooth();
                for (data_AddBluetooth data_addBluetooth : data_addBluetooths) {
                    if (data_addBluetooth.getCurrent().trim().equals("1")) {
                        Add_BLT = data_addBluetooth.getAdd().trim();
                        if (data_addBluetooth.getType().equals("WOOSIM")) {
                            Type_BLT_Run = "WOOSIM";
                        } else if (data_addBluetooth.getType().equals("STAR")) {
                            Type_BLT_Run = "STAR";
                        }
                        break;
                    }
                }
            }
        }catch (Exception e){

        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,@NonNull String[] permissions,@NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_DEVICE_SCAN_SECURE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(this, activity_DeviceList.class);
                    startActivityForResult(intent, REQUEST_CONNECT_DEVICE_SECURE);
                }
                break;
            case PERMISSION_DEVICE_SCAN_INSECURE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(this, activity_DeviceList.class);
                    startActivityForResult(intent, REQUEST_CONNECT_DEVICE_INSECURE);
                }
                break;
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(D) Log.d(TAG, "onActivityResult " + resultCode);
        switch (requestCode) {
            case REQUEST_CONNECT_DEVICE_SECURE:
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    connectDevice(data, true);
                }
                break;
            case REQUEST_CONNECT_DEVICE_INSECURE:
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    connectDevice(data, false);
                }
                break;
            case REQUEST_ENABLE_BT:
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    // Bluetooth is now enabled, so set up a print
                    setupPrint();
                } else {
                    // User did not enable Bluetooth or an error occurred
                    if(D) Log.d(TAG, "BT not enabled");
                    Toast.makeText(this, R.string.bt_not_enabled_leaving, Toast.LENGTH_SHORT).show();
                    finish();
                }
        }
    }
    @Override
    public void onStart() {
       try {
           if (!Change_Screen) {
               if (!Danhba.isEmpty()) {
                   Display_DocSoChiTiet_KhachHang(Danhba);
               }
           } else {
               if (Change_Screen_CongDung) {
                   String nam = String.valueOf(Nam);
                   String ky = String.valueOf(Ky);
                   String dot = String.valueOf(Dot);
                   String somay = String.valueOf(SoMay);

                   sqlite_DanhSachKhackHang_DocSo sqlite_danhSachKhackHang_docSo = new sqlite_DanhSachKhackHang_DocSo(activity_Danhsachdocso_chitiet_khachhang.this,nam,ky,dot,somay);
                   if (sqlite_danhSachKhackHang_docSo.getStatus_Table_DanhSachKhachHang_Exists()) {
                       String congdung = sqlite_danhSachKhackHang_docSo.Get_CongDung(Danhba);
                       edt_congdung.setText(congdung);
                   }
                   Change_Screen_CongDung = false;
               } else if (Status_TTTthu) {
                   Status_TTTthu = false;
                   Display_DocSoChiTiet_KhachHang_TaiMayChu(Danhba);
               }
               Change_Screen = false;
           }
       }catch (Exception e){
           Dialog_App dialog_app =new Dialog_App(activity_Danhsachdocso_chitiet_khachhang.this);
           dialog_app.Dialog_Notification("Lỗi:"+"\r\n"+"Thiết lập danh bạ hiển thị lỗi");
       }
        // If BT is not on, request that it be enabled.
        // setupPrint() will then be called during onActivityResult
        if (!mBluetoothAdapter.isEnabled()) {
        //  if (true) {
        Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
            // Otherwise, setup the chat session
        } else {
            if (mPrintService == null)  setupPrint();
        }
        super.onStart();
    }

    boolean display_setting_bt = false;
    @Override
    public synchronized void onResume() {
        super.onResume();
        //if(D) Log.i(TAG, "+ ON RESUME +");

        // Performing this check in onResume() covers the case in which BT was
        // not enabled during onStart(), so we were paused to enable it...
        // onResume() will be called when ACTION_REQUEST_ENABLE activity returns.

        if (mPrintService != null) {
            // Only if the state is STATE_NONE, do we know that we haven't started already
          //  if (mPrintService.getState() != BluetoothPrintService.STATE_CONNECTED)
           // {
           // Log.d("BLT: State ", String.valueOf(mPrintService.getState()));
                if (mPrintService.getState() == BluetoothPrintService.STATE_NONE) {
                    // Start the Bluetooth print services
                    mPrintService.start();
                    if(!Add_BLT.equals("")) {
                        connectDevice_Ready(Add_BLT,false);
                    }else {
                        btn_in.setEnabled(false);
                        Dialog_App dialog_app = new Dialog_App(activity_Danhsachdocso_chitiet_khachhang.this);
                        String text_thongbao = "Vui lòng thiết mở Bluetooth và kết nối thiết bị in hóa đơn";
                        dialog_app.Dialog_Notification(text_thongbao);
                    }
                }else {
                    if (mPrintService.getState() != BluetoothPrintService.STATE_CONNECTED)
                    {
                        if (!Add_BLT.equals("")) {
                            connectDevice_Ready(Add_BLT,false);
                        } else {
                            btn_in.setEnabled(false);
                            if(!display_setting_bt) {
                                Dialog_App dialog_app = new Dialog_App(activity_Danhsachdocso_chitiet_khachhang.this);
                                String text_thongbao = "Vui lòng thiết lâp số mac Bluetooth để In Hóa Đơn"+"Thiết lập trong phần cài đặt";
                                dialog_app.Dialog_Notification(text_thongbao);
                                display_setting_bt = true;
                            }
                        }
                     }
                }
           // }
        }

    }



    @Override
    protected void onDestroy() {
        if (mPrintService != null) {
            mPrintService.stop();
        }
        super.onDestroy();
    }

    private void connectDevice_Ready(String address,boolean secure) {
        // Get the BLuetoothDevice object
        BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        // Attempt to connect to the device
        mPrintService.connect(device, secure);
    }
    private void setupPrint() {
        /*
        Spinner spinner = findViewById(R.id.spn_charsize);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                this, R.array.char_size_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        if (spinner != null) {
            spinner.setAdapter(adapter);
            spinner.setOnItemSelectedListener(
                    new AdapterView.OnItemSelectedListener() {
                        public void onItemSelected(AdapterView<?> parent,View view,int position,long id) {
                            if (position == 1) mCharsize = 2;
                            else if (position == 2) mCharsize = 3;
                            else if (position == 3) mCharsize = 4;
                            else if (position == 4) mCharsize = 5;
                            else if (position == 5) mCharsize = 6;
                            else if (position == 6) mCharsize = 7;
                            else if (position == 7) mCharsize = 8;
                            else mCharsize = 1;
                        }
                        public void onNothingSelected(AdapterView<?> parent) { }
                    }
            );
        }
        woosim
        mCharsize = 3;
         */
        mCharsize = 2;


        // Initialize the BluetoothPrintService to perform bluetooth connections
        mPrintService = new BluetoothPrintService(mHandler);
        mWoosim = new WoosimService(mHandler);
    }
    private final activity_Danhsachdocso_chitiet_khachhang.MyHandler mHandler = new activity_Danhsachdocso_chitiet_khachhang.MyHandler(activity_Danhsachdocso_chitiet_khachhang.this);

    private static class MyHandler extends Handler {
        private final WeakReference<activity_Danhsachdocso_chitiet_khachhang> mActivity;

        MyHandler(activity_Danhsachdocso_chitiet_khachhang activity) {
            mActivity = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            activity_Danhsachdocso_chitiet_khachhang activity = mActivity.get();
            if (activity != null) {
                activity.handleMessage(msg);
            }
        }
    }
    private void handleMessage(Message msg) {
        switch (msg.what) {
            case MESSAGE_DEVICE_NAME:
                // save the connected device's name
                String mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
             //   Toast.makeText(getApplicationContext(), "Connected to " + mConnectedDeviceName, Toast.LENGTH_SHORT).show();
                redrawMenu();
                btn_in.setEnabled(true);
                btn_in.setBackgroundColor(getResources().getColor(R.color.btn_Green));
                break;
            case MESSAGE_TOAST:
                Toast.makeText(getApplicationContext(), msg.getData().getInt(TOAST), Toast.LENGTH_SHORT).show();
                btn_in.setEnabled(false);
                btn_in.setBackgroundColor(getResources().getColor(R.color.btn_Red));
                break;
            case MESSAGE_READ:
                mWoosim.processRcvData((byte[])msg.obj, msg.arg1);
                break;
            case WoosimService.MESSAGE_PRINTER:
                if (msg.arg1 == WoosimService.MSR) {
                    if (msg.arg2 == 0) {
                        Toast.makeText(getApplicationContext(), "MSR reading failure", Toast.LENGTH_SHORT).show();
                    } else {
                        byte[][] track = (byte[][]) msg.obj;
                        if (track[0] != null) {
                            String str = new String(track[0]);
                            Toast.makeText(getApplicationContext(), str, Toast.LENGTH_SHORT).show();

                        }
                        if (track[1] != null) {
                            String str = new String(track[1]);
                            Toast.makeText(getApplicationContext(), str, Toast.LENGTH_SHORT).show();
                        }
                        if (track[2] != null) {
                            String str = new String(track[2]);
                            Toast.makeText(getApplicationContext(), str, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                break;
        }
    }
    private void redrawMenu() {
        /*
      //  MenuItem itemSecureConnect = mMenu.findItem(R.id.secure_connect_scan);
      //  MenuItem itemInsecureConnect = mMenu.findItem(R.id.insecure_connect_scan);
      //  MenuItem itemDisconnect = mMenu.findItem(R.id.disconnect);

        // Context sensitive option menu
        if (mPrintService.getState() != BluetoothPrintService.STATE_CONNECTED) {
            if (!itemSecureConnect.isVisible()) itemSecureConnect.setVisible(true);
            if (!itemInsecureConnect.isVisible()) itemInsecureConnect.setVisible(true);
            if (itemDisconnect.isVisible()) itemDisconnect.setVisible(false);
        } else {
            if (itemSecureConnect.isVisible()) itemSecureConnect.setVisible(false);
            if (itemInsecureConnect.isVisible()) itemInsecureConnect.setVisible(false);
            if (!itemDisconnect.isVisible()) itemDisconnect.setVisible(true);
        }

         */
    }
    private void connectDevice(Intent data, boolean secure) {
        String address = null;
        // Get the device MAC address
        if (data.getExtras() != null)
            address = data.getExtras().getString(activity_DeviceList.EXTRA_DEVICE_ADDRESS);
        // Get the BLuetoothDevice object
     //   BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        // Attempt to connect to the device
     //   mPrintService.connect(device, secure);
    }

    private void sendData(byte[] data) {
        // Check that we're actually connected before trying printing
        if (mPrintService.getState() != BluetoothPrintService.STATE_CONNECTED) {
            Toast.makeText(this, R.string.not_connected, Toast.LENGTH_SHORT).show();
            return;
        }
        // Check that there's actually something to send
        if (data.length > 0)
            mPrintService.write(data);
    }

    /**
     * On click function for sample print button.
     */
    public void printReceipt(View v) {
        InputStream inStream = getResources().openRawResource(R.raw.receipt2);
        sendData(WoosimCmd.initPrinter());
        try {
            byte[] data = new byte[inStream.available()];
            while (inStream.read(data) != -1)
            {
                sendData(data);
            }
        } catch (IOException e) {
            Log.e(TAG, "sample 2inch receipt print fail.", e);
        } finally {
            try {
                inStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void printImage() {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;
        Bitmap bmp = BitmapFactory.decodeResource(getResources(), R.drawable.logo, options);
        if (bmp == null) {
            Log.e(TAG, "resource decoding is failed");
            return;
        }
        byte[] data = WoosimImage.printBitmap(0, 0, 384, 200, bmp);
        bmp.recycle();

        sendData(WoosimCmd.setPageMode());
        sendData(data);
        sendData(WoosimCmd.PM_setStdMode());
    }


    private void Print_HoaDonCnCl_Mau() throws IOException {
        ByteArrayOutputStream byteStream_in = new ByteArrayOutputStream();


        // header
        String header_text ="\r\n"+"CÔNG TY CỔ PHẦN CÁP NƯỚC CHỢ LỚN" + "\r\n" +
                "97 Phạm Hữu Chí P. 12 Q. 5" +"\r\n" +
                "Tổng đài : 0865. 851. 088" +"\r\n" +
                "------------------------------------" +"\r\n" ;
        byteStream_in = Init_Text_In(byteStream_in,header_text,true,1,1,WoosimCmd.ALIGN_CENTER);

        /// ngay ghi
        Date currentTime = Calendar.getInstance().getTime();
        String currentDate = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(currentTime);
        String ngayghi_text = "Ngày gửi giấy báo: "+currentDate +"\r\n";
        byteStream_in = Init_Text_In(byteStream_in,ngayghi_text,true,1,1,WoosimCmd.ALIGN_CENTER);
        // title
        String title_text ="GIẤY BÁO TIỀN NƯỚC" + "\r\n" +
                "(KHÔNG THAY THẾ HÓA ĐƠN)" +"\r\n" ;
        byteStream_in = Init_Text_In(byteStream_in,title_text,true,1,2,WoosimCmd.ALIGN_CENTER);
        // ky_text

        String ky_text = "Kỳ:";
        byteStream_in = Init_Text_In(byteStream_in,ky_text,false,1,1,WoosimCmd.ALIGN_LEFT);
        String ky_value_text = "11/2020"+"\r\n";
        byteStream_in = Init_Text_In(byteStream_in,ky_value_text,false,1,1,WoosimCmd.ALIGN_RIGHT);

        //tungay-> den ngay
        String tungay_text = "Từ ngày: 26/10/2020";
        byteStream_in = Init_Text_In(byteStream_in,tungay_text,false,1,1,WoosimCmd.ALIGN_LEFT);
        String denngay_text = "-> 27/11/2020" +"\r\n";
        byteStream_in = Init_Text_In(byteStream_in,denngay_text,false,1,1,WoosimCmd.ALIGN_RIGHT);

        // Danh ba
        String danhba_text = "Danh bạ:" ;
        byteStream_in = Init_Text_In(byteStream_in,danhba_text,false,1,1,WoosimCmd.ALIGN_LEFT);
        String danhba_value_text = "60147471521"  +"\r\n";
        byteStream_in = Init_Text_In(byteStream_in,danhba_value_text,true,1,2,WoosimCmd.ALIGN_RIGHT);

        // Khanh hang
        String khachhang_text = "KH:";
        byteStream_in = Init_Text_In(byteStream_in,khachhang_text,false,1,1,WoosimCmd.ALIGN_LEFT);
        String khachhang_value_text = "NGO DINH DUY KHANH" +"\r\n";
        byteStream_in = Init_Text_In(byteStream_in,khachhang_value_text,true,1,1,WoosimCmd.ALIGN_RIGHT);

        //Dia chi
        String diachi_text = "Đ/C:";
        byteStream_in = Init_Text_In(byteStream_in,diachi_text,false,1,1,WoosimCmd.ALIGN_LEFT);
        String diachi_value_text = "123A LÂUD 2 TRẸT BÌNH TÂY" +"\r\n";
        byteStream_in = Init_Text_In(byteStream_in,diachi_value_text,false,1,1,WoosimCmd.ALIGN_RIGHT);

        //Gia bieu , Dinh muc, Code
        String giabieu_text = "GB: "+"11" +"      ";;
        byteStream_in = Init_Text_In(byteStream_in,giabieu_text,false,1,1,WoosimCmd.ALIGN_LEFT);

        String dinhmuc_text = "ĐM: "+"28" +"      ";;
        byteStream_in = Init_Text_In(byteStream_in,dinhmuc_text,false,1,1,WoosimCmd.ALIGN_LEFT);


        String code_text = "Code: "+"4" +"\r\n";
        byteStream_in = Init_Text_In(byteStream_in,code_text,false,1,1,WoosimCmd.ALIGN_LEFT);


        // CS moi -CS cu
        String csm_text = "Cs mới: "+"400";
        byteStream_in = Init_Text_In(byteStream_in,csm_text,false,1,1,WoosimCmd.ALIGN_LEFT);
        String csc_text = "Cs cũ: "+"200" ;
        byteStream_in = Init_Text_In(byteStream_in,csc_text,false,1,1,WoosimCmd.ALIGN_RIGHT);

        // tieu thu
        String tieuthu_text = "Tiêu thụ:";
        byteStream_in = Init_Text_In(byteStream_in,tieuthu_text,false,1,1,WoosimCmd.ALIGN_LEFT);
        String tieuthu_value_text = "200"+" m3" +"\r\n";
        byteStream_in = Init_Text_In(byteStream_in,tieuthu_value_text,true,1,2,WoosimCmd.ALIGN_RIGHT);

        // tien nuoc
        String tiennuoc_text = "Tiền nước:";
        byteStream_in = Init_Text_In(byteStream_in,tiennuoc_text,false,1,1,WoosimCmd.ALIGN_LEFT);
        String tiennuoc_value_text = "123.000"+" đ" +"\r\n";
        byteStream_in = Init_Text_In(byteStream_in,tiennuoc_value_text,true,1,1,WoosimCmd.ALIGN_RIGHT);

        // tien thue
        String tienthue_text = "Tiền thuế:";
        byteStream_in = Init_Text_In(byteStream_in,tienthue_text,false,1,1,WoosimCmd.ALIGN_LEFT);
        String tienthue_value_text = "12.000"+" đ" +"\r\n";
        byteStream_in = Init_Text_In(byteStream_in,tienthue_value_text,true,1,1,WoosimCmd.ALIGN_RIGHT);

        // phi bao ve moi truong
        String tienpbvmt_text = "Phí BVMT:";
        byteStream_in = Init_Text_In(byteStream_in,tienpbvmt_text,false,1,1,WoosimCmd.ALIGN_LEFT);
        String tienpbvmt_value_text = "69.000"+" đ" +"\r\n";
        byteStream_in = Init_Text_In(byteStream_in,tienpbvmt_value_text,true,1,1,WoosimCmd.ALIGN_RIGHT);

        // tong
        String tong_text = "Tổng:";
        byteStream_in = Init_Text_In(byteStream_in,tong_text,false,1,1,WoosimCmd.ALIGN_LEFT);
        String tong_value_text = "204.000"+" đ" +"\r\n";
        byteStream_in = Init_Text_In(byteStream_in,tong_value_text,true,1,1,WoosimCmd.ALIGN_RIGHT);

        // ket thuc
        String ketthuc_text = "------------------------------------" +"\r\n";
        byteStream_in = Init_Text_In(byteStream_in,ketthuc_text,true,1,1,WoosimCmd.ALIGN_LEFT);


        // Neu có nợ
        String [] ky_no = {"6/2020","7/2020","8/2020","9/2020","10/2020"};
        String [] tien_no = {"100.000","200.000","100.000","200.000","250.000"};
        if(ky_no.length >0) {
            // ky no
            String nocu_text = "Nợ cũ"+"\r\n";
            byteStream_in = Init_Text_In(byteStream_in,nocu_text,true,1,2,WoosimCmd.ALIGN_LEFT);
            for (int i = 0; i < ky_no.length ; i++){
                String kyno_text = "Kỳ: "+ky_no[i];
                byteStream_in = Init_Text_In(byteStream_in,kyno_text,true,1,1,WoosimCmd.ALIGN_LEFT);
                String kyno_value_text = tien_no[i]+" đ" +"\r\n";
                byteStream_in = Init_Text_In(byteStream_in,kyno_value_text,true,1,1,WoosimCmd.ALIGN_RIGHT);
            }
            // tong cong
            String tongcong_text = "Tổng cộng: ";
            byteStream_in = Init_Text_In(byteStream_in,tongcong_text,true,1,1,WoosimCmd.ALIGN_LEFT);
            String tongcong_value_text = "1.004.000" +" đ" +"\r\n";
            byteStream_in = Init_Text_In(byteStream_in,tongcong_value_text,true,1,2,WoosimCmd.ALIGN_RIGHT);
            //ket thuc
            byteStream_in = Init_Text_In(byteStream_in,ketthuc_text,true,1,1,WoosimCmd.ALIGN_LEFT);
        }

        // nhan vien
        String nhanvien_text = "N.viên: ";
        byteStream_in = Init_Text_In(byteStream_in,nhanvien_text,true,1,1,WoosimCmd.ALIGN_LEFT);
        String nhanvien_value_text = "Lê Phước Hiện" +"\r\n";
        byteStream_in = Init_Text_In(byteStream_in,nhanvien_value_text,true,1,2,WoosimCmd.ALIGN_RIGHT);

        // dt zalo nhan vien
        String dt_zalo_nv_text = "ĐT hoặc Zalo: ";
        byteStream_in = Init_Text_In(byteStream_in,dt_zalo_nv_text,true,1,1,WoosimCmd.ALIGN_LEFT);
        String dt_zalo_nv_value_text = "0933418618" +"\r\n";
        byteStream_in = Init_Text_In(byteStream_in,dt_zalo_nv_value_text,true,1,2,WoosimCmd.ALIGN_RIGHT);

        // ket thuc
        byteStream_in = Init_Text_In(byteStream_in,ketthuc_text,true,1,1,WoosimCmd.ALIGN_LEFT);
        if(ky_no.length >0){
            // thong bao lan 2
            String thongbaolan2_text = "THÔNG BÁO LẦN "+String.valueOf( ky_no.length)+"\r\n";
            byteStream_in = Init_Text_In(byteStream_in,thongbaolan2_text,false,1,2,WoosimCmd.ALIGN_LEFT);
            String noidungthongbao_text = " Đề nghị khách hàng thanh toán tiền\n" +
                    " nước do đã quá hạn. Trường hợp\n" +
                    " không thanh toán. Công ty sẽ tạm\n" +
                    " ngừng dịch vụ cấp nước kể từ ngày:\n" +
                    " 6/1/2021 và ngừng dịch vụ cấp nước\n" +
                    " sau 05 tuần. Để biết thông tin chi\n" +
                    " tiết tiền nước Quý khách hàng vui\n" +
                    " lòng liên hệ tổng đài 0865.851.088\n" +
                    " hoặc truy cập vào website\n" +
                    " http://capnuoccholon.com.vn\n" +
                    " ứng dụng trên thiết bị di động\n" +
                    " Android và IOS" +"\r\n";
            byteStream_in = Init_Text_In(byteStream_in,noidungthongbao_text,false,1,1,WoosimCmd.ALIGN_LEFT);
            byteStream_in = Init_Text_In(byteStream_in,ketthuc_text,true,1,1,WoosimCmd.ALIGN_LEFT);
        }

        // luu y
        String luuy_text = "Lưu ý: "+"\r\n";
        byteStream_in = Init_Text_In(byteStream_in,luuy_text,false,1,2,WoosimCmd.ALIGN_LEFT);
        String luuy_value_text = " 1. Trường hợp khách hàng đã thanh\n" +
                " toán toàn nợ cũ trước ngày nhận\n" +
                " giấy báo này. Xin chỉ thanh toán\n" +
                " hóa đơn kỳ mới.\n" +
                " 2. Nếu số điện thoại khách hàng \n" +
                " 0913752611 không dùng xin vui lòng\n" +
                " liên hệ để cập nhật lại"+"\r\n"+"\r\n"+"\r\n";
        byteStream_in = Init_Text_In(byteStream_in,luuy_value_text,true,1,1,WoosimCmd.ALIGN_LEFT);
        ketthuc_text =ketthuc_text + "\r\n";
        byteStream_in = Init_Text_In(byteStream_in,ketthuc_text,true,1,1,WoosimCmd.ALIGN_LEFT);

        byteStream_in.write(WoosimCmd.printData());
        sendData(WoosimCmd.initPrinter());
        sendData(byteStream_in.toByteArray());

    }
    private void Print_HoaDonCnCl_Woosim() throws IOException {
        String nam = String.valueOf(Nam).trim();
        String ky = String.valueOf(Ky).trim();
        String dot = String.valueOf(Dot).trim();
        String somay = String.valueOf(SoMay);
        long tongtienky =0 ;
        DecimalFormat formatter = new DecimalFormat("###,###,###");
        SimpleDateFormat format_datetime = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat format_datetime_display = new SimpleDateFormat("yyyy/MM/dd");
        // hien thi thong tin doc so
        sqlite_DanhSachKhackHang_DocSo sqlite_danhSachKhackHang_docSo = new sqlite_DanhSachKhackHang_DocSo(activity_Danhsachdocso_chitiet_khachhang.this,nam,ky,dot,somay);
        if(sqlite_danhSachKhackHang_docSo.getStatus_Table_DanhSachKhachHang_Exists()){
            List<data_Danhsachdocso_chitiet> khachhangs = sqlite_danhSachKhackHang_docSo.Get_DanhSachDocSo_Chitiet();
            data_danhsachdocso_chitiet_khachhang khachhang =  sqlite_danhSachKhackHang_docSo.Get_DanhSachDocSo_Chitiet_KhachHang(Danhba);
            if(khachhang.getId() != 0) {
                String tieuthu = tv_tieuthu.getText().toString().trim();
                if(!tieuthu.equals("")) {
                    String hoten_khachhang = khachhang.getHoten();
                    String sonha_cu = khachhang.getSonha().toString().trim();
                    String sonha_moi = khachhang.getSonha_moi().toString().trim();
                    String diachi = sonha_cu + " " + khachhang.getDuong();
                    if(!sonha_moi.isEmpty()){
                        diachi = sonha_cu +" ("+sonha_moi+")"+ "\r\n" + khachhang.getDuong();
                    }
                    String giabieu = khachhang.getGiabieu();
                    String dinhmuc = khachhang.getDinhmuc();
                    String tinhtrang = spn_tinhtrang.getSelectedItem().toString().trim();
                    String code = get_Codetinhtrang(tinhtrang,Code_old);
                    if(Status_Sync.equals("3")){
                        code = khachhang.getCodemoi();
                    }
                    String csc = khachhang.getCscu();
                    String csm = edt_csm.getText().toString().trim();
                    String tungay = khachhang.getTungay();
                    String denngay = khachhang.getDenngay();
                    String dinhmucngheo = khachhang.getDinhmucngheo();
                    String tldv = khachhang.getTiledv();
                    String tlhcsn = khachhang.getTilehcsn();
                    String tlsh = khachhang.getTilesh();
                    String tlsx = khachhang.getTilesx();
                    String dienthoai = edt_dienthoai1.getText().toString().trim();
                    int tieuthu_i = 0;
                    try {
                        tieuthu_i = Integer.parseInt(tieuthu);
                    }catch(Exception e) {}

                    if(tieuthu_i < 0){
                        Dialog_App dialog_app = new Dialog_App(activity_Danhsachdocso_chitiet_khachhang.this);
                        dialog_app.Dialog_Notification("Tiêu thụ âm không chính xác"+"\r\n"+"Vui lòng kiểm tra lại");
                        return;
                    }

                    int giabieu_i = 0;
                    try {
                        giabieu_i = Integer.parseInt(giabieu);
                    }catch (Exception e){}

                    int dinhmuc_i  = 0;
                    try {
                        dinhmuc_i = Integer.parseInt(dinhmuc);
                    }catch (Exception e){}


                    int dinhmucngheo_i = 0;
                    try {
                        dinhmucngheo_i = Integer.parseInt(dinhmucngheo);
                    }catch (Exception e){}


                    int tl_kd_i = 0;
                    try {
                        tl_kd_i= Integer.parseInt(tldv);
                    }catch (Exception e){}


                    int tl_hcsn_i = 0;
                    try {
                        tl_hcsn_i =  Integer.parseInt(tlhcsn);
                    }catch (Exception e){}


                    int tl_sh_i =0;
                    try {
                        tl_sh_i=  Integer.parseInt(tlsh);
                    }catch (Exception e){}


                    int tl_sx_i =0;
                    try {
                        tl_sx_i=  Integer.parseInt(tlsx);
                    }catch (Exception e){};


                    Thanhtoan_tiennuoc thanhtoan_tiennuoc = new Thanhtoan_tiennuoc();
                    data_Tiennuoc data_tiennuoc = thanhtoan_tiennuoc.Tiennuoc(tieuthu_i,giabieu_i,dinhmuc_i,dinhmucngheo_i,tl_kd_i,tl_hcsn_i,tl_sh_i,tl_sx_i,tungay,denngay);
                    long tiennuoc =  (long) Long.parseLong(data_tiennuoc.getTiennuoc());
                    long thue_5 = 0;
                    long thue_bvmt = 0;
                    long tiendichvuthoatnuoc = 0;
                    long thuetiendichvuthoatnuoc =0;
                    boolean covid = false;
                    long tongtiengiam = 0 ;
                    long pre_tiennuoc = tiennuoc;
                    if(nam.equals("2021") && (ky.equals("9")|| ky.equals("10") || ky.equals("11"))){
                        // DICH GIAM 10 %
                        // CHO GB: 11-19, 21-29, 51, 56, 57, 59
                        if(
                                (11<=giabieu_i && giabieu_i<=19) ||
                                        (21<=giabieu_i && giabieu_i<=29) ||
                                        (giabieu_i == 51 || giabieu_i == 56 || giabieu_i == 59)
                        ){

                            tiennuoc = (long)(tiennuoc*0.9);
                            thue_5 = (long) Math.round(tiennuoc * 0.05);
                            String enble_thuevbmt = khachhang.getKhongtinhphi_bvmt().trim();

                            if(enble_thuevbmt.equals("0") || enble_thuevbmt.isEmpty()) {
                                if(giabieu_i == 51 || giabieu_i == 56 || giabieu_i == 59){
                                    thue_bvmt = (long) thanhtoan_tiennuoc.Tiennuoc_ThueBVMT_Covid(tieuthu_i,giabieu_i,dinhmuc_i,dinhmucngheo_i,tl_kd_i,tl_hcsn_i,tl_sh_i,tl_sx_i);
                                }else {
                                    thue_bvmt = (long) Math.round(tiennuoc * 0.1);
                                }
                            }
                            covid = true;


                        }else {

                            thue_5 = Long.parseLong(data_tiennuoc.getThuetiennuoc_5());
                            String enble_thuevbmt = khachhang.getKhongtinhphi_bvmt().trim();

                            if(enble_thuevbmt.equals("0") || enble_thuevbmt.isEmpty()) {
                                thue_bvmt = Long.parseLong(data_tiennuoc.getPhiBVMT());
                            }
                        }
                    }else {
                        thue_5 = (long) Long.parseLong(data_tiennuoc.getThuetiennuoc_5());
                        String enble_thuevbmt = khachhang.getKhongtinhphi_bvmt().trim();

                        if(enble_thuevbmt.equals("0") || enble_thuevbmt.isEmpty()) {
                            thue_bvmt = Long.parseLong(data_tiennuoc.getPhiBVMT());
                            tiendichvuthoatnuoc = (long) Long.parseLong(data_tiennuoc.getDichvuthoatnuoc());
                            thuetiendichvuthoatnuoc = (long) Long.parseLong(data_tiennuoc.getThuedichvuthoatnuoc());
                        }

                    }

                    tongtienky = tiennuoc + thue_5 + thue_bvmt + tiendichvuthoatnuoc +thuetiendichvuthoatnuoc;

                    long thue_chuagiam =  (long) Math.round(pre_tiennuoc * 0.05);
                    long thue_mt_chuagiam = (long) Math.round(pre_tiennuoc * 0.1);
                    long tongtiennuoc_chuagiam = pre_tiennuoc+thue_chuagiam+thue_mt_chuagiam;
                    tongtiengiam = tongtiennuoc_chuagiam - tongtienky ;
                    sqlite_TienNo sqlite_tienNo = new sqlite_TienNo(activity_Danhsachdocso_chitiet_khachhang.this,nam,ky,dot,somay);
                    long tongtienno = 0;
                    data_tiennos tiennos = new data_tiennos(Danhba);
                    if (sqlite_tienNo.getStatus_Table_DanhSachHangHang_TienNo_Exists()) {
                        tiennos = sqlite_tienNo.Get_Data_TienNo(Danhba);
                        for (int i = 0; i < tiennos.getTiennos().length; i++) {
                            String tienno_ky = tiennos.getTiennos()[i].getTienNo();
                            tongtienno = tongtienno + (long)Long.parseLong(tienno_ky);
                        }
                    }
                    long tongtien = (long)tongtienky + (long) tongtienno;
                    String ten_nhanvien = "";
                    String dienthoai_nhanvien = "";
                    sqlite_User sqlite_user = new sqlite_User(activity_Danhsachdocso_chitiet_khachhang.this);
                    if (sqlite_user.getStatus_Table_User_Exists()) {
                        data_User nhanvien = sqlite_user.Get_User("1");
                        ten_nhanvien = nhanvien.getTennhanvien().toString().trim();
                        dienthoai_nhanvien = nhanvien.getdienthoai().toString().trim();
                    }
                    /// ngay thu tien nuoc
                    SimpleDateFormat format_datetime_chukyds = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                    Date date_denngay = null;
                    try {
                        date_denngay = format_datetime_chukyds.parse(denngay);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    Calendar calendar_denngay = Calendar.getInstance();
                    calendar_denngay.setTime(date_denngay);
                    if(dot.trim().equals("1")|| dot.trim().equals("2")) {
                        calendar_denngay.add(Calendar.DATE,4);
                        int thu = calendar_denngay.get(Calendar.DAY_OF_WEEK);
                        if (thu == 7) {
                            calendar_denngay.add(Calendar.DATE,2);
                        } else if (thu == 1){
                            calendar_denngay.add(Calendar.DATE,1);
                        }
                    }else {
                        calendar_denngay.add(Calendar.DATE,3);
                        int thu = calendar_denngay.get(Calendar.DAY_OF_WEEK);
                        if (thu == 7) {
                            calendar_denngay.add(Calendar.DATE,2);
                        } else if (thu == 1){
                            calendar_denngay.add(Calendar.DATE,1);
                        }
                    }
                    SimpleDateFormat hienthi_ngay = new SimpleDateFormat("dd/MM/yyyy");
                    String date_thutien = hienthi_ngay.format(calendar_denngay.getTime());


                    // In

                    ByteArrayOutputStream byteStream_in = new ByteArrayOutputStream();
                    // header
                    String header_text = "CÔNG TY CỔ PHẦN CẤP NƯỚC CHỢ LỚN" + "\r\n" ;

                    byteStream_in = Init_Text_In(byteStream_in,header_text,true,1,2,WoosimCmd.ALIGN_CENTER);
                    String header_add_text = "97 Phạm Hữu Chí P. 12 Q. 5" + "\r\n" ;
                    byteStream_in = Init_Text_In(byteStream_in,header_add_text,false,1,1,WoosimCmd.ALIGN_CENTER);
                    String header_tongdai =   "Tổng đài: 0865.851.088" + "\r\n" ;
                    byteStream_in = Init_Text_In(byteStream_in,header_tongdai,false,1,1,WoosimCmd.ALIGN_CENTER);
                    String header_gachngang =       "------------" + "\r\n";
                    byteStream_in = Init_Text_In(byteStream_in,header_gachngang,true,1,1,WoosimCmd.ALIGN_CENTER);

                    /// ngay ghi
                    Date currentTime = Calendar.getInstance().getTime();
                    String currentDate = new SimpleDateFormat("dd/MM/yyyy",Locale.getDefault()).format(currentTime);
                    String ngayghi_text = "Ngày gửi giấy báo: " + currentDate + "\r\n";
                    byteStream_in = Init_Text_In(byteStream_in,ngayghi_text,false,1,1,WoosimCmd.ALIGN_CENTER);
                    // title
                    String title_text = "GIẤY BÁO TIỀN NƯỚC" + "\r\n" +
                            "KHÔNG THAY THẾ HÓA ĐƠN" + "\r\n";
                    byteStream_in = Init_Text_In(byteStream_in,title_text,true,1,2,WoosimCmd.ALIGN_CENTER);
                    // ky_text

                    String ky_value_text = "Kỳ: "+ Ky + "/" + Nam + "\r\n";
                    byteStream_in = Init_Text_In(byteStream_in,ky_value_text,false,1,1,WoosimCmd.ALIGN_LEFT);

                    //tungay-> den ngay
                    String[] com_tungay = tungay.split("T");
                    String[] com_denngay = denngay.split("T");

                    String tungay_new = ToDate_Display((com_tungay[0]));
                    String denngay_new = ToDate_Display((com_denngay[0]));

                    String tungay_text = "Từ ngày: " + tungay_new.trim();

                    String denngay_text = tungay_text+ " -> " + denngay_new.trim() + "\r\n";
                    byteStream_in = Init_Text_In(byteStream_in,denngay_text,false,1,1,WoosimCmd.ALIGN_CENTER);

                    // Danh ba
                    String danhba_text = "Danh bạ:";
                    byteStream_in = Init_Text_In(byteStream_in,danhba_text,false,1,1,WoosimCmd.ALIGN_LEFT);
                    String danhba1 = Danhba.substring(0,4);
                    String danhba2 = Danhba.substring(4,7);
                    String danhba3 = Danhba.substring(7);
                    String danhba_value_text = danhba1 +" "+danhba2 +" "+danhba3 +"  "+"\r\n";
                    byteStream_in = Init_Text_In(byteStream_in,danhba_value_text,true,1,3,WoosimCmd.ALIGN_RIGHT);

                    // Khanh hang
                    String khachhang_text = "KH:";
                    byteStream_in = Init_Text_In(byteStream_in,khachhang_text,false,1,1,WoosimCmd.ALIGN_LEFT);
                    String khachhang_value_text = hoten_khachhang.toUpperCase() + "\r\n";
                    byteStream_in = Init_Text_In(byteStream_in,khachhang_value_text,false,1,1,WoosimCmd.ALIGN_LEFT);

                    //Dia chi
                    String diachi_text = "Đ/C:";
                    byteStream_in = Init_Text_In(byteStream_in,diachi_text,false,1,1,WoosimCmd.ALIGN_LEFT);
                    String diachi_value_text = diachi.toUpperCase() + "\r\n";
                    byteStream_in = Init_Text_In(byteStream_in,diachi_value_text,false,1,1,WoosimCmd.ALIGN_LEFT);

                    String ketthuc_text = "------------------------------------" + "\r\n";

                    //Gia bieu , Dinh muc, Code
                    String giabieu_text = "GB: " ;
                    byteStream_in = Init_Text_In(byteStream_in,giabieu_text,false,1,1,WoosimCmd.ALIGN_LEFT);
                    String giabieu_value = giabieu + "      ";
                    byteStream_in = Init_Text_In(byteStream_in,giabieu_value,true,1,1,WoosimCmd.ALIGN_LEFT);
                    String dinhmuc_text = "ĐM: " ;
                    byteStream_in = Init_Text_In(byteStream_in,dinhmuc_text,false,1,1,WoosimCmd.ALIGN_LEFT);
                    String dinhmuc_value = dinhmuc + "      ";
                    byteStream_in = Init_Text_In(byteStream_in,dinhmuc_value,true,1,1,WoosimCmd.ALIGN_LEFT);
                    String code_text = "Code: " ;
                    byteStream_in = Init_Text_In(byteStream_in,code_text,false,1,1,WoosimCmd.ALIGN_LEFT);
                    String code_value = code + "\r\n";
                    byteStream_in = Init_Text_In(byteStream_in,code_value,true,1,1,WoosimCmd.ALIGN_LEFT);
                    // CS moi -CS cu
                    String csm_text = "Cs mới: " ;
                    byteStream_in = Init_Text_In(byteStream_in,csm_text,false,1,1,WoosimCmd.ALIGN_LEFT);
                    byteStream_in = Init_Text_In(byteStream_in,csm+"  -  ",true,1,1,WoosimCmd.ALIGN_LEFT);

                    String csc_text = "Cs cũ: " ;
                    byteStream_in = Init_Text_In(byteStream_in,csc_text,false,1,1,WoosimCmd.ALIGN_LEFT);
                    byteStream_in = Init_Text_In(byteStream_in,csc +"\r\n",true,1,1,WoosimCmd.ALIGN_LEFT);

                    // tieu thu
                    String tieuthu_text = "Tiêu thụ:";
                    byteStream_in = Init_Text_In(byteStream_in,tieuthu_text,false,1,1,WoosimCmd.ALIGN_LEFT);
                    String tieuthu_value_text = "     "+tieuthu.toString() +" m3"+"\r\n";
                    byteStream_in = Init_Text_In(byteStream_in,tieuthu_value_text,false,2,2,WoosimCmd.ALIGN_LEFT);


                    // tien nuoc
                    String tiennuoc_text = "Tiền nước:";
                    byteStream_in = Init_Text_In(byteStream_in,tiennuoc_text,false,1,1,WoosimCmd.ALIGN_LEFT);
                    String tiennuoc_value_text = formatter.format(tiennuoc) + " đ  " + "\r\n";
                    byteStream_in = Init_Text_In(byteStream_in,tiennuoc_value_text,false,1,1,WoosimCmd.ALIGN_RIGHT);

                    String per = "%:";
                    // tien thue
                    String tienthue_text = "Thuế tiền nước 5 p.trăm:";
                    byteStream_in = Init_Text_In(byteStream_in,tienthue_text,false,1,1,WoosimCmd.ALIGN_LEFT);

                    String tienthue_value_text = formatter.format(thue_5) + " đ  " + "\r\n";
                    byteStream_in = Init_Text_In(byteStream_in,tienthue_value_text,false,1,1,WoosimCmd.ALIGN_RIGHT);

                    // phi bao ve moi truong
                  /*
                    String tienpbvmt_text = "Phí BVMT:";
                    byteStream_in = Init_Text_In(byteStream_in,tienpbvmt_text,false,1,1,WoosimCmd.ALIGN_LEFT);
                   // byteStream_in = Init_Text_In_ptram(byteStream_in,false,1,1,WoosimCmd.ALIGN_LEFT);
                    String tienpbvmt_value_text = formatter.format(thue_bvmt) + " đ  " + "\r\n";
                    byteStream_in = Init_Text_In(byteStream_in,tienpbvmt_value_text,false,1,1,WoosimCmd.ALIGN_RIGHT);
                 */
                    // Dich vu thoat nuoc
                    String dichvuthoatnuoc_text = "Dịch vụ thoát nước:";
                    byteStream_in = Init_Text_In(byteStream_in,dichvuthoatnuoc_text,false,1,1,WoosimCmd.ALIGN_LEFT);
                    String dichvuthoatnuoc_value_text = formatter.format(tiendichvuthoatnuoc) + " đ  " + "\r\n";
                    byteStream_in = Init_Text_In(byteStream_in,dichvuthoatnuoc_value_text,false,1,1,WoosimCmd.ALIGN_RIGHT);

                    // Thue Dich vu thoat nuoc
                    String thuedichvuthoatnuoc_text = "Thuế DVTN 10 p.trăm:";
                    byteStream_in = Init_Text_In(byteStream_in,thuedichvuthoatnuoc_text,false,1,1,WoosimCmd.ALIGN_LEFT);
                    String thuedichvuthoatnuoc_value_text = formatter.format(thuetiendichvuthoatnuoc) + " đ  " + "\r\n";
                    byteStream_in = Init_Text_In(byteStream_in,thuedichvuthoatnuoc_value_text,false,1,1,WoosimCmd.ALIGN_RIGHT);

                    // tong
                    String tong_text = "Cộng:";
                    byteStream_in = Init_Text_In(byteStream_in,tong_text,false,1,1,WoosimCmd.ALIGN_LEFT);
                    String tong_value_text = formatter.format(tongtienky) + " đ  " + "\r\n";
                    byteStream_in = Init_Text_In(byteStream_in,tong_value_text,true,1,2,WoosimCmd.ALIGN_RIGHT);
                    if(covid){
                        String noidung_giamcovid =     "(Tiền nước trên đã được giảm 10 " +"\r\n"+
                                                       "phần trăm hỗ trợ do dịch Covid-19)" +"\r\n";

                        byteStream_in = Init_Text_In(byteStream_in,noidung_giamcovid,true,1,2,WoosimCmd.ALIGN_CENTER);
                        String tongcong_giam_text =    "Số tiền đã giảm: ";
                        byteStream_in = Init_Text_In(byteStream_in,tongcong_giam_text,true,1,1,WoosimCmd.ALIGN_LEFT);
                        String tongcong_giam_value_text = formatter.format(tongtiengiam) + " đ" + "\r\n";
                        byteStream_in = Init_Text_In(byteStream_in,tongcong_giam_value_text,true,1,2,WoosimCmd.ALIGN_RIGHT);
                    }
                    // ket thuc
                    byteStream_in = Init_Text_In(byteStream_in,ketthuc_text,true,1,1,WoosimCmd.ALIGN_LEFT);


                    // Neu có nợ

                    if (tiennos.getTiennos().length > 0) {
                        // ky no
                        String nocu_text = "Nợ cũ" + "\r\n";
                        byteStream_in = Init_Text_In(byteStream_in,nocu_text,true,1,1,WoosimCmd.ALIGN_LEFT);
                        for (int i = 0; i < tiennos.getTiennos().length; i++) {
                            String kyno_text = "Kỳ: " + tiennos.getTiennos()[i].getKy().toString() + "/" + tiennos.getTiennos()[i].getNam().toString();
                            byteStream_in = Init_Text_In(byteStream_in,kyno_text,false,1,1,WoosimCmd.ALIGN_LEFT);
                            int no = Integer.valueOf(tiennos.getTiennos()[i].getTienNo());
                            String kyno_value_text = formatter.format(no) + " đ" + "\r\n";
                            byteStream_in = Init_Text_In(byteStream_in,kyno_value_text,false,1,1,WoosimCmd.ALIGN_RIGHT);
                        }
                        // tong cong
                        String tongcong_text = "Tổng cộng: ";
                        byteStream_in = Init_Text_In(byteStream_in,tongcong_text,true,1,1,WoosimCmd.ALIGN_LEFT);
                        String tongcong_value_text = formatter.format(tongtien) + " đ  " + "\r\n";
                        byteStream_in = Init_Text_In(byteStream_in,tongcong_value_text,true,1,2,WoosimCmd.ALIGN_RIGHT);

                        //ket thuc
                        byteStream_in = Init_Text_In(byteStream_in,ketthuc_text,true,1,1,WoosimCmd.ALIGN_LEFT);
                    }

                    // nhan vien
                    String nhanvien_text = "Nhân viên: ";
                    byteStream_in = Init_Text_In(byteStream_in,nhanvien_text,false,1,1,WoosimCmd.ALIGN_LEFT);
                    String nhanvien_value_text = ten_nhanvien + "\r\n";
                    byteStream_in = Init_Text_In(byteStream_in,nhanvien_value_text,true,1,1,WoosimCmd.ALIGN_RIGHT);

                    // dt zalo nhan vien
                    String dt_zalo_nv_text = "ĐT hoặc Zalo: ";
                    byteStream_in = Init_Text_In(byteStream_in,dt_zalo_nv_text,false,1,1,WoosimCmd.ALIGN_LEFT);
                    String dt_zalo_nv_value_text = dienthoai_nhanvien + "\r\n";
                    byteStream_in = Init_Text_In(byteStream_in,dt_zalo_nv_value_text,true,1,1,WoosimCmd.ALIGN_RIGHT);

                    // ket thuc
                  //  byteStream_in = Init_Text_In(byteStream_in,ketthuc_text,true,1,1,WoosimCmd.ALIGN_LEFT);

                        // thong bao lan 2
                    if(tiennos.getTiennos().length >0) {
                        String thongbaolan2_text = "THÔNG BÁO LẦN 2" + "\r\n";
                        byteStream_in = Init_Text_In(byteStream_in,thongbaolan2_text,true,1,2,WoosimCmd.ALIGN_LEFT);
                        // String ketthuc_text =       "------------------------------------" + "\r\n";
                        String noidung_thongbao2 =     "Đề nghị khách hàng thanh toán tiền" +"\r\n"+
                                                       "nước do đã quá hạn. Trường hợp không" +"\r\n"+
                                                       "thanh toán. Công ty sẽ tạm ngừng " +"\r\n"+
                                                       "dịch vụ cấp nước kể từ ngày :"+"\r\n";
                        byteStream_in = Init_Text_In(byteStream_in,noidung_thongbao2,false,1,1,WoosimCmd.ALIGN_LEFT);
                        String noidungthongbao_text1 = date_thutien ;
                        byteStream_in = Init_Text_In(byteStream_in,noidungthongbao_text1,true,1,1,WoosimCmd.ALIGN_LEFT);
                        String noidung_thongbao2_1 =   " và ngừng"+"\r\n"+
                                                       "dịch vụ cấp nước sau 05 tuần."+"\r\n";
                        byteStream_in = Init_Text_In(byteStream_in,noidung_thongbao2_1,false,1,1,WoosimCmd.ALIGN_LEFT);
                    }

                    else {
                       // String thongbaolan2_text = "THÔNG BÁO" + "\r\n";
                       // byteStream_in = Init_Text_In(byteStream_in,thongbaolan2_text,true,1,2,WoosimCmd.ALIGN_LEFT);
                        String noidungthongbao_text = "Quý khách vui lòng thanh toán trong"+"\r\n" +
                                                      "vòng 07 ngày kể từ ngày: " ;
                        byteStream_in = Init_Text_In(byteStream_in,noidungthongbao_text,false,1,1,WoosimCmd.ALIGN_LEFT);
                        String noidungthongbao_text1 = date_thutien +"\r\n";
                        byteStream_in = Init_Text_In(byteStream_in,noidungthongbao_text1,true,1,1,WoosimCmd.ALIGN_LEFT);

                    }
                    byteStream_in = Init_Text_In(byteStream_in,ketthuc_text,false,1,1,WoosimCmd.ALIGN_LEFT);

                       // String ketthuc_text =       "------------------------------------" + "\r\n";

                    /*
                        String noidungthongbao_text2 ="Để biết thêm thông tin chi tiết xin" +"\r\n" +
                                                      "vui lòng liên hệ tổng đài :"+"\r\n";

                     */
                       String noidungthongbao_text2 = "Quý khách hàng vui lòng truy cập" +"\r\n" +
                                                      "website: ";
                        byteStream_in = Init_Text_In(byteStream_in,noidungthongbao_text2,false,1,1,WoosimCmd.ALIGN_LEFT);
                    String noidungthongbao_text3 =    "http://capnuoccholon.com.vn"+"\r\n" ;
                    byteStream_in = Init_Text_In(byteStream_in,noidungthongbao_text3,true,1,1,WoosimCmd.ALIGN_LEFT);
                    String noidungthongbao_text4 =    "hoặc liên hệ Tổng đài: ";
                     byteStream_in = Init_Text_In(byteStream_in,noidungthongbao_text4,false,1,1,WoosimCmd.ALIGN_LEFT);
                         String noidungthongbao_text5 = "0865.851.088" +"\r\n";
                       byteStream_in = Init_Text_In(byteStream_in,noidungthongbao_text5,true,1,1,WoosimCmd.ALIGN_LEFT);
                       String noidungthongbao_text6 =    "khi có yêu cầu:"+"\r\n" ;
                       byteStream_in = Init_Text_In(byteStream_in,noidungthongbao_text6,false,1,1,WoosimCmd.ALIGN_LEFT);

                    String noidungthongbao_text7=     "- Tra cứu thông tin chi tiết hoá đơn"+"\r\n" +
                                                      "tiền nước." + "\r\n"+
                                                      "- Cập nhật thay đổi thông tin số" + "\r\n"+
                                                      "điện thoại trong trường hợp số" + "\r\n"+
                                                      "ĐT(đã đăng ký):";
                        byteStream_in = Init_Text_In(byteStream_in,noidungthongbao_text7,false,1,1,WoosimCmd.ALIGN_LEFT);
                    String noidungthongbao_text8 =    dienthoai+"\r\n" ;
                    byteStream_in = Init_Text_In(byteStream_in,noidungthongbao_text8,true,1,1,WoosimCmd.ALIGN_LEFT);
                    String noidungthongbao_text9=     "là không đúng."+"\r\n" ;

                    byteStream_in = Init_Text_In(byteStream_in,noidungthongbao_text9,true,1,1,WoosimCmd.ALIGN_LEFT);
                    byteStream_in = Init_Text_In(byteStream_in,ketthuc_text,false,1,1,WoosimCmd.ALIGN_LEFT);
                    // luu y
                    String luuy_text = "Lưu ý: " + "\r\n";
                    byteStream_in = Init_Text_In(byteStream_in,luuy_text,true,1,1,WoosimCmd.ALIGN_LEFT);


                    String    luuy_value_text = " Trường hợp khách hàng đã thanh\n" +
                                                "toán nợ cũ trước ngày nhận giấy\n" +
                                                "báo này. Xin chỉ thanh toán hóa\n" +
                                                "đơn kỳ mới.\n" +"\r\n"+"\r\n";


                    byteStream_in = Init_Text_In(byteStream_in,luuy_value_text,false,1,1,WoosimCmd.ALIGN_LEFT);


                    byteStream_in.write(WoosimCmd.printData());
                    sendData(WoosimCmd.initPrinter());
                    sendData(byteStream_in.toByteArray());
                }else {
                    Dialog_App dialog_app = new Dialog_App(activity_Danhsachdocso_chitiet_khachhang.this);
                    String text_notification = "Vui lòng kiểm tra hóa đơn rồi in GIẤY BÁO"+"\r\n"+"Xin Cám Ơn";
                    dialog_app.Dialog_Notification(text_notification);
                }
            }
        }

    }
    private void Print_HoaDonCnCl_Woosim_Trongai(String tinhtrang) throws IOException {
        String nam = String.valueOf(Nam);
        String ky = String.valueOf(Ky);
        String dot = String.valueOf(Dot);
        String somay = String.valueOf(SoMay);
        int tongtienky =0 ;
        DecimalFormat formatter = new DecimalFormat("###,###,###");
        SimpleDateFormat format_datetime = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat format_datetime_display = new SimpleDateFormat("yyyy/MM/dd");
        // hien thi thong tin doc so
        sqlite_DanhSachKhackHang_DocSo sqlite_danhSachKhackHang_docSo = new sqlite_DanhSachKhackHang_DocSo(activity_Danhsachdocso_chitiet_khachhang.this,nam,ky,dot,somay);
        if(sqlite_danhSachKhackHang_docSo.getStatus_Table_DanhSachKhachHang_Exists()){
           // List<data_Danhsachdocso_chitiet> khachhangs = sqlite_danhSachKhackHang_docSo.Get_DanhSachDocSo_Chitiet();
            data_danhsachdocso_chitiet_khachhang khachhang =  sqlite_danhSachKhackHang_docSo.Get_DanhSachDocSo_Chitiet_KhachHang(Danhba);
            if(khachhang.getId() != 0) {

                    String hoten_khachhang = khachhang.getHoten();
                    String sonha_cu = khachhang.getSonha().toString().trim();
                    String sonha_moi = khachhang.getSonha_moi().toString().trim();
                    String diachi = sonha_cu + " " + khachhang.getDuong();
                    if(!sonha_moi.isEmpty()){
                        diachi = sonha_cu +" ("+sonha_moi+")"+ "\r\n" + khachhang.getDuong();
                    }
                    String giabieu = khachhang.getGiabieu();
                    String dinhmuc = khachhang.getDinhmuc();

                    String code = get_Codetinhtrang(tinhtrang,Code_old);
                    if(Status_Sync.equals("3")){
                        code = khachhang.getCodemoi();
                    }
                    String tungay = khachhang.getTungay();
                    String denngay = khachhang.getDenngay();
                    // In
                    String ten_nhanvien = "";
                    String dienthoai_nhanvien = "";
                    sqlite_User sqlite_user = new sqlite_User(activity_Danhsachdocso_chitiet_khachhang.this);
                    if (sqlite_user.getStatus_Table_User_Exists()) {
                        data_User nhanvien = sqlite_user.Get_User("1");
                        ten_nhanvien = nhanvien.getTennhanvien().toString().trim();
                        dienthoai_nhanvien = nhanvien.getdienthoai().toString().trim();
                    }
                    ByteArrayOutputStream byteStream_in = new ByteArrayOutputStream();
                    // header
                // header
                String header_text = "CÔNG TY CỔ PHẦN CẤP NƯỚC CHỢ LỚN" + "\r\n" ;

                byteStream_in = Init_Text_In(byteStream_in,header_text,true,1,2,WoosimCmd.ALIGN_CENTER);
                String header_add_text = "97 Phạm Hữu Chí P. 12 Q. 5" + "\r\n" ;
                byteStream_in = Init_Text_In(byteStream_in,header_add_text,false,1,1,WoosimCmd.ALIGN_CENTER);
                String header_tongdai =   "Tổng đài: 0865.851.088" + "\r\n" ;
                byteStream_in = Init_Text_In(byteStream_in,header_tongdai,false,1,1,WoosimCmd.ALIGN_CENTER);
                String header_gachngang =       "------------" + "\r\n";
                byteStream_in = Init_Text_In(byteStream_in,header_gachngang,true,1,1,WoosimCmd.ALIGN_CENTER);

                /// ngay ghi
                Date currentTime = Calendar.getInstance().getTime();
                String currentDate = new SimpleDateFormat("dd/MM/yyyy",Locale.getDefault()).format(currentTime);
                String ngayghi_text = "Ngày gửi giấy báo: " + currentDate + "\r\n";
                byteStream_in = Init_Text_In(byteStream_in,ngayghi_text,false,1,1,WoosimCmd.ALIGN_CENTER);
                // title
                String title_text = "GIẤY BÁO TRỞ NGẠI ĐỌC SỐ NƯỚC" + "\r\n" ;

                byteStream_in = Init_Text_In(byteStream_in,title_text,true,1,2,WoosimCmd.ALIGN_CENTER);
                // ky_text

                String ky_value_text = "Kỳ: "+ Ky + "/" + Nam + "\r\n";
                byteStream_in = Init_Text_In(byteStream_in,ky_value_text,false,1,1,WoosimCmd.ALIGN_LEFT);

                //tungay-> den ngay
                String[] com_tungay = tungay.split("T");
                String[] com_denngay = denngay.split("T");

                String tungay_new = ToDate_Display((com_tungay[0]));
                String denngay_new = ToDate_Display((com_denngay[0]));

                String tungay_text = "Từ ngày: " + tungay_new.trim();

                String denngay_text = tungay_text+ " -> " + denngay_new.trim() + "\r\n";
                byteStream_in = Init_Text_In(byteStream_in,denngay_text,false,1,1,WoosimCmd.ALIGN_CENTER);

                // Danh ba
                String danhba_text = "Danh bạ:";
                byteStream_in = Init_Text_In(byteStream_in,danhba_text,false,1,1,WoosimCmd.ALIGN_LEFT);
                String danhba1 = Danhba.substring(0,4);
                String danhba2 = Danhba.substring(4,7);
                String danhba3 = Danhba.substring(7);
                String danhba_value_text = danhba1 +" "+danhba2 +" "+danhba3 +"  "+"\r\n";
                byteStream_in = Init_Text_In(byteStream_in,danhba_value_text,true,1,3,WoosimCmd.ALIGN_RIGHT);

                // Khanh hang
                String khachhang_text = "KH:";
                byteStream_in = Init_Text_In(byteStream_in,khachhang_text,false,1,1,WoosimCmd.ALIGN_LEFT);
                String khachhang_value_text = hoten_khachhang.toUpperCase() + "\r\n";
                byteStream_in = Init_Text_In(byteStream_in,khachhang_value_text,false,1,1,WoosimCmd.ALIGN_LEFT);

                //Dia chi
                String diachi_text = "Đ/C:";
                byteStream_in = Init_Text_In(byteStream_in,diachi_text,false,1,1,WoosimCmd.ALIGN_LEFT);
                String diachi_value_text = diachi.toUpperCase() + "\r\n";
                byteStream_in = Init_Text_In(byteStream_in,diachi_value_text,false,1,1,WoosimCmd.ALIGN_LEFT);

                String ketthuc_text = "------------------------------------" + "\r\n";

                //Gia bieu , Dinh muc, Code
                String giabieu_text = "GB: " ;
                byteStream_in = Init_Text_In(byteStream_in,giabieu_text,false,1,1,WoosimCmd.ALIGN_LEFT);
                String giabieu_value = giabieu + "      ";
                byteStream_in = Init_Text_In(byteStream_in,giabieu_value,true,1,1,WoosimCmd.ALIGN_LEFT);
                String dinhmuc_text = "ĐM: " ;
                byteStream_in = Init_Text_In(byteStream_in,dinhmuc_text,false,1,1,WoosimCmd.ALIGN_LEFT);
                String dinhmuc_value = dinhmuc + "      ";
                byteStream_in = Init_Text_In(byteStream_in,dinhmuc_value,true,1,1,WoosimCmd.ALIGN_LEFT);
                String code_text = "Code: " ;
                byteStream_in = Init_Text_In(byteStream_in,code_text,false,1,1,WoosimCmd.ALIGN_LEFT);
                String code_value = code + "\r\n";
                byteStream_in = Init_Text_In(byteStream_in,code_value,true,1,1,WoosimCmd.ALIGN_LEFT);

                //    String ketthuc_text = "------------------------------------" + "\r\n";
                    byteStream_in = Init_Text_In(byteStream_in,ketthuc_text,true,1,1,WoosimCmd.ALIGN_LEFT);
                    if(tinhtrang.equals("Thay Đ.cửa")){
                       // String ketthuc_text =                    "------------------------------------" + "\r\n";
                        String noidungthongbao_thay_dongcua_text = "Do đồng hồ nước mới thay"+"\r\n"+
                                                      "không xem được chỉ số nước. Công ty"+"\r\n"+
                                                      "sẽ tạm tính tiêu thụ bằng bình quân"+"\r\n"+
                                                      "của 3 kỳ gần nhất."+"\r\n";
                        byteStream_in = Init_Text_In(byteStream_in,noidungthongbao_thay_dongcua_text,true,1,1,WoosimCmd.ALIGN_LEFT);
                    }else  if(tinhtrang.equals("Chất đồ")){
                        //    builder.append((("--------------------------------"+"\r\n").getBytes()));
                              // String ketthuc_text =       "------------------------------------" + "\r\n";
                        String noidungthongbao_chatdo_text = "Do vị trí đồng hồ nước chất đồ"+"\r\n"+
                                                             "không xem được chỉ số nước. Công"+"\r\n"+
                                                             "ty sẽ tạm tính tiêu thụ bằng bình"+"\r\n"+
                                                             "quân của 3 kỳ gần nhất."+"\r\n";
                        byteStream_in = Init_Text_In(byteStream_in,noidungthongbao_chatdo_text,true,1,1,WoosimCmd.ALIGN_LEFT);
                    }else  if(tinhtrang.equals("Ngập nước")){
                        //    builder.append((("--------------------------------"+"\r\n").getBytes()));
                              // String ketthuc_text =        "------------------------------------" + "\r\n";
                        String noidungthongbao_ngapnuoc_text ="Do vị trí đồng hồ nước ngập nước"+"\r\n"+
                                                              "không xem được chỉ số nước. Công"+"\r\n"+
                                                              "ty sẽ tạm tính tiêu thụ bằng bình"+"\r\n"+
                                                              "quân của 3 kỳ gần nhất."+"\r\n";
                        byteStream_in = Init_Text_In(byteStream_in,noidungthongbao_ngapnuoc_text,true,1,1,WoosimCmd.ALIGN_LEFT);
                    }
                    else  if(tinhtrang.equals("Lấp")){
                        //     builder.append((("--------------------------------"+"\r\n").getBytes()));
                           // String ketthuc_text =       "------------------------------------" + "\r\n";
                        String noidungthongbao_lap_text = "Do vị trí đồng hồ nước bị lấp không"+"\r\n"+
                                                          "xem được chỉ số nước. Công ty sẽ"+"\r\n"+
                                                          "tạm tính tiêu thụ bằng bình quân"+"\r\n"+
                                                          "của 3 kỳ gần nhất."+"\r\n";
                        byteStream_in = Init_Text_In(byteStream_in,noidungthongbao_lap_text,true,1,1,WoosimCmd.ALIGN_LEFT);
                    }
                    else  if(tinhtrang.equals("Đóng cửa")){
                        //   builder.append((("--------------------------------"+"\r\n").getBytes()));
                           // String ketthuc_text =           "------------------------------------" + "\r\n";
                        String noidungthongbao_dongcua_text = "Do nhà khách hàng đóng cửa. Đề nghị"+"\r\n"+
                                                              "khách hàng vui lòng báo chỉ số nước"+"\r\n"+
                                                              "gấp trong ngày để tính tiêu thụ."+"\r\n";
                                byteStream_in = Init_Text_In(byteStream_in,noidungthongbao_dongcua_text,true,1,1,WoosimCmd.ALIGN_LEFT);

                    }else  if(tinhtrang.equals("Kẹt khóa")){
                           // String ketthuc_text =           "------------------------------------" + "\r\n";
                        String noidungthongbao_dongcua_text = "Do vị trí đồng hồ nước kẹt khóa. "+"\r\n"+
                                                              "Đề nghị khách hàng vui lòng báo "+"\r\n"+
                                                              "chỉ số nước gấp trong ngày để tính"+"\r\n"+
                                                              "tiêu thụ."+"\r\n";
                        byteStream_in = Init_Text_In(byteStream_in,noidungthongbao_dongcua_text,true,1,1,WoosimCmd.ALIGN_LEFT);
                    }
                    // nhan vien
                    // String ketthuc_te ="------------------------------------" + "\r\n";
                    String thonbao_text ="Để biết thông tin chi tiết chính xác"+"\r\n" +
                                          "tiền nước phải thanh toán. Quý khách"+"\r\n" +
                                          "hàng vui lòng liên hệ tổng đài:"+"\r\n" ;
                    byteStream_in = Init_Text_In(byteStream_in,thonbao_text,false,1,1,WoosimCmd.ALIGN_LEFT);
                    String tongdai =      "0865.851.088 hoặc truy cập vào"+"\r\n";
                    byteStream_in = Init_Text_In(byteStream_in,tongdai,true,1,1,WoosimCmd.ALIGN_LEFT);
                    String thonbao_text1 ="website: http://capnuoccholon.com.vn";
                    byteStream_in = Init_Text_In(byteStream_in,thonbao_text1,true,1,1,WoosimCmd.ALIGN_LEFT);
                    if(tinhtrang.equals("Đóng cửa")|| tinhtrang.equals("Kẹt khóa")) {
                        String thonbao_text2 = "Nếu quý khách không thông báo, công" + "\r\n" +
                                               "ty sẽ tạm tính tiêu thụ bình quân 3" + "\r\n" +
                                               "kỳ gần nhát." + "\r\n";
                        byteStream_in = Init_Text_In(byteStream_in,thonbao_text2,false,1,1,WoosimCmd.ALIGN_LEFT);
                    }
                   byteStream_in = Init_Text_In(byteStream_in,ketthuc_text,false,1,1,WoosimCmd.ALIGN_LEFT);
                    String nhanvien_text = "Nhân viên: ";
                    byteStream_in = Init_Text_In(byteStream_in,nhanvien_text,false,1,1,WoosimCmd.ALIGN_LEFT);
                    String nhanvien_value_text = ten_nhanvien + "\r\n";
                    byteStream_in = Init_Text_In(byteStream_in,nhanvien_value_text,false,1,2,WoosimCmd.ALIGN_RIGHT);

                    // dt zalo nhan vien
                    String dt_zalo_nv_text = "ĐT hoặc Zalo: ";
                    byteStream_in = Init_Text_In(byteStream_in,dt_zalo_nv_text,false,1,1,WoosimCmd.ALIGN_LEFT);
                    String dt_zalo_nv_value_text = dienthoai_nhanvien + "\r\n"+ "\r\n" + "\r\n";;
                    byteStream_in = Init_Text_In(byteStream_in,dt_zalo_nv_value_text,false,1,2,WoosimCmd.ALIGN_RIGHT);
                  //  ketthuc_text = ketthuc_text + "\r\n";
                   // byteStream_in = Init_Text_In(byteStream_in,ketthuc_text,false,1,1,WoosimCmd.ALIGN_LEFT);
                    byteStream_in.write(WoosimCmd.printData());
                    sendData(WoosimCmd.initPrinter());
                    sendData(byteStream_in.toByteArray());

            }
        }

    }
    private void Print_HoaDonCnCl_Star(StarIoExt.Emulation emulation) throws IOException {
        String nam = String.valueOf(Nam).trim();
        String ky = String.valueOf(Ky).trim();
        String dot = String.valueOf(Dot).trim();
        String somay = String.valueOf(SoMay);
        long tongtienky =0 ;
        DecimalFormat formatter = new DecimalFormat("###,###,###");
        SimpleDateFormat format_datetime = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat format_datetime_display = new SimpleDateFormat("yyyy/MM/dd");
        // hien thi thong tin doc so
        sqlite_DanhSachKhackHang_DocSo sqlite_danhSachKhackHang_docSo = new sqlite_DanhSachKhackHang_DocSo(activity_Danhsachdocso_chitiet_khachhang.this,nam,ky,dot,somay);
        if(sqlite_danhSachKhackHang_docSo.getStatus_Table_DanhSachKhachHang_Exists()){
            List<data_Danhsachdocso_chitiet> khachhangs = sqlite_danhSachKhackHang_docSo.Get_DanhSachDocSo_Chitiet();
            data_danhsachdocso_chitiet_khachhang khachhang =  sqlite_danhSachKhackHang_docSo.Get_DanhSachDocSo_Chitiet_KhachHang(Danhba);
            if(khachhang.getId() != 0) {
                String tieuthu = tv_tieuthu.getText().toString().trim();
                if(!tieuthu.equals("")) {
                    String hoten_khachhang = khachhang.getHoten();
                    String sonha_cu = khachhang.getSonha().toString().trim();
                    String sonha_moi = khachhang.getSonha_moi().toString().trim();
                    String diachi = sonha_cu + " " + khachhang.getDuong();
                    if(!sonha_moi.isEmpty()){
                        diachi = sonha_cu +" ("+sonha_moi+")"+ "\r\n" + khachhang.getDuong();
                    }
                    String giabieu = khachhang.getGiabieu();
                    String dinhmuc = khachhang.getDinhmuc();
                    String tinhtrang = spn_tinhtrang.getSelectedItem().toString().trim();
                    String code = get_Codetinhtrang(tinhtrang,Code_old);
                    if(Status_Sync.equals("3")){
                        code = khachhang.getCodemoi();
                    }
                    String csc = khachhang.getCscu();
                    String csm = edt_csm.getText().toString().trim();
                    String tungay = khachhang.getTungay();
                    String denngay = khachhang.getDenngay();
                    String dinhmucngheo = khachhang.getDinhmucngheo();
                    String tldv = khachhang.getTiledv();
                    String tlhcsn = khachhang.getTilehcsn();
                    String tlsh = khachhang.getTilesh();
                    String tlsx = khachhang.getTilesx();
                    int tieuthu_i = 0;
                    try {
                        tieuthu_i = Integer.parseInt(tieuthu);
                    }catch(Exception e) {}

                    if(tieuthu_i < 0){
                        Dialog_App dialog_app = new Dialog_App(activity_Danhsachdocso_chitiet_khachhang.this);
                        dialog_app.Dialog_Notification("Tiêu thụ âm không chính xác"+"\r\n"+"Vui lòng kiểm tra lại");
                        return;
                    }

                    int giabieu_i = 0;
                    try {
                        giabieu_i = Integer.parseInt(giabieu);
                    }catch (Exception e){}

                    int dinhmuc_i  = 0;
                    try {
                        dinhmuc_i = Integer.parseInt(dinhmuc);
                    }catch (Exception e){}


                    int dinhmucngheo_i = 0;
                    try {
                        dinhmucngheo_i = Integer.parseInt(dinhmucngheo);
                    }catch (Exception e){}


                    int tl_kd_i = 0;
                    try {
                        tl_kd_i= Integer.parseInt(tldv);
                    }catch (Exception e){}


                    int tl_hcsn_i = 0;
                    try {
                        tl_hcsn_i =  Integer.parseInt(tlhcsn);
                    }catch (Exception e){}


                    int tl_sh_i =0;
                    try {
                        tl_sh_i=  Integer.parseInt(tlsh);
                    }catch (Exception e){}


                    int tl_sx_i =0;
                    try {
                        tl_sx_i=  Integer.parseInt(tlsx);
                    }catch (Exception e){};


                    Thanhtoan_tiennuoc thanhtoan_tiennuoc = new Thanhtoan_tiennuoc();
                    data_Tiennuoc data_tiennuoc = thanhtoan_tiennuoc.Tiennuoc(tieuthu_i,giabieu_i,dinhmuc_i,dinhmucngheo_i,tl_kd_i,tl_hcsn_i,tl_sh_i,tl_sx_i,tungay,denngay);
                    long tiennuoc =  (long) Long.parseLong(data_tiennuoc.getTiennuoc());
                    long thue_5 = 0;
                    long thue_bvmt = 0;
                    long tiendichvuthoatnuoc = 0;
                    long thuetiendichvuthoatnuoc =0;
                    boolean covid = false;
                    long tongtiengiam = 0 ;
                    long pre_tiennuoc = tiennuoc;
                    if(nam.equals("2021") && (ky.equals("9")|| ky.equals("10") || ky.equals("11"))){
                        // DICH GIAM 10 %
                        // CHO GB: 11-19, 21-29, 51, 56, 57, 59
                        if(
                                (11<=giabieu_i && giabieu_i<=19) ||
                                        (21<=giabieu_i && giabieu_i<=29) ||
                                        (giabieu_i == 51 || giabieu_i == 56 || giabieu_i == 59)
                        ){

                            tiennuoc = (long)(tiennuoc*0.9);
                            thue_5 = (long) Math.round(tiennuoc * 0.05);
                            String enble_thuevbmt = khachhang.getKhongtinhphi_bvmt().trim();

                            if(enble_thuevbmt.equals("0") || enble_thuevbmt.isEmpty()) {
                                if(giabieu_i == 51 || giabieu_i == 56 || giabieu_i == 59){
                                    thue_bvmt = (long) thanhtoan_tiennuoc.Tiennuoc_ThueBVMT_Covid(tieuthu_i,giabieu_i,dinhmuc_i,dinhmucngheo_i,tl_kd_i,tl_hcsn_i,tl_sh_i,tl_sx_i);
                                }else {
                                    thue_bvmt = (long) Math.round(tiennuoc * 0.1);
                                }
                            }
                            covid = true;


                        }else {

                            thue_5 = Long.parseLong(data_tiennuoc.getThuetiennuoc_5());
                            String enble_thuevbmt = khachhang.getKhongtinhphi_bvmt().trim();

                            if(enble_thuevbmt.equals("0") || enble_thuevbmt.isEmpty()) {
                                thue_bvmt = Long.parseLong(data_tiennuoc.getPhiBVMT());
                            }
                        }
                    }else {
                        thue_5 = (long) Long.parseLong(data_tiennuoc.getThuetiennuoc_5());
                        String enble_thuevbmt = khachhang.getKhongtinhphi_bvmt().trim();

                        if(enble_thuevbmt.equals("0") || enble_thuevbmt.isEmpty()) {
                            thue_bvmt =Long.parseLong(data_tiennuoc.getPhiBVMT());
                            tiendichvuthoatnuoc = (long) Long.parseLong(data_tiennuoc.getDichvuthoatnuoc());
                            thuetiendichvuthoatnuoc = (long) Long.parseLong(data_tiennuoc.getThuedichvuthoatnuoc());
                        }

                    }

                    tongtienky = tiennuoc + thue_5 + thue_bvmt + tiendichvuthoatnuoc +thuetiendichvuthoatnuoc;

                    long thue_chuagiam =  (long) Math.round(pre_tiennuoc * 0.05);
                    long thue_mt_chuagiam = (long) Math.round(pre_tiennuoc * 0.1);
                    long tongtiennuoc_chuagiam = pre_tiennuoc+thue_chuagiam+thue_mt_chuagiam;
                    tongtiengiam = tongtiennuoc_chuagiam - tongtienky ;
                    sqlite_TienNo sqlite_tienNo = new sqlite_TienNo(activity_Danhsachdocso_chitiet_khachhang.this,nam,ky,dot,somay);
                    long tongtienno = 0;
                    data_tiennos tiennos = new data_tiennos(Danhba);
                    if (sqlite_tienNo.getStatus_Table_DanhSachHangHang_TienNo_Exists()) {
                        tiennos = sqlite_tienNo.Get_Data_TienNo(Danhba);
                        for (int i = 0; i < tiennos.getTiennos().length; i++) {
                            String tienno_ky = tiennos.getTiennos()[i].getTienNo();
                            tongtienno = tongtienno + (long)Long.parseLong(tienno_ky);
                        }
                    }
                    long tongtien = (long)tongtienky + (long) tongtienno;
                    String ten_nhanvien = "";
                    String dienthoai_nhanvien = "";
                    sqlite_User sqlite_user = new sqlite_User(activity_Danhsachdocso_chitiet_khachhang.this);
                    if (sqlite_user.getStatus_Table_User_Exists()) {
                        data_User nhanvien = sqlite_user.Get_User("1");
                        ten_nhanvien = nhanvien.getTennhanvien().toString().trim();
                        dienthoai_nhanvien = nhanvien.getdienthoai().toString().trim();
                    }
                    /// ngay thu tien nuoc
                    SimpleDateFormat format_datetime_chukyds = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                    Date date_denngay = null;
                    try {
                        date_denngay = format_datetime_chukyds.parse(denngay);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    Calendar calendar_denngay = Calendar.getInstance();
                    calendar_denngay.setTime(date_denngay);
                    if(dot.trim().equals("1")|| dot.trim().equals("2")) {
                        calendar_denngay.add(Calendar.DATE,4);
                        int thu = calendar_denngay.get(Calendar.DAY_OF_WEEK);
                        if (thu == 7) {
                            calendar_denngay.add(Calendar.DATE,2);
                        } else if (thu == 1){
                            calendar_denngay.add(Calendar.DATE,1);
                        }
                    }else {
                        calendar_denngay.add(Calendar.DATE,3);
                        int thu = calendar_denngay.get(Calendar.DAY_OF_WEEK);
                        if (thu == 7) {
                            calendar_denngay.add(Calendar.DATE,2);
                        } else if (thu == 1){
                            calendar_denngay.add(Calendar.DATE,1);
                        }
                    }
                    SimpleDateFormat hienthi_ngay = new SimpleDateFormat("dd/MM/yyyy");
                    String date_thutien = hienthi_ngay.format(calendar_denngay.getTime());

                    // In
                    ICommandBuilder builder = StarIoExt.createCommandBuilder(emulation);

                    builder.beginDocument();

                    // header
                    builder.appendAlignment(ICommandBuilder.AlignmentPosition.Center);
                    builder.appendFontStyle(ICommandBuilder.FontStyleType.B);
                    builder.appendMultiple(("CÔNG TY CPCN CHỢ LỚN\r\n").getBytes(),2,1);
                    builder.appendFontStyle(ICommandBuilder.FontStyleType.A);
                    builder.append(("97 Phạm Hữu Chí P.12 Q.5\r\n" +
                            "Tổng đài : 0865.851.088\r\n" +
                            "------------\r\n").getBytes());
                    /// ngay ghi
                    Date currentTime = Calendar.getInstance().getTime();
                    String currentDate = new SimpleDateFormat("dd/MM/yyyy",Locale.getDefault()).format(currentTime);
                    String ngayghi_text = "Ngày gửi giấy báo: " + currentDate+"\r\n" ;
                    builder.appendAlignment(ICommandBuilder.AlignmentPosition.Left);
                    builder.append((ngayghi_text.getBytes()));
                    // title
                    builder.appendAlignment(ICommandBuilder.AlignmentPosition.Center);
                    builder.appendFontStyle(ICommandBuilder.FontStyleType.A);
                    builder.appendMultiple(("GIẤY BÁO TIỀN NƯỚC\r\n" +
                            "KHÔNG THAY THẾ HÓA ĐƠN").getBytes(),1,2);
                    builder.append(("\r\n").getBytes());
                    // ky nam
                    builder.appendAlignment(ICommandBuilder.AlignmentPosition.Left);
                    builder.appendFontStyle(ICommandBuilder.FontStyleType.A);
                    String ky_text ="Kỳ: " + Ky + "/" + Nam +"\r\n";
                    builder.append(ky_text.getBytes());
                    // tu ngay ->den ngay
                    String[] com_tungay = tungay.split("T");
                    String[] com_denngay = denngay.split("T");

                    String tungay_new = ToDate_Display((com_tungay[0]));
                    String denngay_new = ToDate_Display((com_denngay[0]));

                    builder.appendAlignment(ICommandBuilder.AlignmentPosition.Center);
                    builder.appendFontStyle(ICommandBuilder.FontStyleType.A);
                    String tungay_text = "Từ ngày: " + tungay_new.trim();
                    String denngay_text = tungay_text+"->" + denngay_new.trim() +"\r\n";
                    builder.append((denngay_text.getBytes()));
                    // danh ba
                    builder.appendAlignment(ICommandBuilder.AlignmentPosition.Left);
                    builder.appendAbsolutePosition(("Danh bạ:").getBytes(),0);
                    builder.appendAbsolutePosition(200);
                    String danhba1 = Danhba.substring(0,4);
                    String danhba2 = Danhba.substring(4,7);
                    String danhba3 = Danhba.substring(7);
                    String danhba_in = danhba1 +" "+danhba2 +" "+danhba3 +"\r\n";
                    builder.appendMultiple(danhba_in.getBytes(),1,2);
                    // khach hang
                    builder.appendAlignment(ICommandBuilder.AlignmentPosition.Left);
                    String khachhang_value = "KH: "+hoten_khachhang +"\r\n";
                    builder.append(khachhang_value.getBytes());
                    // dia chi khachhang.getDuong()
                    String diachi_value ="Đ/C: "+diachi+"\r\n";
                    builder.append(diachi_value.getBytes());

                    builder.appendAlignment(ICommandBuilder.AlignmentPosition.Left);
                    //Gia bieu , Dinh muc, Code
                    String giabieu_text = "GB: " ;
                    String dinhmuc_text = "ĐM: "  ;
                    String code_text = "Code: " ;

                    builder.appendAbsolutePosition(giabieu_text.getBytes(),0);
                    builder.appendAbsolutePosition(48);
                    builder.appendMultiple(giabieu.getBytes(),2,1);
                    builder.appendAbsolutePosition(dinhmuc_text.getBytes(),128);
                    builder.appendAbsolutePosition(176);
                    builder.appendMultiple(dinhmuc.getBytes(),2,1);
                    builder.appendAbsolutePosition(code_text.getBytes(),256);
                    builder.appendAbsolutePosition(328);
                    builder.appendMultiple((code+"\r\n").getBytes(),2,1);



                    // CS moi -CS cu
                    String csm_text = "Cs mới: "+csm;
                    String csc_text = "Cs cũ: "+csc +"\r\n" ;;
                    builder.appendAlignment(ICommandBuilder.AlignmentPosition.Left);
                    builder.appendAbsolutePosition(csm_text.getBytes(),0);

                    builder.appendAbsolutePosition(csc_text.getBytes(),200);


                    int lenght =0;
                    // tieu thu
                    builder.appendFontStyle(ICommandBuilder.FontStyleType.A);
                    builder.appendAlignment(ICommandBuilder.AlignmentPosition.Right);
                    String tieuthu_value = tieuthu.toString() + " m3 ";
                    builder.append(("Tiêu thụ:").getBytes());
                    lenght = 32 - tieuthu_value.length();
                    builder.appendAbsolutePosition(lenght*12);
                    builder.appendMultiple((tieuthu_value+"\r\n").getBytes(),1,2);
                    // tien nuoc
                    String tiennuoc_value = formatter.format(tiennuoc) +" đ " ;
                    builder.append(("Tiền nước:").getBytes());
                    lenght = 32 - tiennuoc_value.length();
                    builder.appendAbsolutePosition(lenght*12);
                    builder.append((tiennuoc_value +"\r\n").getBytes());

                    // tien thue
                    String tienthue_value = formatter.format(thue_5) +" đ ";
                    builder.append(("Thuế tiền nước 5%:").getBytes());
                    lenght = 32 - tienthue_value.length();
                    builder.appendAbsolutePosition(lenght*12);
                    builder.append((tienthue_value+"\r\n").getBytes());

                    // phi bao ve moi truong
                    /*
                    String tienpbvmt_value =formatter.format(thue_bvmt) +" đ ";
                    builder.append(("Phí BVMT:").getBytes());
                    lenght = 32 - tienpbvmt_value.length();
                    builder.appendAbsolutePosition(lenght*12);
                    builder.append((tienpbvmt_value+"\r\n").getBytes());
*/
                    // dich vu thoat nuoc
                    String dichvuthoatnuoc_value =formatter.format(tiendichvuthoatnuoc) +" đ ";
                    builder.append(("Dich vụ thoát nước:").getBytes());
                    lenght = 32 - dichvuthoatnuoc_value.length();
                    builder.appendAbsolutePosition(lenght*12);
                    builder.append((dichvuthoatnuoc_value+"\r\n").getBytes());

                    // thue dich vu thoat nuoc
                    String thuedichvuthoatnuoc_value =formatter.format(thuetiendichvuthoatnuoc) +" đ ";
                    builder.append(("Thuế DVTN 10%:").getBytes());
                    lenght = 32 - thuedichvuthoatnuoc_value.length();
                    builder.appendAbsolutePosition(lenght*12);
                    builder.append((thuedichvuthoatnuoc_value+"\r\n").getBytes());

                    // tong

                    String tong_value = formatter.format(tongtienky)  +" đ ";
                    builder.append(("Cộng:").getBytes());
                    lenght = 32 - tong_value.length();
                    builder.appendAbsolutePosition(lenght*12);
                    builder.appendMultiple((tong_value+"\r\n").getBytes(),1,2);

                    // giam covid
                    if(covid){
                        builder.appendAlignment(ICommandBuilder.AlignmentPosition.Center);
                        String title_covid = "(Tiền nước trên đã được giảm" +"\r\n"+
                                             "10% hỗ trợ do dịch Covid-19)"+"\r\n";
                        builder.appendMultiple((title_covid).getBytes(),1,2);
                        builder.appendAlignment(ICommandBuilder.AlignmentPosition.Right);
                        String tong_tiengiam_value = formatter.format(tongtiengiam)  +" đ ";
                        builder.append(("Số tiền đã giảm:").getBytes());
                        lenght = 32 - tong_tiengiam_value.length();
                        builder.appendAbsolutePosition(lenght*12);
                        builder.appendMultiple((tong_tiengiam_value+"\r\n").getBytes(),1,2);
                    }

                    builder.appendAlignment(ICommandBuilder.AlignmentPosition.Center);
                    builder.appendFontStyle(ICommandBuilder.FontStyleType.A);
                    builder.append(("-------------------------------\"\r\n".getBytes()));
                    // Neu có nợ
                    if (tiennos.getTiennos().length > 0) {
                        // ky no
                        String nocu_text = "Nợ cũ" +"\r\n";
                        builder.appendAlignment(ICommandBuilder.AlignmentPosition.Left);
                        builder.appendMultiple(nocu_text.getBytes(),1,2);

                        for (int i = 0; i < tiennos.getTiennos().length; i++) {
                            String ky_nocu = "";
                            if(Integer.parseInt(tiennos.getTiennos()[i].getKy()) <10){
                                ky_nocu = "0"+String.valueOf(Integer.parseInt(tiennos.getTiennos()[i].getKy()));
                            }else {
                                ky_nocu = String.valueOf(Integer.parseInt(tiennos.getTiennos()[i].getKy()));
                            }
                            String kyno_text = "Kỳ: " + ky_nocu + "/" + tiennos.getTiennos()[i].getNam().toString() ;
                            int no = Integer.valueOf(tiennos.getTiennos()[i].getTienNo());
                            String kyno_value = formatter.format(no)+" đ " ;
                            builder.appendAlignment(ICommandBuilder.AlignmentPosition.Right);
                            builder.append(kyno_text.getBytes());
                            lenght = 32 - kyno_value.length();
                            builder.appendAbsolutePosition(lenght*12);
                            builder.append((kyno_value+"\r\n").getBytes());
                        }
                        // tong cong
                        String tongcong_text = "Tổng cộng:";
                        String tongcong_value = formatter.format(tongtien) +" đ " ;
                        builder.append(tongcong_text.getBytes());
                        lenght = 32 - tongcong_value.length();
                        builder.appendAbsolutePosition(lenght*12);
                        builder.appendMultiple((tongcong_value+"\r\n").getBytes(),1,2);
                        builder.appendAlignment(ICommandBuilder.AlignmentPosition.Center);
                        builder.append(("-------------------------------\"\r\n".getBytes()));
                    }

                    // nhan vien
                    builder.appendFontStyle(ICommandBuilder.FontStyleType.A);
                    builder.appendAlignment(ICommandBuilder.AlignmentPosition.Right);

                    builder.append(("Nhân viên:").getBytes());
                    lenght = 32 - ten_nhanvien.length();
                    builder.appendAbsolutePosition(lenght*12);
                    builder.appendMultiple((ten_nhanvien+"\r\n").getBytes(),1,2);



                    // dt zalo nhan vien
                    builder.append(("Zalo:").getBytes());
                    lenght = 32 - dienthoai_nhanvien.length();
                    builder.appendAbsolutePosition(lenght*12);
                    builder.appendMultiple((dienthoai_nhanvien+"\r\n").getBytes(),1,2);



                    builder.appendAlignment(ICommandBuilder.AlignmentPosition.Left);
                    if(tiennos.getTiennos().length >0){
                        builder.append(("THÔNG BÁO LẦN 2"+"\r\n").getBytes());
                 //    builder.append((("--------------------------------"+"\r\n").getBytes()));
                        builder.append(("Đề nghị khách hàng thanh toán" +"\r\n"+
                                        "tiền nước do đã quá hạn. Trường" +"\r\n"+
                                        "hợp không thanh toán. Công ty " +"\r\n"+
                                        "sẽ tạm ngừng dịch vụ cấp nước "+"\r\n"+
                                        "kể từ ngày: ").getBytes());
                        builder.appendFontStyle(ICommandBuilder.FontStyleType.B);
                        builder.appendMultiple((date_thutien+"\r\n").getBytes(),2,1);
                        builder.appendFontStyle(ICommandBuilder.FontStyleType.A);
                        builder.append((("và ngừng dịch vụ cấp nước sau" +"\r\n" +
                                         "05 tuần."+"\r\n" ).getBytes()));
                    }else {

                        builder.append((("Quý khách vui lòng thanh toán \r\n" +
                                "trong vòng 07 ngày kể từ ngày:\r\n").getBytes()));
                        builder.appendFontStyle(ICommandBuilder.FontStyleType.B);
                        builder.appendMultiple((date_thutien + "\r\n").getBytes(),2,1);
                    }
                    builder.appendAlignment(ICommandBuilder.AlignmentPosition.Center);
                    builder.appendFontStyle(ICommandBuilder.FontStyleType.A);
                    builder.append((("--------------------------------"+"\r\n").getBytes()));
                    builder.appendAlignment(ICommandBuilder.AlignmentPosition.Left);
                    builder.appendFontStyle(ICommandBuilder.FontStyleType.A);
                    builder.append((("Quý khách hàng vui lòng truy cập\r\n" +
                                     "website: capnuoccholon.com.vn \r\n" ).getBytes()));
                    builder.appendAbsolutePosition(("hoặc liên hệ Tổng đài: \r\n ").getBytes(),0);
                    builder.appendFontStyle(ICommandBuilder.FontStyleType.B);
                    builder.appendMultiple(("0865.851.088\r\n").getBytes(),2,1) ;
                    builder.appendFontStyle(ICommandBuilder.FontStyleType.A);
                    builder.append((  "khi có yêu cầu:\r\n"+
                                      "- Tra cứu thông tin chi tiết " +
                                      "hoá đơn tiền nước.\r\n"+
                                      "- Cập nhật thay đổi thông tin số\r\n" +
                                      "điện thoại trong trường hợp số\r\n" +
                                      "ĐT(đã đăng ký):").getBytes());
                    builder.appendFontStyle(ICommandBuilder.FontStyleType.B);
                    builder.appendMultiple((edt_dienthoai1.getText()+"\r\n").getBytes(),2,1) ;
                    builder.appendFontStyle(ICommandBuilder.FontStyleType.A);
                    builder.append((  "là không đúng.\r\n").getBytes());
                    builder.append(("-------------------------------\"\r\n".getBytes()));
                    builder.appendAlignment(ICommandBuilder.AlignmentPosition.Left);

                    builder.appendMultiple(("Lưu ý:\r\n").getBytes(),2,1) ;
                        builder.append((" Trường hợp khách hàng đã \r\n"+
                                "thanh toán nợ cũ trước ngày\r\n"+
                                "nhận giấy báo này. Xin chỉ \r\n"+
                                "thanh toán hóa đơn kỳ mới.\r\n").getBytes());

                    builder.appendCutPaper(ICommandBuilder.CutPaperAction.PartialCutWithFeed);
                    builder.endDocument();
                    byte [] data_in_receipt = builder.getCommands();
                    sendData(data_in_receipt);
                }else {
                    Dialog_App dialog_app = new Dialog_App(activity_Danhsachdocso_chitiet_khachhang.this);
                    String text_notification = "Vui lòng kiểm tra hóa đơn rồi in GIẤY BÁO"+"\r\n"+"Xin Cám Ơn";
                    dialog_app.Dialog_Notification(text_notification);
                }
            }
        }

    }
    private void Print_HoaDonCnCl_Star_TroNgai(StarIoExt.Emulation emulation, String tinhtrang) throws IOException {
        String nam = String.valueOf(Nam);
        String ky = String.valueOf(Ky);
        String dot = String.valueOf(Dot);
        String somay = String.valueOf(SoMay);
        int tongtienky =0 ;

        // hien thi thong tin doc so
        sqlite_DanhSachKhackHang_DocSo sqlite_danhSachKhackHang_docSo = new sqlite_DanhSachKhackHang_DocSo(activity_Danhsachdocso_chitiet_khachhang.this,nam,ky,dot,somay);
        if(sqlite_danhSachKhackHang_docSo.getStatus_Table_DanhSachKhachHang_Exists()){
            List<data_Danhsachdocso_chitiet> khachhangs = sqlite_danhSachKhackHang_docSo.Get_DanhSachDocSo_Chitiet();
            data_danhsachdocso_chitiet_khachhang khachhang =  sqlite_danhSachKhackHang_docSo.Get_DanhSachDocSo_Chitiet_KhachHang(Danhba);
            if(khachhang.getId() != 0) {

                    String hoten_khachhang = khachhang.getHoten();
                    String sonha_cu = khachhang.getSonha().toString().trim();
                    String sonha_moi = khachhang.getSonha_moi().toString().trim();
                String diachi = sonha_cu + " " + khachhang.getDuong();
                if(!sonha_moi.isEmpty()){
                    diachi = sonha_cu +" ("+sonha_moi+")"+ "\r\n" + khachhang.getDuong();
                }
                    String giabieu = khachhang.getGiabieu();
                    String dinhmuc = khachhang.getDinhmuc();
                    String code = get_Codetinhtrang(tinhtrang,Code_old);
                    if(Status_Sync.equals("3")){
                        code = khachhang.getCodemoi();
                    }
                    String tungay = khachhang.getTungay();
                    String denngay = khachhang.getDenngay();
                    String ten_nhanvien = "";
                    String dienthoai_nhanvien = "";
                    sqlite_User sqlite_user = new sqlite_User(activity_Danhsachdocso_chitiet_khachhang.this);
                    if (sqlite_user.getStatus_Table_User_Exists()) {
                        data_User nhanvien = sqlite_user.Get_User("1");
                        ten_nhanvien = nhanvien.getTennhanvien().toString().trim();
                        dienthoai_nhanvien = nhanvien.getdienthoai().toString().trim();
                    }

                    // In
                    ICommandBuilder builder = StarIoExt.createCommandBuilder(emulation);

                    builder.beginDocument();
                    // header
                builder.appendAlignment(ICommandBuilder.AlignmentPosition.Center);
                builder.appendFontStyle(ICommandBuilder.FontStyleType.B);
                builder.appendMultiple(("CÔNG TY CPCN CHỢ LỚN\r\n").getBytes(),2,1);
                builder.appendFontStyle(ICommandBuilder.FontStyleType.A);
                builder.append(("97 Phạm Hữu Chí P.12 Q.5\r\n" +
                        "Tổng đài : 0865.851.088\r\n" +
                        "------------\r\n").getBytes());
                /// ngay ghi
                Date currentTime = Calendar.getInstance().getTime();
                String currentDate = new SimpleDateFormat("dd/MM/yyyy",Locale.getDefault()).format(currentTime);
                String ngayghi_text = "Ngày gửi giấy báo: " + currentDate+"\r\n" ;
                builder.appendAlignment(ICommandBuilder.AlignmentPosition.Left);
                builder.append((ngayghi_text.getBytes()));
                // title
                builder.appendAlignment(ICommandBuilder.AlignmentPosition.Center);
                builder.appendFontStyle(ICommandBuilder.FontStyleType.A);
                builder.appendMultiple(("GIẤY BÁO TRỞ NGẠI ĐỌC SỐ NƯỚC").getBytes(),1,2);
                builder.append(("\r\n").getBytes());
                // ky nam
                builder.appendAlignment(ICommandBuilder.AlignmentPosition.Left);
                builder.appendFontStyle(ICommandBuilder.FontStyleType.A);
                String ky_text ="Kỳ: " + Ky + "/" + Nam +"\r\n";
                builder.append(ky_text.getBytes());
                // tu ngay ->den ngay
                String[] com_tungay = tungay.split("T");
                String[] com_denngay = denngay.split("T");

                String tungay_new = ToDate_Display((com_tungay[0]));
                String denngay_new = ToDate_Display((com_denngay[0]));

                builder.appendAlignment(ICommandBuilder.AlignmentPosition.Center);
                builder.appendFontStyle(ICommandBuilder.FontStyleType.A);
                String tungay_text = "Từ ngày: " + tungay_new.trim();
                String denngay_text = tungay_text+"->" + denngay_new.trim() +"\r\n";
                builder.append((denngay_text.getBytes()));
                // danh ba
                builder.appendAlignment(ICommandBuilder.AlignmentPosition.Left);
                builder.appendAbsolutePosition(("Danh bạ:").getBytes(),0);
                builder.appendAbsolutePosition(200);
                String danhba1 = Danhba.substring(0,4);
                String danhba2 = Danhba.substring(4,7);
                String danhba3 = Danhba.substring(7);
                String danhba_in = danhba1 +" "+danhba2 +" "+danhba3 +"\r\n";
                builder.appendMultiple(danhba_in.getBytes(),1,2);
                // khach hang
                builder.appendAlignment(ICommandBuilder.AlignmentPosition.Left);
                String khachhang_value = "KH: "+hoten_khachhang +"\r\n";
                builder.append(khachhang_value.getBytes());
                // dia chi khachhang.getDuong()
                String diachi_value ="Đ/C: "+diachi+"\r\n";
                builder.append(diachi_value.getBytes());

                builder.appendAlignment(ICommandBuilder.AlignmentPosition.Left);
                //Gia bieu , Dinh muc, Code
                String giabieu_text = "GB: " ;
                String dinhmuc_text = "ĐM: "  ;
                String code_text = "Code: " ;

                builder.appendAbsolutePosition(giabieu_text.getBytes(),0);
                builder.appendAbsolutePosition(48);
                builder.appendMultiple(giabieu.getBytes(),2,1);
                builder.appendAbsolutePosition(dinhmuc_text.getBytes(),128);
                builder.appendAbsolutePosition(176);
                builder.appendMultiple(dinhmuc.getBytes(),2,1);
                builder.appendAbsolutePosition(code_text.getBytes(),256);
                builder.appendAbsolutePosition(328);
                builder.appendMultiple((code+"\r\n").getBytes(),2,1);


                    builder.appendAlignment(ICommandBuilder.AlignmentPosition.Center);
                    builder.append((("--------------------------------"+"\r\n").getBytes()));
                    builder.appendAlignment(ICommandBuilder.AlignmentPosition.Left);
                    /*
                    if(tinhtrang.equals("Đóng cửa")||
                            tinhtrang.equals("Thay Đ.cửa")||
                            tinhtrang.equals("Chất đồ")||
                            tinhtrang.equals("Kẹt khóa")||
                            tinhtrang.equals("Ngập nước")||
                            tinhtrang.equals("Lấp"))
                    {
                    */

                    if(tinhtrang.equals("Thay Đ.cửa")){
                    //    builder.append((("--------------------------------"+"\r\n").getBytes()));
                        builder.append((("Do đồng hồ nước mới thay"+"\r\n"+
                                         "không xem được chỉ số nước. Công"+"\r\n"+
                                         "ty sẽ tạm tính tiêu thụ bằng"+"\r\n"+
                                         "bình quân của 3 kỳ gần nhất."+"\r\n").getBytes()));
                    }else  if(tinhtrang.equals("Chất đồ")){
                    //    builder.append((("--------------------------------"+"\r\n").getBytes()));
                        builder.append(((  "Do vị trí đồng hồ nước chất đồ"+"\r\n"+
                                           "không xem được chỉ số nước. Công"+"\r\n"+
                                           "ty sẽ tạm tính tiêu thụ bằng"+"\r\n"+
                                           "bình quân của 3 kỳ gần nhất."+"\r\n").getBytes()));
                    }else  if(tinhtrang.equals("Ngập nước")){
                    //    builder.append((("--------------------------------"+"\r\n").getBytes()));
                        builder.append(((  "Do vị trí đồng hồ nước ngập nước"+"\r\n"+
                                           "không xem được chỉ số nước. Công"+"\r\n"+
                                           "ty sẽ tạm tính tiêu thụ bằng"+"\r\n"+
                                           "bình quân của 3 kỳ gần nhất."+"\r\n").getBytes()));
                    }
                    else  if(tinhtrang.equals("Lấp")){
                   //     builder.append((("--------------------------------"+"\r\n").getBytes()));
                          builder.append((("Do vị trí đồng hồ nước bị lấp"+"\r\n"+
                                           "không xem được chỉ số nước. "+"\r\n"+
                                           "Công ty sẽ tạm tính tiêu thụ"+"\r\n"+
                                           "bằng bằng bình quân của 3 kỳ" +"\r\n"+
                                           "gần nhất."+"\r\n").getBytes()));
                    }
                    else  if(tinhtrang.equals("Đóng cửa")){
                     //   builder.append((("--------------------------------"+"\r\n").getBytes()));
                        builder.append(((  "Do nhà khách hàng đóng cửa. Đề"+"\r\n"+
                                           "nghị khách hàng vui lòng báo chỉ"+"\r\n"+
                                           "số nước gấp trong ngày để tính"+"\r\n"+
                                           "tiêu thụ."+"\r\n").getBytes()));

                    }else  if(tinhtrang.equals("Kẹt khóa")){
                      //  builder.append((("--------------------------------"+"\r\n").getBytes()));
                        builder.append(((  "Do vị trí đồng hồ nước kẹt khóa."+"\r\n"+
                                           "Đề nghị khách hàng vui lòng báo"+"\r\n"+
                                           "chỉ số nước gấp trong ngày để"+"\r\n"+
                                           "tính tiêu thụ."+"\r\n").getBytes()));
                    }
                    builder.append((("Để biết thông tin chi tiết chính"+"\r\n" +
                                     "xác tiền nước phải thanh toán."+"\r\n" +
                                     "Quý khách hàng vui lòng liên hệ"+"\r\n" +
                                     "tổng đài: ").getBytes()));
                    builder.appendFontStyle(ICommandBuilder.FontStyleType.B);
                    builder.appendMultiple(("0865.851.088"+ "\r\n").getBytes(),2,1) ;
                    builder.appendFontStyle(ICommandBuilder.FontStyleType.A);
                    builder.append((  "hoặc truy cập website:"+"\r\n" ).getBytes());
                    builder.appendFontStyle(ICommandBuilder.FontStyleType.B);
                    builder.appendMultiple(("capnuoccholon.com.vn"+"\r\n").getBytes(),2,1);
                    builder.appendFontStyle(ICommandBuilder.FontStyleType.A);
                    if(tinhtrang.equals("Đóng cửa")|| tinhtrang.equals("Kẹt khóa")) {
                          //  builder.append((("--------------------------------"+"\r\n").getBytes()));
                        String thonbao_text2 = "Nếu quý khách không thông báo," + "\r\n" +
                                               "Công ty sẽ tạm tính tiêu thụ" + "\r\n" +
                                               "bình quân 3 kỳ gần nhát." + "\r\n";
                        builder.append(thonbao_text2.getBytes());
                    }
                    // ket thuc
                    builder.appendAlignment(ICommandBuilder.AlignmentPosition.Center);
                    builder.append((("--------------------------------"+"\r\n").getBytes()));
                    // nhan vien
                    builder.appendFontStyle(ICommandBuilder.FontStyleType.A);
                    builder.appendAlignment(ICommandBuilder.AlignmentPosition.Right);

                    builder.append(("Nhân viên:").getBytes());
                    int lenght = 32 - ten_nhanvien.length();
                    builder.appendAbsolutePosition(lenght*12);
                    builder.appendMultiple((ten_nhanvien+"\r\n").getBytes(),1,2);



                    // dt zalo nhan vien
                    builder.append(("Zalo:").getBytes());
                    lenght = 32 - dienthoai_nhanvien.length();
                    builder.appendAbsolutePosition(lenght*12);
                    builder.appendMultiple((dienthoai_nhanvien+"\r\n").getBytes(),1,2);

                    builder.appendCutPaper(ICommandBuilder.CutPaperAction.PartialCutWithFeed);
                    builder.endDocument();
                    byte [] data_in_receipt = builder.getCommands();

                    sendData(data_in_receipt);

            }
        }

    }

    private ByteArrayOutputStream Init_Text_In(ByteArrayOutputStream byteStream, String text,boolean bold, int w_text, int h_text, int align) throws IOException {
        byte [] text_b =null;
        text_b = text.getBytes("UTF-8");
        byteStream.write(WoosimCmd.setTextStyle(bold, false, false, w_text, h_text));
        byteStream.write(WoosimCmd.setTextAlign(align));
        byteStream.write(text_b);
        return  byteStream;
    }
    private ByteArrayOutputStream Init_Text_In_ptram(ByteArrayOutputStream byteStream,boolean bold, int w_text, int h_text, int align) throws IOException {
        byte [] text_b =null;
        text_b = (" % @ '/, `/.").getBytes("windows-1258");
      //  byteStream.write(WoosimCmd.setTextAlign(WoosimCmd.setCodeTable(WoosimCmd.MCU_M16C,WoosimCmd.CT_WIN1258,WoosimCmd.FONT_MEDIUM)));
        byteStream.write(WoosimCmd.setTextStyle(bold, false, false, w_text, h_text));
        byteStream.write(WoosimCmd.setTextAlign(align));
       byte [] aa = WoosimCmd.getTTFcode(w_text,h_text,"%");
        byteStream.write(text_b);
        return  byteStream;
    }



    private class get_Enable_Docso_Server extends AsyncTask<Void,Void,String> {

        private String Nam;
        private String Ky ;
        private String Dot;
        private String May;

        private String ResponseString;
        private String BASE_URL_API ="/apikhachhang/api/DocSoThamSoNgayDoc?";
        private String BASE_URL = "";

        public get_Enable_Docso_Server(String nam,String ky,String dot, String somay ){
            Nam =nam ;
            Ky = ky;
            Dot = dot;
            String [] com_somay =somay.split("_");
            May = com_somay[0];
            ResponseString ="";
            data_Http data_http = new data_Http(activity_Danhsachdocso_chitiet_khachhang.this);
         String url_run = data_http.get_Url_run();
         if(!url_run.isEmpty()){
             BASE_URL = url_run +BASE_URL_API;
         }else {
             BASE_URL = "http://viettel.capnuoccholon.com.vn"+BASE_URL_API;
         }
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... voids) {
            try {

                HttpClient httpclient = new DefaultHttpClient();

                URI uri_     = new URIBuilder(BASE_URL)
                        .addParameter("somay",May)
                        .addParameter("nam",Nam)
                        .addParameter("ky",Ky)
                        .addParameter("dot",Dot)
                        .build();

                HttpPost httppost = new HttpPost(uri_);
                //  httppost.setHeader("Accept", "application/json");
                httppost.setHeader("Content-type","application/json;charset=utf-8");
                //     httpget.setEntity(stringEntity_send);
                HttpResponse response = httpclient.execute(httppost);
                if (response.getStatusLine().getStatusCode() == 200) {
                    String responseString = EntityUtils.toString(response.getEntity());
                    ResponseString =responseString;
                }
            } catch (IOException | URISyntaxException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            //   dialog_app.Dialog_Progress_Wait_Close();
            Json_EnableGhiso_Server =ResponseString;
        }
    }

}
package com.dkhanhaptechs.mghiso.danhsachdocso;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.dkhanhaptechs.mghiso.MainActivity;
import com.dkhanhaptechs.mghiso.R;
import com.dkhanhaptechs.mghiso.Share.Dialog_App;
import com.dkhanhaptechs.mghiso.Share.User.data_User;
import com.dkhanhaptechs.mghiso.Share.User.sqlite_User;
import com.dkhanhaptechs.mghiso.dangnhap.activity_DangNhap;
import com.dkhanhaptechs.mghiso.danhsachdocso_chitiet.activity_Danhsachdocso_chitiet;
import com.dkhanhaptechs.mghiso.Share.DanhSach_DocSo.*;
import com.dkhanhaptechs.mghiso.Share.SyncGhiSo.*;
import com.dkhanhaptechs.mghiso.Share.TienNo.*;
import com.dkhanhaptechs.mghiso.Share.HinhAnh.*;
import com.dkhanhaptechs.mghiso.mayin.activity_Mayin;
import com.google.gson.Gson;
import com.loopj.android.http.HttpGet;
import com.loopj.android.http.TextHttpResponseHandler;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;


import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.utils.URIBuilder;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import cz.msebera.android.httpclient.util.EntityUtils;
import  com.dkhanhaptechs.mghiso.Share.User.*;

public class activity_Danhsachdocso extends AppCompatActivity  {
    //
    private ListView lv_danhsachdocso;
    private Toolbar toolbar;
    private Button btn_taidulieu, btn_taitangcuong, btn_xoa, btn_dongbodulieu;

    private TextView tv_nam, tv_ky, tv_dot, tv_somay;
    // BIEN
    private ArrayList<data_Danhsachdocso> Arraylist_data_danhsachdocso;
    private adapter_Danhsachdocso Adapter_danhsachdocso;
    //NavigationView
    ImageView imv_user;
    TextView tv_tennv_navi , tv_somay_navi , tv_sodienthoai_navi;
    ImageButton imbt_dangxuat;
    private Boolean Status_taitangcuong =false;
    private String Somay_tangcuong ="";
    private String Pre_Nam,Pre_Dot, Pre_Ky,Pre_Somay;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_danhsachdocso);
        Mappping_Layout();
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this,drawer,toolbar,R.string.navigation_drawer_open,R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
     //   View header = Mappping_Layout_NavigationView(navigationView);
        Arraylist_data_danhsachdocso = new ArrayList<data_Danhsachdocso>();
        Adapter_danhsachdocso = new adapter_Danhsachdocso(activity_Danhsachdocso.this,R.layout.dong_danhsachdocso,Arraylist_data_danhsachdocso);
        lv_danhsachdocso.setAdapter(Adapter_danhsachdocso);
        // test gan du lieu
        // Test_GanData_Listview();
        lv_danhsachdocso.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        // Adapter_danhsachdocso.notifyDataSetChanged();

        // ACTION : LIST danh sach

        lv_danhsachdocso.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent,View view,int position,long id) {
                data_Danhsachdocso data_danhsachdocso = Arraylist_data_danhsachdocso.get(position);
                String title = data_danhsachdocso.getTieude();
                String[] com_title = title.split("-");
                String nam = com_title[0].trim().substring(1).toString().trim();
                String dot = com_title[1].trim().substring(1).toString().trim();

                String ky = com_title[2].trim().substring(1).toString().trim();

                String may = com_title[3].trim().substring(1).toString().trim();

                String sum_dh = String.valueOf(data_danhsachdocso.getSo_dh());
                String sum_dadoc = String.valueOf(data_danhsachdocso.getSo_dh_doc());
                String sum_sanluong = String.valueOf(data_danhsachdocso.getSanluong());

                Intent intent = new Intent(activity_Danhsachdocso.this,activity_Danhsachdocso_chitiet.class);

                String json_send = "{\"Nam\":\"" + nam + "\"," +
                        "\"Ky\":\"" + ky + "\"," +
                        "\"Dot\":\"" + dot + "\"," +
                        "\"SoMay\":\"" + may + "\"," +
                        "\"Sum_DH\":" + sum_dh + "," +
                        "\"Sum_DaDoc\":" + sum_dadoc + "," +
                        "\"Sum_SanLuong\":" + sum_sanluong +
                        "}";
              // Log.e("11",json_send);
                intent.putExtra("TABLE_DANHSACHDOCSO",json_send);
                startActivity(intent);
                return false;
            }
        });


        // ACTION : Tai du lieu
        btn_taidulieu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String nam = tv_nam.getText().toString().trim();
                String ky = tv_ky.getText().toString().trim();
                String dot = tv_dot.getText().toString().trim();
                String somay = tv_somay.getText().toString().trim();

                if (!nam.isEmpty() && !ky.isEmpty() && !dot.isEmpty() && !somay.isEmpty()) {
                    String notification_taidulieu ="Bạn có muốn tải dữ liệu danh sách đọc số"+"\r\n"+" năm: " + nam + ", kỳ: " + ky + ", đợt: " + dot + " của máy: " + somay +" không ?";
                    Dialog_Notification_Taidulieu(notification_taidulieu ,nam,ky, dot, somay);

                }else {
                    Dialog_App dialog_app = new Dialog_App(activity_Danhsachdocso.this);
                    String notification = "Vui lòng nhập đủ thông tin, Năm, Kỳ, Dợt, Số Máy";
                    dialog_app.Dialog_Notification(notification);
                }
            }
        });

        // ACTION : Tai tang cuong du lieu
        btn_taitangcuong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Somay_tangcuong  = "";
                dialog_taitangcuong.show();

            }
        });

        // ACTION : xoa
        btn_xoa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String notìication ="Bạn muốn xóa dữ liệu của: "+"\r\n";
                sqlite_DanhSachDocSo sqlite_danhSachDocSo = new sqlite_DanhSachDocSo(activity_Danhsachdocso.this);
                List<String> list_title = new ArrayList<>();
                List<Integer> list_id = new ArrayList<Integer>();
                if (sqlite_danhSachDocSo.getStatus_Table_DanhSachDocSo_Exists()) {
                    for (int i = 0; i < Arraylist_data_danhsachdocso.size(); i++) {
                        data_Danhsachdocso data_danhsachdocso = Arraylist_data_danhsachdocso.get(i);
                        boolean selete_delete = data_danhsachdocso.getChon();
                        if (selete_delete) {
                            String title = data_danhsachdocso.getTieude();
                            notìication = notìication +title +"\r\n";
                             list_title.add(title);
                            int id = data_danhsachdocso.getId();
                            list_id.add(id);
                        }
                    }
                }
                if(list_title.size() >0){
                    Dialog_Notification_TXoaDulieu(notìication,list_title,list_id);
                }else {
                    Dialog_App dialog_app = new Dialog_App(activity_Danhsachdocso.this);
                    String notification = "không có dữ liệu nào được chọn để xóa";
                    dialog_app.Dialog_Notification(notification);
                }

            }
        });


        // ACTION : dongbo du lieu
        btn_dongbodulieu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Sync_DuLieu();
            }
        });
        // ACTION : Click menu
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                int id = menuItem.getItemId();
                if(id == R.id.nav_mainmenu){
                    Intent intent = new Intent(activity_Danhsachdocso.this,MainActivity.class);
                    //intent.putExtra("SODIENTHOAI",com_reponse[1]);
                      finish();
                    startActivity(intent);
                }
               else if(id == R.id.nav_bandoghiso){
                    Dialog_App dialog_app = new Dialog_App(activity_Danhsachdocso.this);
                    String notification = "Trang bản đồ ghi số đang phát triển."+"\r\b"+"Xin vui lòng thông cảm";
                    dialog_app.Dialog_Notification(notification);

                }else if(id == R.id.nav_bandodaghiso){
                    Dialog_App dialog_app = new Dialog_App(activity_Danhsachdocso.this);
                    String notification = "Trang bản đồ đã ghi số đang phát triển."+"\r\b"+"Xin vui lòng thông cảm";
                    dialog_app.Dialog_Notification(notification);
                }
                else if (id == R.id.nav_mayinbluetooth) {
                    // danh sach bat thuong
                    Intent intent = new Intent(activity_Danhsachdocso.this,activity_Mayin.class);
                    //intent.putExtra("SODIENTHOAI",com_reponse[1]);
                    //  finish();
                    startActivity(intent);
                    // Toast.makeText(MainActivity.this,"BB",Toast.LENGTH_SHORT).show();
                }else if(id == R.id.nav_dangxuat){
                    String notìication ="Bạn muốn đăng xuất tài khoản này.";
                    Dialog_Notification_Dangxuat(notìication);
                }

                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
                return false;
            }
        });

        Display_User();
        showDialog_taitangcuong(activity_Danhsachdocso.this);
        dialog_taitangcuong.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if(Status_taitangcuong && !Somay_tangcuong.isEmpty()){
                    String nam = tv_nam.getText().toString().trim();
                    String ky = tv_ky.getText().toString().trim();
                    String dot = tv_dot.getText().toString().trim();
                    String somay = tv_somay.getText().toString().trim();

                        get_Danhsachdocso asynctask = new get_Danhsachdocso(nam,ky,dot,somay,Somay_tangcuong);
                        asynctask.execute();


                 //   get_DanhSachDocSo(nam,ky,dot,somay,Somay_tangcuong);
                }
            }
        });
        Init_EditText();
       tv_nam.setOnTouchListener(new View.OnTouchListener() {
           @Override
           public boolean onTouch(View v,MotionEvent event) {

             //      if (event.getX()>(v.getWidth()-v.getPaddingRight())){
               Pre_Nam =   ((EditText)v).getText().toString();
                       ((EditText)v).setText("");
             //      }

               return false;
           }
       });
       tv_ky.setOnTouchListener(new View.OnTouchListener() {
           @Override
           public boolean onTouch(View v,MotionEvent event) {
               ((EditText)v).setText("");
               return false;
           }
       });
       tv_dot.setOnTouchListener(new View.OnTouchListener() {
           @Override
           public boolean onTouch(View v,MotionEvent event) {
               ((EditText)v).setText("");
               return false;
           }
       });
       tv_somay.setOnTouchListener(new View.OnTouchListener() {
           @Override
           public boolean onTouch(View v,MotionEvent event) {
               ((EditText)v).setText("");
               return false;
           }
       });


    }
    private void Init_EditText(){
        tv_nam.setImeOptions(EditorInfo.IME_ACTION_DONE);
        tv_dot.setImeOptions(EditorInfo.IME_ACTION_DONE);
        tv_ky.setImeOptions(EditorInfo.IME_ACTION_DONE);
        tv_somay.setImeOptions(EditorInfo.IME_ACTION_DONE);


    }
    AlertDialog dialog_notification_dangxuat ;
    private void Dialog_Notification_Dangxuat(final String text){
        AlertDialog.Builder dialog_notification =new AlertDialog.Builder(activity_Danhsachdocso.this);
        dialog_notification.setMessage(text);
        // dong y
        dialog_notification.setPositiveButton("Đồng ý",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog,int which) {
                Intent intent = new Intent(activity_Danhsachdocso.this,activity_DangNhap.class);
                //intent.putExtra("SODIENTHOAI",com_reponse[1]);
                finish();
                startActivity(intent);
                dialog_notification_dangxuat.dismiss();
            }
        });
        // bo qua
        dialog_notification.setNegativeButton("Hủy",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog,int which) {
                dialog_notification_dangxuat.dismiss();
            }
        });
        dialog_notification_dangxuat = dialog_notification.create();
        dialog_notification_dangxuat.show();
    }
    private void Display_User(){
        sqlite_User sqlite_user = new sqlite_User(activity_Danhsachdocso.this);
        if(sqlite_user.getStatus_Table_User_Exists()) {
            try {
                Date currentTime = Calendar.getInstance().getTime();
                String y_currentDate = new SimpleDateFormat("yyyy",Locale.getDefault()).format(currentTime);
                tv_nam.setText(y_currentDate);
                data_User user_dangnhap = sqlite_user.Get_User("1");
                String somay = user_dangnhap.getSomay().trim();
                if(!somay.isEmpty()) {
                    tv_somay.setText(somay);
                }else {
                    tv_somay.setText("");
                }
                /*
                String tennv = user_dangnhap.getTennhanvien().toString();
                String sodienthoai = user_dangnhap.getdienthoai().toString();

                tv_tennv_navi.setText("  Nhân viên: "+tennv);
                tv_sodienthoai_navi.setText("Điện thoại: "+sodienthoai);
                tv_somay_navi.setText("Số máy: "+somay);

                 */

            }catch (Exception e){

            }
        }
    }


    private AlertDialog dialog_notification_taidulieu;
    public void Dialog_Notification_Taidulieu(final String title, final String nam, final String ky, final String dot, final String somay){
        AlertDialog.Builder alert_taidulieu = new AlertDialog.Builder(activity_Danhsachdocso.this);
        alert_taidulieu.setMessage(title);
        // dong y
        alert_taidulieu.setPositiveButton("Xác nhận",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog,int which) {
                dialog_notification_taidulieu.dismiss();
                sqlite_DanhSachDocSo sqlite_danhSachDocSo = new sqlite_DanhSachDocSo(activity_Danhsachdocso.this);
                if (sqlite_danhSachDocSo.getStatus_Table_DanhSachDocSo_Exists()) {
                    data_DanhSachDocSo data_danhSachDocSo = new data_DanhSachDocSo(nam,ky,dot,somay,"","","");
                    data_danhSachDocSo = sqlite_danhSachDocSo.Get_DanhSachDocSo(data_danhSachDocSo);
                    if (data_danhSachDocSo.getId() == 0) {
                        get_Danhsachdocso asynctask = new get_Danhsachdocso(nam,ky,dot,somay,"");
                        asynctask.execute();
                    } else {
                        Dialog_App dialog_app_notification = new Dialog_App(activity_Danhsachdocso.this);
                        String title_dialog = "Danh sách đọc số năm: " + nam + ", kỳ: " + ky + ", đợt: " + dot + " của máy: " + somay + "\r\n" + "Đã được tải.Nếu muốn tải thêm danh sách vui lòng chọn tải tăng cường ";
                        dialog_app_notification.Dialog_Notification(title_dialog);
                    }
                }
            }
        });

        alert_taidulieu.setNegativeButton("Hủy",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog,int which) {
                dialog_notification_taidulieu.dismiss();
            }
        });
        // bo qua

        dialog_notification_taidulieu = alert_taidulieu.create();
        dialog_notification_taidulieu.show();
    }


    private AlertDialog dialog_notification_xoadulieu;
    public void Dialog_Notification_TXoaDulieu(final String title, final List<String> list_title,final List<Integer> list_id){
        AlertDialog.Builder alert_taidulieu = new AlertDialog.Builder(activity_Danhsachdocso.this);
        alert_taidulieu.setMessage(title);
        // dong y
        alert_taidulieu.setPositiveButton("Xác nhận",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog,int which) {

                sqlite_DanhSachDocSo sqlite_danhSachDocSo = new sqlite_DanhSachDocSo(activity_Danhsachdocso.this);
                for(int i = 0;  i < list_title.size() ; i++){
                    try {
                        String title = list_title.get(i);
                        int id = list_id.get(i);
                        String[] com_title = title.split("-");
                        String nam = com_title[0].trim().substring(1).toString().trim();
                        String dot = com_title[1].trim().substring(1).toString().trim();
                        String ky = com_title[2].trim().substring(1).toString().trim();
                        String may = com_title[3].trim().substring(1).toString().trim();
                        sqlite_DanhSachKhackHang_DocSo sqlite_danhSachKhackHang_docSo = new sqlite_DanhSachKhackHang_DocSo(activity_Danhsachdocso.this,nam,ky,dot,may);
                        sqlite_danhSachDocSo.Delete_DanhSachDocSo(nam,ky,dot,may);
                        sqlite_danhSachKhackHang_docSo.Delete_Table_DanhSachDocSo_HackHang();
                       // sqlite_Hinhanh sqlite_hinhanh =new sqlite_Hinhanh(activity_Danhsachdocso.this,nam,ky,dot,may);
                       // sqlite_hinhanh.Delete_Table_ListHinhAnh();
                        sqlite_TienNo sqlite_tienNo = new sqlite_TienNo(activity_Danhsachdocso.this,nam,ky,dot,may);
                        if(sqlite_tienNo.getStatus_Table_DanhSachHangHang_TienNo_Exists()) {
                            sqlite_tienNo.Delete_Table_DanhSachHangHang_TienNo();
                        }
                    }catch (Exception e){}
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                Display_DanhSachDocSo();
                    }
                });
                dialog_notification_xoadulieu.dismiss();
            }
        });
        alert_taidulieu.setNegativeButton("Hủy",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog,int which) {
                dialog_notification_xoadulieu.dismiss();
            }
        });
        // bo qua

        dialog_notification_xoadulieu = alert_taidulieu.create();
        dialog_notification_xoadulieu.show();
    }
    private AlertDialog dialog_notification_taidulieu_tangcuong;
    public void Dialog_Notification_Taidulieu_TangCuong(final String title, final String nam, final String ky, final String dot, final String somay){
        AlertDialog.Builder alert_taidulieu = new AlertDialog.Builder(activity_Danhsachdocso.this);
        alert_taidulieu.setMessage(title);
        // dong y
        alert_taidulieu.setPositiveButton("Xác nhận",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog,int which) {
                dialog_notification_taidulieu_tangcuong.dismiss();
                sqlite_DanhSachDocSo sqlite_danhSachDocSo = new sqlite_DanhSachDocSo(activity_Danhsachdocso.this);
                if (sqlite_danhSachDocSo.getStatus_Table_DanhSachDocSo_Exists()) {
                 //   Get_DanhSachDocSo(nam,ky,dot,somay);
                }

            }
        });
        alert_taidulieu.setNegativeButton("Hủy",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog,int which) {
                dialog_notification_taidulieu_tangcuong.dismiss();
            }
        });
        // bo qua

        dialog_notification_taidulieu_tangcuong = alert_taidulieu.create();
        dialog_notification_taidulieu_tangcuong.show();
    }
    /*
    private View  Mappping_Layout_NavigationView(NavigationView navigationView ){
        View header = navigationView.getHeaderView(0);

        // NavigationView
        imv_user = (ImageView) header.findViewById(R.id.imageview_user_NavigationView);
        tv_tennv_navi =(TextView) header.findViewById(R.id.textview_tennv_NavigationView);
        tv_somay_navi = (TextView) header.findViewById(R.id.textview_somay_NavigationView);
        tv_sodienthoai_navi = (TextView) header.findViewById(R.id.textview_sodienthoai_NavigationView);
        imbt_dangxuat =(ImageButton) header.findViewById(R.id.imagebutton_dangxuat_NavigationView);
        return  header;
    }
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        Display_DanhSachDocSo();
        super.onResume();
    }

    private void Mappping_Layout() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        lv_danhsachdocso = (ListView) findViewById(R.id.listview_danhsach_danhsachdocso);
        btn_taidulieu = (Button) findViewById(R.id.button_taidulieu_Danhsachdocso);
        btn_taitangcuong = (Button) findViewById(R.id.button_taitangcuong_Danhsachdocso);
        btn_xoa = (Button) findViewById(R.id.button_xoa_Danhsachdocso);
        btn_dongbodulieu = (Button) findViewById(R.id.button_dongbomaychu_Danhsachdocso);

        tv_nam = (TextView) findViewById(R.id.textview_nam_DanhSachDocSo);
        tv_ky = (TextView) findViewById(R.id.textview_ky_DanhSachDocSo);
        tv_dot = (TextView) findViewById(R.id.textview_dot_DanhSachDocSo);
        tv_somay = (TextView) findViewById(R.id.textview_somay_DanhSachDocSo);

    }
/*
    private void Test_GanData_Listview() {
        data_Danhsachdocso data1 = new data_Danhsachdocso(1,false,"N2020 - DD18 - K2 - M46",0,100,100,1000);
        Arraylist_data_danhsachdocso.add(data1);
        data_Danhsachdocso data2 = new data_Danhsachdocso(2,false,"N2020 - DD18 - K3 - M46",1,100,75,1100);
        Arraylist_data_danhsachdocso.add(data2);
        data_Danhsachdocso data3 = new data_Danhsachdocso(3,false,"N2020 - DD18 - K4 - M46",2,100,50,1200);
        Arraylist_data_danhsachdocso.add(data3);
        data_Danhsachdocso data4 = new data_Danhsachdocso(4,false,"N2020 - DD18 - K5 - M46",0,100,25,1300);
        Arraylist_data_danhsachdocso.add(data4);

    }

 */

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),0);
        }
        return super.dispatchTouchEvent(ev);
    }
/*
    private void Get_DanhSachDocSo(final String nam,final String ky,final String dot,final String may) {
        data_Api_DanhSachDocSo_send data_api_danhSachDocSo_send = new data_Api_DanhSachDocSo_send(nam,ky,dot,may);
        Gson gson_send = new Gson();
        String data_send = gson_send.toJson(data_api_danhSachDocSo_send);
        final Dialog_App dialog_app_wait = new Dialog_App(activity_Danhsachdocso.this);
        String title_dialog = "Vui lòng chờ trong giây phút";
        String messagẻ_dialog = "Đang lấy dữ liệu";
        dialog_app_wait.Dialog_Progress_Wait_Open(title_dialog,messagẻ_dialog);
        Api_DanhSachDocSo.get(data_send,
                null,new TextHttpResponseHandler() {
                    @Override
                    public void onFailure(int statusCode,Header[] headers,String responseString,Throwable throwable) {
                        Dialog_App dialog_app_notification = new Dialog_App(activity_Danhsachdocso.this);
                        String title_dialog = "Kết nối máy chủ lỗi" + "\r\n" + "Vui lòng thử lại";
                        dialog_app_notification.Dialog_Notification(title_dialog);
                        dialog_app_wait.Dialog_Progress_Wait_Close();
                    }

                    @Override
                    public void onSuccess(int statusCode,Header[] headers,String responseString) {
                        dialog_app_wait.Dialog_Progress_Wait_Close();
                        data_Api_DanhSachDocSo_response data_api_danhSachDocSo_response = new data_Api_DanhSachDocSo_response();
                        Gson gson_reponse = new Gson();
                        data_api_danhSachDocSo_response = gson_reponse.fromJson(responseString,data_Api_DanhSachDocSo_response.class);

                        final int sum_dh = data_api_danhSachDocSo_response.getResult().getKhachhangs().length;
                        if (sum_dh > 0) {
                            Dialog_Progress_TaiDuLieu(data_api_danhSachDocSo_response);
                        } else {
                            Dialog_App dialog_app_notification = new Dialog_App(activity_Danhsachdocso.this);
                            String title_dialog = "Không có thông tin đọc số năm: " + nam + ", kỳ: " + ky + ", đợt: " + dot + " của máy: " + may;
                            dialog_app_notification.Dialog_Notification(title_dialog);
                        }

                    }
                });
    }


 */
    private void Display_DanhSachDocSo() {
        sqlite_DanhSachDocSo sqlite_danhSachDocSo = new sqlite_DanhSachDocSo(activity_Danhsachdocso.this);

        if (sqlite_danhSachDocSo.getStatus_Table_DanhSachDocSo_Exists()) {
            List<data_DanhSachDocSo> all_danhSachDocSo = sqlite_danhSachDocSo.Get_All_DanhSachDocSo();
            Arraylist_data_danhsachdocso.clear();
            int nam_mmax = 1;
            int ky_max = 1;
            int dot_max = 1;

            for (int i = all_danhSachDocSo.size() - 1; i >= 0; i--) {
                try {
                    data_DanhSachDocSo data_sql = all_danhSachDocSo.get(i);
                    int sum_dh = 0;
                    int sum_dadoc = 0;
                    int sum_sanluong = 0;
                    int sum_anh = 0;


                    sqlite_DanhSachKhackHang_DocSo sqlite_danhSachKhackHang_docSo = new sqlite_DanhSachKhackHang_DocSo
                            (activity_Danhsachdocso.this,data_sql.getNam(),data_sql.getKy(),data_sql.getDot(),data_sql.getSomay());
                    if (sqlite_danhSachKhackHang_docSo.getStatus_Table_DanhSachKhachHang_Exists()) {
                        sum_dh = sqlite_danhSachKhackHang_docSo.Get_Sum_DH();
                        sum_dadoc = sqlite_danhSachKhackHang_docSo.Get_Sum_DaDoc();
                        sum_sanluong = sqlite_danhSachKhackHang_docSo.Get_Sum_SanLuong();
                    }
                    sqlite_Hinhanh sqlite_hinhanh = new sqlite_Hinhanh
                            (activity_Danhsachdocso.this,data_sql.getNam(),data_sql.getKy(),data_sql.getDot(),data_sql.getSomay());
                    if (sqlite_hinhanh.getStatus_Table_ListHinhAnh_Exists()) {
                        sum_anh = sqlite_hinhanh.Get_Sum_AnhSync();
                    }
                    data_Danhsachdocso data = new data_Danhsachdocso(i,false,
                            "N" + data_sql.getNam() + " - Đ" + data_sql.getDot() + " - K" + data_sql.getKy() + " - M" + data_sql.getSomay(),
                            0,sum_dh,sum_dadoc,sum_sanluong,sum_anh);
                    Arraylist_data_danhsachdocso.add(data);
                    if(nam_mmax < Integer.parseInt(data_sql.getNam())){
                        nam_mmax =Integer.parseInt(data_sql.getNam());
                        ky_max = Integer.parseInt(data_sql.getKy());
                        dot_max = Integer.parseInt(data_sql.getDot());
                    }else if(nam_mmax == Integer.parseInt(data_sql.getNam())) {
                        if (ky_max < Integer.parseInt(data_sql.getKy())) {
                            ky_max = Integer.parseInt(data_sql.getKy());
                            dot_max = Integer.parseInt(data_sql.getDot());
                        } else if (ky_max == Integer.parseInt(data_sql.getKy())) {
                            if (dot_max < Integer.parseInt(data_sql.getDot())) {
                                dot_max = Integer.parseInt(data_sql.getDot());
                            }
                        }
                    }
                }catch (Exception e){

                }

            }
            if(ky_max < 12){
                if(dot_max >=20) {
                    ky_max = ky_max +1;
                    dot_max = 1;
                }else {
                    dot_max =dot_max+1;
                }
            }else {
                if(dot_max >=20) {
                    nam_mmax = nam_mmax+1;
                    ky_max = 1;
                    dot_max = 1;
                }
            }
            tv_nam.setText(String.valueOf(nam_mmax));
            tv_ky.setText(String.valueOf(ky_max));
            tv_dot.setText(String.valueOf(dot_max));
            Adapter_danhsachdocso.notifyDataSetChanged();
        }
    }

    ProgressDialog dialog_progress_taidulieu;
    private void Dialog_Progress_TaiDuLieu(final data_Api_DanhSachDocSo_response data_api_danhSachDocSo_response,final String type) {
        dialog_progress_taidulieu = new ProgressDialog(activity_Danhsachdocso.this);
        final String nam = data_api_danhSachDocSo_response.getResult().getNam();
        final String ky = data_api_danhSachDocSo_response.getResult().getKy();
        final String dot = data_api_danhSachDocSo_response.getResult().getDot();
        final String may = data_api_danhSachDocSo_response.getResult().getMay();
        final int sum_dh = data_api_danhSachDocSo_response.getResult().getKhachhangs().length;
        String title_dialog = "Tải thông tin đọc số năm: " + nam + ", kỳ: " + ky + ", đợt: " + dot + " của máy: " + may;
        if(!type.isEmpty()){
            title_dialog = "Tải thông tin đọc số năm: " + nam + ", kỳ: " + ky + ", đợt: " + dot + " của máy: " + may+" máy tăng cường : "+type;
        }
        dialog_progress_taidulieu.setTitle(title_dialog);
        dialog_progress_taidulieu.setMessage("Dang tải dữ liệu");
        dialog_progress_taidulieu.setMax(sum_dh);
        dialog_progress_taidulieu.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        dialog_progress_taidulieu.setCancelable(false);
        dialog_progress_taidulieu.show();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    int i = 0;
                    String type_may = may;
                    if(!type.isEmpty()){
                        type_may = type_may +"_"+type;
                    }
                    sqlite_DanhSachKhackHang_DocSo sqlite_danhSachKhackHang_docSo = new sqlite_DanhSachKhackHang_DocSo(activity_Danhsachdocso.this,nam,ky,dot,type_may);
                    sqlite_TienNo sqlite_tienNo = new sqlite_TienNo(activity_Danhsachdocso.this,nam,ky,dot,type_may);
                    if (sqlite_danhSachKhackHang_docSo.getStatus_Table_DanhSachKhachHang_Exists()) {
                        List<String> all_danhba = sqlite_danhSachKhackHang_docSo.Get_All_DanhBa();

                        data_Api_DanhSachDocSo_response.KhachHangs[] khachHangs = data_api_danhSachDocSo_response.getResult().getKhachhangs();
                        while (dialog_progress_taidulieu.getProgress() < dialog_progress_taidulieu.getMax()) {

                            data_Api_DanhSachDocSo_response.KhachHang khachHang = khachHangs[i].getKhachhang();
                            String danhba = khachHang.getDANHBA();

                            data_tiennos data_tiennos = new data_tiennos(danhba,khachHangs[i].getTiennos());
                            sqlite_tienNo.Insert_Update_Table_DanhSachHangHang_TienNo(data_tiennos);
                            boolean status_add = true;
                            for (int j = 0; j < all_danhba.size(); j++) {
                                if (all_danhba.get(j).equals(danhba)) {
                                    status_add = false;
                                    all_danhba.remove(j);
                                    break;
                                }
                            }
                            if(danhba.equals("06056530740")){
                                String a ="";
                            }
                            if (status_add) {
                                String cs_gan = Objects.toString(khachHangs[i].getCSGan(),"");
                                String cs_go = Objects.toString(khachHangs[i].getCSGan(),"");
                                String tieuthucu =Objects.toString(khachHangs[i].getTieuThuCu(),"");
                                String tieuthu = Objects.toString(khachHangs[i].getKhachhang().getTIEUTHU(),"").trim();
                                String tinhtrang = Objects.toString(khachHangs[i].getKhachhang().getTinhTrang(),"").trim();
                                String tinhtang_dongcua ="";
                                if(!tieuthu.isEmpty() && tinhtrang.equals("Đóng cửa")){
                                    tinhtang_dongcua = "Đóng cửa";
                                }
                                sqlite_danhSachKhackHang_docSo.Insert_Update_Table_DanhSachHangHang(khachHang,cs_go,cs_gan,tieuthucu,"2",tinhtang_dongcua);
                            }
                            if (dialog_progress_taidulieu.getProgress() == dialog_progress_taidulieu.getMax() - 1) {
                                //luu danh sach doc so
                                sqlite_DanhSachDocSo sqlite_danhSachDocSo = new sqlite_DanhSachDocSo(activity_Danhsachdocso.this);
                                if (sqlite_danhSachDocSo.getStatus_Table_DanhSachDocSo_Exists()) {
                                    data_DanhSachDocSo data_danhSachDocSo = new data_DanhSachDocSo(nam,ky,dot,type_may,String.valueOf(sum_dh),"","");
                                    sqlite_danhSachDocSo.Insert_Update_Table_DanhSachDocSo(data_danhSachDocSo);
                                }
                            }
                            i++;
                            Thread.sleep(2);
                            handler_taidulieu.handleMessage(handler_taidulieu.obtainMessage());

                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Display_DanhSachDocSo();
                            }
                        });

                        dialog_progress_taidulieu.dismiss();


                    }
                    Thread.sleep(100);
                    handler_taidulieu.handleMessage(handler_taidulieu.obtainMessage());

                } catch (Exception e) {
                    Log.e("handler_taidulieu",e.toString());
                }
            }
        }).start();

    }

    Handler handler_taidulieu = new Handler() {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            dialog_progress_taidulieu.incrementProgressBy(1);
        }
    };


    private void Sync_DuLieu() {
        int num_check = 0;
        String title = "";
        for (int i = 0; i < Arraylist_data_danhsachdocso.size(); i++) {
            data_Danhsachdocso data_danhSachDocSo = Arraylist_data_danhsachdocso.get(i);
            if (data_danhSachDocSo.getChon()) {
                if (num_check > 1) {
                    break;
                }
                num_check++;
                title = data_danhSachDocSo.getTieude();
            }
        }
        Dialog_App dialog_app = new Dialog_App(activity_Danhsachdocso.this);
        if (num_check == 0) {
                String notìication ="Vui lòng chọn 1 kỳ đọc số cần gửi dữ liệu về server";
                dialog_app.Dialog_Notification(notìication);
        } else if (num_check < 2) {
            if (!title.isEmpty()) {
                Dialog_Notification_Sync("Bạn muốn đồng bộ dữ liệu:"+"\r\n"+title);
            }
        } else {
            String notìication ="Vui lòng chọn 1 kỳ đọc số cần gửi dữ liệu về server";
            dialog_app.Dialog_Notification(notìication);
        }
    }

    private AlertDialog dialog_notification_sync;
    public void Dialog_Notification_Sync(final String title) {
        AlertDialog.Builder alert_taidulieu = new AlertDialog.Builder(activity_Danhsachdocso.this);
        alert_taidulieu.setMessage(title);
        // dong y
        alert_taidulieu.setPositiveButton("Xác nhận",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog,int which) {
                String[] com_notìicationtitle = title.split("\r\n");
                String[] com_title = com_notìicationtitle[1].split("-");
                String nam = com_title[0].trim().substring(1).toString().trim();
                String dot = com_title[1].trim().substring(1).toString().trim();
                String ky = com_title[2].trim().substring(1).toString().trim();
                String may = com_title[3].trim().substring(1).toString().trim();
                SendDocSo_AsyncTask sendDocSo_asyncTask = new SendDocSo_AsyncTask(activity_Danhsachdocso.this,nam,ky,dot,"",may,"all");
                sendDocSo_asyncTask.execute();

            }
        });
        alert_taidulieu.setNegativeButton("Hủy",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog,int which) {
                dialog_notification_sync.dismiss();
            }
        });
        // bo qua

        dialog_notification_sync = alert_taidulieu.create();
        dialog_notification_sync.show();
    }




    ProgressDialog dialog_progress_dongbodulieu;
    boolean send_ok = false;

    private void Dialog_Progress_DongboDulieu(final data_syncghiso_send.Content data_content_syncdulieu) {
        dialog_progress_dongbodulieu = new ProgressDialog(activity_Danhsachdocso.this);
        final String nam = data_content_syncdulieu.getNam();
        final String ky = data_content_syncdulieu.getKy();
        final String dot = data_content_syncdulieu.getDot();
        final String may = data_content_syncdulieu.getMay();
        final int sum_kh = data_content_syncdulieu.getKhachhangs().length;
        String title_dialog = "Đồng bộ dữ liệu năm: " + nam + ", kỳ: " + ky + ", đợt: " + dot + " của máy: " + may;
        dialog_progress_dongbodulieu.setTitle(title_dialog);
        dialog_progress_dongbodulieu.setMessage("Dang tải dữ liệu");
        dialog_progress_dongbodulieu.setMax(sum_kh);
        dialog_progress_dongbodulieu.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        dialog_progress_dongbodulieu.setCancelable(false);
        dialog_progress_dongbodulieu.show();

                try {

                   // for (int i = 0; i < data_content_syncdulieu.getKhachhangs().length; i++) {

                        send_ok = true;
                        data_syncghiso_send.data_khachhang[] khachhangs = data_content_syncdulieu.getKhachhangs();
                        //khachhangs[0] = data_content_syncdulieu.getKhachhangs()[i];
                        data_syncghiso_send.Content content = new data_syncghiso_send.Content(nam,ky,dot,may,khachhangs);
                        data_syncghiso_send data_syncghiso_send = new data_syncghiso_send(content);
                        Gson gson_send = new Gson();
                        String json_send = gson_send.toJson(data_syncghiso_send);
                        StringEntity stringEntity_send = new StringEntity(json_send,"utf-8");
                        Api_syncghiso.post(activity_Danhsachdocso.this,
                                "",
                                stringEntity_send,
                                new TextHttpResponseHandler() {
                                    @Override
                                    public void onFailure(int statusCode,Header[] headers,String responseString,Throwable throwable) {
                                     //   Log.e("ERROR",responseString);
                                        String text = "Lỗi dong bo du lieu doc so" + "\r\n" + "Vui lòng kiểm tra lại.";
                                        Toast.makeText(activity_Danhsachdocso.this,text,Toast.LENGTH_SHORT).show();
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                dialog_progress_dongbodulieu.dismiss();
                                            }
                                        });
                                        send_ok = false;
                                    }

                                    @Override
                                    public void onSuccess(int statusCode,Header[] headers,String responseString) {

                                        Gson gson_reponse = new Gson();
                                        data_syncghiso_response data_syncghiso_response = gson_reponse.fromJson(responseString,data_syncghiso_response.class);
                                //        Log.e("data_R_SDT",gson_reponse.toJson(data_syncghiso_response));
                                        String danhbas = data_syncghiso_response.getResult().getDanhbas();
                                        String [] com_danhba = danhbas.split(",");
                                        if(com_danhba.length >0) {
                                            sqlite_DanhSachKhackHang_DocSo sqlite_danhSachKhackHang_docSo = new sqlite_DanhSachKhackHang_DocSo(activity_Danhsachdocso.this,nam,ky,dot,may);
                                            for (String danhba : com_danhba) {

                                                if (sqlite_danhSachKhackHang_docSo.getStatus_Table_DanhSachKhachHang_Exists() && !danhba.trim().isEmpty()) {
                                                    sqlite_danhSachKhackHang_docSo.Update_Sync_DanhSachDocSo_KhachHang(danhba.trim());
                                                }
                                            }
                                            Dialog_App dialog_app1 = new Dialog_App(activity_Danhsachdocso.this);
                                            String notification = "Đồng bộ dữ liệu thành công danh bạ:"+"\r\n"+danhbas ;
                                            dialog_app1.Dialog_Notification(notification);
                                        }

                                        send_ok = false;
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                dialog_progress_dongbodulieu.dismiss();
                                            }
                                        });
                                    }
                                });

                 //   }

                } catch (Exception e) {
                    Log.e("handler_taidulieu",e.toString());
                }
    }

    Handler handler_update = new Handler() {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            dialog_progress_dongbodulieu.incrementProgressBy(1);

        }
    };


    // TAI TANG CUONG
    private Dialog dialog_taitangcuong ;
    public void showDialog_taitangcuong(final Context context){
        dialog_taitangcuong = new Dialog(context);
        Status_taitangcuong =false;
        dialog_taitangcuong.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_taitangcuong.setCancelable(false);
        dialog_taitangcuong.setContentView(R.layout.dialog_taitangcuong);
        Button btn_xacnhan = (Button) dialog_taitangcuong.findViewById(R.id.button_xacnhan_dialogthongbao);
        Button btn_huy = (Button) dialog_taitangcuong.findViewById(R.id.button_huy_dialogthongbao);
        final EditText edt_Somaytangcuong = ( EditText) dialog_taitangcuong.findViewById(R.id.editext_maytangcuong_dialogthongbao);

        btn_xacnhan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String somay_tangcuong = edt_Somaytangcuong.getText().toString().trim();
                if(!somay_tangcuong.isEmpty()){
                    Status_taitangcuong = true;
                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
                    View view = dialog_taitangcuong.getCurrentFocus();
                    //If no view currently has focus, create a new one, just so we can grab a window token from it
                    if (view == null) {
                        view = new View(context);
                    }
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    Somay_tangcuong =somay_tangcuong;
                }
                dialog_taitangcuong.dismiss();
            }
        });
        btn_huy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_taitangcuong.dismiss();
            }
        });
    }


    private class get_Danhsachdocso extends AsyncTask<Void,Void,String>{
        private Dialog_App dialog_app ;
        private String Nam;
        private String Ky ;
        private String Dot;
        private String Somay;
        private String May_tangcuong;
        private String Thongbao;
        private String ResponseString;
        private String BASE_URL_API = "/apikhachhang/api/DocSoDanhSachDocSo?";
        private String BASE_URL = "";
        public get_Danhsachdocso(String nam,String ky,String dot, String somay , String soma_tangcuong ){
            dialog_app = new Dialog_App(activity_Danhsachdocso.this);
            Nam =nam ;
            Ky = ky;
            Dot = dot;
            Somay = somay;
            Thongbao ="";
            May_tangcuong = soma_tangcuong;
            data_Http data_http = new data_Http(activity_Danhsachdocso.this);
            String url_run = data_http.get_Url_run();
            if(!url_run.isEmpty()){
                BASE_URL = url_run +BASE_URL_API;
            }else {
                BASE_URL = "http://viettel.capnuoccholon.com.vn"+BASE_URL_API;
            }
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if(May_tangcuong.isEmpty()) {
                dialog_app.Dialog_Progress_Wait_Open("Đang tải dữ liệu đọc số.","Vui lòng chờ");
            }else {
                dialog_app.Dialog_Progress_Wait_Open("Đang tải dữ liệu tăng cường.","Vui lòng chờ");
            }
        }

        @Override
        protected String doInBackground(Void... voids) {
            try {
                data_Api_DanhSachDocSo_send data_api_danhSachDocSo_send = new data_Api_DanhSachDocSo_send(Nam,Ky,Dot,Somay);
                Gson gson_send = new Gson();
                String json_send = gson_send.toJson(data_api_danhSachDocSo_send);
                HttpClient httpclient = new DefaultHttpClient();
                URI uri_ = new URIBuilder(BASE_URL).build();
                if(Somay_tangcuong.isEmpty()){
                    uri_ = new URIBuilder(BASE_URL)
                            .addParameter("thamso",json_send)
                            .build();
                }else {
                    uri_ = new URIBuilder(BASE_URL)
                            .addParameter("thamso",json_send)
                            .addParameter("nhanvientangcuong",Somay_tangcuong)
                            .build();
                }
                HttpGet httpget = new HttpGet(uri_);
                //  httppost.setHeader("Accept", "application/json");
                httpget.setHeader("Content-type","application/json;charset=utf-8");
                //     httpget.setEntity(stringEntity_send);
                HttpResponse response = httpclient.execute(httpget);
                if (response.getStatusLine().getStatusCode() == 200) {
                    String responseString = EntityUtils.toString(response.getEntity());
                    ResponseString =responseString;

                }
            } catch (IOException | URISyntaxException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            dialog_app.Dialog_Progress_Wait_Close();
            Gson gson_reponse = new Gson();
            data_Api_DanhSachDocSo_response data_api_danhSachDocSo_response = gson_reponse.fromJson(ResponseString,data_Api_DanhSachDocSo_response.class);
            if(data_api_danhSachDocSo_response.getResult().getKhachhangs().length >0) {

                    Dialog_Progress_TaiDuLieu(data_api_danhSachDocSo_response,May_tangcuong);

            }else {
                if(May_tangcuong.isEmpty()) {
                    dialog_app.Dialog_Notification("Không có dữ liệu đọc số" + "\r\n" + "Năm: " + Nam + " Kỳ: " + Ky + " Đợt: " + Dot + " Số máy: " + Somay);
                }else {
                    dialog_app.Dialog_Notification("Không có dữ liệu đọc số" + "\r\n" + "Năm: " + Nam + " Kỳ: " + Ky + " Đợt: " + Dot + " Số máy: " + Somay+" Máy tăng cường : "+May_tangcuong);
                }
            }
        }
    }


 
}
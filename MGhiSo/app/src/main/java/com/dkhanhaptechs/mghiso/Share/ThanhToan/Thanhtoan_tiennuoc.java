package com.dkhanhaptechs.mghiso.Share.ThanhToan;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

public class Thanhtoan_tiennuoc {
    private SimpleDateFormat format_datetime = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    public Thanhtoan_tiennuoc() {
    }

    public data_Tiennuoc Tiennuoc(int tieuthu,int giabieu,int dinhmuc,int dinhmucngheo,int tl_kinhdoanh,int tl_hcsn,int tl_sh,int tl_sx,String tungay,String denngay) {
        long tiennuoc = 0;
        data_Tiennuoc data_tiennuoc = new data_Tiennuoc();
        try {
            Date date_tungay = format_datetime.parse(tungay);

            Calendar calendar_tungay = Calendar.getInstance();
            calendar_tungay.setTime(date_tungay);
            int year_tungay = calendar_tungay.get(Calendar.YEAR);

            Date date_denngay = format_datetime.parse(denngay);
            Calendar calendar_denngay = Calendar.getInstance();
            calendar_denngay.setTime(date_denngay);
            int year_denngay = calendar_denngay.get(Calendar.YEAR);
            if(year_tungay !=year_denngay){
                long diff_tungay = date_tungay.getTime();
                long diff_denngay = date_denngay.getTime();
                long diff = diff_denngay - diff_tungay;
                int songay_ttieuthu = (int)( diff / 1000 / 60 / 60 / 24);
                double songay_nammoi = (double)calendar_denngay.get(Calendar.DAY_OF_MONTH)-1;
                int tieuthu_cu = (int) Math.round(tieuthu * (songay_ttieuthu - songay_nammoi) / songay_ttieuthu);
                int tieuthu_moi = tieuthu - tieuthu_cu;
                int dinhmuc_cu = (int) Math.round(dinhmuc * (songay_ttieuthu - songay_nammoi) / songay_ttieuthu);
                int dinhmuc_moi = dinhmuc - dinhmuc_cu;
                int dinhmucngheo_cu = (int) Math.round(dinhmucngheo * (songay_ttieuthu - songay_nammoi) / songay_ttieuthu);
                int dinhmucngheo_moi = dinhmucngheo - dinhmucngheo_cu;
                if(year_tungay ==2020 && year_denngay == 2021) {

                    long tiennuoc_2021 = GiaNuoc_2021(tieuthu_moi, giabieu, dinhmuc, dinhmucngheo, tl_kinhdoanh, tl_hcsn, tl_sh, tl_sx);
                    long tiennuoc_2020 = GiaNuoc_2020(tieuthu_cu, giabieu, dinhmuc, dinhmucngheo, tl_kinhdoanh, tl_hcsn, tl_sh, tl_sx);
                    tiennuoc = tiennuoc_2020 + tiennuoc_2021;
                }else  if(year_tungay ==2021 && year_denngay == 2022) {
                    // nam 20220
                    long tiennuoc_2022 = (long)GiaNuoc_2022(tieuthu_moi, giabieu, dinhmuc_moi, dinhmucngheo_moi, tl_kinhdoanh, tl_hcsn, tl_sh, tl_sx,1.0);
                   // int tienthue_5_2022 = (int)Math.round(tiennuoc_2022*0.05);
                    long tiendichvuthoatnuoc_2022 = (long)GiaNuoc_2022(tieuthu_moi, giabieu, dinhmuc_moi, dinhmucngheo_moi, tl_kinhdoanh, tl_hcsn, tl_sh, tl_sx,0.15);
                    long thue_dichvuthoatnuoc10_2022 = (long)Math.round(tiendichvuthoatnuoc_2022*(0.1));

                    long tiennuoc_2021 = (long) GiaNuoc_2021(tieuthu_cu, giabieu, dinhmuc_cu, dinhmucngheo_cu, tl_kinhdoanh, tl_hcsn, tl_sh, tl_sx,1.0);
                  //  int tienthue_5_2021 = (int)Math.round(tiennuoc_2021*0.05);
                    long tien_phibvmt_2021 = (long)GiaNuoc_2021(tieuthu_cu, giabieu, dinhmuc_cu, dinhmucngheo_cu, tl_kinhdoanh, tl_hcsn, tl_sh, tl_sx,0.1);

                    tiennuoc = tiennuoc_2022 + tiennuoc_2021;
                    long tienthue_5 = (long)Math.round(tiennuoc*0.05);
                    long tien_phibvmt = tien_phibvmt_2021;
                    long tiendichvuthoatnuoc = tiendichvuthoatnuoc_2022;
                    long thuedichvuthoatnuoc = thue_dichvuthoatnuoc10_2022;
                    data_tiennuoc.setTiennuoc(String.valueOf(tiennuoc));
                    data_tiennuoc.setThuetiennuoc_5(String.valueOf(tienthue_5));
                    data_tiennuoc.setPhiBVMT(String.valueOf(tien_phibvmt));
                    data_tiennuoc.setDichvuthoatnuoc(String.valueOf(tiendichvuthoatnuoc));
                    data_tiennuoc.setThuedichvuthoatnuoc(String.valueOf(thuedichvuthoatnuoc));
                }

            }else {
                if(year_tungay == 2021) {
                    tiennuoc = GiaNuoc_2021(tieuthu, giabieu, dinhmuc, dinhmucngheo, tl_kinhdoanh, tl_hcsn, tl_sh, tl_sx,1.0);
                    long tienthue_5 = (long)Math.round(tiennuoc*0.05);
                    long tien_phibvmt = (long)GiaNuoc_2021(tieuthu, giabieu, dinhmuc, dinhmucngheo, tl_kinhdoanh, tl_hcsn, tl_sh, tl_sx,0.1);
                    data_tiennuoc.setTiennuoc(String.valueOf(tiennuoc));
                    data_tiennuoc.setThuetiennuoc_5(String.valueOf(tienthue_5));
                    data_tiennuoc.setPhiBVMT(String.valueOf(tien_phibvmt));
                    data_tiennuoc.setDichvuthoatnuoc("0");
                    data_tiennuoc.setThuedichvuthoatnuoc("0");
                }else  if(year_tungay == 2022) {
                    tiennuoc = GiaNuoc_2022(tieuthu, giabieu, dinhmuc, dinhmucngheo, tl_kinhdoanh, tl_hcsn, tl_sh, tl_sx,1.0);
                    long tienthue_5 = (long)Math.round(tiennuoc*0.05);
                    long tiendichvuthoatnuoc = (long)GiaNuoc_2022(tieuthu, giabieu, dinhmuc, dinhmucngheo, tl_kinhdoanh, tl_hcsn, tl_sh, tl_sx,0.15);
                    long thue_dichvuthoatnuoc10 =(long)Math.round(tiendichvuthoatnuoc *0.1);
                    // thay doi thue dich vu thoat nuoc tu 10% xuong 8%
                  //  long thue_dichvuthoatnuoc10 =(long)Math.round(tiendichvuthoatnuoc *0.08);
                    data_tiennuoc.setTiennuoc(String.valueOf(tiennuoc));
                    data_tiennuoc.setThuetiennuoc_5(String.valueOf(tienthue_5));
                    data_tiennuoc.setDichvuthoatnuoc(String.valueOf(tiendichvuthoatnuoc));
                    data_tiennuoc.setThuedichvuthoatnuoc(String.valueOf(thue_dichvuthoatnuoc10));
                    data_tiennuoc.setPhiBVMT("0");
                }
             //   tiennuoc = GiaNuoc_2021(19,11,12,0,0,0,0,0);
              //  String aa ="";
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }



        return data_tiennuoc;
    }
    public int Tiennuoc_ThueBVMT_Covid(int tieuthu,int giabieu,int dinhmuc,int dinhmucngheo,int tl_kinhdoanh,int tl_hcsn,int tl_sh,int tl_sx) {
        double tiennuoc = 0;
        try {

                tiennuoc = GiaNuoc_ThueBVMT_2021_Covid(tieuthu,giabieu,dinhmuc,dinhmucngheo,tl_kinhdoanh,tl_hcsn,tl_sh,tl_sx);
                //   tiennuoc = GiaNuoc_2021(19,11,12,0,0,0,0,0);
                //  String aa ="";
        } catch (Exception e) {
            e.printStackTrace();
        }


        return (int) (tiennuoc);
    }
    private long GiaNuoc_2022(int tieuthu,int giabieu,int dinhmuc,int dinhmucngheo,int tl_kinhdoanh,int tl_hcsn,int tl_sh,int tl_sx, double tl_tiennuoc) {
        long tiennuoc = 0;
        int tt_sh = (int) Math.round((tieuthu* tl_sh*0.01));
        int tt_hcsn = (int) Math.round((tieuthu*tl_hcsn*0.01));
        int tt_sx = (int) Math.round((tieuthu * tl_sx*0.01));
        int tt_kd = (int) Math.round((tieuthu* tl_kinhdoanh*0.01));
        // gia nuoc

        int g_shn =(int)Math.round(6300*tl_tiennuoc);
        int g_sh =(int)Math.round(6700*tl_tiennuoc);
        int g_shvm1 =(int)Math.round(12900*tl_tiennuoc);
        int g_shvm2 =(int)Math.round(14400*tl_tiennuoc);
        int g_hcsn =(int)Math.round(13000*tl_tiennuoc);
        int g_sx =(int)Math.round(12100*tl_tiennuoc);
        int g_kd =(int)Math.round(21300*tl_tiennuoc);
        int g_dv =(int)Math.round(21300*tl_tiennuoc);

        int g_shn_tckcn = (int)Math.round(5670*tl_tiennuoc);
        int g_sh_tckcn =(int)Math.round(6030*tl_tiennuoc);
        int g_shvm1_tckcn =(int)Math.round(11610*tl_tiennuoc);
        int g_shvm2_tckcn =(int)Math.round(12960*tl_tiennuoc);
        int g_hcsn_tckcn =(int)Math.round(11700*tl_tiennuoc);
        int g_sx_tckcn =(int)Math.round(10890*tl_tiennuoc);
        int g_kd_tckcn =(int)Math.round(19170*tl_tiennuoc);




        switch (giabieu) {

            // TU GIA
            case 10: {
                //SH Nghèo thuần túy
                // (SHN x G_SHN) + (SH x G_SH) + (SHVM1 x G_SHVM1) + (SHVM2 x G_SHVM2)
                int tieuthu_sh =tieuthu;
                int dinhmuc_sh = dinhmuc;
                int tieuthu_shn =0;
                if(dinhmucngheo !=0){
                    if(dinhmuc < dinhmucngheo){
                        dinhmucngheo= dinhmuc;
                        dinhmuc_sh = 0;
                    }else {
                        dinhmuc_sh = dinhmuc - dinhmucngheo;
                    }
                    if(tieuthu < dinhmuc) {
                        tieuthu_shn = Math.round((dinhmucngheo * tieuthu) / dinhmuc);
                        tieuthu_sh = tieuthu - tieuthu_shn;
                    }else {
                        tieuthu_shn = dinhmucngheo;
                        tieuthu_sh = tieuthu - dinhmucngheo;
                    }
                }
                int tt_vuotdm1 =  (int)Math.round(dinhmuc_sh*1.5);
                if (tieuthu_sh <= dinhmuc_sh) {
                    tiennuoc = tieuthu_shn * g_shn + tieuthu_sh*g_sh;
                } else if ( tieuthu_sh <= tt_vuotdm1) {
                    tiennuoc = tieuthu_shn * g_shn + (dinhmuc_sh)*g_sh+ (tieuthu_sh - dinhmuc_sh)*g_shvm1;
                    //tiennuoc = g_shn * dinhmucngheo + (tieuthu - dinhmucngheo) * g_shvm1;
                } else {
                    int tt_vm1 = (int)Math.round(dinhmuc_sh*0.5);
                    int tt_vm2 = tieuthu_sh - dinhmuc_sh - tt_vm1;
                    tiennuoc = tieuthu_shn * g_shn + (dinhmuc_sh)*g_sh+ tt_vm1*g_shvm1 +tt_vm2*g_shvm2;
                    //tiennuoc = g_shn * dinhmucngheo + dinhmucngheo*0.5 * g_shvm1 + (tieuthu - dinhmucngheo * 1.5) * g_shvm2;
                }
                break;
            }
            case 11: {
                //SH thuần túy
                // (SHN x G_SHN) + (SH x G_SH) + (SHVM1 x G_SHVM1) + (SHVM2 x G_SHVM2)
                int tieuthu_sh =tieuthu;
                int dinhmuc_sh = dinhmuc;
                int tieuthu_shn =0;
                if(dinhmucngheo !=0) {
                    if (dinhmuc < dinhmucngheo) {
                        dinhmucngheo= dinhmuc;
                        dinhmuc_sh = 0;
                    } else {
                        dinhmuc_sh = dinhmuc - dinhmucngheo;
                    }
                    if (tieuthu < dinhmuc) {
                        tieuthu_shn = Math.round((dinhmucngheo * tieuthu) / dinhmuc);
                        tieuthu_sh = tieuthu - tieuthu_shn;
                    } else {
                        tieuthu_shn = dinhmucngheo;
                        tieuthu_sh = tieuthu - dinhmucngheo;
                    }

                }
                int tt_vuotdm1 =  (int)Math.round(dinhmuc_sh*1.5);
                if (tieuthu_sh <= dinhmuc_sh) {
                    tiennuoc = tieuthu_shn * g_shn + tieuthu_sh*g_sh;
                } else if ( tieuthu_sh <= tt_vuotdm1) {
                    tiennuoc = tieuthu_shn * g_shn + (dinhmuc_sh)*g_sh+ (tieuthu_sh - dinhmuc_sh)*g_shvm1;
                    //tiennuoc = g_shn * dinhmucngheo + (tieuthu - dinhmucngheo) * g_shvm1;
                } else {
                    int tt_vm1 = (int)Math.round(dinhmuc_sh*0.5);
                    int tt_vm2 = tieuthu_sh - dinhmuc_sh - tt_vm1;
                    tiennuoc = tieuthu_shn * g_shn + (dinhmuc_sh)*g_sh+ tt_vm1*g_shvm1 +tt_vm2*g_shvm2;
                    //tiennuoc = g_shn * dinhmucngheo + dinhmucngheo*0.5 * g_shvm1 + (tieuthu - dinhmucngheo * 1.5) * g_shvm2;
                }
                break;
            }
            case 12: {
                //SX thuần túy
                // (Toàn bộ tiêu thụ) x G_SX
                tiennuoc = tieuthu * g_sx;
                break;
            }
            case 13: {
                //KD thuần túy
                // (Toàn bộ tiêu thụ) x G_KD_DC
                tiennuoc = tieuthu * g_kd;
                break;
            }
            case 14: {
                //SH + SX
                // (SHN x G_SHN) + (SH x G_SH) + (Tiêu thụ VM x G_SX)

                int tieuthu_sh =tieuthu;
                int dinhmuc_sh = dinhmuc;
                int tieuthu_shn =0;
                if(dinhmucngheo !=0){
                    if(dinhmuc < dinhmucngheo){
                        dinhmucngheo= dinhmuc;
                        dinhmuc_sh = 0;
                    }else {
                        dinhmuc_sh = dinhmuc - dinhmucngheo;
                    }
                    if(tieuthu < dinhmuc) {
                        tieuthu_shn = Math.round((dinhmucngheo * tieuthu) / dinhmuc);
                        tieuthu_sh = tieuthu - tieuthu_shn;
                    }else {
                        tieuthu_shn = dinhmucngheo;
                        tieuthu_sh = tieuthu - dinhmucngheo;
                    }
                }

                if (tieuthu_sh <= dinhmuc_sh) {
                    tiennuoc = tieuthu_shn * g_shn + tieuthu_sh*g_sh;
                } else {
                    tiennuoc = tieuthu_shn * g_shn + (dinhmuc_sh)*g_sh+ (tieuthu_sh- dinhmuc_sh)*g_sx ;
                    //tiennuoc = g_shn * dinhmucngheo + dinhmucngheo*0.5 * g_shvm1 + (tieuthu - dinhmucngheo * 1.5) * g_shvm2;
                }
                break;


            }
            case 15: {
                //SH + KD
                // (SHN x G_SHN) + (SH x G_SH) + (Tiêu thụ VM x G_KD_DV)
                int tieuthu_sh =tieuthu;
                int dinhmuc_sh = dinhmuc;
                int tieuthu_shn =0;
                if(dinhmucngheo !=0){
                    if(dinhmuc < dinhmucngheo){
                        dinhmucngheo= dinhmuc;
                        dinhmuc_sh = 0;
                    }else {
                        dinhmuc_sh = dinhmuc - dinhmucngheo;
                    }
                    if(tieuthu < dinhmuc) {
                        tieuthu_shn = Math.round((dinhmucngheo * tieuthu) / dinhmuc);
                        tieuthu_sh = tieuthu - tieuthu_shn;
                    }else {
                        tieuthu_shn = dinhmucngheo;
                        tieuthu_sh = tieuthu - dinhmucngheo;
                    }
                }

                if (tieuthu_sh <= dinhmuc_sh) {
                    tiennuoc = tieuthu_shn * g_shn + tieuthu_sh*g_sh;
                } else {
                    tiennuoc = tieuthu_shn * g_shn + (dinhmuc_sh)*g_sh+ (tieuthu_sh- dinhmuc_sh)*g_kd ;
                    //tiennuoc = g_shn * dinhmucngheo + dinhmucngheo*0.5 * g_shvm1 + (tieuthu - dinhmucngheo * 1.5) * g_shvm2;
                }
                break;
            }
            case 16: {
                //SH + SX + KD
                //Tỉ lệ không có Định mức: (% SX) x G_SX + (% KD) x G_KD_DV + (%SH)x G_SHVM2
                //Tỉ lệ có Định mức: (%SH) = [(SH x G_SH) + (SHVM1 x G_SHVM1) + (SHVM2 x G_SHVM2)] + (%SX) x G_SX + (%KD) x G_KD_DV

                if (dinhmuc == 0) {
                    if((tt_sx+tt_kd + tt_sh)!=tieuthu){
                        tt_kd =tieuthu -tt_sx - tt_sh;
                    }
                    tiennuoc = tt_sx * g_sx + tt_kd * g_kd + tt_sh*g_shvm2;
                } else {
                    if((tt_sx+tt_kd+tt_sh)!=tieuthu){
                        tt_kd =tieuthu -tt_sx- tt_sh;
                    }
                    int tt_vuotdm1 =  (int)Math.round(dinhmuc*1.5);
                    if (tt_sh <= dinhmuc) {
                        tiennuoc = tt_sh * g_sh  + tt_sx* g_sx + tt_kd * g_kd;
                    } else if (tt_sh <= tt_vuotdm1) {
                        tiennuoc = dinhmuc * g_sh + (tt_sh - dinhmuc) * g_shvm1 + tt_sx * g_sx + tt_kd * g_kd;
                    } else {
                        int tt_vm1 = (int)Math.round(dinhmuc*0.5);
                        int tt_vm2 = tt_sh - dinhmuc - tt_vm1;
                        tiennuoc = dinhmuc * g_sh + tt_vm1 * g_shvm1  + tt_vm2 * g_shvm2 + tt_sx * g_sx + tt_kd * g_kd;
                    }
                }
                break;
            }
            case 17: {
                //SH đặc biệt
                //(Toàn bộ tiêu thụ) x G_SH
                tiennuoc = tieuthu * g_shn;
                break;
            }
            case 18: {
                //SH + HCSN
                //(SHN x G_SHN) + (SH x G_SH) + (Tiêu thụ VM x G_HCSN)
                int tieuthu_sh =tieuthu;
                int dinhmuc_sh = dinhmuc;
                int tieuthu_shn =0;
                if(dinhmucngheo !=0){
                    if(dinhmuc < dinhmucngheo){
                        dinhmucngheo= dinhmuc;
                        dinhmuc_sh = 0;
                    }else {
                        dinhmuc_sh = dinhmuc - dinhmucngheo;
                    }
                    if(tieuthu < dinhmuc) {
                        tieuthu_shn = Math.round((dinhmucngheo * tieuthu) / dinhmuc);
                        tieuthu_sh = tieuthu - tieuthu_shn;
                    }else {
                        tieuthu_shn = dinhmucngheo;
                        tieuthu_sh = tieuthu - dinhmucngheo;
                    }
                }

                if (tieuthu_sh <= dinhmuc_sh) {
                    tiennuoc = tieuthu_shn * g_shn + tieuthu_sh*g_sh;
                } else {
                    tiennuoc = tieuthu_shn * g_shn + (dinhmuc_sh)*g_sh+ (tieuthu_sh- dinhmuc_sh)*g_hcsn ;
                    //tiennuoc = g_shn * dinhmucngheo + dinhmucngheo*0.5 * g_shvm1 + (tieuthu - dinhmucngheo * 1.5) * g_shvm2;
                }
                break;
            }
            case 19: {
                //SH + SX + KD + HCSN
                //Tỉ lệ không có Định mức: (% SX) x G_SX + (% KD) x G_KD_DV + (%HCSN) x G_HCSN + (% SH) x G_SHVM2
                //Tỉ lệ có Định mức: (%SH) = [(SHN)x G_SHN + (SH x G_SH) + (SHVM1 x G_SHVM1) + (SHVM2 x G_SHVM2)] + (%SX) x G_SX + (%KD) x G_KD_DV + (%HCSN) x G_HCSN

                if (dinhmuc == 0) {
                    if((tt_hcsn+tt_sx+tt_kd + tt_sh) !=tieuthu){
                        tt_kd =tieuthu -tt_hcsn-tt_sx - tt_sh;
                    }
                    tiennuoc = tt_sx * g_sx + tt_kd * g_kd + tt_hcsn * g_hcsn + tt_sh*g_shvm2;
                } else {
                    if((tt_sx+tt_kd+tt_hcsn+tt_sh) !=tieuthu){
                        tt_kd = tieuthu - tt_sx -tt_hcsn- tt_sh;
                    }

                    //////
                    int tieuthu_sh =tt_sh;
                    int dinhmuc_sh = dinhmuc;
                    int tieuthu_shn =0;
                    if(dinhmucngheo !=0){
                        if(dinhmuc < dinhmucngheo){
                            dinhmucngheo= dinhmuc;
                            dinhmuc_sh = 0;
                        }else {
                            dinhmuc_sh = dinhmuc - dinhmucngheo;
                        }
                        if(tt_sh < dinhmuc) {
                            tieuthu_shn = Math.round((dinhmucngheo * tt_sh) / dinhmuc);
                            tieuthu_sh = tt_sh - tieuthu_shn;
                        }else {
                            tieuthu_shn = dinhmucngheo;
                            tieuthu_sh = tt_sh - dinhmucngheo;
                        }
                    }
                    int tt_vuotdm1 =  (int)Math.round(dinhmuc_sh*1.5);
                    if (tieuthu_sh <= dinhmuc_sh) {
                        tiennuoc = tieuthu_shn * g_shn + tieuthu_sh*g_sh +tt_sx * g_sx + tt_kd * g_kd + tt_hcsn * g_hcsn;;
                    } else if ( tieuthu_sh <= tt_vuotdm1) {
                        tiennuoc = tieuthu_shn * g_shn + (dinhmuc_sh)*g_sh+ (tieuthu_sh - dinhmuc_sh)*g_shvm1+ tt_sx * g_sx + tt_kd * g_kd + tt_hcsn * g_hcsn;;
                        //tiennuoc = g_shn * dinhmucngheo + (tieuthu - dinhmucngheo) * g_shvm1;
                    } else {
                        int tt_vm1 = (int)Math.round(dinhmuc_sh*0.5);
                        int tt_vm2 = tieuthu_sh - dinhmuc_sh - tt_vm1;
                        tiennuoc = tieuthu_shn * g_shn + (dinhmuc_sh)*g_sh+ tt_vm1*g_shvm1 +tt_vm2*g_shvm2+ tt_sx * g_sx + tt_kd * g_kd + tt_hcsn * g_hcsn;;
                        //tiennuoc = g_shn * dinhmucngheo + dinhmucngheo*0.5 * g_shvm1 + (tieuthu - dinhmucngheo * 1.5) * g_shvm2;
                    }


                }
                break;
            }

            // HO TAP THE
            case 21: {
                //SH thuần túy
                //(SHN x G_SHN) + (SH x G_SH) + (SHVM1 x G_SHVM1) + (SHVM2 x G_SHVM2)

                int tieuthu_sh =tieuthu;
                int dinhmuc_sh = dinhmuc;
                int tieuthu_shn =0;
                if(dinhmucngheo !=0){
                    if(dinhmuc < dinhmucngheo){
                        dinhmucngheo= dinhmuc;
                        dinhmuc_sh = 0;
                    }else {
                        dinhmuc_sh = dinhmuc - dinhmucngheo;
                    }
                    if(tieuthu < dinhmuc) {
                        tieuthu_shn = Math.round((dinhmucngheo * tieuthu) / dinhmuc);
                        tieuthu_sh = tieuthu - tieuthu_shn;
                    }else {
                        tieuthu_shn = dinhmucngheo;
                        tieuthu_sh = tieuthu - dinhmucngheo;
                    }
                }
                int tt_vuotdm1 =  (int)Math.round(dinhmuc_sh*1.5);
                if (tieuthu_sh <= dinhmuc_sh) {
                    tiennuoc = tieuthu_shn * g_shn + tieuthu_sh*g_sh;
                } else if ( tieuthu_sh <= tt_vuotdm1) {
                    tiennuoc = tieuthu_shn * g_shn + (dinhmuc_sh)*g_sh+ (tieuthu_sh - dinhmuc_sh)*g_shvm1;
                    //tiennuoc = g_shn * dinhmucngheo + (tieuthu - dinhmucngheo) * g_shvm1;
                } else {
                    int tt_vm1 = (int)Math.round(dinhmuc_sh*0.5);
                    int tt_vm2 = tieuthu_sh - dinhmuc_sh - tt_vm1;
                    tiennuoc = tieuthu_shn * g_shn + (dinhmuc_sh)*g_sh+ tt_vm1*g_shvm1 +tt_vm2*g_shvm2;
                    //tiennuoc = g_shn * dinhmucngheo + dinhmucngheo*0.5 * g_shvm1 + (tieuthu - dinhmucngheo * 1.5) * g_shvm2;
                }
                break;
            }
            case 22: {
                //SX thuần túy
                // (Toàn bộ tiêu thụ) x G_SX
                tiennuoc = tieuthu * g_sx;
                break;
            }
            case 23: {
                //KD thuần túy
                // (Toàn bộ tiêu thụ) x G_KD_DV
                tiennuoc = tieuthu * g_kd;
                break;
            }
            case 24: {
                //SH + SX
                // (SHN x G_SHN) + (SH x G_SH) + (Tiêu thụ VM x G_SX)
                int tieuthu_sh =tieuthu;
                int dinhmuc_sh = dinhmuc;
                int tieuthu_shn =0;
                if(dinhmucngheo !=0){
                    if(dinhmuc < dinhmucngheo){
                        dinhmucngheo= dinhmuc;
                        dinhmuc_sh = 0;
                    }else {
                        dinhmuc_sh = dinhmuc - dinhmucngheo;
                    }
                    if(tieuthu < dinhmuc) {
                        tieuthu_shn = Math.round((dinhmucngheo * tieuthu) / dinhmuc);
                        tieuthu_sh = tieuthu - tieuthu_shn;
                    }else {
                        tieuthu_shn = dinhmucngheo;
                        tieuthu_sh = tieuthu - dinhmucngheo;
                    }
                }

                if (tieuthu_sh <= dinhmuc_sh) {
                    tiennuoc = tieuthu_shn * g_shn + tieuthu_sh*g_sh;
                } else {
                    tiennuoc = tieuthu_shn * g_shn + (dinhmuc_sh)*g_sh+ (tieuthu_sh- dinhmuc_sh)*g_sx ;
                    //tiennuoc = g_shn * dinhmucngheo + dinhmucngheo*0.5 * g_shvm1 + (tieuthu - dinhmucngheo * 1.5) * g_shvm2;
                }
                break;
            }
            case 25: {
                // SH + KD
                // (SHN x G_SHN) + (SH x G_SH) + (Tiêu thụ VM x G_KD_DV)
                int tieuthu_sh =tieuthu;
                int dinhmuc_sh = dinhmuc;
                int tieuthu_shn =0;
                if(dinhmucngheo !=0){
                    if(dinhmuc < dinhmucngheo){
                        dinhmucngheo= dinhmuc;
                        dinhmuc_sh = 0;
                    }else {
                        dinhmuc_sh = dinhmuc - dinhmucngheo;
                    }
                    if(tieuthu < dinhmuc) {
                        tieuthu_shn = Math.round((dinhmucngheo * tieuthu) / dinhmuc);
                        tieuthu_sh = tieuthu - tieuthu_shn;
                    }else {
                        tieuthu_shn = dinhmucngheo;
                        tieuthu_sh = tieuthu - dinhmucngheo;
                    }
                }

                if (tieuthu_sh <= dinhmuc_sh) {
                    tiennuoc = tieuthu_shn * g_shn + tieuthu_sh*g_sh;
                } else {
                    tiennuoc = tieuthu_shn * g_shn + (dinhmuc_sh)*g_sh+ (tieuthu_sh- dinhmuc_sh)*g_kd ;
                    //tiennuoc = g_shn * dinhmucngheo + dinhmucngheo*0.5 * g_shvm1 + (tieuthu - dinhmucngheo * 1.5) * g_shvm2;
                }
                break;
            }
            case 26: {
                //SH + SX + KD
                //Tỉ lệ không có Định mức: (% SX) x G_SX + (% KD) x G_KD_DV + (% SH) x G_SHVM2
                //Tỉ lệ có Định mức: (%SH) = [(SH x G_SH) + (SHVM1 x G_SHVM1) + (SHVM2 x G_SHVM2)] + (%SX) x G_SX + (%KD) x G_KD_DV

                if (dinhmuc == 0) {
                    if((tt_sx+tt_kd + tt_sh)!=tieuthu){
                        tt_kd =tieuthu -tt_sx - tt_sh;
                    }
                    tiennuoc = tt_sx * g_sx + tt_kd * g_kd + tt_sh* g_shvm2;
                } else {
                    if((tt_sx+tt_kd+tt_hcsn+tt_sh) !=tieuthu){
                        tt_kd = tieuthu - tt_sx -tt_hcsn- tt_sh;
                    }
                    int tt_vuotdm1 =  (int)Math.round(dinhmuc*1.5);
                    if (tt_sh <= dinhmuc) {
                        tiennuoc = tt_sh * g_sh + tt_sx * g_sx + tt_kd * g_kd;
                    } else if (tt_sh <= tt_vuotdm1) {
                        tiennuoc = dinhmuc * g_sh + (tt_sh - dinhmuc) * g_shvm1  + tt_sx * g_sx + tt_kd * g_kd;
                    } else {
                        int tt_vm1 = (int)Math.round(dinhmuc*0.5);
                        int tt_vm2 = tt_sh - dinhmuc - tt_vm1;
                        tiennuoc = dinhmuc * g_sh + tt_vm1 * g_shvm1  + tt_vm2 * g_shvm2  + tt_sx * g_sx + tt_kd * g_kd;
                    }
                }
                break;
            }
            case 27: {
                //SH đặc biệt
                //(Toàn bộ tiêu thụ) x G_SH
                tiennuoc = tieuthu * g_sh;
                break;
            }
            case 28: {
                //SH + HCSN
                //(SHN x G_SHN) + (SH x G_SH) + (Tiêu thụ VM x G_HCSN)
                int tieuthu_sh =tieuthu;
                int dinhmuc_sh = dinhmuc;
                int tieuthu_shn =0;
                if(dinhmucngheo !=0){
                    if(dinhmuc < dinhmucngheo){
                        dinhmucngheo= dinhmuc;
                        dinhmuc_sh = 0;
                    }else {
                        dinhmuc_sh = dinhmuc - dinhmucngheo;
                    }
                    if(tieuthu < dinhmuc) {
                        tieuthu_shn = Math.round((dinhmucngheo * tieuthu) / dinhmuc);
                        tieuthu_sh = tieuthu - tieuthu_shn;
                    }else {
                        tieuthu_shn = dinhmucngheo;
                        tieuthu_sh = tieuthu - dinhmucngheo;
                    }
                }

                if (tieuthu_sh <= dinhmuc_sh) {
                    tiennuoc = tieuthu_shn * g_shn + tieuthu_sh*g_sh;
                } else {
                    tiennuoc = tieuthu_shn * g_shn + (dinhmuc_sh)*g_sh+ (tieuthu_sh- dinhmuc_sh)*g_hcsn ;
                    //tiennuoc = g_shn * dinhmucngheo + dinhmucngheo*0.5 * g_shvm1 + (tieuthu - dinhmucngheo * 1.5) * g_shvm2;
                }
                break;
            }
            case 29: {
                //SH + SX + KD + HCSN
                //Tỉ lệ không có Định mức: (% SX) x G_SX + (% KD) x G_KD_DV + (%HCSN) x G_HCSN + (%SH)x G_SHVM2
                //Tỉ lệ có Định mức: (%SH) = [(SHN)x G_SHN + (SH x G_SH) + (SHVM1 x G_SHVM1) + (SHVM2 x G_SHVM2)] + (%SX) x G_SX + (%KD) x G_KD_DV + (%HCSN) x G_HCSN

                if (dinhmuc == 0) {
                    if((tt_hcsn+tt_sx+tt_kd + tt_sh) !=tieuthu){
                        tt_kd =tieuthu -tt_hcsn-tt_sx - tt_sh;
                    }
                    tiennuoc = tt_sx * g_sx + tt_kd * g_kd + tt_hcsn * g_hcsn + tt_sh*g_shvm2;
                } else {

                    if((tt_sx+tt_kd+tt_hcsn+tt_sh) !=tieuthu){
                        tt_kd = tieuthu - tt_sx -tt_hcsn- tt_sh;
                    }

                    int tieuthu_sh =tt_sh;
                    int dinhmuc_sh = dinhmuc;
                    int tieuthu_shn =0;
                    if(dinhmucngheo !=0){
                        if(dinhmuc < dinhmucngheo){
                            dinhmucngheo= dinhmuc;
                            dinhmuc_sh = 0;
                        }else {
                            dinhmuc_sh = dinhmuc - dinhmucngheo;
                        }
                        if(tt_sh < dinhmuc) {
                            tieuthu_shn = Math.round((dinhmucngheo * tt_sh) / dinhmuc);
                            tieuthu_sh = tt_sh - tieuthu_shn;
                        }else {
                            tieuthu_shn = dinhmucngheo;
                            tieuthu_sh = tt_sh - dinhmucngheo;
                        }
                    }
                    int tt_vuotdm1 =  (int)Math.round(dinhmuc_sh*1.5);
                    if (tieuthu_sh <= dinhmuc_sh) {
                        tiennuoc = tieuthu_shn * g_shn + tieuthu_sh*g_sh +tt_sx * g_sx + tt_kd * g_kd + tt_hcsn * g_hcsn;;
                    } else if ( tieuthu_sh <= tt_vuotdm1) {
                        tiennuoc = tieuthu_shn * g_shn + (dinhmuc_sh)*g_sh+ (tieuthu_sh - dinhmuc_sh)*g_shvm1+ tt_sx * g_sx + tt_kd * g_kd + tt_hcsn * g_hcsn;;
                        //tiennuoc = g_shn * dinhmucngheo + (tieuthu - dinhmucngheo) * g_shvm1;
                    } else {
                        int tt_vm1 = (int)Math.round(dinhmuc_sh*0.5);
                        int tt_vm2 = tieuthu_sh - dinhmuc_sh - tt_vm1;
                        tiennuoc = tieuthu_shn * g_shn + (dinhmuc_sh)*g_sh+ tt_vm1*g_shvm1 +tt_vm2*g_shvm2+ tt_sx * g_sx + tt_kd * g_kd + tt_hcsn * g_hcsn;;
                        //tiennuoc = g_shn * dinhmucngheo + dinhmucngheo*0.5 * g_shvm1 + (tieuthu - dinhmucngheo * 1.5) * g_shvm2;
                    }
                }
                break;
            }
            // CO QUAN
            case 31: {
                //Hành Chánh Sự Nghiệp
                //(Toàn bộ tiêu thụ) x G_HCSN
                tiennuoc = tieuthu * g_hcsn;
                break;
            }
            case 32: {
                //Sản xuất
                //(Toàn bộ tiêu thụ) x G_SX
                tiennuoc = tieuthu * g_sx;
                break;
            }
            case 33: {
                //Kinh doanh dịch vụ
                //(Toàn bộ tiêu thụ) x G_KD_DV
                tiennuoc = tieuthu * g_dv;
                break;
            }
            case 34: {
                //HCSN + SX
                //(% HCSN) x G_HCSN + (% SX) x G_SX
                if((tt_hcsn+tt_sx)!=tieuthu){
                    tt_sx = tieuthu - tt_hcsn;
                }
                tiennuoc =  tt_hcsn * g_hcsn + tt_sx * g_sx;
                break;
            }
            case 35: {
                //HCSN + KD
                //(% HCSN) x G_HCSN + (% KD) x G_KD_DV
                if((tt_hcsn+tt_kd) !=tieuthu){
                    tt_kd =tieuthu -tt_hcsn;
                }
                tiennuoc = tt_hcsn * g_hcsn + tt_kd * g_kd;
                break;
            }
            case 36: {
                //HCSN + SX + KD
                //(% HCSN) x G_HCSN + (% SX) x G_SX + (% KD) x G_KD_DV
                if((tt_hcsn+tt_sx+tt_kd) !=tieuthu){
                    tt_kd =tieuthu -tt_hcsn-tt_sx;
                }
                tiennuoc = tt_hcsn* g_hcsn + tt_sx * g_sx + tt_kd * g_kd;
                break;
            }
            case 38: {
                //SH + HCSN
                //(SHN x G_SHN) + (SH x G_SH) + (Tiêu thụ VM x G_HCSN)
                int tieuthu_sh =tieuthu;
                int dinhmuc_sh = dinhmuc;
                int tieuthu_shn =0;
                if(dinhmucngheo !=0){
                    if(dinhmuc < dinhmucngheo){
                        dinhmucngheo= dinhmuc;
                        dinhmuc_sh = 0;
                    }else {
                        dinhmuc_sh = dinhmuc - dinhmucngheo;
                    }
                    if(tieuthu < dinhmuc) {
                        tieuthu_shn = Math.round((dinhmucngheo * tieuthu) / dinhmuc);
                        tieuthu_sh = tieuthu - tieuthu_shn;
                    }else {
                        tieuthu_shn = dinhmucngheo;
                        tieuthu_sh = tieuthu - dinhmucngheo;
                    }
                }

                if (tieuthu_sh <= dinhmuc_sh) {
                    tiennuoc = tieuthu_shn * g_shn + tieuthu_sh*g_sh;
                } else {
                    tiennuoc = tieuthu_shn * g_shn + (dinhmuc_sh)*g_sh+ (tieuthu_sh- dinhmuc_sh)*g_hcsn ;
                    //tiennuoc = g_shn * dinhmucngheo + dinhmucngheo*0.5 * g_shvm1 + (tieuthu - dinhmucngheo * 1.5) * g_shvm2;
                }
                break;
            }
            case 39: {
                //SH + HCSN + SX + KD
                //Tỉ lệ không có Định mức: (% SX) x G_SX + (% KD) x G_KD_DV + (%HCSN) x G_HCSN +(%SH)xG_SHVM2
                //Tỉ lệ có Định mức: (%SH) = [(SHN)x G_SHN + (SH x G_SH) + (SHVM1 x G_SHVM1) + (SHVM2 x G_SHVM2)] + (%SX) x G_SX + (%KD) x G_KD_DV + (%HCSN) x G_HCSN

                if (dinhmuc == 0) {
                    if((tt_hcsn+tt_sx+tt_kd + tt_sh) !=tieuthu){
                        tt_kd =tieuthu -tt_hcsn-tt_sx - tt_sh;
                    }
                    tiennuoc = tt_sx * g_sx + tt_kd * g_kd + tt_hcsn * g_hcsn + tt_sh*g_shvm2;
                } else {
                    if((tt_sx+tt_kd+tt_hcsn+tt_sh) !=tieuthu){
                        tt_kd = tieuthu - tt_sx -tt_hcsn- tt_sh;
                    }

                    int tieuthu_sh =tt_sh;
                    int dinhmuc_sh = dinhmuc;
                    int tieuthu_shn =0;
                    if(dinhmucngheo !=0){
                        if(dinhmuc < dinhmucngheo){
                            dinhmucngheo= dinhmuc;
                            dinhmuc_sh = 0;
                        }else {
                            dinhmuc_sh = dinhmuc - dinhmucngheo;
                        }
                        if(tt_sh < dinhmuc) {
                            tieuthu_shn = Math.round((dinhmucngheo * tt_sh) / dinhmuc);
                            tieuthu_sh = tt_sh - tieuthu_shn;
                        }else {
                            tieuthu_shn = dinhmucngheo;
                            tieuthu_sh = tt_sh - dinhmucngheo;
                        }
                    }
                    int tt_vuotdm1 =  (int)Math.round(dinhmuc_sh*1.5);
                    if (tieuthu_sh <= dinhmuc_sh) {
                        tiennuoc = tieuthu_shn * g_shn + tieuthu_sh*g_sh +tt_sx * g_sx + tt_kd * g_kd + tt_hcsn * g_hcsn;
                    } else if ( tieuthu_sh <= tt_vuotdm1) {
                        tiennuoc = tieuthu_shn * g_shn + (dinhmuc_sh)*g_sh+ (tieuthu_sh - dinhmuc_sh)*g_shvm1+ tt_sx * g_sx + tt_kd * g_kd + tt_hcsn * g_hcsn;
                        //tiennuoc = g_shn * dinhmucngheo + (tieuthu - dinhmucngheo) * g_shvm1;
                    } else {
                        int tt_vm1 = (int)Math.round(dinhmuc_sh*0.5);
                        int tt_vm2 = tieuthu_sh - dinhmuc_sh - tt_vm1;
                        tiennuoc = tieuthu_shn * g_shn + (dinhmuc_sh)*g_sh+ tt_vm1*g_shvm1 +tt_vm2*g_shvm2+ tt_sx * g_sx + tt_kd * g_kd + tt_hcsn * g_hcsn;
                        //tiennuoc = g_shn * dinhmucngheo + dinhmucngheo*0.5 * g_shvm1 + (tieuthu - dinhmucngheo * 1.5) * g_shvm2;
                    }
                }
                break;
            }
            case 51: {
                //CHUNG CƯ - KHU CÔNG NGHIỆP
                //Giá sỉ khu dân cư
                //(SHN x G_SHN_TC_KCN) + (SH x G_SH_TC_KCN) + (SHVM1 x G_SHVM1_TC_KCN) + (SHVM2 x G_SHVM2_TC_KCN)

                int tieuthu_sh =tieuthu;
                int dinhmuc_sh = dinhmuc;
                int tieuthu_shn =0;
                if(dinhmucngheo !=0){
                    if(dinhmuc < dinhmucngheo){
                        dinhmucngheo= dinhmuc;
                        dinhmuc_sh = 0;
                    }else {
                        dinhmuc_sh = dinhmuc - dinhmucngheo;
                    }
                    if(tieuthu < dinhmuc) {
                        tieuthu_shn = Math.round((dinhmucngheo * tieuthu) / dinhmuc);
                        tieuthu_sh = tieuthu - tieuthu_shn;
                    }else {
                        tieuthu_shn = dinhmucngheo;
                        tieuthu_sh = tieuthu - dinhmucngheo;
                    }
                }
                int tt_vuotdm1 =  (int)Math.round(dinhmuc_sh*1.5);
                if (tieuthu_sh <= dinhmuc_sh) {
                    tiennuoc = tieuthu_shn * g_shn_tckcn + tieuthu_sh*g_sh_tckcn;
                } else if ( tieuthu_sh <= tt_vuotdm1) {
                    tiennuoc = tieuthu_shn * g_shn_tckcn + (dinhmuc_sh)*g_sh_tckcn+ (tieuthu_sh - dinhmuc_sh)*g_shvm1_tckcn;
                    //tiennuoc = g_shn * dinhmucngheo + (tieuthu - dinhmucngheo) * g_shvm1;
                } else {
                    int tt_vm1 = (int)Math.round(dinhmuc_sh*0.5);
                    int tt_vm2 = tieuthu_sh - dinhmuc_sh - tt_vm1;
                    tiennuoc = tieuthu_shn * g_shn_tckcn + (dinhmuc_sh)*g_sh_tckcn+ tt_vm1*g_shvm1_tckcn +tt_vm2*g_shvm2_tckcn;
                    //tiennuoc = g_shn * dinhmucngheo + dinhmucngheo*0.5 * g_shvm1 + (tieuthu - dinhmucngheo * 1.5) * g_shvm2;
                }
                break;
            }
            case 52: {
                //Giá sỉ Khu công nghiệp
                //(Toàn bộ tiêu thụ) x G_SX_TC_KCN
                tiennuoc = tieuthu * g_sx_tckcn;
                break;
            }
            case 53: {
                //Giá sỉ TT thương mại
                //(Toàn bộ tiêu thụ) x G_KDDV_TC_KCN
                tiennuoc = tieuthu * g_kd_tckcn;
                break;
            }
            case 54: {
                //Giá sỉ HCSN
                //(Toàn bộ tiêu thụ) x G_HCSN_TC_KCN
                tiennuoc = tieuthu * g_hcsn_tckcn;
                break;
            }
            case 55: {
                //Giá sỉ đặc biệt
                //(Toàn bộ tiêu thụ) x giá thỏa thuận
                int g_tt = 0;
                tiennuoc = tieuthu * g_tt;
                break;
            }
            case 56: {
                //Giá sỉ CG
                //(Toàn bộ tiêu thụ) x giá CG
                int g_cg = 0;
                tiennuoc = tieuthu * g_cg;
                break;
            }
            case 57: {
                //Giá sỉ SH thuần túy
                //(Toàn bộ tiêu thụ) x G_SH_TC_KCN
                tiennuoc = tieuthu * g_sh_tckcn;
                break;
            }
            case 58: {
                //Giá sỉ phức hợp
                //(%SX) x G_SX_TC_KCN + (%KD) x G_KDDV_TC_KCN + (%HCSN) x G_HCSN_TC_KCN
                // (%SX) x 10,260 + (%KD) x 18,090 + (%HCSN) x 11,070
                if((tt_hcsn+tt_sx+tt_kd) !=tieuthu){
                    tt_kd =tieuthu -tt_hcsn-tt_sx;
                }
                tiennuoc = tt_sx * g_sx_tckcn + tt_kd * g_kd_tckcn + tt_hcsn * g_hcsn_tckcn;
                break;
            }
            case 59: {
                //Giá sỉ phức hợp
                // %SH = [(SHN) x G_SHN_TC_KCN + (SH) x G_SH_TC_KCN + (SHVM1 x G_SHVM1_TC_KCN) + (SHVM2 x G_SHVM2_TC_KCN)] + (%SX) x G_SX_TC_KCN + (%KD) x G_KDDV_TC_KCN
                if((tt_sx+tt_kd+tt_hcsn+tt_sh) !=tieuthu){
                    tt_kd = tieuthu - tt_sx -tt_hcsn- tt_sh;
                }


                int tieuthu_sh =tt_sh;
                int dinhmuc_sh = dinhmuc;
                int tieuthu_shn =0;
                if(dinhmucngheo !=0){
                    if(dinhmuc < dinhmucngheo){
                        dinhmucngheo= dinhmuc;
                        dinhmuc_sh = 0;
                    }else {
                        dinhmuc_sh = dinhmuc - dinhmucngheo;
                    }
                    if(tt_sh < dinhmuc) {
                        tieuthu_shn = Math.round((dinhmucngheo * tt_sh) / dinhmuc);
                        tieuthu_sh = tt_sh - tieuthu_shn;
                    }else {
                        tieuthu_shn = dinhmucngheo;
                        tieuthu_sh = tt_sh - dinhmucngheo;
                    }
                }
                int tt_vuotdm1 =  (int)Math.round(dinhmuc_sh*1.5);
                if (tieuthu_sh <= dinhmuc_sh) {
                    tiennuoc = tieuthu_shn * g_shn_tckcn + tieuthu_sh*g_sh_tckcn + tt_sx * g_sx_tckcn + tt_kd * g_kd_tckcn;;
                } else if ( tieuthu_sh <= tt_vuotdm1) {
                    tiennuoc = tieuthu_shn * g_shn_tckcn + (dinhmuc_sh)*g_sh_tckcn+ (tieuthu_sh - dinhmuc_sh)*g_shvm1_tckcn+ tt_sx * g_sx_tckcn + tt_kd * g_kd_tckcn;;
                    //tiennuoc = g_shn * dinhmucngheo + (tieuthu - dinhmucngheo) * g_shvm1;
                } else {
                    int tt_vm1 = (int)Math.round(dinhmuc_sh*0.5);
                    int tt_vm2 = tieuthu_sh - dinhmuc_sh - tt_vm1;
                    tiennuoc = tieuthu_shn * g_shn_tckcn + (dinhmuc_sh)*g_sh_tckcn+ tt_vm1*g_shvm1_tckcn +tt_vm2*g_shvm2_tckcn+ tt_sx * g_sx_tckcn + tt_kd * g_kd_tckcn;;
                    //tiennuoc = g_shn * dinhmucngheo + dinhmucngheo*0.5 * g_shvm1 + (tieuthu - dinhmucngheo * 1.5) * g_shvm2;
                }

                break;
            }
            case 68: {
                //Giá sỉ SH + Lẻ Dịch vụ
                //%SH = [(SHN) x G_SHN_TC_KCN + (SH) x G_SH_TC_KCN + (SHVM1 x G_SHVM1_TC_KCN) + (SHVM2 x G_SHVM2_TC_KCN)] + (%KD) x G_KD_DV
                // %SH = [(SHN) x 5,400 + (SH) x 5,670 + (SHVM1 x 10.890) + (SHVM2 x 12,240)] + (%KD) x 20,100
                int tieuthu_sh =tt_sh;
                int dinhmuc_sh = dinhmuc;
                int tieuthu_shn =0;
                if(dinhmucngheo !=0){
                    if(dinhmuc < dinhmucngheo){
                        dinhmucngheo= dinhmuc;
                        dinhmuc_sh = 0;
                    }else {
                        dinhmuc_sh = dinhmuc - dinhmucngheo;
                    }
                    if(tieuthu < dinhmuc) {
                        tieuthu_shn = Math.round((dinhmucngheo * tt_sh) / dinhmuc);
                        tieuthu_sh = tt_sh - tieuthu_shn;
                    }else {
                        tieuthu_shn = dinhmucngheo;
                        tieuthu_sh = tt_sh - dinhmucngheo;
                    }
                }
                int tt_vuotdm1 =  (int)Math.round(dinhmuc_sh*1.5);
                if (tieuthu_sh <= dinhmuc_sh) {
                    tiennuoc = tieuthu_shn * g_shn_tckcn + tieuthu_sh*g_sh_tckcn + tt_kd * g_kd_tckcn;
                } else if ( tieuthu_sh <= tt_vuotdm1) {
                    tiennuoc = tieuthu_shn * g_shn_tckcn + (dinhmuc_sh)*g_sh_tckcn+ (tieuthu_sh - dinhmuc_sh)*g_shvm1_tckcn+ tt_kd * g_kd_tckcn;
                    //tiennuoc = g_shn * dinhmucngheo + (tieuthu - dinhmucngheo) * g_shvm1;
                } else {
                    int tt_vm1 = (int)Math.round(dinhmuc_sh*0.5);
                    int tt_vm2 = tieuthu_sh - dinhmuc_sh - tt_vm1;
                    tiennuoc = tieuthu_shn * g_shn_tckcn + (dinhmuc_sh)*g_sh_tckcn+ tt_vm1*g_shvm1_tckcn +tt_vm2*g_shvm2_tckcn+ tt_kd * g_kd_tckcn;;
                    //tiennuoc = g_shn * dinhmucngheo + dinhmucngheo*0.5 * g_shvm1 + (tieuthu - dinhmucngheo * 1.5) * g_shvm2;
                }

                break;
            }

        }
        return  (tiennuoc);

    }
    private long GiaNuoc_2021(int tieuthu,int giabieu,int dinhmuc,int dinhmucngheo,int tl_kinhdoanh,int tl_hcsn,int tl_sh,int tl_sx, double tl_tiennuoc) {
        long  tiennuoc = 0;
        int tt_sh = (int) Math.round((tieuthu* tl_sh*0.01));
        int tt_hcsn = (int) Math.round((tieuthu*tl_hcsn*0.01));
        int tt_sx = (int) Math.round((tieuthu * tl_sx*0.01));
        int tt_kd = (int) Math.round((tieuthu* tl_kinhdoanh*0.01));
        // gia nuoc

        int g_shn = (int)Math.round(6000*tl_tiennuoc);
        int g_sh =(int)Math.round(6300*tl_tiennuoc);
        int g_shvm1 =(int)Math.round(12100*tl_tiennuoc);
        int g_shvm2 =(int)Math.round(13600*tl_tiennuoc);
        int g_hcsn =(int)Math.round(12300*tl_tiennuoc);
        int g_sx =(int)Math.round(11400*tl_tiennuoc);
        int g_kd =(int)Math.round(20100*tl_tiennuoc);
        int g_dv =(int)Math.round(20100*tl_tiennuoc);

        int g_shn_tckcn = (int)Math.round(5400*tl_tiennuoc);
        int g_sh_tckcn =(int)Math.round(5670*tl_tiennuoc );
        int g_shvm1_tckcn =(int)Math.round(10890*tl_tiennuoc);
        int g_shvm2_tckcn =(int)Math.round(12240*tl_tiennuoc);
        int g_hcsn_tckcn =(int)Math.round(11070*tl_tiennuoc);
        int g_sx_tckcn =(int)Math.round(10260*tl_tiennuoc);
        int g_kd_tckcn =(int)Math.round(18090*tl_tiennuoc);
        int g_dv_tckcn =(int)Math.round(18090*tl_tiennuoc);


        switch (giabieu) {

            // TU GIA
            case 10: {
                //SH Nghèo thuần túy
                // (SHN x G_SHN) + (SH x G_SH) + (SHVM1 x G_SHVM1) + (SHVM2 x G_SHVM2)
                int tieuthu_sh =tieuthu;
                int dinhmuc_sh = dinhmuc;
                int tieuthu_shn =0;
                if(dinhmucngheo !=0){
                    if(dinhmuc < dinhmucngheo){
                        dinhmuc= dinhmucngheo;
                        dinhmuc_sh = dinhmucngheo;
                    }else {
                        dinhmuc_sh = dinhmuc - dinhmucngheo;
                    }
                    if(tieuthu < dinhmuc) {
                        tieuthu_shn = Math.round((dinhmucngheo * tieuthu) / dinhmuc);
                        tieuthu_sh = tieuthu - tieuthu_shn;
                    }else {
                        tieuthu_shn = dinhmucngheo;
                        tieuthu_sh = tieuthu - dinhmucngheo;
                    }
                }
                int tt_vuotdm1 =  (int)Math.round(dinhmuc_sh*1.5);
                if (tieuthu_sh <= dinhmuc_sh) {
                    tiennuoc = tieuthu_shn * g_shn + tieuthu_sh*g_sh;
                } else if ( tieuthu_sh <= tt_vuotdm1) {
                    tiennuoc = tieuthu_shn * g_shn + (dinhmuc_sh)*g_sh+ (tieuthu_sh - dinhmuc_sh)*g_shvm1;
                    //tiennuoc = g_shn * dinhmucngheo + (tieuthu - dinhmucngheo) * g_shvm1;
                } else {
                    int tt_vm1 = (int)Math.round(dinhmuc_sh*0.5);
                    int tt_vm2 = tieuthu_sh - dinhmuc_sh - tt_vm1;
                    tiennuoc = tieuthu_shn * g_shn + (dinhmuc_sh)*g_sh+ tt_vm1*g_shvm1 +tt_vm2*g_shvm2;
                    //tiennuoc = g_shn * dinhmucngheo + dinhmucngheo*0.5 * g_shvm1 + (tieuthu - dinhmucngheo * 1.5) * g_shvm2;
                }
                break;
            }
            case 11: {
                //SH thuần túy
                // (SHN x G_SHN) + (SH x G_SH) + (SHVM1 x G_SHVM1) + (SHVM2 x G_SHVM2)
                int tieuthu_sh =tieuthu;
                int dinhmuc_sh = dinhmuc;
                int tieuthu_shn =0;
                if(dinhmucngheo !=0) {
                    if (dinhmuc < dinhmucngheo) {
                        dinhmuc = dinhmucngheo;
                        dinhmuc_sh = dinhmucngheo;
                    } else {
                        dinhmuc_sh = dinhmuc - dinhmucngheo;
                    }
                    if (tieuthu < dinhmuc) {
                        tieuthu_shn = Math.round((dinhmucngheo * tieuthu) / dinhmuc);
                        tieuthu_sh = tieuthu - tieuthu_shn;
                    } else {
                        tieuthu_shn = dinhmucngheo;
                        tieuthu_sh = tieuthu - dinhmucngheo;
                    }

                }
                int tt_vuotdm1 =  (int)Math.round(dinhmuc_sh*1.5);
                if (tieuthu_sh <= dinhmuc_sh) {
                    tiennuoc = tieuthu_shn * g_shn + tieuthu_sh*g_sh;
                } else if ( tieuthu_sh <= tt_vuotdm1) {
                    tiennuoc = tieuthu_shn * g_shn + (dinhmuc_sh)*g_sh+ (tieuthu_sh - dinhmuc_sh)*g_shvm1;
                    //tiennuoc = g_shn * dinhmucngheo + (tieuthu - dinhmucngheo) * g_shvm1;
                } else {
                    int tt_vm1 = (int)Math.round(dinhmuc_sh*0.5);
                    int tt_vm2 = tieuthu_sh - dinhmuc_sh - tt_vm1;
                    tiennuoc = tieuthu_shn * g_shn + (dinhmuc_sh)*g_sh+ tt_vm1*g_shvm1 +tt_vm2*g_shvm2;
                    //tiennuoc = g_shn * dinhmucngheo + dinhmucngheo*0.5 * g_shvm1 + (tieuthu - dinhmucngheo * 1.5) * g_shvm2;
                }
                break;
            }
            case 12: {
                //SX thuần túy
                // (Toàn bộ tiêu thụ) x G_SX
                tiennuoc = tieuthu * g_sx;
                break;
            }
            case 13: {
                //KD thuần túy
                // (Toàn bộ tiêu thụ) x G_KD_DC
                tiennuoc = tieuthu * g_kd;
                break;
            }
            case 14: {
                //SH + SX
                // (SHN x G_SHN) + (SH x G_SH) + (Tiêu thụ VM x G_SX)

                int tieuthu_sh =tieuthu;
                int dinhmuc_sh = dinhmuc;
                int tieuthu_shn =0;
                if(dinhmucngheo !=0){
                    if(dinhmuc < dinhmucngheo){
                        dinhmuc= dinhmucngheo;
                        dinhmuc_sh = dinhmucngheo;
                    }else {
                        dinhmuc_sh = dinhmuc - dinhmucngheo;
                    }
                    if(tieuthu < dinhmuc) {
                        tieuthu_shn = Math.round((dinhmucngheo * tieuthu) / dinhmuc);
                        tieuthu_sh = tieuthu - tieuthu_shn;
                    }else {
                        tieuthu_shn = dinhmucngheo;
                        tieuthu_sh = tieuthu - dinhmucngheo;
                    }
                }

                if (tieuthu_sh <= dinhmuc_sh) {
                    tiennuoc = tieuthu_shn * g_shn + tieuthu_sh*g_sh;
                } else {
                    tiennuoc = tieuthu_shn * g_shn + (dinhmuc_sh)*g_sh+ (tieuthu_sh- dinhmuc_sh)*g_sx ;
                    //tiennuoc = g_shn * dinhmucngheo + dinhmucngheo*0.5 * g_shvm1 + (tieuthu - dinhmucngheo * 1.5) * g_shvm2;
                }
                break;


            }
            case 15: {
                //SH + KD
                // (SHN x G_SHN) + (SH x G_SH) + (Tiêu thụ VM x G_KD_DV)
                int tieuthu_sh =tieuthu;
                int dinhmuc_sh = dinhmuc;
                int tieuthu_shn =0;
                if(dinhmucngheo !=0){
                    if(dinhmuc < dinhmucngheo){
                        dinhmuc= dinhmucngheo;
                        dinhmuc_sh = dinhmucngheo;
                    }else {
                        dinhmuc_sh = dinhmuc - dinhmucngheo;
                    }
                    if(tieuthu < dinhmuc) {
                        tieuthu_shn = Math.round((dinhmucngheo * tieuthu) / dinhmuc);
                        tieuthu_sh = tieuthu - tieuthu_shn;
                    }else {
                        tieuthu_shn = dinhmucngheo;
                        tieuthu_sh = tieuthu - dinhmucngheo;
                    }
                }

                if (tieuthu_sh <= dinhmuc_sh) {
                    tiennuoc = tieuthu_shn * g_shn + tieuthu_sh*g_sh;
                } else {
                    tiennuoc = tieuthu_shn * g_shn + (dinhmuc_sh)*g_sh+ (tieuthu_sh- dinhmuc_sh)*g_kd ;
                    //tiennuoc = g_shn * dinhmucngheo + dinhmucngheo*0.5 * g_shvm1 + (tieuthu - dinhmucngheo * 1.5) * g_shvm2;
                }
                break;
            }
            case 16: {
                //SH + SX + KD
                //Tỉ lệ không có Định mức: (% SX) x G_SX + (% KD) x G_KD_DV + (%SH)x G_SHVM2
                //Tỉ lệ có Định mức: (%SH) = [(SH x G_SH) + (SHVM1 x G_SHVM1) + (SHVM2 x G_SHVM2)] + (%SX) x G_SX + (%KD) x G_KD_DV

                if (dinhmuc == 0) {
                    if((tt_sx+tt_kd + tt_sh)!=tieuthu){
                        tt_kd =tieuthu -tt_sx - tt_sh;
                    }
                    tiennuoc = tt_sx * g_sx + tt_kd * g_kd + tt_sh*g_shvm2;
                } else {
                    if((tt_sx+tt_kd+tt_sh)!=tieuthu){
                        tt_kd =tieuthu -tt_sx- tt_sh;
                    }
                    int tt_vuotdm1 =  (int)Math.round(dinhmuc*1.5);
                    if (tt_sh <= dinhmuc) {
                        tiennuoc = tt_sh * g_sh  + tt_sx* g_sx + tt_kd * g_kd;
                    } else if (tt_sh <= tt_vuotdm1) {
                        tiennuoc = dinhmuc * g_sh + (tt_sh - dinhmuc) * g_shvm1 + tt_sx * g_sx + tt_kd * g_kd;
                    } else {
                        int tt_vm1 = (int)Math.round(dinhmuc*0.5);
                        int tt_vm2 = tt_sh - dinhmuc - tt_vm1;
                        tiennuoc = dinhmuc * g_sh + tt_vm1 * g_shvm1  + tt_vm2 * g_shvm2 + tt_sx * g_sx + tt_kd * g_kd;
                    }
                }
                break;
            }
            case 17: {
                //SH đặc biệt
                //(Toàn bộ tiêu thụ) x G_SH
                tiennuoc = tieuthu * g_shn;
                break;
            }
            case 18: {
                //SH + HCSN
                //(SHN x G_SHN) + (SH x G_SH) + (Tiêu thụ VM x G_HCSN)
                int tieuthu_sh =tieuthu;
                int dinhmuc_sh = dinhmuc;
                int tieuthu_shn =0;
                if(dinhmucngheo !=0){
                    if(dinhmuc < dinhmucngheo){
                        dinhmuc= dinhmucngheo;
                        dinhmuc_sh = dinhmucngheo;
                    }else {
                        dinhmuc_sh = dinhmuc - dinhmucngheo;
                    }
                    if(tieuthu < dinhmuc) {
                        tieuthu_shn = Math.round((dinhmucngheo * tieuthu) / dinhmuc);
                        tieuthu_sh = tieuthu - tieuthu_shn;
                    }else {
                        tieuthu_shn = dinhmucngheo;
                        tieuthu_sh = tieuthu - dinhmucngheo;
                    }
                }

                if (tieuthu_sh <= dinhmuc_sh) {
                    tiennuoc = tieuthu_shn * g_shn + tieuthu_sh*g_sh;
                } else {
                    tiennuoc = tieuthu_shn * g_shn + (dinhmuc_sh)*g_sh+ (tieuthu_sh- dinhmuc_sh)*g_hcsn ;
                    //tiennuoc = g_shn * dinhmucngheo + dinhmucngheo*0.5 * g_shvm1 + (tieuthu - dinhmucngheo * 1.5) * g_shvm2;
                }
                break;
            }
            case 19: {
                //SH + SX + KD + HCSN
                //Tỉ lệ không có Định mức: (% SX) x G_SX + (% KD) x G_KD_DV + (%HCSN) x G_HCSN + (% SH) x G_SHVM2
                //Tỉ lệ có Định mức: (%SH) = [(SHN)x G_SHN + (SH x G_SH) + (SHVM1 x G_SHVM1) + (SHVM2 x G_SHVM2)] + (%SX) x G_SX + (%KD) x G_KD_DV + (%HCSN) x G_HCSN

                if (dinhmuc == 0) {
                    if((tt_hcsn+tt_sx+tt_kd + tt_sh) !=tieuthu){
                        tt_kd =tieuthu -tt_hcsn-tt_sx - tt_sh;
                    }
                    tiennuoc = tt_sx * g_sx + tt_kd * g_kd + tt_hcsn * g_hcsn + tt_sh*g_shvm2;
                } else {
                    if((tt_sx+tt_kd+tt_hcsn+tt_sh) !=tieuthu){
                        tt_kd = tieuthu - tt_sx -tt_hcsn- tt_sh;
                    }

                    //////
                    int tieuthu_sh =tt_sh;
                    int dinhmuc_sh = dinhmuc;
                    int tieuthu_shn =0;
                    if(dinhmucngheo !=0){
                        if(dinhmuc < dinhmucngheo){
                            dinhmuc= dinhmucngheo;
                            dinhmuc_sh = dinhmucngheo;
                        }else {
                            dinhmuc_sh = dinhmuc - dinhmucngheo;
                        }
                        if(tt_sh < dinhmuc) {
                            tieuthu_shn = Math.round((dinhmucngheo * tt_sh) / dinhmuc);
                            tieuthu_sh = tt_sh - tieuthu_shn;
                        }else {
                            tieuthu_shn = dinhmucngheo;
                            tieuthu_sh = tt_sh - dinhmucngheo;
                        }
                    }
                    int tt_vuotdm1 =  (int)Math.round(dinhmuc_sh*1.5);
                    if (tieuthu_sh <= dinhmuc_sh) {
                        tiennuoc = tieuthu_shn * g_shn + tieuthu_sh*g_sh +tt_sx * g_sx + tt_kd * g_kd + tt_hcsn * g_hcsn;;
                    } else if ( tieuthu_sh <= tt_vuotdm1) {
                        tiennuoc = tieuthu_shn * g_shn + (dinhmuc_sh)*g_sh+ (tieuthu_sh - dinhmuc_sh)*g_shvm1+ tt_sx * g_sx + tt_kd * g_kd + tt_hcsn * g_hcsn;;
                        //tiennuoc = g_shn * dinhmucngheo + (tieuthu - dinhmucngheo) * g_shvm1;
                    } else {
                        int tt_vm1 = (int)Math.round(dinhmuc_sh*0.5);
                        int tt_vm2 = tieuthu_sh - dinhmuc_sh - tt_vm1;
                        tiennuoc = tieuthu_shn * g_shn + (dinhmuc_sh)*g_sh+ tt_vm1*g_shvm1 +tt_vm2*g_shvm2+ tt_sx * g_sx + tt_kd * g_kd + tt_hcsn * g_hcsn;;
                        //tiennuoc = g_shn * dinhmucngheo + dinhmucngheo*0.5 * g_shvm1 + (tieuthu - dinhmucngheo * 1.5) * g_shvm2;
                    }


                }
                break;
            }

            // HO TAP THE
            case 21: {
                //SH thuần túy
                //(SHN x G_SHN) + (SH x G_SH) + (SHVM1 x G_SHVM1) + (SHVM2 x G_SHVM2)

                int tieuthu_sh =tieuthu;
                int dinhmuc_sh = dinhmuc;
                int tieuthu_shn =0;
                if(dinhmucngheo !=0){
                    if(dinhmuc < dinhmucngheo){
                        dinhmuc= dinhmucngheo;
                        dinhmuc_sh = dinhmucngheo;
                    }else {
                        dinhmuc_sh = dinhmuc - dinhmucngheo;
                    }
                    if(tieuthu < dinhmuc) {
                        tieuthu_shn = Math.round((dinhmucngheo * tieuthu) / dinhmuc);
                        tieuthu_sh = tieuthu - tieuthu_shn;
                    }else {
                        tieuthu_shn = dinhmucngheo;
                        tieuthu_sh = tieuthu - dinhmucngheo;
                    }
                }
                int tt_vuotdm1 =  (int)Math.round(dinhmuc_sh*1.5);
                if (tieuthu_sh <= dinhmuc_sh) {
                    tiennuoc = tieuthu_shn * g_shn + tieuthu_sh*g_sh;
                } else if ( tieuthu_sh <= tt_vuotdm1) {
                    tiennuoc = tieuthu_shn * g_shn + (dinhmuc_sh)*g_sh+ (tieuthu_sh - dinhmuc_sh)*g_shvm1;
                    //tiennuoc = g_shn * dinhmucngheo + (tieuthu - dinhmucngheo) * g_shvm1;
                } else {
                    int tt_vm1 = (int)Math.round(dinhmuc_sh*0.5);
                    int tt_vm2 = tieuthu_sh - dinhmuc_sh - tt_vm1;
                    tiennuoc = tieuthu_shn * g_shn + (dinhmuc_sh)*g_sh+ tt_vm1*g_shvm1 +tt_vm2*g_shvm2;
                    //tiennuoc = g_shn * dinhmucngheo + dinhmucngheo*0.5 * g_shvm1 + (tieuthu - dinhmucngheo * 1.5) * g_shvm2;
                }
                break;
            }
            case 22: {
                //SX thuần túy
                // (Toàn bộ tiêu thụ) x G_SX
                tiennuoc = tieuthu * g_sx;
                break;
            }
            case 23: {
                //KD thuần túy
                // (Toàn bộ tiêu thụ) x G_KD_DV
                tiennuoc = tieuthu * g_kd;
                break;
            }
            case 24: {
                //SH + SX
                // (SHN x G_SHN) + (SH x G_SH) + (Tiêu thụ VM x G_SX)
                int tieuthu_sh =tieuthu;
                int dinhmuc_sh = dinhmuc;
                int tieuthu_shn =0;
                if(dinhmucngheo !=0){
                    if(dinhmuc < dinhmucngheo){
                        dinhmuc= dinhmucngheo;
                        dinhmuc_sh = dinhmucngheo;
                    }else {
                        dinhmuc_sh = dinhmuc - dinhmucngheo;
                    }
                    if(tieuthu < dinhmuc) {
                        tieuthu_shn = Math.round((dinhmucngheo * tieuthu) / dinhmuc);
                        tieuthu_sh = tieuthu - tieuthu_shn;
                    }else {
                        tieuthu_shn = dinhmucngheo;
                        tieuthu_sh = tieuthu - dinhmucngheo;
                    }
                }

                if (tieuthu_sh <= dinhmuc_sh) {
                    tiennuoc = tieuthu_shn * g_shn + tieuthu_sh*g_sh;
                } else {
                    tiennuoc = tieuthu_shn * g_shn + (dinhmuc_sh)*g_sh+ (tieuthu_sh- dinhmuc_sh)*g_sx ;
                    //tiennuoc = g_shn * dinhmucngheo + dinhmucngheo*0.5 * g_shvm1 + (tieuthu - dinhmucngheo * 1.5) * g_shvm2;
                }
                break;
            }
            case 25: {
                // SH + KD
                // (SHN x G_SHN) + (SH x G_SH) + (Tiêu thụ VM x G_KD_DV)
                int tieuthu_sh =tieuthu;
                int dinhmuc_sh = dinhmuc;
                int tieuthu_shn =0;
                if(dinhmucngheo !=0){
                    if(dinhmuc < dinhmucngheo){
                        dinhmuc= dinhmucngheo;
                        dinhmuc_sh = dinhmucngheo;
                    }else {
                        dinhmuc_sh = dinhmuc - dinhmucngheo;
                    }
                    if(tieuthu < dinhmuc) {
                        tieuthu_shn = Math.round((dinhmucngheo * tieuthu) / dinhmuc);
                        tieuthu_sh = tieuthu - tieuthu_shn;
                    }else {
                        tieuthu_shn = dinhmucngheo;
                        tieuthu_sh = tieuthu - dinhmucngheo;
                    }
                }

                if (tieuthu_sh <= dinhmuc_sh) {
                    tiennuoc = tieuthu_shn * g_shn + tieuthu_sh*g_sh;
                } else {
                    tiennuoc = tieuthu_shn * g_shn + (dinhmuc_sh)*g_sh+ (tieuthu_sh- dinhmuc_sh)*g_kd ;
                    //tiennuoc = g_shn * dinhmucngheo + dinhmucngheo*0.5 * g_shvm1 + (tieuthu - dinhmucngheo * 1.5) * g_shvm2;
                }
                break;
            }
            case 26: {
                //SH + SX + KD
                //Tỉ lệ không có Định mức: (% SX) x G_SX + (% KD) x G_KD_DV + (% SH) x G_SHVM2
                //Tỉ lệ có Định mức: (%SH) = [(SH x G_SH) + (SHVM1 x G_SHVM1) + (SHVM2 x G_SHVM2)] + (%SX) x G_SX + (%KD) x G_KD_DV

                if (dinhmuc == 0) {
                    if((tt_sx+tt_kd + tt_sh)!=tieuthu){
                        tt_kd =tieuthu -tt_sx - tt_sh;
                    }
                    tiennuoc = tt_sx * g_sx + tt_kd * g_kd + tt_sh* g_shvm2;
                } else {
                    if((tt_sx+tt_kd+tt_hcsn+tt_sh) !=tieuthu){
                        tt_kd = tieuthu - tt_sx -tt_hcsn- tt_sh;
                    }
                    int tt_vuotdm1 =  (int)Math.round(dinhmuc*1.5);
                    if (tt_sh <= dinhmuc) {
                        tiennuoc = tt_sh * g_sh + tt_sx * g_sx + tt_kd * g_kd;
                    } else if (tt_sh <= tt_vuotdm1) {
                        tiennuoc = dinhmuc * g_sh + (tt_sh - dinhmuc) * g_shvm1  + tt_sx * g_sx + tt_kd * g_kd;
                    } else {
                        int tt_vm1 = (int)Math.round(dinhmuc*0.5);
                        int tt_vm2 = tt_sh - dinhmuc - tt_vm1;
                        tiennuoc = dinhmuc * g_sh + tt_vm1 * g_shvm1  + tt_vm2 * g_shvm2  + tt_sx * g_sx + tt_kd * g_kd;
                    }
                }
                break;
            }
            case 27: {
                //SH đặc biệt
                //(Toàn bộ tiêu thụ) x G_SH
                tiennuoc = tieuthu * g_sh;
                break;
            }
            case 28: {
                //SH + HCSN
                //(SHN x G_SHN) + (SH x G_SH) + (Tiêu thụ VM x G_HCSN)
                int tieuthu_sh =tieuthu;
                int dinhmuc_sh = dinhmuc;
                int tieuthu_shn =0;
                if(dinhmucngheo !=0){
                    if(dinhmuc < dinhmucngheo){
                        dinhmuc= dinhmucngheo;
                        dinhmuc_sh = dinhmucngheo;
                    }else {
                        dinhmuc_sh = dinhmuc - dinhmucngheo;
                    }
                    if(tieuthu < dinhmuc) {
                        tieuthu_shn = Math.round((dinhmucngheo * tieuthu) / dinhmuc);
                        tieuthu_sh = tieuthu - tieuthu_shn;
                    }else {
                        tieuthu_shn = dinhmucngheo;
                        tieuthu_sh = tieuthu - dinhmucngheo;
                    }
                }

                if (tieuthu_sh <= dinhmuc_sh) {
                    tiennuoc = tieuthu_shn * g_shn + tieuthu_sh*g_sh;
                } else {
                    tiennuoc = tieuthu_shn * g_shn + (dinhmuc_sh)*g_sh+ (tieuthu_sh- dinhmuc_sh)*g_hcsn ;
                    //tiennuoc = g_shn * dinhmucngheo + dinhmucngheo*0.5 * g_shvm1 + (tieuthu - dinhmucngheo * 1.5) * g_shvm2;
                }
                break;
            }
            case 29: {
                //SH + SX + KD + HCSN
                //Tỉ lệ không có Định mức: (% SX) x G_SX + (% KD) x G_KD_DV + (%HCSN) x G_HCSN + (%SH)x G_SHVM2
                //Tỉ lệ có Định mức: (%SH) = [(SHN)x G_SHN + (SH x G_SH) + (SHVM1 x G_SHVM1) + (SHVM2 x G_SHVM2)] + (%SX) x G_SX + (%KD) x G_KD_DV + (%HCSN) x G_HCSN

                if (dinhmuc == 0) {
                    if((tt_hcsn+tt_sx+tt_kd + tt_sh) !=tieuthu){
                        tt_kd =tieuthu -tt_hcsn-tt_sx - tt_sh;
                    }
                    tiennuoc = tt_sx * g_sx + tt_kd * g_kd + tt_hcsn * g_hcsn + tt_sh*g_shvm2;
                } else {

                    if((tt_sx+tt_kd+tt_hcsn+tt_sh) !=tieuthu){
                        tt_kd = tieuthu - tt_sx -tt_hcsn- tt_sh;
                    }

                    int tieuthu_sh =tt_sh;
                    int dinhmuc_sh = dinhmuc;
                    int tieuthu_shn =0;
                    if(dinhmucngheo !=0){
                        if(dinhmuc < dinhmucngheo){
                            dinhmuc= dinhmucngheo;
                            dinhmuc_sh = dinhmucngheo;
                        }else {
                            dinhmuc_sh = dinhmuc - dinhmucngheo;
                        }
                        if(tt_sh < dinhmuc) {
                            tieuthu_shn = Math.round((dinhmucngheo * tt_sh) / dinhmuc);
                            tieuthu_sh = tt_sh - tieuthu_shn;
                        }else {
                            tieuthu_shn = dinhmucngheo;
                            tieuthu_sh = tt_sh - dinhmucngheo;
                        }
                    }
                    int tt_vuotdm1 =  (int)Math.round(dinhmuc_sh*1.5);
                    if (tieuthu_sh <= dinhmuc_sh) {
                        tiennuoc = tieuthu_shn * g_shn + tieuthu_sh*g_sh +tt_sx * g_sx + tt_kd * g_kd + tt_hcsn * g_hcsn;;
                    } else if ( tieuthu_sh <= tt_vuotdm1) {
                        tiennuoc = tieuthu_shn * g_shn + (dinhmuc_sh)*g_sh+ (tieuthu_sh - dinhmuc_sh)*g_shvm1+ tt_sx * g_sx + tt_kd * g_kd + tt_hcsn * g_hcsn;;
                        //tiennuoc = g_shn * dinhmucngheo + (tieuthu - dinhmucngheo) * g_shvm1;
                    } else {
                        int tt_vm1 = (int)Math.round(dinhmuc_sh*0.5);
                        int tt_vm2 = tieuthu_sh - dinhmuc_sh - tt_vm1;
                        tiennuoc = tieuthu_shn * g_shn + (dinhmuc_sh)*g_sh+ tt_vm1*g_shvm1 +tt_vm2*g_shvm2+ tt_sx * g_sx + tt_kd * g_kd + tt_hcsn * g_hcsn;;
                        //tiennuoc = g_shn * dinhmucngheo + dinhmucngheo*0.5 * g_shvm1 + (tieuthu - dinhmucngheo * 1.5) * g_shvm2;
                    }
                }
                break;
            }
            // CO QUAN
            case 31: {
                //Hành Chánh Sự Nghiệp
                //(Toàn bộ tiêu thụ) x G_HCSN
                tiennuoc = tieuthu * g_hcsn;
                break;
            }
            case 32: {
                //Sản xuất
                //(Toàn bộ tiêu thụ) x G_SX
                tiennuoc = tieuthu * g_sx;
                break;
            }
            case 33: {
                //Kinh doanh dịch vụ
                //(Toàn bộ tiêu thụ) x G_KD_DV
                tiennuoc = tieuthu * g_dv;
                break;
            }
            case 34: {
                //HCSN + SX
                //(% HCSN) x G_HCSN + (% SX) x G_SX
                if((tt_hcsn+tt_sx)!=tieuthu){
                    tt_sx = tieuthu - tt_hcsn;
                }
                tiennuoc =  tt_hcsn * g_hcsn + tt_sx * g_sx;
                break;
            }
            case 35: {
                //HCSN + KD
                //(% HCSN) x G_HCSN + (% KD) x G_KD_DV
                if((tt_hcsn+tt_kd) !=tieuthu){
                    tt_kd =tieuthu -tt_hcsn;
                }
                tiennuoc = tt_hcsn * g_hcsn + tt_kd * g_kd;
                break;
            }
            case 36: {
                //HCSN + SX + KD
                //(% HCSN) x G_HCSN + (% SX) x G_SX + (% KD) x G_KD_DV
                if((tt_hcsn+tt_sx+tt_kd) !=tieuthu){
                    tt_kd =tieuthu -tt_hcsn-tt_sx;
                }
                tiennuoc = tt_hcsn* g_hcsn + tt_sx * g_sx + tt_kd * g_kd;
                break;
            }
            case 38: {
                //SH + HCSN
                //(SHN x G_SHN) + (SH x G_SH) + (Tiêu thụ VM x G_HCSN)
                int tieuthu_sh =tieuthu;
                int dinhmuc_sh = dinhmuc;
                int tieuthu_shn =0;
                if(dinhmucngheo !=0){
                    if(dinhmuc < dinhmucngheo){
                        dinhmuc= dinhmucngheo;
                        dinhmuc_sh = dinhmucngheo;
                    }else {
                        dinhmuc_sh = dinhmuc - dinhmucngheo;
                    }
                    if(tieuthu < dinhmuc) {
                        tieuthu_shn = Math.round((dinhmucngheo * tieuthu) / dinhmuc);
                        tieuthu_sh = tieuthu - tieuthu_shn;
                    }else {
                        tieuthu_shn = dinhmucngheo;
                        tieuthu_sh = tieuthu - dinhmucngheo;
                    }
                }

                if (tieuthu_sh <= dinhmuc_sh) {
                    tiennuoc = tieuthu_shn * g_shn + tieuthu_sh*g_sh;
                } else {
                    tiennuoc = tieuthu_shn * g_shn + (dinhmuc_sh)*g_sh+ (tieuthu_sh- dinhmuc_sh)*g_hcsn ;
                    //tiennuoc = g_shn * dinhmucngheo + dinhmucngheo*0.5 * g_shvm1 + (tieuthu - dinhmucngheo * 1.5) * g_shvm2;
                }
                break;
            }
            case 39: {
                //SH + HCSN + SX + KD
                //Tỉ lệ không có Định mức: (% SX) x G_SX + (% KD) x G_KD_DV + (%HCSN) x G_HCSN +(%SH)xG_SHVM2
                //Tỉ lệ có Định mức: (%SH) = [(SHN)x G_SHN + (SH x G_SH) + (SHVM1 x G_SHVM1) + (SHVM2 x G_SHVM2)] + (%SX) x G_SX + (%KD) x G_KD_DV + (%HCSN) x G_HCSN

                if (dinhmuc == 0) {
                    if((tt_hcsn+tt_sx+tt_kd + tt_sh) !=tieuthu){
                        tt_kd =tieuthu -tt_hcsn-tt_sx - tt_sh;
                    }
                    tiennuoc = tt_sx * g_sx + tt_kd * g_kd + tt_hcsn * g_hcsn + tt_sh*g_shvm2;
                } else {
                    if((tt_sx+tt_kd+tt_hcsn+tt_sh) !=tieuthu){
                        tt_kd = tieuthu - tt_sx -tt_hcsn- tt_sh;
                    }

                    int tieuthu_sh =tt_sh;
                    int dinhmuc_sh = dinhmuc;
                    int tieuthu_shn =0;
                    if(dinhmucngheo !=0){
                        if(dinhmuc < dinhmucngheo){
                            dinhmuc= dinhmucngheo;
                            dinhmuc_sh = dinhmucngheo;
                        }else {
                            dinhmuc_sh = dinhmuc - dinhmucngheo;
                        }
                        if(tt_sh < dinhmuc) {
                            tieuthu_shn = Math.round((dinhmucngheo * tt_sh) / dinhmuc);
                            tieuthu_sh = tt_sh - tieuthu_shn;
                        }else {
                            tieuthu_shn = dinhmucngheo;
                            tieuthu_sh = tt_sh - dinhmucngheo;
                        }
                    }
                    int tt_vuotdm1 =  (int)Math.round(dinhmuc_sh*1.5);
                    if (tieuthu_sh <= dinhmuc_sh) {
                        tiennuoc = tieuthu_shn * g_shn + tieuthu_sh*g_sh +tt_sx * g_sx + tt_kd * g_kd + tt_hcsn * g_hcsn;
                    } else if ( tieuthu_sh <= tt_vuotdm1) {
                        tiennuoc = tieuthu_shn * g_shn + (dinhmuc_sh)*g_sh+ (tieuthu_sh - dinhmuc_sh)*g_shvm1+ tt_sx * g_sx + tt_kd * g_kd + tt_hcsn * g_hcsn;
                        //tiennuoc = g_shn * dinhmucngheo + (tieuthu - dinhmucngheo) * g_shvm1;
                    } else {
                        int tt_vm1 = (int)Math.round(dinhmuc_sh*0.5);
                        int tt_vm2 = tieuthu_sh - dinhmuc_sh - tt_vm1;
                        tiennuoc = tieuthu_shn * g_shn + (dinhmuc_sh)*g_sh+ tt_vm1*g_shvm1 +tt_vm2*g_shvm2+ tt_sx * g_sx + tt_kd * g_kd + tt_hcsn * g_hcsn;
                        //tiennuoc = g_shn * dinhmucngheo + dinhmucngheo*0.5 * g_shvm1 + (tieuthu - dinhmucngheo * 1.5) * g_shvm2;
                    }
                }
                break;
            }
            case 51: {
                //CHUNG CƯ - KHU CÔNG NGHIỆP
                //Giá sỉ khu dân cư
                //(SHN x G_SHN_TC_KCN) + (SH x G_SH_TC_KCN) + (SHVM1 x G_SHVM1_TC_KCN) + (SHVM2 x G_SHVM2_TC_KCN)

                int tieuthu_sh =tieuthu;
                int dinhmuc_sh = dinhmuc;
                int tieuthu_shn =0;
                if(dinhmucngheo !=0){
                    if(dinhmuc < dinhmucngheo){
                        dinhmuc= dinhmucngheo;
                        dinhmuc_sh = dinhmucngheo;
                    }else {
                        dinhmuc_sh = dinhmuc - dinhmucngheo;
                    }
                    if(tieuthu < dinhmuc) {
                        tieuthu_shn = Math.round((dinhmucngheo * tieuthu) / dinhmuc);
                        tieuthu_sh = tieuthu - tieuthu_shn;
                    }else {
                        tieuthu_shn = dinhmucngheo;
                        tieuthu_sh = tieuthu - dinhmucngheo;
                    }
                }
                int tt_vuotdm1 =  (int)Math.round(dinhmuc_sh*1.5);
                if (tieuthu_sh <= dinhmuc_sh) {
                    tiennuoc = tieuthu_shn * g_shn_tckcn + tieuthu_sh*g_sh_tckcn;
                } else if ( tieuthu_sh <= tt_vuotdm1) {
                    tiennuoc = tieuthu_shn * g_shn_tckcn + (dinhmuc_sh)*g_sh_tckcn+ (tieuthu_sh - dinhmuc_sh)*g_shvm1_tckcn;
                    //tiennuoc = g_shn * dinhmucngheo + (tieuthu - dinhmucngheo) * g_shvm1;
                } else {
                    int tt_vm1 = (int)Math.round(dinhmuc_sh*0.5);
                    int tt_vm2 = tieuthu_sh - dinhmuc_sh - tt_vm1;
                    tiennuoc = tieuthu_shn * g_shn_tckcn + (dinhmuc_sh)*g_sh_tckcn+ tt_vm1*g_shvm1_tckcn +tt_vm2*g_shvm2_tckcn;
                    //tiennuoc = g_shn * dinhmucngheo + dinhmucngheo*0.5 * g_shvm1 + (tieuthu - dinhmucngheo * 1.5) * g_shvm2;
                }
                break;
            }
            case 52: {
                //Giá sỉ Khu công nghiệp
                //(Toàn bộ tiêu thụ) x G_SX_TC_KCN
                tiennuoc = tieuthu * g_sx_tckcn;
                break;
            }
            case 53: {
                //Giá sỉ TT thương mại
                //(Toàn bộ tiêu thụ) x G_KDDV_TC_KCN
                tiennuoc = tieuthu * g_kd_tckcn;
                break;
            }
            case 54: {
                //Giá sỉ HCSN
                //(Toàn bộ tiêu thụ) x G_HCSN_TC_KCN
                tiennuoc = tieuthu * g_hcsn_tckcn;
                break;
            }
            case 55: {
                //Giá sỉ đặc biệt
                //(Toàn bộ tiêu thụ) x giá thỏa thuận
                int g_tt = 0;
                tiennuoc = tieuthu * g_tt;
                break;
            }
            case 56: {
                //Giá sỉ CG
                //(Toàn bộ tiêu thụ) x giá CG
                int g_cg = 0;
                tiennuoc = tieuthu * g_cg;
                break;
            }
            case 57: {
                //Giá sỉ SH thuần túy
                //(Toàn bộ tiêu thụ) x G_SH_TC_KCN
                tiennuoc = tieuthu * g_sh_tckcn;
                break;
            }
            case 58: {
                //Giá sỉ phức hợp
                //(%SX) x G_SX_TC_KCN + (%KD) x G_KDDV_TC_KCN + (%HCSN) x G_HCSN_TC_KCN
                // (%SX) x 10,260 + (%KD) x 18,090 + (%HCSN) x 11,070
                if((tt_hcsn+tt_sx+tt_kd) !=tieuthu){
                    tt_kd =tieuthu -tt_hcsn-tt_sx;
                }
                tiennuoc = tt_sx * g_sx_tckcn + tt_kd * g_kd_tckcn + tt_hcsn * g_hcsn_tckcn;
                break;
            }
            case 59: {
                //Giá sỉ phức hợp
                // %SH = [(SHN) x G_SHN_TC_KCN + (SH) x G_SH_TC_KCN + (SHVM1 x G_SHVM1_TC_KCN) + (SHVM2 x G_SHVM2_TC_KCN)] + (%SX) x G_SX_TC_KCN + (%KD) x G_KDDV_TC_KCN
                if((tt_sx+tt_kd+tt_hcsn+tt_sh) !=tieuthu){
                    tt_kd = tieuthu - tt_sx -tt_hcsn- tt_sh;
                }


                int tieuthu_sh =tt_sh;
                int dinhmuc_sh = dinhmuc;
                int tieuthu_shn =0;
                if(dinhmucngheo !=0){
                    if(dinhmuc < dinhmucngheo){
                        dinhmuc= dinhmucngheo;
                        dinhmuc_sh = dinhmucngheo;
                    }else {
                        dinhmuc_sh = dinhmuc - dinhmucngheo;
                    }
                    if(tt_sh < dinhmuc) {
                        tieuthu_shn = Math.round((dinhmucngheo * tt_sh) / dinhmuc);
                        tieuthu_sh = tt_sh - tieuthu_shn;
                    }else {
                        tieuthu_shn = dinhmucngheo;
                        tieuthu_sh = tt_sh - dinhmucngheo;
                    }
                }
                int tt_vuotdm1 =  (int)Math.round(dinhmuc_sh*1.5);
                if (tieuthu_sh <= dinhmuc_sh) {
                    tiennuoc = tieuthu_shn * g_shn_tckcn + tieuthu_sh*g_sh_tckcn + tt_sx * g_sx_tckcn + tt_kd * g_kd_tckcn;;
                } else if ( tieuthu_sh <= tt_vuotdm1) {
                    tiennuoc = tieuthu_shn * g_shn_tckcn + (dinhmuc_sh)*g_sh_tckcn+ (tieuthu_sh - dinhmuc_sh)*g_shvm1_tckcn+ tt_sx * g_sx_tckcn + tt_kd * g_kd_tckcn;;
                    //tiennuoc = g_shn * dinhmucngheo + (tieuthu - dinhmucngheo) * g_shvm1;
                } else {
                    int tt_vm1 = (int)Math.round(dinhmuc_sh*0.5);
                    int tt_vm2 = tieuthu_sh - dinhmuc_sh - tt_vm1;
                    tiennuoc = tieuthu_shn * g_shn_tckcn + (dinhmuc_sh)*g_sh_tckcn+ tt_vm1*g_shvm1_tckcn +tt_vm2*g_shvm2_tckcn+ tt_sx * g_sx_tckcn + tt_kd * g_kd_tckcn;;
                    //tiennuoc = g_shn * dinhmucngheo + dinhmucngheo*0.5 * g_shvm1 + (tieuthu - dinhmucngheo * 1.5) * g_shvm2;
                }

                break;
            }
            case 68: {
                //Giá sỉ SH + Lẻ Dịch vụ
                //%SH = [(SHN) x G_SHN_TC_KCN + (SH) x G_SH_TC_KCN + (SHVM1 x G_SHVM1_TC_KCN) + (SHVM2 x G_SHVM2_TC_KCN)] + (%KD) x G_KD_DV
                // %SH = [(SHN) x 5,400 + (SH) x 5,670 + (SHVM1 x 10.890) + (SHVM2 x 12,240)] + (%KD) x 20,100
                int tieuthu_sh =tt_sh;
                int dinhmuc_sh = dinhmuc;
                int tieuthu_shn =0;
                if(dinhmucngheo !=0){
                    if(dinhmuc < dinhmucngheo){
                        dinhmuc= dinhmucngheo;
                        dinhmuc_sh = dinhmucngheo;
                    }else {
                        dinhmuc_sh = dinhmuc - dinhmucngheo;
                    }
                    if(tieuthu < dinhmuc) {
                        tieuthu_shn = Math.round((dinhmucngheo * tt_sh) / dinhmuc);
                        tieuthu_sh = tt_sh - tieuthu_shn;
                    }else {
                        tieuthu_shn = dinhmucngheo;
                        tieuthu_sh = tt_sh - dinhmucngheo;
                    }
                }
                int tt_vuotdm1 =  (int)Math.round(dinhmuc_sh*1.5);
                if (tieuthu_sh <= dinhmuc_sh) {
                    tiennuoc = tieuthu_shn * g_shn_tckcn + tieuthu_sh*g_sh_tckcn + tt_kd * g_kd_tckcn;
                } else if ( tieuthu_sh <= tt_vuotdm1) {
                    tiennuoc = tieuthu_shn * g_shn_tckcn + (dinhmuc_sh)*g_sh_tckcn+ (tieuthu_sh - dinhmuc_sh)*g_shvm1_tckcn+ tt_kd * g_kd_tckcn;
                    //tiennuoc = g_shn * dinhmucngheo + (tieuthu - dinhmucngheo) * g_shvm1;
                } else {
                    int tt_vm1 = (int)Math.round(dinhmuc_sh*0.5);
                    int tt_vm2 = tieuthu_sh - dinhmuc_sh - tt_vm1;
                    tiennuoc = tieuthu_shn * g_shn_tckcn + (dinhmuc_sh)*g_sh_tckcn+ tt_vm1*g_shvm1_tckcn +tt_vm2*g_shvm2_tckcn+ tt_kd * g_kd_tckcn;;
                    //tiennuoc = g_shn * dinhmucngheo + dinhmucngheo*0.5 * g_shvm1 + (tieuthu - dinhmucngheo * 1.5) * g_shvm2;
                }

                break;
            }

        }
        return  (tiennuoc);
    }

    private long GiaNuoc_2021(int tieuthu,int giabieu,int dinhmuc,int dinhmucngheo,int tl_kinhdoanh,int tl_hcsn,int tl_sh,int tl_sx) {
        double tiennuoc = 0;
        int tt_sh = (int) Math.round((tieuthu* tl_sh*0.01));
        int tt_hcsn = (int) Math.round((tieuthu*tl_hcsn*0.01));
        int tt_sx = (int) Math.round((tieuthu * tl_sx*0.01));
        int tt_kd = (int) Math.round((tieuthu* tl_kinhdoanh*0.01));


        switch (giabieu) {

            // TU GIA
            case 10: {
                // (shn*6000 +shvm1*12100 + shvm2*13600)
                if (tieuthu <= dinhmucngheo) {
                    tiennuoc = tieuthu * 6000;
                } else if ( tieuthu <= dinhmucngheo * 1.5) {
                    tiennuoc = 6000 * dinhmucngheo + (tieuthu - dinhmucngheo) * 12100;
                } else {
                    tiennuoc = 6000 * dinhmucngheo + dinhmucngheo*0.5 * 12100 + (tieuthu - dinhmucngheo * 1.5) * 13600;
                }
                break;
            }
            case 11: {
                // (shn*6000 + sh*6300 +shvm1*12100 + shvm2*13600)
                if (tieuthu <= dinhmucngheo) {
                    tiennuoc = tieuthu * 6000;
                } else if (dinhmucngheo < tieuthu && tieuthu <= dinhmuc) {
                    tiennuoc = dinhmucngheo * 6000 + (tieuthu - dinhmucngheo) * 6300;
                } else if (tieuthu <= dinhmuc * 1.5) {
                    tiennuoc = dinhmucngheo * 6000 + (dinhmuc - dinhmucngheo) * 6300 + (tieuthu - dinhmuc) * 12100;
                } else {
                    tiennuoc = dinhmucngheo * 6000 + (dinhmuc - dinhmucngheo) * 6300 + dinhmuc*0.5 * 12100 + (tieuthu - dinhmuc * 1.5) * 13600;
                }
                break;
            }
            case 12: {
                // tieuthu*11400
                tiennuoc = tieuthu * 11400;
                break;
            }
            case 13: {
                // tieuthu*20100
                tiennuoc = tieuthu * 20100;
                break;
            }
            case 14: {
                // shn*6000 + sh*6300 +shvm*11400
                if (tieuthu <= dinhmucngheo) {
                    tiennuoc = tieuthu * 6000;
                } else if (dinhmucngheo < tieuthu && tieuthu <= dinhmuc) {
                    tiennuoc = dinhmucngheo * 6000 + (tieuthu - dinhmucngheo) * 6300;
                } else {
                    tiennuoc = dinhmucngheo * 6000 + (dinhmuc - dinhmucngheo) * 6300 + (tieuthu - dinhmuc) * 11400;
                }
                break;
            }
            case 15: {
                // shn*6000 + sh*6300 +shvm*20100
                if (tieuthu <= dinhmucngheo) {
                    tiennuoc = tieuthu * 6000;
                } else if (dinhmucngheo < tieuthu && tieuthu <= dinhmuc) {
                    tiennuoc = dinhmucngheo * 6000 + (tieuthu - dinhmucngheo) * 6300;
                } else {
                    tiennuoc = dinhmucngheo * 6000 + (dinhmuc - dinhmucngheo) * 6300 + (tieuthu - dinhmuc) * 20100;
                }
                break;
            }
            case 16: {
                //Tỉ lệ không có Định mức: (% SX) x 11,400 + (% KD) x 20,100
                //Tỉ lệ có Định mức: (%SH) = [(SH x 6.300) + (SHVM1 x 12.100) + (SHVM2 x 13.600)] + (%SX) x 11,400 + (%KD) x 20,100

                if (dinhmuc == 0) {
                    if((tt_sx+tt_kd)!=tieuthu){
                        tt_kd =tieuthu -tt_sx;
                    }
                    tiennuoc = tt_sx * 11400 + tt_kd * 20100;
                } else {
                    if((tt_sx+tt_kd+tt_sh)!=tieuthu){
                        tt_kd =tieuthu -tt_sx- tt_sh;
                    }

                    if (tt_sh <= dinhmuc) {
                        tiennuoc = tt_sh * 6300  + tt_sx* 11400 + tt_kd * 20100;
                    } else if (tt_sh <= dinhmuc * 1.5) {
                        tiennuoc = dinhmuc * 6300 + (tt_sh - dinhmuc) * 12100 + tt_sx * 11400 + tt_kd * 20100;
                    } else {
                        tiennuoc = dinhmuc * 6300 + (dinhmuc*0.5) * 12100  +(tt_sh - dinhmuc * 1.5) * 13600 + tt_sx * 11400 + tt_kd * 20100;
                    }
                }
                break;
            }
            case 17: {
                //(Toàn bộ tiêu thụ) x 6.300
                tiennuoc = tieuthu * 6300;
                break;
            }
            case 18: {
                //(SHN x 6,000) + (SH x 6.300) + (Tiêu thụ VM x 12,300)
                if (tieuthu <= dinhmucngheo) {
                    tiennuoc = tieuthu * 6000;
                } else if (tieuthu <= dinhmuc) {
                    tiennuoc = dinhmucngheo * 6000 + (tieuthu - dinhmucngheo) * 6300;
                } else {
                    tiennuoc = dinhmucngheo * 6000 + (dinhmuc - dinhmucngheo) * 6300 + (tieuthu - dinhmuc) * 12300;
                }
                break;
            }
            case 19: {
                //Tỉ lệ không có Định mức: (% SX) x 11.400 + (% KD) x 20,100 + (%HCSN) x 12,300
                //Tỉ lệ có Định mức: (%SH) = [(SHN)x 6,000 + (SH x 6.300) + (SHVM1 x 12,000) + (SHVM2 x 13,600)] + (%SX) x 11.400 + (%KD) x 20,100 + (%HCSN) x 12,300

                if (dinhmuc == 0) {
                    if((tt_sx+tt_kd+tt_hcsn) !=tieuthu){
                        tt_kd = tieuthu - tt_sx -tt_hcsn;
                    }
                    tiennuoc = tt_sx * 11400 + tt_kd * 20100 + tt_hcsn * 12300;
                } else {
                    if((tt_sx+tt_kd+tt_hcsn+tt_sh) !=tieuthu){
                        tt_kd = tieuthu - tt_sx -tt_hcsn- tt_sh;
                    }

                    if (tt_sh <= dinhmucngheo) {
                        tiennuoc = tt_sh * 6300 + tt_sx * 11400 + tt_kd * 20100 + tt_hcsn * 12300;
                    } else if (tt_sh <= dinhmuc) {
                        tiennuoc = dinhmucngheo * 6300 + (tt_sh - dinhmucngheo) * 6300  + tt_sh * 11400 + tt_kd * 20100 + tt_hcsn * 12300;
                    } else if (tt_sh <= dinhmuc * 1.5) {
                        tiennuoc = dinhmucngheo * 6300 + (dinhmuc - dinhmucngheo) * 6300 + (tt_sh - dinhmuc) * 12100 + tt_sx * 11400 + tt_kd * 20100 + tt_hcsn * 12300;
                    } else {
                        tiennuoc = dinhmucngheo * 6300 + (dinhmuc - dinhmucngheo) * 6300 + (dinhmuc *0.5) * 12100 + (tt_sh - dinhmuc * 1.5) * 13600  + tt_sx * 11400 + tt_kd * 20100 + tt_hcsn * 12300;
                    }
                }
                break;
            }
            // HO TAP THE
            case 21: {
                // (shn*6000 + sh*6300 +shvm1*12100 + shvm2*13600)
                if (tieuthu <= dinhmucngheo) {
                    tiennuoc = tieuthu * 6000;
                } else if (dinhmucngheo < tieuthu && tieuthu <= dinhmuc) {
                    tiennuoc = dinhmucngheo * 6000 + (tieuthu - dinhmucngheo) * 6300;
                } else if (tieuthu <= dinhmuc * 1.5) {
                    tiennuoc = dinhmucngheo * 6000 + (dinhmuc - dinhmucngheo) * 6300 + (tieuthu - dinhmuc) * 12100;
                } else {
                    tiennuoc = dinhmucngheo * 6000 + (dinhmuc - dinhmucngheo) * 6300 + dinhmuc*0.5 * 12100 + (tieuthu - dinhmuc * 1.5) * 13600;
                }
                break;
            }
            case 22: {
                // tieuthu*11400
                tiennuoc = tieuthu * 11400;
                break;
            }
            case 23: {
                // tieuthu*20100
                tiennuoc = tieuthu * 20100;
                break;
            }
            case 24: {
                // shn*6000 + sh*6300 +shvm*11400
                if (tieuthu <= dinhmucngheo) {
                    tiennuoc = tieuthu * 6000;
                } else if (dinhmucngheo < tieuthu && tieuthu <= dinhmuc) {
                    tiennuoc = dinhmucngheo * 6000 + (tieuthu - dinhmucngheo) * 6300;
                } else {
                    tiennuoc = dinhmucngheo * 6000 + (dinhmuc - dinhmucngheo) * 6300 + (tieuthu - dinhmuc) * 11400;
                }
                break;
            }
            case 25: {
                // shn*6000 + sh*6300 +shvm*20100
                if (tieuthu <= dinhmucngheo) {
                    tiennuoc = tieuthu * 6000;
                } else if (dinhmucngheo < tieuthu && tieuthu <= dinhmuc) {
                    tiennuoc = dinhmucngheo * 6000 + (tieuthu - dinhmucngheo) * 6300;
                } else {
                    tiennuoc = dinhmucngheo * 6000 + (dinhmuc - dinhmucngheo) * 6300 + (tieuthu - dinhmuc) * 20100;
                }
                break;
            }
            case 26: {
                //Tỉ lệ không có Định mức: (% SX) x 11,400 + (% KD) x 20,100
                //Tỉ lệ có Định mức: (%SH) = [(SH x 6.300) + (SHVM1 x 12.100) + (SHVM2 x 13.600)] + (%SX) x 11,400 + (%KD) x 20,100

                if (dinhmuc == 0) {
                    if((tt_sx+tt_kd)!=tieuthu){
                        tt_kd =tieuthu -tt_sx;
                    }
                    tiennuoc = tt_sx * 11400 + tt_kd * 20100;
                } else {
                    if((tt_sx+tt_kd+tt_hcsn+tt_sh) !=tieuthu){
                        tt_kd = tieuthu - tt_sx -tt_hcsn- tt_sh;
                    }

                    if (tt_sh <= dinhmuc) {
                        tiennuoc = tt_sh * 6300 + tt_sx * 11400 + tt_kd * 20100;
                    } else if (tt_sh <= dinhmuc * 1.5) {
                        tiennuoc = dinhmuc * 6300 + (tt_sh - dinhmuc) * 12100  + tt_sx * 11400 + tt_kd * 20100;
                    } else {
                        tiennuoc = dinhmuc * 6300 + (dinhmuc*0.5) * 12100  +(tt_sh - dinhmuc * 1.5) * 13600  + tt_sx * 11400 + tt_kd * 20100;
                    }
                }
                break;
            }
            case 27: {
                //(Toàn bộ tiêu thụ) x 6.300
                tiennuoc = tieuthu * 6300;
                break;
            }
            case 28: {
                //(SHN x 6,000) + (SH x 6.300) + (Tiêu thụ VM x 12,300)
                if (tieuthu <= dinhmucngheo) {
                    tiennuoc = tieuthu * 6000;
                } else if (tieuthu <= dinhmuc) {
                    tiennuoc = dinhmucngheo * 6000 + (tieuthu - dinhmucngheo) * 6300;
                } else {
                    tiennuoc = dinhmucngheo * 6000 + (dinhmuc - dinhmucngheo) * 6300 + (tieuthu - dinhmuc) * 12300;
                }
                break;
            }
            case 29: {
                //Tỉ lệ không có Định mức: (% SX) x 11.400 + (% KD) x 20,100 + (%HCSN) x 12,300
                //Tỉ lệ có Định mức: (%SH) = [(SHN)x 6,000 + (SH x 6.300) + (SHVM1 x 12,000) + (SHVM2 x 13,600)] + (%SX) x 11.400 + (%KD) x 20,100 + (%HCSN) x 12,300

                if (dinhmuc == 0) {
                    if((tt_sx+tt_kd+tt_hcsn)!=tieuthu){
                        tt_kd = tieuthu - tt_sx-tt_hcsn;
                    }
                    tiennuoc = tt_sx * 11400 + tt_kd * 20100 + tt_hcsn * 12300;
                } else {

                    if((tt_sx+tt_kd+tt_hcsn+tt_sh) !=tieuthu){
                        tt_kd = tieuthu - tt_sx -tt_hcsn- tt_sh;
                    }

                    if (tt_sh <= dinhmucngheo) {
                        tiennuoc = tt_sh * 6000  + tt_sx * 11400 + tt_kd * 20100 + tt_hcsn * 12300;
                    } else if (tt_sh <= dinhmuc) {
                        tiennuoc = dinhmucngheo * 6000 + (tt_sh - dinhmucngheo) * 6300  + tt_sx * 11400 + tt_kd * 20100 + tt_hcsn * 12300;
                    } else if (tt_sh <= dinhmuc * 1.5) {
                        tiennuoc = dinhmucngheo * 6000 + (dinhmuc - dinhmucngheo) * 6300 + (tt_sh - dinhmuc) * 12100  + tt_sx * 11400 + tt_kd * 20100 + tt_hcsn * 12300;
                    } else {
                        tiennuoc = dinhmucngheo * 6000 + (dinhmuc - dinhmucngheo) * 6300 + (dinhmuc *0.5) * 12100 + (tt_sh - dinhmuc * 1.5) * 13600 + tt_sx * 11400 + tt_kd * 20100 + tt_hcsn * 12300;
                    }
                }
                break;
            }
            // CO QUAN
            case 31: {
                //(Toàn bộ tiêu thụ) x 12.300
                tiennuoc = tieuthu * 12300;
                break;
            }
            case 32: {
                //(Toàn bộ tiêu thụ) x 11.400
                tiennuoc = tieuthu * 11400;
                break;
            }
            case 33: {
                //(Toàn bộ tiêu thụ) x 20.100
                tiennuoc = tieuthu * 20100;
                break;
            }
            case 34: {
                //(% HCSN) x 12,300 + (% SX) x 11,400
                if((tt_hcsn+tt_sx)!=tieuthu){
                    tt_sx = tieuthu - tt_hcsn;
                }
                tiennuoc =  tt_hcsn * 12300 + tt_sx * 11400;
                break;
            }
            case 35: {
                //(% HCSN) x 12,300 + (% KD) x 20,100
                if((tt_hcsn+tt_kd) !=tieuthu){
                    tt_kd =tieuthu -tt_hcsn;
                }
                tiennuoc = tt_hcsn * 12300 + tt_kd * 20100;
                break;
            }
            case 36: {
                //(% HCSN) x 12,300 + (% SX) x 11,400 + (% KD) x 20,100
                if((tt_hcsn+tt_sx+tt_kd) !=tieuthu){
                    tt_kd =tieuthu -tt_hcsn-tt_sx;
                }
                tiennuoc = tt_hcsn* 12300 + tt_sx * 11400 + tt_kd * 20100;
                break;
            }
            case 38: {
                //(SHN x 6,000) + (SH x 6.300) + (Tiêu thụ VM x 12,300)
                if (tieuthu <= dinhmucngheo) {
                    tiennuoc = tieuthu * 6000;
                } else if (tieuthu <= dinhmuc) {
                    tiennuoc = dinhmucngheo * 6000 + (tieuthu - dinhmucngheo) * 6300;
                } else {
                    tiennuoc = dinhmucngheo * 6000 + (dinhmuc - dinhmucngheo) * 6300 + (tieuthu - dinhmuc) * 12300;
                }
                break;
            }
            case 39: {
                //Tỉ lệ không có Định mức: (% SX) x 11.400 + (% KD) x 20,100 + (%HCSN) x 12,300
                //Tỉ lệ có Định mức: (%SH) = [(SHN)x 6,000 + (SH x 6.300) + (SHVM1 x 12,000) + (SHVM2 x 13,600)] + (%SX) x 11.400 + (%KD) x 20,100 + (%HCSN) x 12,300

                if (dinhmuc == 0) {
                    if((tt_hcsn+tt_sx+tt_kd) !=tieuthu){
                        tt_kd =tieuthu -tt_hcsn-tt_sx;
                    }
                    tiennuoc = tt_sx * 11400 + tt_kd * 20100 + tt_hcsn * 12300;
                } else {
                    if((tt_sx+tt_kd+tt_hcsn+tt_sh) !=tieuthu){
                        tt_kd = tieuthu - tt_sx -tt_hcsn- tt_sh;
                    }

                    if (tt_sh <= dinhmucngheo) {
                        tiennuoc = tt_sh * 6000 + tt_sx * 11400 + tt_kd * 20100 + tt_hcsn * 12300;
                    } else if (tt_sh <= dinhmuc) {
                        tiennuoc = dinhmucngheo * 6000 + (tt_sh - dinhmucngheo) * 6300  + tt_sx * 11400 + tt_kd * 20100 + tt_hcsn * 12300;
                    } else if (tt_sh <= dinhmuc * 1.5) {
                        tiennuoc = dinhmucngheo * 6000 + (dinhmuc - dinhmucngheo) * 6300 + (tt_sh - dinhmuc) * 12100  +tt_sx * 11400 + tt_kd * 20100 + tt_hcsn * 12300;
                    } else {
                        tiennuoc = dinhmucngheo * 6000 + (dinhmuc - dinhmucngheo) * 6300 + (dinhmuc *0.5 ) * 12100 + (tt_sh - dinhmuc * 1.5) * 13600  + tt_sx * 11400 +   tt_kd * 20100 + tt_hcsn * 12300;
                    }
                }
                break;
            }
            case 51: {
                //CHUNG CƯ - KHU CÔNG NGHIỆP
                //(SHN x 5.400) + (SH x 5.670) + (SHVM1 x 10.890) + (SHVM2 x 12,240)
                if (tieuthu <= dinhmucngheo) {
                    tiennuoc = tieuthu * 5400;
                } else if (dinhmucngheo < tieuthu && tieuthu <= dinhmuc) {
                    tiennuoc = dinhmucngheo * 5400 + (tieuthu - dinhmucngheo) * 5670;
                } else if (tieuthu <= dinhmuc * 1.5) {
                    tiennuoc = dinhmucngheo * 5400 + (dinhmuc - dinhmucngheo) * 5670 + (tieuthu - dinhmuc) * 10890;
                } else {
                    tiennuoc = dinhmucngheo * 5400 + (dinhmuc - dinhmucngheo) * 5670 + dinhmuc*0.5 * 10890 + (tieuthu - dinhmuc * 1.5) * 12240;
                }
                break;
            }
            case 52: {
                tiennuoc = tieuthu * 10260;
                break;
            }
            case 53: {
                tiennuoc = tieuthu * 18090;
                break;
            }
            case 54: {
                tiennuoc = tieuthu * 11070;
                break;
            }
            case 55: {
                tiennuoc = tieuthu * giabieu;
                break;
            }
            case 56: {
                tiennuoc = tieuthu * giabieu;
                break;
            }
            case 57: {
                tiennuoc = tieuthu * 5670;
                break;
            }
            case 58: {
                // (%SX) x 10,260 + (%KD) x 18,090 + (%HCSN) x 11,070
                if((tt_hcsn+tt_sx+tt_kd) !=tieuthu){
                    tt_kd =tieuthu -tt_hcsn-tt_sx;
                }
                tiennuoc = tt_sx * 10260 + tt_kd * 18090 + tt_hcsn * 11070;
                break;
            }
            case 59: {
                // %SH = [(SHN) x 5,400 + (SH) x 5,670 + (SHVM1 x 10.890) + (SHVM2 x 12,240)] + (%SX) x 10,260 + (%KD) x 18,090
                if((tt_sx+tt_kd+tt_hcsn+tt_sh) !=tieuthu){
                    tt_kd = tieuthu - tt_sx -tt_hcsn- tt_sh;
                }

                if (tt_sh <= dinhmucngheo) {
                    tiennuoc = (tt_sh * 5400)  + tt_sx * 10260 + tt_kd * 18090;
                } else if (tt_sh <= dinhmuc) {
                    tiennuoc = dinhmucngheo * 5400 + (tt_sh - dinhmucngheo) * 5670  + tt_sx * 10260 + tt_kd * 18090;
                } else if (tt_sh <= dinhmuc * 1.5) {
                    tiennuoc = dinhmucngheo * 5400 + (dinhmuc - dinhmucngheo) * 5670 + (tt_sh - dinhmuc) * 10890  + tt_sx * 10260 + tt_kd * 18090;
                } else {
                    tiennuoc = dinhmucngheo * 5400 + (dinhmuc - dinhmucngheo) * 5670 + (dinhmuc *0.5) * 10890 + (tt_sh - dinhmuc * 1.5) * 12240  + tt_sx * 10260 + tt_kd * 18090;
                }
                break;
            }
            case 68: {
                // %SH = [(SHN) x 5,400 + (SH) x 5,670 + (SHVM1 x 10.890) + (SHVM2 x 12,240)] + (%KD) x 20,100
                if((tt_sx+tt_kd) !=tieuthu){
                    tt_kd =tieuthu -tt_sx;
                }
                if (tt_sh <= dinhmucngheo) {
                    tiennuoc = (tt_sh * 5400)  + tt_kd * 20100;
                } else if (tt_sh <= dinhmuc) {
                    tiennuoc = dinhmucngheo * 5400 + (tt_sh - dinhmucngheo) * 5670  + tt_kd * 20100;
                } else if (tt_sh <= dinhmuc * 1.5) {
                    tiennuoc = dinhmucngheo * 5400 + (dinhmuc - dinhmucngheo) * 5670 + (tt_sh - dinhmuc) * 10890  + tt_kd * 20100;
                } else {
                    tiennuoc = dinhmucngheo * 5400 + (dinhmuc - dinhmucngheo) * 5670 + (dinhmuc *0.5) * 10890 + (tt_sh - dinhmuc * 1.5) * 12240  + tt_kd * 20100;
                }
                break;
            }

        }
        return (long) (tiennuoc);
    }

    private long GiaNuoc_ThueBVMT_2021_Covid(int tieuthu,int giabieu,int dinhmuc,int dinhmucngheo,int tl_kinhdoanh,int tl_hcsn,int tl_sh,int tl_sx) {
        double tiennuoc = 0;
        int tt_sh = (int) Math.round((tieuthu* tl_sh*0.01));
        int tt_hcsn = (int) Math.round((tieuthu*tl_hcsn*0.01));
        int tt_sx = (int) Math.round((tieuthu * tl_sx*0.01));
        int tt_kd = (int) Math.round((tieuthu* tl_kinhdoanh*0.01));


        switch (giabieu) {
            /*
            // TU GIA
            case 10: {
                // (shn*6000 +shvm1*12100 + shvm2*13600)
                if (tieuthu <= dinhmucngheo) {
                    tiennuoc = tieuthu * 6000;
                } else if ( tieuthu <= dinhmucngheo * 1.5) {
                    tiennuoc = 6000 * dinhmucngheo + (tieuthu - dinhmucngheo) * 12100;
                } else {
                    tiennuoc = 6000 * dinhmucngheo + dinhmucngheo*0.5 * 12100 + (tieuthu - dinhmucngheo * 1.5) * 13600;
                }
                break;
            }
            case 11: {
                // (shn*6000 + sh*6300 +shvm1*12100 + shvm2*13600)
                if (tieuthu <= dinhmucngheo) {
                    tiennuoc = tieuthu * 6000;
                } else if (dinhmucngheo < tieuthu && tieuthu <= dinhmuc) {
                    tiennuoc = dinhmucngheo * 6000 + (tieuthu - dinhmucngheo) * 6300;
                } else if (tieuthu <= dinhmuc * 1.5) {
                    tiennuoc = dinhmucngheo * 6000 + (dinhmuc - dinhmucngheo) * 6300 + (tieuthu - dinhmuc) * 12100;
                } else {
                    tiennuoc = dinhmucngheo * 6000 + (dinhmuc - dinhmucngheo) * 6300 + dinhmuc*0.5 * 12100 + (tieuthu - dinhmuc * 1.5) * 13600;
                }
                break;
            }
            case 12: {
                // tieuthu*11400
                tiennuoc = tieuthu * 11400;
                break;
            }
            case 13: {
                // tieuthu*20100
                tiennuoc = tieuthu * 20100;
                break;
            }
            case 14: {
                // shn*6000 + sh*6300 +shvm*11400
                if (tieuthu <= dinhmucngheo) {
                    tiennuoc = tieuthu * 6000;
                } else if (dinhmucngheo < tieuthu && tieuthu <= dinhmuc) {
                    tiennuoc = dinhmucngheo * 6000 + (tieuthu - dinhmucngheo) * 6300;
                } else {
                    tiennuoc = dinhmucngheo * 6000 + (dinhmuc - dinhmucngheo) * 6300 + (tieuthu - dinhmuc) * 11400;
                }
                break;
            }
            case 15: {
                // shn*6000 + sh*6300 +shvm*20100
                if (tieuthu <= dinhmucngheo) {
                    tiennuoc = tieuthu * 6000;
                } else if (dinhmucngheo < tieuthu && tieuthu <= dinhmuc) {
                    tiennuoc = dinhmucngheo * 6000 + (tieuthu - dinhmucngheo) * 6300;
                } else {
                    tiennuoc = dinhmucngheo * 6000 + (dinhmuc - dinhmucngheo) * 6300 + (tieuthu - dinhmuc) * 20100;
                }
                break;
            }
            case 16: {
                //Tỉ lệ không có Định mức: (% SX) x 11,400 + (% KD) x 20,100
                //Tỉ lệ có Định mức: (%SH) = [(SH x 6.300) + (SHVM1 x 12.100) + (SHVM2 x 13.600)] + (%SX) x 11,400 + (%KD) x 20,100

                if (dinhmuc == 0) {
                    if((tt_sx+tt_kd)!=tieuthu){
                        tt_kd =tieuthu -tt_sx;
                    }
                    tiennuoc = tt_sx * 11400 + tt_kd * 20100;
                } else {
                    if((tt_sx+tt_kd+tt_sh)!=tieuthu){
                        tt_kd =tieuthu -tt_sx- tt_sh;
                    }

                    if (tt_sh <= dinhmuc) {
                        tiennuoc = tt_sh * 6300  + tt_sx* 11400 + tt_kd * 20100;
                    } else if (tt_sh <= dinhmuc * 1.5) {
                        tiennuoc = dinhmuc * 6300 + (tt_sh - dinhmuc) * 12100 + tt_sx * 11400 + tt_kd * 20100;
                    } else {
                        tiennuoc = dinhmuc * 6300 + (dinhmuc*0.5) * 12100  +(tt_sh - dinhmuc * 1.5) * 13600 + tt_sx * 11400 + tt_kd * 20100;
                    }
                }
                break;
            }
            case 17: {
                //(Toàn bộ tiêu thụ) x 6.300
                tiennuoc = tieuthu * 6300;
                break;
            }
            case 18: {
                //(SHN x 6,000) + (SH x 6.300) + (Tiêu thụ VM x 12,300)
                if (tieuthu <= dinhmucngheo) {
                    tiennuoc = tieuthu * 6000;
                } else if (tieuthu <= dinhmuc) {
                    tiennuoc = dinhmucngheo * 6000 + (tieuthu - dinhmucngheo) * 6300;
                } else {
                    tiennuoc = dinhmucngheo * 6000 + (dinhmuc - dinhmucngheo) * 6300 + (tieuthu - dinhmuc) * 12300;
                }
                break;
            }
            case 19: {
                //Tỉ lệ không có Định mức: (% SX) x 11.400 + (% KD) x 20,100 + (%HCSN) x 12,300
                //Tỉ lệ có Định mức: (%SH) = [(SHN)x 6,000 + (SH x 6.300) + (SHVM1 x 12,000) + (SHVM2 x 13,600)] + (%SX) x 11.400 + (%KD) x 20,100 + (%HCSN) x 12,300

                if (dinhmuc == 0) {
                    if((tt_sx+tt_kd+tt_hcsn) !=tieuthu){
                        tt_kd = tieuthu - tt_sx -tt_hcsn;
                    }
                    tiennuoc = tt_sx * 11400 + tt_kd * 20100 + tt_hcsn * 12300;
                } else {
                    if((tt_sx+tt_kd+tt_hcsn+tt_sh) !=tieuthu){
                        tt_kd = tieuthu - tt_sx -tt_hcsn- tt_sh;
                    }

                    if (tt_sh <= dinhmucngheo) {
                        tiennuoc = tt_sh * 6300 + tt_sx * 11400 + tt_kd * 20100 + tt_hcsn * 12300;
                    } else if (tt_sh <= dinhmuc) {
                        tiennuoc = dinhmucngheo * 6300 + (tt_sh - dinhmucngheo) * 6300  + tt_sh * 11400 + tt_kd * 20100 + tt_hcsn * 12300;
                    } else if (tt_sh <= dinhmuc * 1.5) {
                        tiennuoc = dinhmucngheo * 6300 + (dinhmuc - dinhmucngheo) * 6300 + (tt_sh - dinhmuc) * 12100 + tt_sx * 11400 + tt_kd * 20100 + tt_hcsn * 12300;
                    } else {
                        tiennuoc = dinhmucngheo * 6300 + (dinhmuc - dinhmucngheo) * 6300 + (dinhmuc *0.5) * 12100 + (tt_sh - dinhmuc * 1.5) * 13600  + tt_sx * 11400 + tt_kd * 20100 + tt_hcsn * 12300;
                    }
                }
                break;
            }
            // HO TAP THE
            case 21: {
                // (shn*6000 + sh*6300 +shvm1*12100 + shvm2*13600)
                if (tieuthu <= dinhmucngheo) {
                    tiennuoc = tieuthu * 6000;
                } else if (dinhmucngheo < tieuthu && tieuthu <= dinhmuc) {
                    tiennuoc = dinhmucngheo * 6000 + (tieuthu - dinhmucngheo) * 6300;
                } else if (tieuthu <= dinhmuc * 1.5) {
                    tiennuoc = dinhmucngheo * 6000 + (dinhmuc - dinhmucngheo) * 6300 + (tieuthu - dinhmuc) * 12100;
                } else {
                    tiennuoc = dinhmucngheo * 6000 + (dinhmuc - dinhmucngheo) * 6300 + dinhmuc*0.5 * 12100 + (tieuthu - dinhmuc * 1.5) * 13600;
                }
                break;
            }
            case 22: {
                // tieuthu*11400
                tiennuoc = tieuthu * 11400;
                break;
            }
            case 23: {
                // tieuthu*20100
                tiennuoc = tieuthu * 20100;
                break;
            }
            case 24: {
                // shn*6000 + sh*6300 +shvm*11400
                if (tieuthu <= dinhmucngheo) {
                    tiennuoc = tieuthu * 6000;
                } else if (dinhmucngheo < tieuthu && tieuthu <= dinhmuc) {
                    tiennuoc = dinhmucngheo * 6000 + (tieuthu - dinhmucngheo) * 6300;
                } else {
                    tiennuoc = dinhmucngheo * 6000 + (dinhmuc - dinhmucngheo) * 6300 + (tieuthu - dinhmuc) * 11400;
                }
                break;
            }
            case 25: {
                // shn*6000 + sh*6300 +shvm*20100
                if (tieuthu <= dinhmucngheo) {
                    tiennuoc = tieuthu * 6000;
                } else if (dinhmucngheo < tieuthu && tieuthu <= dinhmuc) {
                    tiennuoc = dinhmucngheo * 6000 + (tieuthu - dinhmucngheo) * 6300;
                } else {
                    tiennuoc = dinhmucngheo * 6000 + (dinhmuc - dinhmucngheo) * 6300 + (tieuthu - dinhmuc) * 20100;
                }
                break;
            }
            case 26: {
                //Tỉ lệ không có Định mức: (% SX) x 11,400 + (% KD) x 20,100
                //Tỉ lệ có Định mức: (%SH) = [(SH x 6.300) + (SHVM1 x 12.100) + (SHVM2 x 13.600)] + (%SX) x 11,400 + (%KD) x 20,100

                if (dinhmuc == 0) {
                    if((tt_sx+tt_kd)!=tieuthu){
                        tt_kd =tieuthu -tt_sx;
                    }
                    tiennuoc = tt_sx * 11400 + tt_kd * 20100;
                } else {
                    if((tt_sx+tt_kd+tt_hcsn+tt_sh) !=tieuthu){
                        tt_kd = tieuthu - tt_sx -tt_hcsn- tt_sh;
                    }

                    if (tt_sh <= dinhmuc) {
                        tiennuoc = tt_sh * 6300 + tt_sx * 11400 + tt_kd * 20100;
                    } else if (tt_sh <= dinhmuc * 1.5) {
                        tiennuoc = dinhmuc * 6300 + (tt_sh - dinhmuc) * 12100  + tt_sx * 11400 + tt_kd * 20100;
                    } else {
                        tiennuoc = dinhmuc * 6300 + (dinhmuc*0.5) * 12100  +(tt_sh - dinhmuc * 1.5) * 13600  + tt_sx * 11400 + tt_kd * 20100;
                    }
                }
                break;
            }
            case 27: {
                //(Toàn bộ tiêu thụ) x 6.300
                tiennuoc = tieuthu * 6300;
                break;
            }
            case 28: {
                //(SHN x 6,000) + (SH x 6.300) + (Tiêu thụ VM x 12,300)
                if (tieuthu <= dinhmucngheo) {
                    tiennuoc = tieuthu * 6000;
                } else if (tieuthu <= dinhmuc) {
                    tiennuoc = dinhmucngheo * 6000 + (tieuthu - dinhmucngheo) * 6300;
                } else {
                    tiennuoc = dinhmucngheo * 6000 + (dinhmuc - dinhmucngheo) * 6300 + (tieuthu - dinhmuc) * 12300;
                }
                break;
            }
            case 29: {
                //Tỉ lệ không có Định mức: (% SX) x 11.400 + (% KD) x 20,100 + (%HCSN) x 12,300
                //Tỉ lệ có Định mức: (%SH) = [(SHN)x 6,000 + (SH x 6.300) + (SHVM1 x 12,000) + (SHVM2 x 13,600)] + (%SX) x 11.400 + (%KD) x 20,100 + (%HCSN) x 12,300

                if (dinhmuc == 0) {
                    if((tt_sx+tt_kd+tt_hcsn)!=tieuthu){
                        tt_kd = tieuthu - tt_sx-tt_hcsn;
                    }
                    tiennuoc = tt_sx * 11400 + tt_kd * 20100 + tt_hcsn * 12300;
                } else {

                    if((tt_sx+tt_kd+tt_hcsn+tt_sh) !=tieuthu){
                        tt_kd = tieuthu - tt_sx -tt_hcsn- tt_sh;
                    }

                    if (tt_sh <= dinhmucngheo) {
                        tiennuoc = tt_sh * 6000  + tt_sx * 11400 + tt_kd * 20100 + tt_hcsn * 12300;
                    } else if (tt_sh <= dinhmuc) {
                        tiennuoc = dinhmucngheo * 6000 + (tt_sh - dinhmucngheo) * 6300  + tt_sx * 11400 + tt_kd * 20100 + tt_hcsn * 12300;
                    } else if (tt_sh <= dinhmuc * 1.5) {
                        tiennuoc = dinhmucngheo * 6000 + (dinhmuc - dinhmucngheo) * 6300 + (tt_sh - dinhmuc) * 12100  + tt_sx * 11400 + tt_kd * 20100 + tt_hcsn * 12300;
                    } else {
                        tiennuoc = dinhmucngheo * 6000 + (dinhmuc - dinhmucngheo) * 6300 + (dinhmuc *0.5) * 12100 + (tt_sh - dinhmuc * 1.5) * 13600 + tt_sx * 11400 + tt_kd * 20100 + tt_hcsn * 12300;
                    }
                }
                break;
            }
            // CO QUAN
            case 31: {
                //(Toàn bộ tiêu thụ) x 12.300
                tiennuoc = tieuthu * 12300;
                break;
            }
            case 32: {
                //(Toàn bộ tiêu thụ) x 11.400
                tiennuoc = tieuthu * 11400;
                break;
            }
            case 33: {
                //(Toàn bộ tiêu thụ) x 20.100
                tiennuoc = tieuthu * 20100;
                break;
            }
            case 34: {
                //(% HCSN) x 12,300 + (% SX) x 11,400
                if((tt_hcsn+tt_sx)!=tieuthu){
                    tt_sx = tieuthu - tt_hcsn;
                }
                tiennuoc =  tt_hcsn * 12300 + tt_sx * 11400;
                break;
            }
            case 35: {
                //(% HCSN) x 12,300 + (% KD) x 20,100
                if((tt_hcsn+tt_kd) !=tieuthu){
                    tt_kd =tieuthu -tt_hcsn;
                }
                tiennuoc = tt_hcsn * 12300 + tt_kd * 20100;
                break;
            }
            case 36: {
                //(% HCSN) x 12,300 + (% SX) x 11,400 + (% KD) x 20,100
                if((tt_hcsn+tt_sx+tt_kd) !=tieuthu){
                    tt_kd =tieuthu -tt_hcsn-tt_sx;
                }
                tiennuoc = tt_hcsn* 12300 + tt_sx * 11400 + tt_kd * 20100;
                break;
            }
            case 38: {
                //(SHN x 6,000) + (SH x 6.300) + (Tiêu thụ VM x 12,300)
                if (tieuthu <= dinhmucngheo) {
                    tiennuoc = tieuthu * 6000;
                } else if (tieuthu <= dinhmuc) {
                    tiennuoc = dinhmucngheo * 6000 + (tieuthu - dinhmucngheo) * 6300;
                } else {
                    tiennuoc = dinhmucngheo * 6000 + (dinhmuc - dinhmucngheo) * 6300 + (tieuthu - dinhmuc) * 12300;
                }
                break;
            }
            case 39: {
                //Tỉ lệ không có Định mức: (% SX) x 11.400 + (% KD) x 20,100 + (%HCSN) x 12,300
                //Tỉ lệ có Định mức: (%SH) = [(SHN)x 6,000 + (SH x 6.300) + (SHVM1 x 12,000) + (SHVM2 x 13,600)] + (%SX) x 11.400 + (%KD) x 20,100 + (%HCSN) x 12,300

                if (dinhmuc == 0) {
                    if((tt_hcsn+tt_sx+tt_kd) !=tieuthu){
                        tt_kd =tieuthu -tt_hcsn-tt_sx;
                    }
                    tiennuoc = tt_sx * 11400 + tt_kd * 20100 + tt_hcsn * 12300;
                } else {
                    if((tt_sx+tt_kd+tt_hcsn+tt_sh) !=tieuthu){
                        tt_kd = tieuthu - tt_sx -tt_hcsn- tt_sh;
                    }

                    if (tt_sh <= dinhmucngheo) {
                        tiennuoc = tt_sh * 6000 + tt_sx * 11400 + tt_kd * 20100 + tt_hcsn * 12300;
                    } else if (tt_sh <= dinhmuc) {
                        tiennuoc = dinhmucngheo * 6000 + (tt_sh - dinhmucngheo) * 6300  + tt_sx * 11400 + tt_kd * 20100 + tt_hcsn * 12300;
                    } else if (tt_sh <= dinhmuc * 1.5) {
                        tiennuoc = dinhmucngheo * 6000 + (dinhmuc - dinhmucngheo) * 6300 + (tt_sh - dinhmuc) * 12100  +tt_sx * 11400 + tt_kd * 20100 + tt_hcsn * 12300;
                    } else {
                        tiennuoc = dinhmucngheo * 6000 + (dinhmuc - dinhmucngheo) * 6300 + (dinhmuc *0.5 ) * 12100 + (tt_sh - dinhmuc * 1.5) * 13600  + tt_sx * 11400 +   tt_kd * 20100 + tt_hcsn * 12300;
                    }
                }
                break;
            }

             */
            case 51: {
                //CHUNG CƯ - KHU CÔNG NGHIỆP
                //(SHN x 5.400) + (SH x 5.670) + (SHVM1 x 10.890) + (SHVM2 x 12,240)
                // 5400 =? 5400*0.1*0.9 = 486
                // 5.670 =? 5.670*0.1*0.9 = 510
                // 10.890 =? 10.890*0.1*0.9 = 980
                // 12.240 =? 10.890*0.1*0.9 = 1102
                if (tieuthu <= dinhmucngheo) {
                    tiennuoc = tieuthu * 486;
                } else if (dinhmucngheo < tieuthu && tieuthu <= dinhmuc) {
                    tiennuoc = dinhmucngheo * 486 + (tieuthu - dinhmucngheo) * 510;
                } else if (tieuthu <= dinhmuc * 1.5) {
                    tiennuoc = dinhmucngheo * 486 + (dinhmuc - dinhmucngheo) * 510 + (tieuthu - dinhmuc) * 980;
                } else {
                    tiennuoc = dinhmucngheo * 486 + (dinhmuc - dinhmucngheo) * 510 + dinhmuc*0.5 * 980 + (tieuthu - dinhmuc * 1.5) * 1102;
                }
                break;
            }
            case 52: {
                // 10260 =? 10260*0.1*0.9 = 923
                tiennuoc = tieuthu * 923;
                break;
            }
            case 53: {
                // 18090 =? 18090*0.1*0.9 = 1628
                tiennuoc = tieuthu * 1628;
                break;
            }
            case 54: {
                // 11070 =? 11070*0.1*0.9 = 996
                tiennuoc = tieuthu * 996;
                break;
            }
            case 55: {
                //(Toàn bộ tiêu thụ) x giá thỏa thuận
                tiennuoc = tieuthu * giabieu;
                break;
            }
            case 56: {
                //(Toàn bộ tiêu thụ) x giá CG
                tiennuoc = tieuthu * giabieu;
                break;
            }
            case 57: {
                // 5670 =? 5670*0.1*0.9 = 510
                tiennuoc = tieuthu * 510;
                break;
            }
            case 58: {
                // (%SX) x 10,260 + (%KD) x 18,090 + (%HCSN) x 11,070
                // 10260 =? 10260*0.1*0.9 = 923
                // 18090 =? 18090*0.1*0.9 = 1628
                // 11070 =? 11070*0.1*0.9 = 996
                if((tt_hcsn+tt_sx+tt_kd) !=tieuthu){
                    tt_kd =tieuthu -tt_hcsn-tt_sx;
                }
                tiennuoc = tt_sx * 923 + tt_kd * 1628 + tt_hcsn * 996;
                break;
            }
            case 59: {
                // %SH = [(SHN) x 5,400 + (SH) x 5,670 + (SHVM1 x 10.890) + (SHVM2 x 12,240)] + (%SX) x 10,260 + (%KD) x 18,090
                // 5400 =? 5400*0.1*0.9 = 486
                // 5.670 =? 5.670*0.1*0.9 = 510
                // 10260 =? 10260*0.1*0.9 = 923
                // 18090 =? 18090*0.1*0.9 = 1628
                // 10.890 =? 10.890*0.1*0.9 = 980
                // 12.240 =? 10.890*0.1*0.9 = 1102
                if((tt_sx+tt_kd+tt_hcsn+tt_sh) !=tieuthu){
                    tt_kd = tieuthu - tt_sx -tt_hcsn- tt_sh;
                }

                if (tt_sh <= dinhmucngheo) {
                    tiennuoc = (tt_sh * 486)  + tt_sx * 923 + tt_kd * 1628;
                } else if (tt_sh <= dinhmuc) {
                    tiennuoc = dinhmucngheo * 486 + (tt_sh - dinhmucngheo) * 510  + tt_sx * 923 + tt_kd * 1628;
                } else if (tt_sh <= dinhmuc * 1.5) {
                    tiennuoc = dinhmucngheo * 486 + (dinhmuc - dinhmucngheo) * 510 + (tt_sh - dinhmuc) * 980  + tt_sx * 923 + tt_kd * 1628;
                } else {
                    tiennuoc = dinhmucngheo * 486 + (dinhmuc - dinhmucngheo) * 510 + (dinhmuc *0.5) * 980 + (tt_sh - dinhmuc * 1.5) * 1102  + tt_sx * 923 + tt_kd * 1628;
                }
                break;
            }
            /*
            case 68: {
                // %SH = [(SHN) x 5,400 + (SH) x 5,670 + (SHVM1 x 10.890) + (SHVM2 x 12,240)] + (%KD) x 20,100
                if((tt_sx+tt_kd) !=tieuthu){
                    tt_kd =tieuthu -tt_sx;
                }
                if (tt_sh <= dinhmucngheo) {
                    tiennuoc = (tt_sh * 5400)  + tt_kd * 20100;
                } else if (tt_sh <= dinhmuc) {
                    tiennuoc = dinhmucngheo * 5400 + (tt_sh - dinhmucngheo) * 5670  + tt_kd * 20100;
                } else if (tt_sh <= dinhmuc * 1.5) {
                    tiennuoc = dinhmucngheo * 5400 + (dinhmuc - dinhmucngheo) * 5670 + (tt_sh - dinhmuc) * 10890  + tt_kd * 20100;
                } else {
                    tiennuoc = dinhmucngheo * 5400 + (dinhmuc - dinhmucngheo) * 5670 + (dinhmuc *0.5) * 10890 + (tt_sh - dinhmuc * 1.5) * 12240  + tt_kd * 20100;
                }
                break;
            }

             */

        }
        return (long) (tiennuoc);
    }

    private int GiaNuoc_2020(int tieuthu,int giabieu,int dinhmuc,int dinhmucngheo,int tl_kinhdoanh,int tl_hcsn,int tl_sh,int tl_sx) {
        double tiennuoc = 0;
        switch (giabieu) {
            // TU GIA
            case 10: {
                // (SHN x 5.600) + (SHVM1 x 11.500) + (SHVM2 x 12.800)
                if (tieuthu <= dinhmucngheo) {
                    tiennuoc = tieuthu * 5600;
                } else if (dinhmucngheo < tiennuoc && tieuthu <= dinhmucngheo * 1.5) {
                    tiennuoc = 5600 * dinhmucngheo + (tieuthu - dinhmucngheo) * 11500;
                } else {
                    tiennuoc = (int) (5600 * dinhmucngheo + dinhmucngheo / 2 * 11500 + (tieuthu - dinhmucngheo * 1.5) * 12800);
                }
                break;
            }
            case 11: {
                // (SHN x 5.600) + (SH x 6.000) + (SHVM1 x 11.500) + (SHVM2 x 12.800)
                if (tieuthu <= dinhmucngheo) {
                    tiennuoc = tieuthu * 5600;
                } else if (dinhmucngheo < tieuthu && tieuthu <= dinhmuc) {
                    tiennuoc = dinhmucngheo * 5600 + (tieuthu - dinhmucngheo) * 6000;
                } else if (tieuthu <= dinhmuc * 1.5) {
                    tiennuoc = dinhmucngheo * 5600 + (dinhmuc - dinhmucngheo) * 6000 + (tieuthu - dinhmuc) * 11500;
                } else {
                    tiennuoc = dinhmucngheo * 5600 + (dinhmuc - dinhmucngheo) * 6000 + dinhmuc / 2 * 11500 + (tieuthu - dinhmuc * 1.5) * 12800;
                }
                break;
            }
            case 12: {
                // (Toàn bộ tiêu thụ) x 10.800
                tiennuoc = tieuthu * 10800;
                break;
            }
            case 13: {
                // (Toàn bộ tiêu thụ) x 19.000
                tiennuoc = tieuthu * 19000;
                break;
            }
            case 14: {
                //(SHN X 5.600) + (SH x 6.000) + (Tiêu thụ VM x 10.800)
                if (tieuthu <= dinhmucngheo) {
                    tiennuoc = tieuthu * 5600;
                } else if (dinhmucngheo < tieuthu && tieuthu <= dinhmuc) {
                    tiennuoc = dinhmucngheo * 5600 + (tieuthu - dinhmucngheo) * 6000;
                } else {
                    tiennuoc = dinhmucngheo * 5600 + (dinhmuc - dinhmucngheo) * 6000 + (tieuthu - dinhmuc) * 10800;
                }
                break;
            }
            case 15: {
                //(SHN X 5.600) + (SH x 6.000) + (Tiêu thụ VM x 10.800)
                if (tieuthu <= dinhmucngheo) {
                    tiennuoc = tieuthu * 5600;
                } else if (dinhmucngheo < tieuthu && tieuthu <= dinhmuc) {
                    tiennuoc = dinhmucngheo * 5600 + (tieuthu - dinhmucngheo) * 6000;
                } else {
                    tiennuoc = dinhmucngheo * 5600 + (dinhmuc - dinhmucngheo) * 6000 + (tieuthu - dinhmuc) * 19000;
                }
                break;
            }
            case 16: {
                //Tỉ lệ không có Định mức: (% SX) x 10.800 + (% KD) x 19.000
                //Tỉ lệ có Định mức: (%SH) = [(SH x 6.000) + (SHVM1 x 11.500) + (SHVM2 x 12.800)] + (%SX) x 10.800 + (%KD) x 19.000
                if (dinhmuc == 0) {
                    tiennuoc = (tieuthu) * tl_sx / 100 * 10800 + (tieuthu) * tl_kinhdoanh / 100 * 19000;
                } else {
                    if (tieuthu <= dinhmuc) {
                        tiennuoc = (tieuthu * 6000) * tl_sh / 100 + (tieuthu) * tl_sx / 100 * 10800 + (tieuthu) * tl_kinhdoanh / 100 * 19000;
                    } else if (tieuthu <= dinhmuc * 1.5) {
                        tiennuoc = (dinhmuc * 6000 + (tieuthu - dinhmuc) * 11500) * tl_sh / 100 + (tieuthu) * tl_sx / 100 * 10800 + (tieuthu) * tl_kinhdoanh / 100 * 19000;
                    } else {
                        tiennuoc = (dinhmuc * 6000 + (dinhmuc/2) * 11500  +(tieuthu - dinhmuc * 1.5) * 12800) * tl_sh / 100 + (tieuthu) * tl_sx / 100 * 10800 + (tieuthu) * tl_kinhdoanh / 100 * 19000;
                    }
                }
                break;
            }
            case 17: {
                //(Toàn bộ tiêu thụ) x 6.000
                tiennuoc = tieuthu * 6000;
                break;
            }
            case 18: {
                //(SHN x 6,000) + (SH x 6.300) + (Tiêu thụ VM x 12,300)
                if (tieuthu <= dinhmucngheo) {
                    tiennuoc = tieuthu * 5600;
                } else if (tieuthu <= dinhmuc) {
                    tiennuoc = dinhmucngheo * 5600 + (tieuthu - dinhmucngheo) * 6000;
                } else {
                    tiennuoc = dinhmucngheo * 5600 + (dinhmuc - dinhmucngheo) * 6000 + (tieuthu - dinhmuc) * 11600;
                }
                break;
            }
            case 19: {
                //Tỉ lệ không có Định mức: (% SX) x 10.800 + (% KD) x 19.000 + (%HCSN) x 11.600
                //Tỉ lệ có Định mức: (%SH) = [(SHN)x 5.600 + (SH x 6.000) + (SHVM1 x 11.500) + (SHVM2 x 12.800)] + (%SX) x 10.800 + (%KD) x 19.000 + (%HCSN) x 11.600

                if (dinhmuc == 0) {
                    tiennuoc = (tieuthu) * tl_sx / 100 * 10800 + (tieuthu) * tl_kinhdoanh / 100 * 19000 + tieuthu * (tl_hcsn) / 100 * 11600;
                } else {
                    if (tieuthu <= dinhmucngheo) {
                        tiennuoc = (tieuthu * 5600) * tl_sh / 100 + (tieuthu) * tl_sx / 100 * 10800 + (tieuthu) * tl_kinhdoanh / 100 * 19000 + tl_hcsn * tieuthu / 100 * 11600;
                    } else if (tieuthu <= dinhmuc) {
                        tiennuoc = (dinhmucngheo * 5600 + (tieuthu - dinhmucngheo) * 6000) * tl_sh / 100 + (tieuthu) * tl_sx / 100 * 10800 + (tieuthu) * tl_kinhdoanh / 100 * 19000 + tl_hcsn * tieuthu / 100 * 11600;
                    } else if (tieuthu <= dinhmuc * 1.5) {
                        tiennuoc = (dinhmucngheo * 5600 + (dinhmuc - dinhmucngheo) * 6000 + (tieuthu - dinhmuc) * 11500) * tl_sh / 100 + (tieuthu) * tl_sx / 100 * 10800 + (tieuthu) * tl_kinhdoanh / 100 * 19000 + tl_hcsn * tieuthu / 100 * 11600;
                    } else {
                        tiennuoc = (dinhmucngheo * 5600 + (dinhmuc - dinhmucngheo) * 6000 + (dinhmuc / 2) * 11500 + (tieuthu - dinhmuc * 1.5) * 12800) * tl_sh / 100 + (tieuthu) * tl_sx / 100 * 10800 + (tieuthu) * tl_kinhdoanh / 100 * 19000 + tl_hcsn * tieuthu / 100 * 11600;
                    }
                }
                break;
            }
            // HO TAP THE
            case 21: {
                //(SHN x 5.600) + (SH x 6.000) + (SHVM1 x 11.500) + (SHVM2 x 12.800)
                if (tieuthu <= dinhmucngheo) {
                    tiennuoc = tieuthu * 5600;
                } else if (dinhmucngheo < tieuthu && tieuthu <= dinhmuc) {
                    tiennuoc = dinhmucngheo * 5600 + (tieuthu - dinhmucngheo) * 6000;
                } else if (tieuthu <= dinhmuc * 1.5) {
                    tiennuoc = dinhmucngheo * 5600 + (dinhmuc - dinhmucngheo) * 6000 + (tieuthu - dinhmuc) * 11500;
                } else {
                    tiennuoc = dinhmucngheo * 5600 + (dinhmuc - dinhmucngheo) * 6000 + dinhmuc / 2 * 11500 + (tieuthu - dinhmuc * 1.5) * 12800;
                }
                break;
            }
            case 22: {
                // (Toàn bộ tiêu thụ) x 10.800
                tiennuoc = tieuthu * 10800;
                break;
            }
            case 23: {
                //(Toàn bộ tiêu thụ) x 19.000
                tiennuoc = tieuthu * 19000;
                break;
            }
            case 24: {
                // (SHN X 5.600) + (SH x 6.000) + (Tiêu thụ VM x 10.800)
                if (tieuthu <= dinhmucngheo) {
                    tiennuoc = tieuthu * 5600;
                } else if (dinhmucngheo < tieuthu && tieuthu <= dinhmuc) {
                    tiennuoc = dinhmucngheo * 5600 + (tieuthu - dinhmucngheo) * 6000;
                } else {
                    tiennuoc = dinhmucngheo * 5600 + (dinhmuc - dinhmucngheo) * 6000 + (tieuthu - dinhmuc) * 10800;
                }
                break;
            }
            case 25: {
                // (SHN X 5.600) + (SH x 6.000) + (Tiêu thụ VM x 19.000)
                if (tieuthu <= dinhmucngheo) {
                    tiennuoc = tieuthu * 5600;
                } else if (dinhmucngheo < tieuthu && tieuthu <= dinhmuc) {
                    tiennuoc = dinhmucngheo * 5600 + (tieuthu - dinhmucngheo) * 6000;
                } else {
                    tiennuoc = dinhmucngheo * 5600 + (dinhmuc - dinhmucngheo) * 6000 + (tieuthu - dinhmuc) * 19000;
                }
                break;
            }
            case 26: {
                //Tỉ lệ không có Định mức: (% SX) x 10.800 + (% KD) x 19.000
                //Tỉ lệ có Định mức: (%SH) = [(SH x 6.000) + (SHVM1 x 11.500) + (SHVM2 x 12.800)] + (%SX) x 10.800 + (%KD) x 19.000
                if (dinhmuc == 0) {
                    tiennuoc = (tieuthu) * tl_sx / 100 * 10800 + (tieuthu) * tl_kinhdoanh / 100 * 19000;
                } else {
                    if (tieuthu <= dinhmuc) {
                        tiennuoc = (tieuthu * 6000) * tl_sh / 100 + (tieuthu) * tl_sx / 100 * 10800 + (tieuthu) * tl_kinhdoanh / 100 * 19000;
                    } else if (tieuthu <= dinhmuc * 1.5) {
                        tiennuoc = (dinhmuc * 6000 + (tieuthu - dinhmuc) * 11500) * tl_sh / 100 + (tieuthu) * tl_sx / 100 * 10800 + (tieuthu) * tl_kinhdoanh / 100 * 19000;
                    } else {
                        tiennuoc = (dinhmuc * 6000 + (dinhmuc/2) * 11500 + +(tieuthu - dinhmuc * 1.5) * 12800) * tl_sh / 100 + (tieuthu) * tl_sx / 100 * 10800 + (tieuthu) * tl_kinhdoanh / 100 * 19000;
                    }
                }
                break;
            }
            case 27: {
                //(Toàn bộ tiêu thụ) x 6.000
                tiennuoc = tieuthu * 6000;
                break;
            }
            case 28: {
                //(SHN x 5.600) + (SH x 6.000) + (Tiêu thụ VM x 11.600)
                if (tieuthu <= dinhmucngheo) {
                    tiennuoc = tieuthu * 5600;
                } else if (tieuthu <= dinhmuc) {
                    tiennuoc = dinhmucngheo * 5600 + (tieuthu - dinhmucngheo) * 6000;
                } else {
                    tiennuoc = dinhmucngheo * 5600 + (dinhmuc - dinhmucngheo) * 6000 + (tieuthu - dinhmuc) * 11600;
                }
                break;
            }
            case 29: {
                //Tỉ lệ không có Định mức: ((% SX) x 10.800) + ((% KD) x 19.000) + ((%HCSN) x 11.600)
                //Tỉ lệ có Định mức: (%SH) = [(SHN) x 5.600) + (SH x 6.000) + (SHVM1 x 11.500) + (SHVM2 x 12.800)] + (%SX) x 10.800 + (%KD) x 19.000 + (%HCSN) x 11.600

                if (dinhmuc == 0) {
                    tiennuoc = (tieuthu) * tl_sx / 100 * 10800 + (tieuthu) * tl_kinhdoanh / 100 * 19000 + tieuthu * (tl_hcsn) / 100 * 11600;
                } else {
                    if (tieuthu <= dinhmucngheo) {
                        tiennuoc = (tieuthu * 5600) * tl_sh / 100 + (tieuthu) * tl_sx / 100 * 10800 + (tieuthu) * tl_kinhdoanh / 100 * 19000 + tl_hcsn * tieuthu / 100 * 11600;
                    } else if (tieuthu <= dinhmuc) {
                        tiennuoc = (dinhmucngheo * 5600 + (tieuthu - dinhmucngheo) * 6000) * tl_sh / 100 + (tieuthu) * tl_sx / 100 * 10800 + (tieuthu) * tl_kinhdoanh / 100 * 19000 + tl_hcsn * tieuthu / 100 * 11600;
                    } else if (tieuthu <= dinhmuc * 1.5) {
                        tiennuoc = (dinhmucngheo * 5600 + (dinhmuc - dinhmucngheo) * 6000 + (tieuthu - dinhmuc) * 11500) * tl_sh / 100 + (tieuthu) * tl_sx / 100 * 10800 + (tieuthu) * tl_kinhdoanh / 100 * 19000 + tl_hcsn * tieuthu / 100 * 11600;
                    } else {
                        tiennuoc = (dinhmucngheo * 5600 + (dinhmuc - dinhmucngheo) * 6000 + (dinhmuc / 2) * 11500 + (tieuthu - dinhmuc * 1.5) * 12800) * tl_sh / 100 + (tieuthu) * tl_sx / 100 * 10800 + (tieuthu) * tl_kinhdoanh / 100 * 19000 + tl_hcsn * tieuthu / 100 * 11600;
                    }
                }
                break;
            }
            // CO QUAN
            case 31: {
                //(Toàn bộ tiêu thụ) x 11.600
                tiennuoc = tieuthu * 11600;
                break;
            }
            case 32: {
                //(Toàn bộ tiêu thụ) x 10.800
                tiennuoc = tieuthu * 10800;
                break;
            }
            case 33: {
                //((Toàn bộ tiêu thụ) x 19.000
                tiennuoc = tieuthu * 19000;
                break;
            }
            case 34: {
                //(% HCSN) x 11.600 + (% SX) x 10.800
                tiennuoc = tieuthu * tl_hcsn / 100 * 11600 + tieuthu * tl_sx / 100 * 10800;
                break;
            }
            case 35: {
                //(% HCSN) x 11.600 + (% SX) x 19.000
                tiennuoc = tieuthu * tl_hcsn / 100 * 11600 + tieuthu * tl_kinhdoanh / 100 * 19000;
                break;
            }
            case 36: {
                //(% HCSN) x 11.600 + (% SX) x 10.800 + (% KD) x 19.000
                tiennuoc = tieuthu * tl_hcsn / 100 * 11600 + tieuthu * tl_sx / 100 * 10800 + tieuthu * tl_kinhdoanh / 100 * 19000;
                break;
            }
            case 38: {
                //(SHN x 5.600) + (SH x 6.000) + (Tiêu thụ VM x 11.600)
                if (tieuthu <= dinhmucngheo) {
                    tiennuoc = tieuthu * 5600;
                } else if (tieuthu <= dinhmuc) {
                    tiennuoc = dinhmucngheo * 5600 + (tieuthu - dinhmucngheo) * 6000;
                } else {
                    tiennuoc = dinhmucngheo * 5600 + (dinhmuc - dinhmucngheo) * 6000 + (tieuthu - dinhmuc) * 11600;
                }
                break;
            }
            case 39: {
                //Tỉ lệ không có Định mức: ((% SX) x 10.800) + ((% KD) x 19.000) + ((%HCSN) x 11.600)
                //Tỉ lệ có Định mức: (%SH) = [(SHN) x 5.600 + (SH x 6.000) + (SHVM1 x 11.500) + (SHVM2 x 12.800)] + (%SX) x 10.800 + (%KD) x 19.000 + (%HCSN) x 11.600

                if (dinhmuc == 0) {
                    tiennuoc = (tieuthu) * tl_sx / 100 * 10800 + (tieuthu) * tl_kinhdoanh / 100 * 19000 + tieuthu * (tl_hcsn) / 100 * 11600;
                } else {
                    if (tieuthu <= dinhmucngheo) {
                        tiennuoc = (tieuthu * 5600) * tl_sh / 100 + (tieuthu) * tl_sx / 100 * 10800 + (tieuthu) * tl_kinhdoanh / 100 * 19000 + tl_hcsn * tieuthu / 100 * 11600;
                    } else if (tieuthu <= dinhmuc) {
                        tiennuoc = (dinhmucngheo * 5600 + (tieuthu - dinhmucngheo) * 6000) * tl_sh / 100 + (tieuthu) * tl_sx / 100 * 10800 + (tieuthu) * tl_kinhdoanh / 100 * 19000 + tl_hcsn * tieuthu / 100 * 11600;
                    } else if (tieuthu <= dinhmuc * 1.5) {
                        tiennuoc = (dinhmucngheo * 5600 + (dinhmuc - dinhmucngheo) * 6000 + (tieuthu - dinhmuc) * 11500) * tl_sh / 100 + (tieuthu) * tl_sx / 100 * 10800 + (tieuthu) * tl_kinhdoanh / 100 * 19000 + tl_hcsn * tieuthu / 100 * 11600;
                    } else {
                        tiennuoc = (dinhmucngheo * 5600 + (dinhmuc - dinhmucngheo) * 6000 + (dinhmuc / 2) * 11500 + (tieuthu - dinhmuc * 1.5) * 12800) * tl_sh / 100 + (tieuthu) * tl_sx / 100 * 10800 + (tieuthu) * tl_kinhdoanh / 100 * 19000 + tl_hcsn * tieuthu / 100 * 11600;
                    }
                }
                break;
            }
            //CHUNG CƯ - KHU CÔNG NGHIỆP
            case 51: {
                //(SHN x 5.040) + (SH x 5.400) + (SHVM1 x 10.350) + (SHVM2 x 11.520)
                if (tieuthu <= dinhmucngheo) {
                    tiennuoc = tieuthu * 5040;
                } else if (dinhmucngheo < tieuthu && tieuthu <= dinhmuc) {
                    tiennuoc = dinhmucngheo * 5040 + (tieuthu - dinhmucngheo) * 5400;
                } else if (tieuthu <= dinhmuc * 1.5) {
                    tiennuoc = dinhmucngheo * 5040 + (dinhmuc - dinhmucngheo) * 5400 + (tieuthu - dinhmuc) * 10350;
                } else {
                    tiennuoc = dinhmucngheo * 5040 + (dinhmuc - dinhmucngheo) * 5400 + dinhmuc / 2 * 10350 + (tieuthu - dinhmuc * 1.5) * 11520;
                }
                break;
            }
            case 52: {
                //(Toàn bộ tiêu thụ) x 9.720
                tiennuoc = tieuthu * 9720;
                break;
            }
            case 53: {
                //(Toàn bộ tiêu thụ) x 17.100
                tiennuoc = tieuthu * 17100;
                break;
            }
            case 54: {
                //(Toàn bộ tiêu thụ) x 10.440
                tiennuoc = tieuthu * 10440;
                break;
            }
            case 55: {
                //(Toàn bộ tiêu thụ) x giá thỏa thuận
                tiennuoc = tieuthu * giabieu;
                break;
            }
            case 56: {
                //(Toàn bộ tiêu thụ) x giá CG
                tiennuoc = tieuthu * giabieu;
                break;
            }
            case 57: {
                //(Toàn bộ tiêu thụ) x 5.400
                tiennuoc = tieuthu * 5400;
                break;
            }
            case 58: {
                // (%SX) x 9.720 + (%KD) x 17.100 + (%HCSN) x 10.440
                tiennuoc = tieuthu * tl_sx / 100 * 9720 + tieuthu * tl_kinhdoanh / 100 * 17100 + tieuthu * tl_hcsn / 100 * 10440;
                break;
            }
            case 59: {
                // %SH = [(SHN) x 5.040 + (SH) x 5.400 + (SHVM1) x 10.350 + (SHVM2) x 11.520] + (%SX) x 9.720 + (%KD) x 17.100
                if (tieuthu <= dinhmucngheo) {
                    tiennuoc = (tieuthu * 5040) * tl_sh / 100 + (tieuthu) * tl_sx / 100 * 9720 + (tieuthu) * tl_kinhdoanh / 100 * 17100;
                } else if (tieuthu <= dinhmuc) {
                    tiennuoc = (dinhmucngheo * 5040 + (tieuthu - dinhmucngheo) * 5400) * tl_sh / 100 + (tieuthu) * tl_sx / 100 * 9720 + (tieuthu) * tl_kinhdoanh / 100 * 17100;
                } else if (tieuthu <= dinhmuc * 1.5) {
                    tiennuoc = (dinhmucngheo * 5040 + (dinhmuc - dinhmucngheo) * 5400 + (tieuthu - dinhmuc) * 10350) * tl_sh / 100 + (tieuthu) * tl_sx / 100 * 9720 + (tieuthu) * tl_kinhdoanh / 100 * 17100;
                } else {
                    tiennuoc = (dinhmucngheo * 5040 + (dinhmuc - dinhmucngheo) * 5400 + (dinhmuc / 2) * 10350 + (tieuthu - dinhmuc * 1.5) * 11520) * tl_sh / 100 + (tieuthu) * tl_sx / 100 * 9720 + (tieuthu) * tl_kinhdoanh / 100 * 17100;
                }
                break;
            }
            case 68: {
                //%SH = [(SHN) x 5.040 + (SH) x 5.400 + (SHVM1) x 10.350 + (SHVM2) x 11.520] + (%KD) x 17.100
                if (tieuthu <= dinhmucngheo) {
                    tiennuoc = (tieuthu * 5040) * tl_sh / 100 + (tieuthu) * tl_kinhdoanh / 100 * 17100;
                } else if (tieuthu <= dinhmuc) {
                    tiennuoc = (dinhmucngheo * 5040 + (tieuthu - dinhmucngheo) * 5400) * tl_sh / 100 + (tieuthu) * tl_kinhdoanh / 100 * 17100;
                } else if (tieuthu <= dinhmuc * 1.5) {
                    tiennuoc = (dinhmucngheo * 5040 + (dinhmuc - dinhmucngheo) * 5400 + (tieuthu - dinhmuc) * 10350) * tl_sh / 100 + (tieuthu) * tl_kinhdoanh / 100 * 17100;
                } else {
                    tiennuoc = (dinhmucngheo * 5040 + (dinhmuc - dinhmucngheo) * 5400 + (dinhmuc / 2) * 10350 + (tieuthu - dinhmuc * 1.5) * 11520) * tl_sh / 100 + (tieuthu) * tl_kinhdoanh / 100 * 17100;
                }
                break;
            }

        }
        return (int) (tiennuoc);
    }


}

